using Liga.Domain.Entities;
using Liga.Infrastruture.Data.Mapping;
using System.Data.Entity;

namespace Liga.Infrastructure.Data
{
    public partial class Liga_DBContext : DbContext
    {
        static Liga_DBContext()
        {
            Database.SetInitializer<Liga_DBContext>(null);
        }

        public Liga_DBContext()
            : base("Name=Liga_DBContext")
        {
        }
        public DbSet<Agency> Agencys { get; set; }
        public DbSet<Awards> Awards { get; set; }
        public DbSet<AwardsClass> AwardsClasses { get; set; }
        public DbSet<Comments> Comments { get; set; }
        public DbSet<Conferences> Conferences { get; set; }
        public DbSet<Divisions> Divisions { get; set; }
        public DbSet<Drafts> Drafts { get; set; }
        public DbSet<Games> Games { get; set; }
        public DbSet<Goals> Goals { get; set; }
        public DbSet<HistoryGoals> HistoryGoals { get; set; }
        public DbSet<InjuryGames> InjuryGames { get; set; }
        public DbSet<Injuries> Injuries { get; set; }
        public DbSet<InjuryTypes> InjuryTypes { get; set; }
        public DbSet<ResumenLesiones> ResumenLesiones { get; set; }
        public DbSet<LVNBAFranchises> LVNBAFranchises { get; set; }
        public DbSet<ManagerRequests> ManagerRequests { get; set; }
        public DbSet<Managers> Managers { get; set; }
        public DbSet<ManagersFranchises> ManagersFranchises { get; set; }
        public DbSet<Parameters> Parameters { get; set; }
        public DbSet<PFMAwards> PFMAwards { get; set; }
        public DbSet<Players> Players { get; set; }
        public DbSet<PlayersHistories> PlayersHistories { get; set; }
        public DbSet<PlayersPositions> PlayersPositions { get; set; }
        public DbSet<PlayersRoles> PlayersRoles { get; set; }
        public DbSet<PlayersSalariesFull> PlayersSalariesFull { get; set; }
        public DbSet<Webpages_Roles> Webpages_Roles { get; set; }
        public DbSet<Webpages_UsersInRoles> Webpages_UsersInRoles { get; set; }
        public DbSet<PlayersStates> PlayersStates { get; set; }
        public DbSet<PlayersStatsGames> PlayersStatsGames { get; set; }
        public DbSet<Rounds> Rounds { get; set; }
        public DbSet<SalaryConditions> SalaryConditions { get; set; }
        public DbSet<TeamStandings> TeamStandings { get; set; }
        public DbSet<Transfers> Transfers { get; set; }
        public DbSet<TransfersPlayersLists> TransfersPlayersLists { get; set; }
        public DbSet<TransfersRoundsLists> TransfersRoundsLists { get; set; }
        public DbSet<TransfersStates> TransfersStates { get; set; }
        public DbSet<TopPlayerLists> TopPlayerLists { get; set; }
        public DbSet<FranchiseManagementGoal> FranchiseManagementGoals { get; set; }
        public DbSet<FranchiseSportGoal> FranchiseSportGoals { get; set; }
        public DbSet<BoxScorePlayers> BoxScoresPlayers { get; set; }
        public DbSet<BoxScoreTeams> BoxScoresTeams { get; set; }
        public DbSet<GamesPerMonth> GamesPerMonths { get; set; }
        public DbSet<GameStatsResume> GameStatsResumes { get; set; }
        public DbSet<PlayerStatisticsBySeason> PlayerStatisticsBySeasons { get; set; }
        public DbSet<PlayerStatisticsByPof> PlayerStatisticsByPofs { get; set; }
        public DbSet<PlayerStatisticsRSPO> PlayerStatisticsRSPOs { get; set; }
        public DbSet<PlayerStatisticsRSPOByTeam> PlayerStatisticsRSPOByTeams { get; set; }
        public DbSet<PlayersInfoList> PlayersInfoList { get; set; }
        public DbSet<PlayerUsage> PlayerUsages { get; set; }

        //public DbSet<StatsPlayer> StatsPlayers { get; set; }
        public DbSet<RankAPG> RankAPGs { get; set; }
        public DbSet<RankFG> RankFGs { get; set; }
        public DbSet<RankBPG> RankBPGs { get; set; }
        public DbSet<RankMPG> RankMPGs { get; set; }
        public DbSet<RankPPG> RankPPGs { get; set; }
        public DbSet<RankRPG> RankRPGs { get; set; }
        public DbSet<RankSPG> RankSPGs { get; set; }
        public DbSet<ResumenQueja> ResumenQuejas { get; set; }
        public DbSet<LastGame> LastGames { get; set; }
        public DbSet<Standing> Standings { get; set; }
        public DbSet<StatsForReport> StatsForReports { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<NotificationType> NotificationTypes { get; set; }
        public DbSet<ValMediaLocalVisitanteOtorgada> ValMediaLocalVisitanteOtorgadas { get; set; }
        public DbSet<ValMediaLocalVisitanteRecibida> ValMediaLocalVisitanteRecibidas { get; set; }
        public DbSet<ValMediaOtorgada> ValMediaOtorgadas { get; set; }
        public DbSet<ValMediaRecibida> ValMediaRecibidas { get; set; }
        public DbSet<Valoraciones> Valoraciones { get; set; }
        public DbSet<Clasificacion> Clasificacions { get; set; }
        public DbSet<PlazosSemanales> PlazosSemanales { get; set; }
        public DbSet<PlazosMensuales> PlazosMensuales { get; set; }
        public DbSet<FreeAgentOffers> FreeAgentOffers { get; set; }
        public DbSet<Colleges> Colleges { get; set; }
        public DbSet<Playoffs> Playoffs { get; set; }
        public DbSet<LVNBAFranchisesParams> LVNBAFranchisesParams { get; set; }
        public DbSet<PlayersFranchisesStars> PlayersFranchisesStars { get; set; }
       // public DbSet<PostTags> PostTags { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AgencyMap());
            modelBuilder.Configurations.Add(new AwardsMap());
            modelBuilder.Configurations.Add(new AwardsClassMap());
            modelBuilder.Configurations.Add(new CommentsMap());
            modelBuilder.Configurations.Add(new ConferencesMap());
            modelBuilder.Configurations.Add(new DivisionsMap());
            modelBuilder.Configurations.Add(new DraftsMap());
            modelBuilder.Configurations.Add(new GamesMap());
            modelBuilder.Configurations.Add(new GoalsMap());
            modelBuilder.Configurations.Add(new HistoryGoalsMap());
            modelBuilder.Configurations.Add(new InjuryGamesMap());
            modelBuilder.Configurations.Add(new InjuriesMap());
            modelBuilder.Configurations.Add(new InjuryTypesMap());
            modelBuilder.Configurations.Add(new ResumenLesionesMap());
            modelBuilder.Configurations.Add(new LVNBAFranchisesMap());
            modelBuilder.Configurations.Add(new ManagerRequestMap());
            modelBuilder.Configurations.Add(new ManagersMap());
            modelBuilder.Configurations.Add(new ManagersFranchisMap());
            modelBuilder.Configurations.Add(new ParametersMap());
            modelBuilder.Configurations.Add(new PFMAwardsMap());
            modelBuilder.Configurations.Add(new PlayersMap());
            modelBuilder.Configurations.Add(new PlayersHistoryMap());
            modelBuilder.Configurations.Add(new PlayersPositionsMap());
            modelBuilder.Configurations.Add(new PlayersRolesMap());
            modelBuilder.Configurations.Add(new PlayersSalariesFullMap());
            modelBuilder.Configurations.Add(new Webpages_RolesMap());
            modelBuilder.Configurations.Add(new Webpages_UsersInRolesMap());
            modelBuilder.Configurations.Add(new UserProfileMap());
            modelBuilder.Configurations.Add(new PlayersStatesMap());
            modelBuilder.Configurations.Add(new PlayersStatsGamesMap());
            modelBuilder.Configurations.Add(new RoundsMap());
            modelBuilder.Configurations.Add(new SalaryConditionsMap());
            modelBuilder.Configurations.Add(new TeamStandingsMap());
            modelBuilder.Configurations.Add(new TransfersMap());
            modelBuilder.Configurations.Add(new TransfersPlayersListsMap());
            modelBuilder.Configurations.Add(new TransfersRoundsListsMap());
            modelBuilder.Configurations.Add(new TransfersStatesMap());
            modelBuilder.Configurations.Add(new TopPlayerListsMap());
            modelBuilder.Configurations.Add(new FranchiseManagementGoalMap());
            modelBuilder.Configurations.Add(new FranchiseSportGoalMap());
            modelBuilder.Configurations.Add(new BoxScorePlayersMap());
            modelBuilder.Configurations.Add(new BoxScoreTeamsMap());
            modelBuilder.Configurations.Add(new GamesPerMonthMap());
            modelBuilder.Configurations.Add(new GameStatsResumeMap());
            modelBuilder.Configurations.Add(new PlayerStatisticsBySeasonMap());
            modelBuilder.Configurations.Add(new PlayerStatisticsByPofMap());
            modelBuilder.Configurations.Add(new PlayerStatisticsRSPOMap());
            modelBuilder.Configurations.Add(new PlayerStatisticsRSPOByTeamMap()); 
            modelBuilder.Configurations.Add(new PlayersInfoListMap());
            modelBuilder.Configurations.Add(new PlayerUsageMap());
            modelBuilder.Configurations.Add(new RankAPGMap());
            modelBuilder.Configurations.Add(new RankFGMap());
            modelBuilder.Configurations.Add(new RankBPGMap());
            modelBuilder.Configurations.Add(new RankMPGMap());
            modelBuilder.Configurations.Add(new RankPPGMap());
            modelBuilder.Configurations.Add(new RankRPGMap());
            modelBuilder.Configurations.Add(new RankSPGMap());
            modelBuilder.Configurations.Add(new LastGameMap());
            modelBuilder.Configurations.Add(new StatsForReportMap());
            modelBuilder.Configurations.Add(new ResumenQuejaMap());
            modelBuilder.Configurations.Add(new NotificationMap());
            modelBuilder.Configurations.Add(new NotificationTypeMap());
            modelBuilder.Configurations.Add(new ValMediaLocalVisitanteOtorgadaMap());
            modelBuilder.Configurations.Add(new ValMediaLocalVisitanteRecibidaMap());
            modelBuilder.Configurations.Add(new ValMediaOtorgadaMap());
            modelBuilder.Configurations.Add(new ValMediaRecibidaMap());
            modelBuilder.Configurations.Add(new ValoracionesMap());
            modelBuilder.Configurations.Add(new ClasificacionMap());
            modelBuilder.Configurations.Add(new PlazosSemanalesMap());
            modelBuilder.Configurations.Add(new PlazosMensualesMap());
            modelBuilder.Configurations.Add(new CollegesMap());
            modelBuilder.Configurations.Add(new PlayoffsMap());
            modelBuilder.Configurations.Add(new FreeAgentOffersMap());
            modelBuilder.Configurations.Add(new LVNBAFranchiseParamsMap());
            modelBuilder.Configurations.Add(new PlayersFranchiseStarMap());
        }
        public DbSet<UserProfile> UserProfiles { get; set; }
    }
}
