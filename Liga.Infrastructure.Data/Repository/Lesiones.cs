﻿using Liga.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Liga.Infrastructure.Data.Repository
{
    public class Lesiones
    {

        public Injuries SeLesiona(int idPlayer, JugadorMinuto jugMin,  Liga_DBContext db) //Después de descontar 1 partido a los lesionados
        {
            Injuries injury = null;
            int idTypeInjury;
            Injuries isInjured = db.Injuries.Where(x => x.IdPlayer == idPlayer && x.GamesRest > 0).FirstOrDefault();
            if(isInjured == null) //solo te puedes lesionar si no estas lesionado
            {
				Random aleatorio = new Random();
				int[] tabla_gravedad_lesion = new int[1000];
				int[] tabla_gravedad_semanas_numero = {
					-1, 480, 672, 816, 876, 909, 924, 930, 933, 936, 939, 942, 945, 948, 951, 954, 957, 960, 963, 966, 969, 972, 975, 978, 981, 999
				};
				for (int i = 0; i < tabla_gravedad_semanas_numero.Length - 1; i++) {
					for (int j = tabla_gravedad_semanas_numero[i] + 1; j <= tabla_gravedad_semanas_numero[i + 1]; j++)
					tabla_gravedad_lesion[j] = i + 1;
				}

                int rating_lesion = 70; // ponemos ahora un numero a la espera de 2k y ver como van las lesiones // playerstatsgames.get(l).getPlayers().getSdurab(); //Star: coger el atributo de lesiones del jugador
				int minutos = jugMin.Minuto; //Star: coger de bd los segundos que ha jugado
				double posible_lesion = 25 / 15;
                //obtenemos por PArametros
				if(db.Parameters.First().Playoffs == 1) //DGM lo obtenemos por parametros
					posible_lesion /= 2;
                //DGM anulo esto ya vienen los minutos
				//float minutos = segundos / 60;

                aleatorio.NextDouble();
				double num_aleatorio =  aleatorio.NextDouble() * 30; //30 es el número jugadores de los dos equipos
				if (num_aleatorio <= posible_lesion) // hay posible lesión
				{
                    double rango_lesion = (double)(rating_lesion * 0.75 + (48 - minutos) * 25 / 48);
                    double num_aleatorio2 = (double)(aleatorio.NextDouble() * 100);
					if (num_aleatorio2 >= rango_lesion) //hay lesión
					{
                        bool hay_salvacion = false;
						int extra = (rating_lesion - 75) * 10; //73 es la media de lesión
						if (extra > 0) //puede salvarse por la campana
						{
							int num_aleatorio3 = aleatorio.Next(0,1000);
							if (num_aleatorio3 <= extra) //se ha salvado
							hay_salvacion = true;
						}
						if (!hay_salvacion) //Comprobamos gravedad
						{
							int num_aleatorio4 = aleatorio.Next(1,1000);
							int gravedad = tabla_gravedad_lesion[num_aleatorio4] * 3;
							int num_aleatorio5 = aleatorio.Next(0,3);
							int gravedad_total = 0;

							if (gravedad == 75) //Lesionado para todo el año
							gravedad_total = 999;
							else gravedad_total = gravedad - num_aleatorio5;
							
							if(gravedad_total != 0)
							{
								injury = new Injuries();
								idTypeInjury = getTypeInjury(gravedad_total, db); //getTypeInjury(gamesInjury, db);

								injury.IdPlayer = idPlayer;
                                injury.GamesOut = gravedad_total;
								injury.GamesRest = gravedad_total;
								injury.IdType = idTypeInjury;
							}
						}
					}
				}
            }
            return injury;
        }
		

        public int getTypeInjury(int gamesInjury, Liga_DBContext db)
        {
            List<InjuryTypes> listaTiposLesion = db.InjuryTypes.Where(x => x.MinDuration <= gamesInjury && x.MaxDuration >= gamesInjury).AsEnumerable().ToList();
            int numPosiblesTipos = listaTiposLesion.Count();
            if (numPosiblesTipos == 0)
            {
                return 0; //lesion Generica
            }
            else
            {
                Random rnd = new Random();
                int iType =  rnd.Next(1,numPosiblesTipos ); // creates a number between 1 and 12
                return listaTiposLesion.Where((x, i) => i % iType == 0).First().Id;
            }
        }
    }

}

