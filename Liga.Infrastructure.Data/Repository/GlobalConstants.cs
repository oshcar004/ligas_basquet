﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Liga.Infrastructure.Data.Repository
{
    public class GlobalConstants
    {

        public class Ct_Notifications
        {
            public class NotificationsTypes
            {

                public const int ErrorReporte = 1;
                public const int ManagerWarning = 2;
            }

            public class NotificationSenders
            {
                public const int Administrador = 1;
                public const int Comisionado = 2;
                public const int Redaccion = 3;
                
            }
        }
        public class Ct_GlobalMails
        {
            public const string AdminMail = "webmasterLVNBA@gmail.com";
            public const string ComisionadoMail = "comisionadoLVNBA@gmail.com";
            public const string RedaccionMail = "reporterLVNBA@gmail.com";
        }
    }
}