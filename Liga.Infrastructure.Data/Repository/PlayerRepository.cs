﻿using Liga.Domain.Entities;
using Liga.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;


namespace Liga.Infrastructure.Data.Repository
{
    public class PlayerRepository
    {
        private Liga_DBContext db = new Liga_DBContext();

        public IList<Players> GetAllFreeAgents()
        {

            try
            {
                var players = db.Players.Include(p => p.LVNBAFranchis).
                    Include(p => p.PlayersState).
                    Include(p => p.PlayersPosition).
                    Include(p => p.PlayersRole)
                    .Where(x => x.ActualTeam == null && x.Retired == false);
                return players.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int SaveFreeAgentOfert(FreeAgentOffers freeAgentOfert)
        {
            FreeAgentOffers freeAgentOfertToSave = freeAgentOfert;
            try
            {
                db.FreeAgentOffers.Add(freeAgentOfertToSave);
                db.SaveChanges();
                return freeAgentOfertToSave.Id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
