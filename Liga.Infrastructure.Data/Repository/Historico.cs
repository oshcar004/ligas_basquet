﻿
using Liga.Domain.Entities;
using Liga.Infrastructure.Data;
using System;
using System.Data;
using System.Data.Entity;
using System.Linq;

namespace Liga.Infrastructure.Data.Repository
{
    public class Historico
    {
        private Liga_DBContext db = new Liga_DBContext();

        public void ConsolidateHistoryManager(int idFranchise, int? idManager)
        {

            try
            {
                //si no hay manager debemos cerrar el ciclo del manager anterior
                if (idManager == null)
                {
                    ManagersFranchises manFran = db.ManagersFranchises.Where(x => x.IdFranchise == idFranchise && x.EndDate == null).FirstOrDefault();

                    if (manFran != null)
                    {
                        manFran.EndDate = DateTime.Now;
                        db.Entry(manFran).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }
                else //cerramos el ciclo si existe y abrimos el nuevo ciclo
                {
                    ManagersFranchises manFran = db.ManagersFranchises.Where(x => x.IdFranchise == idFranchise && x.EndDate == null).FirstOrDefault();

                    if (manFran != null)
                    {
                        manFran.EndDate = DateTime.Now;
                        db.Entry(manFran).State = EntityState.Modified;
                        ManagersFranchises manFranNew = new ManagersFranchises();
                        manFranNew.StartDate = DateTime.Now;
                        manFranNew.IdFranchise = idFranchise;
                        manFranNew.IdManager = (int)idManager;
                        db.ManagersFranchises.Add(manFranNew);
                        db.SaveChanges();
                    }
                    else
                    {
                        ManagersFranchises manFranNew = new ManagersFranchises();
                        manFranNew.StartDate = DateTime.Now;
                        manFranNew.IdFranchise = idFranchise;
                        manFranNew.IdManager = (int)idManager;
                        db.ManagersFranchises.Add(manFranNew);
                        db.SaveChanges();
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}