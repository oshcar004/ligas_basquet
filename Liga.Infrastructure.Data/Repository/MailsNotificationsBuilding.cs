﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Liga.Infrastructure.Data.Repository
{
    public class MailsNotificationsBuilding
    {
        public string getWarningMailNotification(string sVariableText)
        {
            string sMensaje = "";
            sMensaje = "Buenos días chaval. <br/> Te ha caido una queja por lo siguiente:<br/>";
            sMensaje = sMensaje + sVariableText + "<br/>";
            sMensaje = sMensaje + "Cualquier duda, no dudes en ponecte en contacto con el Comisionado por irc o enviando un mail a: " + GlobalConstants.Ct_GlobalMails.ComisionadoMail  + "<br/> Gracias y un saludo";
            return sMensaje;
        }
    }
}