﻿
using Liga.Domain.Entities;
using Liga.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Liga.Infrastructure.Data.Repository
{
    public class Traspasos
    {
        private Liga_DBContext db = new Liga_DBContext();
        /// <summary>
        /// Valida los traspasos en todos los aspectos
        /// </summary>
        /// <param name="tradeator"></param>
        /// <param name="sMotinoNoValido"></param>
        /// <returns></returns>
        public int IsTradeValid(TradeatorInfo tradeator, out string sMotinoNoValido)
        {
            sMotinoNoValido = "";
            string sMessage = "";
            int isValid = 1;
            try
            {
                //obtenemos datos que vamos a necesitar
                Parameters TradeParameters = db.Parameters.First();
                LVNBAFranchisesParams paramsEquipo1 = db.LVNBAFranchisesParams.Where(x => x.ID == tradeator.Equipo1.Id).First();
                LVNBAFranchisesParams paramsEquipo2 = db.LVNBAFranchisesParams.Where(x => x.ID == tradeator.Equipo2.Id).First();
                LVNBAFranchisesParams paramsEquipo3 = null;
                LVNBAFranchisesParams paramsEquipo4 = null;
                if (tradeator.Equipo3 != null)
                {
                    paramsEquipo3 = db.LVNBAFranchisesParams.Where(x => x.ID == tradeator.Equipo3.Id).First();
                }
                if (tradeator.Equipo4 != null)
                {
                    paramsEquipo4 = db.LVNBAFranchisesParams.Where(x => x.ID == tradeator.Equipo4.Id).First();
                }

                //*************************************************************************
                //revisar epoca en la que estamos*******************************************
                if (!isPeriodToTrade(TradeParameters, out sMessage))
                {
                    isValid = 0;
                    sMotinoNoValido = sMessage;
                }
                //*********************************************************************
                //*********************************************************************



                sMessage = "";
                //*************************************************************************
                //revisar si el equipo está en plazo************************************
                if (!isInPlazo(TradeParameters, tradeator.Equipo1, out sMessage))
                {
                    isValid = 0;
                    sMotinoNoValido = sMotinoNoValido + sMessage;
                }
                if (!isInPlazo(TradeParameters, tradeator.Equipo2, out sMessage))
                {
                    isValid = 0;
                    sMotinoNoValido = sMotinoNoValido + sMessage;
                }
                if (!isInPlazo(TradeParameters, tradeator.Equipo3, out sMessage))
                {
                    isValid = 0;
                    sMotinoNoValido = sMotinoNoValido + sMessage;
                }
                if (!isInPlazo(TradeParameters, tradeator.Equipo4, out sMessage))
                {
                    isValid = 0;
                    sMotinoNoValido = sMotinoNoValido + sMessage;
                }
                //*********************************************************************
                //*********************************************************************


                sMessage = "";
                //*************************************************************************
                //miro si supera los traspasos maximo**********************************************************
                if (!isNotOverComeTrades(tradeator.listaPlayers1.Count, tradeator.listaPlayersDa1.Count, TradeParameters, paramsEquipo1, tradeator.Equipo1, out sMessage))
                {
                    isValid = 0;
                    sMotinoNoValido = sMotinoNoValido + sMessage;
                }
                if (!isNotOverComeTrades(tradeator.listaPlayers2.Count, tradeator.listaPlayersDa2.Count, TradeParameters, paramsEquipo2, tradeator.Equipo2, out sMessage))
                {
                    isValid = 0;
                    sMotinoNoValido = sMotinoNoValido + sMessage;
                }
                if (!isNotOverComeTrades(tradeator.listaPlayers3.Count, tradeator.listaPlayersDa3.Count, TradeParameters, paramsEquipo3, tradeator.Equipo3, out sMessage))
                {
                    isValid = 0;
                    sMotinoNoValido = sMotinoNoValido + sMessage;
                }
                if (!isNotOverComeTrades(tradeator.listaPlayers4.Count, tradeator.listaPlayersDa4.Count, TradeParameters, paramsEquipo4, tradeator.Equipo4, out sMessage))
                {
                    isValid = 0;
                    sMotinoNoValido = sMotinoNoValido + sMessage;
                }

                sMessage = "";
                //********************************************************************************************
                //********************************************************************************************


                //*************************************************************************
                //CUADRAR SALARIOS ENTRE LOS EQUIPOS QUE CRUZAN JUGADORES**********************************************************
                double salarioSaliente = 0;
                double salarioEntrante = 0;

                salarioSaliente = Convert.ToDouble(tradeator.listaPlayers1.Sum(x => x.PlayersSalariesFull.First().Salarie));
                salarioEntrante = Convert.ToDouble(tradeator.listaPlayersDa1.Sum(x => x.PlayersSalariesFull.First().Salarie));
                if (!bCuadranPresupuestos(TradeParameters, salarioSaliente, salarioEntrante, tradeator.PresupuestoAntes1, tradeator.PresupuestoDespues1, tradeator.Equipo1, out sMessage))
                {
                    isValid = 0;
                    sMotinoNoValido = sMotinoNoValido + sMessage;
                }

                salarioSaliente = Convert.ToDouble(tradeator.listaPlayers2.Sum(x => x.PlayersSalariesFull.First().Salarie));
                salarioEntrante = Convert.ToDouble(tradeator.listaPlayersDa2.Sum(x => x.PlayersSalariesFull.First().Salarie));
                if (!bCuadranPresupuestos(TradeParameters, salarioSaliente, salarioEntrante, tradeator.PresupuestoAntes2, tradeator.PresupuestoDespues2, tradeator.Equipo2, out sMessage))
                {
                    isValid = 0;
                    sMotinoNoValido = sMotinoNoValido + sMessage;
                }

                salarioSaliente = Convert.ToDouble(tradeator.listaPlayers3.Sum(x => x.PlayersSalariesFull.First().Salarie));
                salarioEntrante = Convert.ToDouble(tradeator.listaPlayersDa3.Sum(x => x.PlayersSalariesFull.First().Salarie));
                if (!bCuadranPresupuestos(TradeParameters, salarioSaliente, salarioEntrante, tradeator.PresupuestoAntes3, tradeator.PresupuestoDespues3, tradeator.Equipo3, out sMessage))
                {
                    isValid = 0;
                    sMotinoNoValido = sMotinoNoValido + sMessage;
                }

                salarioSaliente = Convert.ToDouble(tradeator.listaPlayers4.Sum(x => x.PlayersSalariesFull.First().Salarie));
                salarioEntrante = Convert.ToDouble(tradeator.listaPlayersDa4.Sum(x => x.PlayersSalariesFull.First().Salarie));
                if (!bCuadranPresupuestos(TradeParameters, salarioSaliente, salarioEntrante, tradeator.PresupuestoAntes4, tradeator.PresupuestoDespues4, tradeator.Equipo4, out sMessage))
                {
                    isValid = 0;
                    sMotinoNoValido = sMotinoNoValido + sMessage;
                }
                //*****************************************************************************************************
                //*****************************************************************************************************


                sMessage = "";
                //*************************************************************************
                //Comprobamos que no haya ningun jugador que haya firmado con la midlevel
                //if (bArePlayersSignedWithMidLevel(tradeator, out sMessage))
                //{
                //    isValid = 0;
                //    sMotinoNoValido = sMotinoNoValido + sMessage;
                //}
                //*****************************************************************************************************
                //*****************************************************************************************************

                //Comprobamos que no haya ningun jugador franquicia que haya sido traspasado en dos años
                //if (bAreFranchisesPlayersTradedInTwoYears(tradeator, out sMessage))
                //{
                //    isValid = 0;
                //    sMotinoNoValido = sMotinoNoValido + sMessage;
                //}
                ////*****************************************************************************************************
                ////*****************************************************************************************************

                ////Comprobamos que no haya ningun jugador estrella que haya sido traspasado este año
                //if (bStarsPlayersTradedThisYears(tradeator, out sMessage))
                //{
                //    isValid = 0;
                //    sMotinoNoValido = sMotinoNoValido + sMessage;
                //}
                //*****************************************************************************************************
                //*****************************************************************************************************

                //TOOOOOOOODOOOOOOOOOOOOOOOOOOOOOOOOOO
                //Comprobamos que no haya ningun jugador legalmente o temporalmente intransferible 
                if (bArePlayersLegallyIntransferibles(tradeator, out sMessage))
                {
                    isValid = 0;
                    sMotinoNoValido = sMotinoNoValido + sMessage;
                }

                //*****************************************************************************************************
                //*****************************************************************************************************

                //Comprobamos que ningun equipo se queda con menos de 12 jugadores (si deja seguir pero avisa de que hay
                //que fichar) ni mas de 15 jugadores (si deja seguir pero avisa de que hay que cortar)
                if (superaMinimoMaximoPlantilla(tradeator.Equipo1 ,tradeator.listaPlayersDa1,tradeator.listaPlayers1 , out sMessage))
                {
                    isValid = 0;
                    sMotinoNoValido = sMotinoNoValido + sMessage;
                }
                if (superaMinimoMaximoPlantilla(tradeator.Equipo2, tradeator.listaPlayersDa2, tradeator.listaPlayers2, out sMessage))
                {
                    isValid = 0;
                    sMotinoNoValido = sMotinoNoValido + sMessage;
                }
                if (tradeator.Equipo3 != null)
                {
                    if (superaMinimoMaximoPlantilla(tradeator.Equipo3, tradeator.listaPlayersDa3, tradeator.listaPlayers3, out sMessage))
                    {
                        isValid = 0;
                        sMotinoNoValido = sMotinoNoValido + sMessage;
                    }
                }
                if (tradeator.Equipo4 != null)
                {
                    if (superaMinimoMaximoPlantilla(tradeator.Equipo4, tradeator.listaPlayersDa4, tradeator.listaPlayers4, out sMessage))
                    {
                        isValid = 0;
                        sMotinoNoValido = sMotinoNoValido + sMessage;
                    }
                }
                return isValid;
            }
            catch (Exception ex)
            {
                isValid = 0;
                sMotinoNoValido = ex.ToString();
                return isValid;

            }

        }

        private bool superaMinimoMaximoPlantilla(LVNBAFranchises equipo , List<Players>listaSale, List<Players>listaEntra, out string sMessage)
        {
            Jugadores jug = new Jugadores();
            bool bSupera = false;
            sMessage = "";
            int numberOfPlayersAfterTrade;

            int iPlayersEnPlantilla = jug.getPlayersRoster(equipo.Id).Count();




            //foreach (Players jugador in listaEntra)
            //{
            //    if (jugador.PlayersSalaries.FirstOrDefault().IdCondition1 == 4 || jugador.PlayersSalaries.FirstOrDefault().Salary1.Value == 0)
            //        listaEntra.Remove(jugador);
            //}
            //int iPlayerEntran = listaEntra.Count();
            int iPlayerEntran = listaEntra.Where(x => x.PlayersSalariesFull.FirstOrDefault().Salarie != 0).Count();
            int iPlayerSalen = listaSale.Where(x => x.PlayersSalariesFull.FirstOrDefault().Salarie != 0).Count();

            //foreach (Players jugador in listaSale)
            //{
            //    if (jugador.PlayersSalaries.FirstOrDefault().IdCondition1 == 4 || jugador.PlayersSalaries.FirstOrDefault().Salary1.Value == 0)
            //        listaSale.Remove(jugador);
            //}
            //foreach (Players jugador in listaSale)
            //{
            //    if (jugador.PlayersSalaries.FirstOrDefault().IdCondition1 == 4 || jugador.PlayersSalaries.FirstOrDefault().Salary1.Value == 0)
            //        listaSale.Remove(jugador);
            //}

            numberOfPlayersAfterTrade = iPlayersEnPlantilla + iPlayerEntran - iPlayerSalen;
            if (numberOfPlayersAfterTrade < 12) 
            {
                sMessage = sMessage + "El equipo " + equipo.Abbreviation + " se queda por debajo del mínimo de jugadores en franquicia (" + numberOfPlayersAfterTrade + ")" + System.Environment.NewLine;
                bSupera = true;
            }
            if (numberOfPlayersAfterTrade > 15)
            {
                sMessage = sMessage +  "El equipo " + equipo.Abbreviation + " se queda por encima del máximo de jugadores en franquicia (" + numberOfPlayersAfterTrade + ")" + System.Environment.NewLine;
                bSupera = true;
            }
            return bSupera;

        }


        /// <summary>
        /// Mira si hay jugadores que han firmado por la midlevel ese año
        /// </summary>
        /// <param name="TradeParameters"></param>
        /// <param name="Franchise"></param>
        /// <param name="sMessage"></param>
        /// <returns></returns>
        //private bool bArePlayersSignedWithMidLevel(TradeatorInfo tradeator, out string sMessage)
        //{

        //    sMessage = "";
        //    List<string> ListaJugadores = new List<string>();
        //    try
        //    {
        //        foreach (Players jugador in tradeator.listaPlayers1)
        //        {
        //            if (jugador.PlayersSalaries.FirstOrDefault(). == true)
        //            {
        //                ListaJugadores.Add(jugador.CompleteName + " (" + jugador.LVNBAFranchis.Abbreviation + ")");
        //            }
        //        }
        //        foreach (Players jugador in tradeator.listaPlayers2)
        //        {
        //            if (jugador.PlayersSalaries.FirstOrDefault().MidLevel == true)
        //            {
        //                ListaJugadores.Add(jugador.CompleteName + " (" + jugador.LVNBAFranchis.Abbreviation + ")");
        //            }
        //        }
        //        foreach (Players jugador in tradeator.listaPlayers3)
        //        {
        //            if (jugador.PlayersSalaries.FirstOrDefault().MidLevel == true)
        //            {
        //                ListaJugadores.Add(jugador.CompleteName + " (" + jugador.LVNBAFranchis.Abbreviation + ")");
        //            }
        //        }
        //        foreach (Players jugador in tradeator.listaPlayers4)
        //        {
        //            if (jugador.PlayersSalaries.FirstOrDefault().MidLevel == true)
        //            {
        //                ListaJugadores.Add(jugador.CompleteName + " (" + jugador.LVNBAFranchis.Abbreviation + ")");
        //            }
        //        }

        //        if (ListaJugadores.Count > 0)
        //        {
        //            sMessage = "Hay jugadores que han firmado este año con la MidLevel y no pueden ser traspasados: ´" + System.Environment.NewLine;
        //            foreach (string sNombre in ListaJugadores)
        //            {
        //                sMessage += sNombre + System.Environment.NewLine;
        //            }
        //            return true;
        //        }

        //        return false;

        //    }
        //    catch (Exception ex)
        //    {
        //        sMessage = ex.Message.ToString() + System.Environment.NewLine;
        //        return true;
        //    }
        //}

        /// <summary>
        /// Mira si hay jugadores que son legalmente intransferibles (menos de 20 partidos en la franquicia después de haber sido firmado)
        /// </summary>
        /// <param name="TradeatorInfo"></param>
        /// <param name="sMessage"></param>
        /// <returns></returns>
        private bool bArePlayersLegallyIntransferibles(TradeatorInfo tradeator, out string sMessage)
        {
            sMessage = "";
            bool arePlayersLegallyIntransferibles = false;
            arePlayersLegallyIntransferibles = arePlayersLegallyIntransferibles || bArePlayersLegallyIntransferiblesByTeam(tradeator.listaPlayers1,tradeator.Equipo1.Id,out sMessage);
            arePlayersLegallyIntransferibles = arePlayersLegallyIntransferibles || bArePlayersLegallyIntransferiblesByTeam(tradeator.listaPlayers2, tradeator.Equipo2.Id, out  sMessage);
            if (tradeator.Equipo3 != null)
            {
                arePlayersLegallyIntransferibles = arePlayersLegallyIntransferibles || bArePlayersLegallyIntransferiblesByTeam(tradeator.listaPlayers3, tradeator.Equipo3.Id, out  sMessage);
            }
            if (tradeator.Equipo4 != null)
            {
                arePlayersLegallyIntransferibles = arePlayersLegallyIntransferibles || bArePlayersLegallyIntransferiblesByTeam(tradeator.listaPlayers4, tradeator.Equipo4.Id, out  sMessage);
            }
            return arePlayersLegallyIntransferibles;
        }

        /// <summary>
        /// Mira si hay jugadores que son legalmente intransferibles (menos de 20 partidos en la franquicia después de haber sido firmado) en el equipo
        /// </summary>
        /// <param name="TradeatorInfo"></param>
        /// <param name="sMessage"></param>
        /// <returns></returns>
        private bool bArePlayersLegallyIntransferiblesByTeam(List<Players> listaPlayers, int idTeam, out string sMessage)
        {

            sMessage = "";
            List<string> ListaJugadores = new List<string>();
            try
            {

                Parameters Parameters = db.Parameters.First();
                foreach (Players jugador in listaPlayers)
                {
                    //TODO
                    //Falta condición para que sean contratados y firmados(Falta campo en salarios pondría la fecha en la que se ha otorgado ese contrato y luego filtrar games posteriores a esa fecha)
                    //creamos campo en playersalaries que marca si viene de agencia , para mirar si ya han pasado de 20 partidos // es en el reporte donde quitamos esa condicion
                    if (jugador.FromAgency == 1 && jugador.PlayersSalariesFull.Any(x => x.IdCondition == 0))
                    {
                        ListaJugadores.Add(jugador.CompleteName + " (" + jugador.LVNBAFranchis.Abbreviation + ")");
                    }
                    

                    //int numberOfGamesInTeam = db.PlayersStatsGames.Where(x => x.idPlayer == jugador.Id && x.idTeam == jugador.ActualTeam && x.Game.season == Parameters.Temporada).length;
                    //if (numberOfGamesInTeam < 20)
                    //{
                    //    ListaJugadores.Add(jugador.CompleteName + " (" + jugador.LVNBAFranchis.Abbreviation + ")");
                    //}
                }

                if (ListaJugadores.Count > 0)
                {
                    sMessage = "Hay jugadores que no han estado en la plantilla del equipo durante almenos 20 partidos y no pueden ser traspasados: ´" + System.Environment.NewLine;
                    foreach (string sNombre in ListaJugadores)
                    {
                        sMessage += sNombre + System.Environment.NewLine;
                    }
                    return true;
                }

                return false;

            }
            catch (Exception ex)
            {
                sMessage = ex.Message.ToString() + System.Environment.NewLine;
                return true;
            }
        }

        /// <summary>
        /// Mira si el equipo cumple el plazo minimo de partidos jugados (cuando estamos en temporada regular)
        /// </summary>
        /// <param name="TradeParameters"></param>
        /// <param name="Franchise"></param>
        /// <param name="sMessage"></param>
        /// <returns></returns>
        private bool isInPlazo(Parameters TradeParameters, LVNBAFranchises Franchise, out string sMessage)
        {
            sMessage = "";
            try
            {

                int iSeason = TradeParameters.Temporada;
                if (Franchise == null) //si viene nulo es que no se debe mirar (ya hemos comprobado que equipo debe venir informado)
                {
                    return true;
                }
                if (TradeParameters.Pretemporada == false && TradeParameters.Postemporada == false && TradeParameters.RegularSeason.Value == 1) //estamos en temporada regular
                {
                    //miramos si el equipo tiene el mínimo de partidos jugados
                    int iPartidosJugados = db.Games.Where(x => x.season == iSeason && x.gameDate != null && (x.IdTeamAway == Franchise.Id || x.IdTeamLocal == Franchise.Id)).Count();
                    if (iPartidosJugados < TradeParameters.PlazoMin)
                    {
                        sMessage = Franchise.Name + " no supera el plazo mínimo de partidos jugados (" + TradeParameters.PlazoMin + ") " + System.Environment.NewLine;
                        return false;
                    }
                }
                else
                { //no estamos en temporada regular, puede ser pretemporada, postemporada o playoffs, no se miran los plazos

                    return true;
                }


                return true;
            }
            catch (Exception ex)
            {
                sMessage = ex.Message.ToString() + System.Environment.NewLine;
                return false;
            }
        }


        /// <summary>
        /// Mira si estamos con el mercado de traspasos abierto
        /// </summary>
        /// <param name="TradeParameters"></param>
        /// <param name="sMessage"></param>
        /// <returns></returns>
        private bool isPeriodToTrade(Parameters TradeParameters, out string sMessage)
        {
            sMessage = "";
            try
            {
                if (TradeParameters.MercadoAbierto == 0)
                {
                    sMessage = "No se permiten traspasos en esta época del año (Mercado Cerrado) " + System.Environment.NewLine;
                    return false;
                }


                return true;
            }
            catch (Exception ex)
            {
                sMessage = ex.Message.ToString() + System.Environment.NewLine;
                return false;
            }
        }

        /// <summary>
        /// Comprueba que el equipo no supere los máximos de traspasos, jugadores que entran y jugadores que salen teniendo en cuenta los del propio traspaso
        /// </summary>
        /// <param name="TradeParameters"></param>
        /// <param name="FranchiseParams"></param>
        /// <param name="Franchise"></param>
        /// <param name="sMessage"></param>
        /// <returns></returns>
        private bool isNotOverComeTrades(int iJugadoresIn, int iJugadoresOut, Parameters TradeParameters, LVNBAFranchisesParams FranchiseParams, LVNBAFranchises Franchise, out string sMessage)
        {
            sMessage = "";
            try
            {
                if (Franchise == null)//si viene nulo es que no se debe mirar (ya hemos comprobado que equipo debe venir informado)
                {
                    return true;
                }

                if (TradeParameters.Pretemporada == false && TradeParameters.Postemporada == false && TradeParameters.RegularSeason.Value == 1) //estamos en temporada regular
                {
                    int iMaxTradesPossible = TradeParameters.SeasonMaxTrades + TradeParameters.OffSeasonMaxTrades - FranchiseParams.OffSeasonTrades.Value;
                    int iMaxPlayersInPossible = TradeParameters.SeasonMaxTradesIn + TradeParameters.OffSeasonMaxTradesIn - FranchiseParams.OffSeasonTradedIn.Value;
                    int iMaxPlayersOutPossible = TradeParameters.SeasonMaxTradesOut + TradeParameters.OffSeasonMaxTradesOut - FranchiseParams.OfSeasonTradedOut.Value;

                    if (iMaxTradesPossible > TradeParameters.WithoutSummerMaxTrades) iMaxTradesPossible = TradeParameters.WithoutSummerMaxTrades;
                    if (iMaxPlayersInPossible > TradeParameters.WithoutSummerMaxTradesIn) iMaxPlayersInPossible = TradeParameters.WithoutSummerMaxTradesIn;
                    if (iMaxPlayersOutPossible > TradeParameters.WithoutSummerMaxTradesOut) iMaxPlayersOutPossible = TradeParameters.WithoutSummerMaxTradesOut;

                    iMaxTradesPossible -= FranchiseParams.SeasonTrades.Value;
                    iMaxPlayersInPossible -= FranchiseParams.SeasonTradedIn.Value;
                    iMaxPlayersOutPossible -= FranchiseParams.SeasonTradedOut.Value;

                    if (iMaxTradesPossible < 1)
                    {
                        sMessage = Franchise.Name + " ha llegado al máximo de traspasos realizados en temporada regular (" + iMaxTradesPossible.ToString() + ") " + System.Environment.NewLine;
                        return false;
                    }
                    if (iMaxTradesPossible - iJugadoresIn < 0)
                    {
                        sMessage = Franchise.Name + " ha llegado al máximo de jugadores que entran en traspasos realizados en temporada regular (" + iMaxPlayersInPossible.ToString() + ") " + System.Environment.NewLine;
                        return false;
                    }
                    if (iMaxTradesPossible - iJugadoresOut < 0)
                    {
                        sMessage = Franchise.Name + " ha llegado al máximo de jugadores que sale en traspasos realizados en temporada regular (" + iMaxPlayersOutPossible.ToString() + ") " + System.Environment.NewLine;
                        return false;
                    }
                }
                else
                {
                    //estamos en pretemporada o postemporada, que es lo mismo
                    if (FranchiseParams.OffSeasonTrades >= TradeParameters.OffSeasonMaxTrades)
                    {
                        sMessage = Franchise.Name + " ha llegado al máximo de traspasos realizados en pretemporada (" + TradeParameters.OffSeasonMaxTrades.ToString() + ") " + System.Environment.NewLine;
                        return false;
                    }
                    if (FranchiseParams.OffSeasonTradedIn + iJugadoresIn > TradeParameters.OffSeasonMaxTradesIn)
                    {
                        sMessage = Franchise.Name + " ha llegado al máximo de jugadores que entran en traspasos realizados en pretemporada (" + TradeParameters.OffSeasonMaxTradesIn.ToString() + ") " + System.Environment.NewLine;
                        return false;
                    }
                    if (FranchiseParams.OfSeasonTradedOut + iJugadoresOut > TradeParameters.OffSeasonMaxTradesOut)
                    {
                        sMessage = Franchise.Name + " ha llegado al máximo de jugadores que sale en traspasos realizados en pretemporada (" + TradeParameters.OffSeasonMaxTradesOut.ToString() + ") " + System.Environment.NewLine;
                        return false;
                    }

                }




                return true;
            }
            catch (Exception ex)
            {
                sMessage = ex.Message.ToString() + System.Environment.NewLine;
                return false;
            }
        }


        /// <summary>
        /// Mira si se cumplen las condiciones presupuestales entre dos equipos
        /// </summary>
        /// <param name="TradeParameters"></param>
        /// <param name="listaPlayersEnvia"></param>
        /// <param name="listaPlayersRecibe"></param>
        /// <param name="Presupuesto">Presupuesto despues del traspaso</param>
        /// <param name="Franchise"></param>
        /// <param name="FranchiseParams"></param>
        /// <param name="listaPlayersEnvia2"></param>
        /// <param name="listaPlayersRecibe2"></param>
        /// <param name="Presupuesto2"></param>
        /// <param name="Franchise2"></param>
        /// <param name="FranchiseParams2"></param>
        /// <param name="sMessage"></param>
        /// <returns></returns>
        private bool bCuadranPresupuestos(Parameters TradeParameters, double dSalarioSaliente, double dSalarioEntrante, decimal PresupuestoAntes, decimal PresupuestoDespues, LVNBAFranchises Franchise, out string sMessage)
        {
            sMessage = "";
            try
            {

                //Si el equipo se queda con menos salario que anteriormente se cumple la condición.
                //if (PresupuestoAntes >= PresupuestoDespues) return true; CORRIJO ESTO
                if (PresupuestoAntes < PresupuestoDespues) return true;

                //Si el equipo recibe más dinero habrá que evaluar las normas salariales.
                //Desglosamos las posibilidades existentes:

                //a) El equipo bajo ningún concepto podrá superar la No Superable Tax.
                if (PresupuestoDespues > TradeParameters.CifraNoSuperable)
                {
                    sMessage = "<span style='color:red'>" + Franchise.Name + " ESTÁ POR ENCIMA DE LA CIFRA NO SUPERABLE (" + TradeParameters.CifraNoSuperable.ToString() + ") </span>";
                    return false;
                }

                //b) El equipo supera el Super Luxury Tax: El equipo podrá recibir hasta un 10% más de lo que recibe más 100,000$.
                if (PresupuestoDespues > TradeParameters.SuperTasaLujo && dSalarioSaliente + (dSalarioSaliente * 0.1) + 100000 < dSalarioEntrante)
                {
                    double dSobrePasa = dSalarioEntrante - (dSalarioSaliente + (dSalarioSaliente * 0.1) + 100000);
                    sMessage = "<span style='color:red'>" + Franchise.Name + " NO CUADRA SALARIALMENTE --> MARGEN (" + dSobrePasa + ") </span>";
                    return false;
                }

                //c) El equipo supera el Salary Cap: El equipo podrá recibir hasta un 25% más de lo que recibe más 100,000$.
                if (PresupuestoDespues > TradeParameters.LimiteSalarial && dSalarioSaliente + (dSalarioSaliente * 0.25) + 100000 < dSalarioEntrante)
                {
                    double dSobrePasa = dSalarioEntrante - (dSalarioSaliente + (dSalarioSaliente * 0.25) + 100000);
                    sMessage = "<span style='color:red'>" + Franchise.Name  + " NO CUADRAN SALARIALMENTE --> MARGEN (" + dSobrePasa + ") </span>";
                    return false;
                }

                //d) El equipo no supera el Salary Cap
                if (PresupuestoDespues <= TradeParameters.LimiteSalarial)
                {
                    return true;
                }

                return true;
            }
            catch (Exception ex)
            {
                sMessage = ex.Message.ToString() + System.Environment.NewLine;
                return false;
            }
        }

    }
}