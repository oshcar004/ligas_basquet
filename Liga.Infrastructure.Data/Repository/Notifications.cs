﻿
using Liga.Domain.Entities;
using Liga.Infrastructure.Data;
using System;
using System.Linq;

namespace Liga.Infrastructure.Data.Repository
{
    public class Notifications
    {
        private Liga_DBContext db = new Liga_DBContext();

        public bool SendNotification(int? idSender, int? idReceiver, int? idType, string sTitle, string sNotification, out string sError)
        {
            sError = "";
            try
            {
                switch (idType)
                {
                    case GlobalConstants.Ct_Notifications.NotificationsTypes.ManagerWarning:
                        if (NotificateWarning(idReceiver,idType,sTitle,sNotification,  out sError))
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    default:
                        sError = "No se ha especificado tipo";
                        return false;
                }
                
            }
            catch (Exception ex)
            {
                sError = ex.ToString();
                return false;
            }
        }

        private bool NotificateWarning(int? idReceiver, int? idType, string sTitle, string sNotification, out string sError)
        {
            sError = "";
            try
            {
                Notification note = new Notification();
                int iSeason = db.Parameters.First().Temporada;
                note.season = iSeason;
                note.sendDate = DateTime.Now;
                note.idSender = GlobalConstants.Ct_Notifications.NotificationSenders.Comisionado;
                note.idReceiver = idReceiver.Value;
                note.idtype = GlobalConstants.Ct_Notifications.NotificationsTypes.ManagerWarning;
                note.title = " Aviso de queja ";
                sNotification = sNotification.Replace("\r\n", "<br/>");
                note.notification1 = new MailsNotificationsBuilding().getWarningMailNotification(sNotification);
                db.Notifications.Add(note);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                sError = ex.ToString();
                return false;
            }
        }
    }
}