﻿
using Liga.Domain.Entities;
using Liga.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Liga.Infrastructure.Data.Repository
{
    public class Salarios
    {
        private Liga_DBContext db = new Liga_DBContext();

        public decimal getSalaryTeam(int idTeam)
        {
            try
            {
                decimal? salaryTeam = db.PlayersSalariesFull.Where(x => x.IdLVNBAFranchises == idTeam).Sum(x => x.Salarie);
                if (salaryTeam != null)
                    return salaryTeam.Value;
                else
                    return 0;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<PlayersSalariesFull> getSalaryPlayers(int? idTeam)
        {
            IEnumerable<PlayersSalariesFull> lista;
            lista = db.PlayersSalariesFull.Where(x => x.IdLVNBAFranchises == idTeam.Value && x.Player.IdState != 4).AsEnumerable();
            return lista;
        }
    }
}