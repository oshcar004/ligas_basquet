﻿
using Liga.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Liga.Infrastructure.Data.Repository
{
    public class Jugadores
    {
        private Liga_DBContext db = new Liga_DBContext();


        public void ConvertirEnFA(int id)
        {
            try
            {
                Players player = db.Players.Find(id);
                foreach (var pSal in db.PlayersSalariesFull.Where(x => x.IdPlayer == id))
                {
                    db.PlayersSalariesFull.Remove(pSal);
                }
                player.ActualTeam = null;
                player.IdRol = 1; //sin rol
                player.IdState = 7; //sin estado
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CutPlayer(int id)
        {
            try
            {
                Players player = db.Players.Find(id);
                var playerSalary = db.PlayersSalariesFull.Where(x => x.IdPlayer == id && x.DateSalarie.Year == db.Parameters.First().Temporada).FirstOrDefault();
                if (playerSalary != null)
                {
                    playerSalary.IdCondition = db.SalaryConditions.Where(x => x.Abrev == "CU").First().Id;
                }

                player.ActualTeam = null;
                player.IdRol = 1; //sin rol
                player.IdState = 7; //sin estado
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RetirePlayer(int id)
        {
            try
            {
                Players player = db.Players.Find(id);
                foreach (var pSal in db.PlayersSalariesFull.Where(x => x.IdPlayer == id))
                {
                    db.PlayersSalariesFull.Remove(pSal);
                }
                player.ActualTeam = null;
                player.IdRol = 1; //sin rol
                player.IdState = 7; //sin estado
                player.Retired = true;
                PlayersHistories pHist = db.PlayersHistories.Where(x => x.IdPlayer == id && x.EndDate == null).FirstOrDefault();
                if (pHist != null)
                    pHist.EndDate = DateTime.Now;

                db.SaveChanges();
                //genera anuncio, pasando el equipo, el jugador, el primer salario, edad
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Dado un jugador, miramos si se convierte en LEGALMENTE INSTRANSFERIBLE
        /// </summary>
        /// <param name="idPlayer"></param>
        /// <returns></returns>
        public bool esLI(int idPlayer)
        {
            Players player = db.Players.Find(idPlayer);
            if (player != null)
            {
                if (player.Level2k != null && player.Level2k > 80)
                {
                    return true;
                }

            }
            return false;
        }

        /// <summary>
        /// Obtiene los jugadores de la plantilla
        /// </summary>
        /// <param name="idTeam"></param>
        /// <returns></returns>
        public IEnumerable<Players> getPlayersRoster(int idTeam)
        {
            List<Players> playersList = new List<Players>();
            List<PlayersSalariesFull> playersSalariesFullList = new List<PlayersSalariesFull>();
            playersSalariesFullList = db.PlayersSalariesFull.Where(x => x.IdLVNBAFranchises == idTeam
            && x.DateSalarie.Year >= db.Parameters.FirstOrDefault().Temporada && x.Salarie > 1).OrderBy(x => x.Salarie).ToList();
            foreach (PlayersSalariesFull playersSalariesFull in playersSalariesFullList)
            {
                if (!playersList.Contains(playersSalariesFull.Player))
                    playersList.Add(playersSalariesFull.Player); // Guarda los jugadores pertenecientes al equipo y los jugadores que fueron cortados por el equipo (que pueden pertenecer a otro equipo) pero se les debe pagar igual
            }
            playersList = playersList.Where(x => x.ActualTeam == idTeam && x.PlayersSalariesFull != null && x.PlayersSalariesFull.Count() > 0).OrderByDescending(x => x.PlayersSalariesFull.First().Salarie).ToList();
            playersList = playersList.Where(x => x.Cutted != 1).ToList();
            return playersList;
        }
        /// <summary>
        /// Obtiene los jugadores que le cuentan para el salary cap
        /// </summary>
        /// <param name="idTeam"></param>
        /// <returns></returns>
        public IEnumerable<PlayersSalariesFull> getPlayersSalaryInSalary(int idTeam)
        {
            List<Players> listaJugadores = new List<Players>();
            var salarios = db.PlayersSalariesFull.Where(x => x.IdLVNBAFranchises == idTeam && x.DateSalarie.Year == db.Parameters.FirstOrDefault().Temporada); //asi un tio que se ha ido a la nba este año si cuenta
            
            return salarios.AsEnumerable();
        }


        /// <summary>
        /// Obtiene los jugadores que no cuentan para el salary cap
        /// </summary>
        /// <param name="idTeam"></param>
        /// <returns></returns>
        public IEnumerable<PlayersSalariesFull> getPlayersOutOfSalaryInSalary(int idTeam)
        {
            IEnumerable<PlayersSalariesFull> salarios = db.PlayersSalariesFull.Where(x => x.IdLVNBAFranchises == idTeam && x.Salarie == 0 ).OrderByDescending(x => x.Salarie); //asi un tio que se ha ido a la nba este año si cuenta
            return salarios.AsEnumerable();// listaJugadores.AsEnumerable();
        }

        /// <summary>
        /// Obtiene los jugadores que son cortados o con derechos 
        /// </summary>
        /// <param name="idTeam"></param>
        /// <returns></returns>
        public IEnumerable<Players> getPlayersRightsAndCutted(int idTeam)
        {
            var listaJugadores = new List<Players>();
            var salarios = db.PlayersSalariesFull.Where(x => x.IdLVNBAFranchises == idTeam && ( x.Salarie == 0 || x.IdCondition == 4)).ToList();
            foreach (var salario in salarios)
            {

                Players jugador = db.Players.Where(x => x.Id == salario.IdPlayer).First();
                listaJugadores.Add(jugador);
            }

            return listaJugadores.AsEnumerable();
        }


        /// <summary>
        /// Obtiene todos los jugadores del equipo (+cortados y derechos)
        /// </summary>
        /// <param name="idTeam"></param>
        /// <returns></returns>
        public IEnumerable<Players> getPlayersAll(int idTeam)
        {
            return db.Players.Where(x => x.ActualTeam == idTeam).AsEnumerable();
        }

    }
}