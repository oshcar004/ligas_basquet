﻿using Liga.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Liga.Infrastruture.Data.Mapping
{
    public class StatsForReportMap : EntityTypeConfiguration<StatsForReport>
    {
        public StatsForReportMap()
        {
            // Primary Key
            this.HasKey(t => new { t.idPlayer, t.idTeam, t.idGame, t.PNTS, t.REBS, t.AST, t.ROB, t.TAP, t.PER, t.TCA, t.TCM, t.C3TCA, t.C3TCM, t.TLA, t.TLM, t.RO, t.FP, t.MASMIN });

            // Properties
            this.Property(t => t.idPlayer)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.idTeam)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.idGame)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ABRE)
                .HasMaxLength(4);

            this.Property(t => t.LOGO)
                .HasMaxLength(50);

            this.Property(t => t.Nombre)
                .HasMaxLength(252);

            this.Property(t => t.TI)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.PNTS)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.REBS)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.AST)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ROB)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.TAP)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.PER)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.TCA)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.TCM)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.C3TCA)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.C3TCM)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.TLA)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.TLM)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.RO)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.FP)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.MASMIN)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("StatsForReport");
            this.Property(t => t.idPlayer).HasColumnName("idPlayer");
            this.Property(t => t.idTeam).HasColumnName("idTeam");
            this.Property(t => t.idGame).HasColumnName("idGame");
            this.Property(t => t.ABRE).HasColumnName("ABRE");
            this.Property(t => t.LOGO).HasColumnName("LOGO");
            this.Property(t => t.Nombre).HasColumnName("Nombre");
            this.Property(t => t.TI).HasColumnName("TI");
            this.Property(t => t.MIN).HasColumnName("MIN");
            this.Property(t => t.PNTS).HasColumnName("PNTS");
            this.Property(t => t.REBS).HasColumnName("REBS");
            this.Property(t => t.AST).HasColumnName("AST");
            this.Property(t => t.ROB).HasColumnName("ROB");
            this.Property(t => t.TAP).HasColumnName("TAP");
            this.Property(t => t.PER).HasColumnName("PER");
            this.Property(t => t.TCA).HasColumnName("TCA");
            this.Property(t => t.TCM).HasColumnName("TCM");
            this.Property(t => t.C3TCA).HasColumnName("3TCA");
            this.Property(t => t.C3TCM).HasColumnName("3TCM");
            this.Property(t => t.TLA).HasColumnName("TLA");
            this.Property(t => t.TLM).HasColumnName("TLM");
            this.Property(t => t.RO).HasColumnName("RO");
            this.Property(t => t.FP).HasColumnName("FP");
            this.Property(t => t.MASMIN).HasColumnName("MASMIN");
        }
    }
}