﻿using Liga.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Liga.Infrastruture.Data.Mapping
{
    public class ClasificacionMap : EntityTypeConfiguration<Clasificacion>
    {
        public ClasificacionMap()
        {
            // Primary Key
            this.HasKey(t => new { t.id });

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(30);

            this.Property(t => t.id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
                .IsRequired();

            this.Property(t => t.Casa)
                .HasMaxLength(61);

            this.Property(t => t.Fuera)
                .HasMaxLength(61);

            this.Property(t => t.Division)
                .HasMaxLength(61);

            this.Property(t => t.Conf)
                .HasMaxLength(61);

            this.Property(t => t.L10)
                .HasMaxLength(61);

            this.Property(t => t.Racha)
                .HasMaxLength(31);

            // Table & Column Mappings
            this.ToTable("Clasificacion");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.G).HasColumnName("G");
            this.Property(t => t.W).HasColumnName("W");
            this.Property(t => t.L).HasColumnName("L");
            this.Property(t => t.Pct).HasColumnName("Pct");
            this.Property(t => t.PPG).HasColumnName("PPG");
            this.Property(t => t.APG).HasColumnName("APG");
            this.Property(t => t.Casa).HasColumnName("Casa");
            this.Property(t => t.Fuera).HasColumnName("Fuera");
            this.Property(t => t.Division).HasColumnName("Division");
            this.Property(t => t.Conf).HasColumnName("Conf");
            this.Property(t => t.L10).HasColumnName("L10");
            this.Property(t => t.Racha).HasColumnName("Racha");

            this.HasRequired(t => t.TeamInfo)
             .WithRequiredPrincipal(t => t.Posicion);

           
        }
    }
}