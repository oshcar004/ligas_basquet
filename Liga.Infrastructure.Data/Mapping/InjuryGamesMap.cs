﻿using Liga.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Liga.Infrastruture.Data.Mapping
{
    public class InjuryGamesMap : EntityTypeConfiguration<InjuryGames>
    {
        public InjuryGamesMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("InjuryGames");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IdPlayer).HasColumnName("IdPlayer");
            this.Property(t => t.IdGame).HasColumnName("IdGame");
           

            // Relationships
            this.HasRequired(t => t.Game)
                .WithMany(t => t.GamesInjuries)
                .HasForeignKey(d => d.IdGame);
            this.HasRequired(t => t.Player)
                .WithMany(t => t.PlayersInjuries)
                .HasForeignKey(d => d.IdPlayer);

        }
    }
}