using Liga.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Liga.Infrastruture.Data.Mapping
{
    public class AwardsMap : EntityTypeConfiguration<Awards>
    {
        public AwardsMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Description)
                .HasMaxLength(300);

            this.Property(t => t.Image)
                .HasMaxLength(100);

            this.Property(t => t.BigImage)
                .HasMaxLength(100);

            this.Property(t => t.Class)
                .IsFixedLength()
                .HasMaxLength(1);

            // Table & Column Mappings
            this.ToTable("Awards");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Class).HasColumnName("Class");
            this.Property(t => t.Priority).HasColumnName("Priority");
            this.Property(t => t.Bonus).HasColumnName("Bonus");
            this.Property(t => t.Image).HasColumnName("Image");
            this.Property(t => t.BigImage).HasColumnName("BigImage");
            // Relationships
            this.HasOptional(t => t.AwardsClass)
                .WithMany(t => t.Awards)
                .HasForeignKey(d => d.Class);

        }
    }
}
