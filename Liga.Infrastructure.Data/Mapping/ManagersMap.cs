using Liga.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Liga.Infrastruture.Data.Mapping
{
    public class ManagersMap : EntityTypeConfiguration<Managers>
    {
        public ManagersMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.GamerTag)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.ForumName)
                .HasMaxLength(50);

            this.Property(t => t.RealName)
                .HasMaxLength(150);

            this.Property(t => t.Email)
                .HasMaxLength(100);

            this.Property(t => t.Category)
                .IsFixedLength()
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("Managers");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.GamerTag).HasColumnName("GamerTag");
            this.Property(t => t.ForumName).HasColumnName("ForumName");
            this.Property(t => t.RealName).HasColumnName("RealName");
            this.Property(t => t.Age).HasColumnName("Age");
            this.Property(t => t.AboutMe).HasColumnName("AboutMe");
            this.Property(t => t.Experience).HasColumnName("Experience");
            this.Property(t => t.LVNBAAge).HasColumnName("LVNBAAge");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.BornDate).HasColumnName("BornDate");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.Photo).HasColumnName("Photo");
            this.Property(t => t.Category).HasColumnName("Category");
            this.Property(t => t.TotalBonus).HasColumnName("TotalBonus");
            this.Property(t => t.Active).HasColumnName("Active");
            this.Property(t => t.Expulsado).HasColumnName("Expulsado");
            this.Property(t => t.Warnings).HasColumnName("Warnings");

            // Relationships
            this.HasMany(t => t.Players)
                .WithMany(t => t.Managers)
                .Map(m =>
                    {
                        m.ToTable("ManagersFolloweds");
                        m.MapLeftKey("IdManager");
                        m.MapRightKey("IdPlayer");
                    });



        }
    }
}
