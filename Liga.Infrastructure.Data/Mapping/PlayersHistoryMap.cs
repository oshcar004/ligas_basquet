using Liga.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Liga.Infrastruture.Data.Mapping
{
    public class PlayersHistoryMap : EntityTypeConfiguration<PlayersHistories>
    {
        public PlayersHistoryMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("PlayersHistories");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IdPlayer).HasColumnName("IdPlayer");
            this.Property(t => t.IdFranchise).HasColumnName("IdFranchise");
            this.Property(t => t.StartDate).HasColumnName("StartDate");
            this.Property(t => t.EndDate).HasColumnName("EndDate");
            this.Property(t => t.IdTransfer).HasColumnName("IdTransfer");

            // Relationships
            this.HasRequired(t => t.LVNBAFranchis)
                .WithMany(t => t.PlayersHistories)
                .HasForeignKey(d => d.IdFranchise);
            this.HasRequired(t => t.Player)
                .WithMany(t => t.PlayersHistories)
                .HasForeignKey(d => d.IdPlayer);
            this.HasOptional(t => t.Transfer)
                .WithMany(t => t.PlayersHistories)
                .HasForeignKey(d => d.IdTransfer);
        }
    }
}
