using Liga.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Liga.Infrastruture.Data.Mapping
{
    public class DivisionsMap : EntityTypeConfiguration<Divisions>
    {
        public DivisionsMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Division1)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Divisions");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Division1).HasColumnName("Division");
            this.Property(t => t.IdConference).HasColumnName("IdConference");

            // Relationships
            this.HasOptional(t => t.Conference)
                .WithMany(t => t.Divisions)
                .HasForeignKey(d => d.IdConference);

        }
    }
}
