using Liga.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Liga.Infrastruture.Data.Mapping
{
    public class HistoryGoalsMap : EntityTypeConfiguration<HistoryGoals>
    {
        public HistoryGoalsMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Type)
                .HasMaxLength(10);

            this.Property(t => t.Who)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("HistoryGoals");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IdGoal).HasColumnName("IdGoal");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.Who).HasColumnName("Who");
            this.Property(t => t.Season).HasColumnName("Season");

            // Relationships
            this.HasRequired(t => t.Goal)
                .WithMany(t => t.HistoryGoals)
                .HasForeignKey(d => d.IdGoal);

        }
    }
}
