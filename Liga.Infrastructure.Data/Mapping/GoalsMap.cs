using Liga.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Liga.Infrastruture.Data.Mapping
{
    public class GoalsMap : EntityTypeConfiguration<Goals>
    {
        public GoalsMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Goal1)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(150);

            this.Property(t => t.Type)
                .IsRequired()
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("Goals");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Goal1).HasColumnName("Goal");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.Bonus).HasColumnName("Bonus");

        }
    }
}
