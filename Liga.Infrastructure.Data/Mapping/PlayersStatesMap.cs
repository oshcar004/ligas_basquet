using Liga.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Liga.Infrastruture.Data.Mapping
{
    public class PlayersStatesMap : EntityTypeConfiguration<PlayersStates>
    {
        public PlayersStatesMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.State)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Abrev)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("PlayersStates");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.Abrev).HasColumnName("Abrev");
        }
    }
}
