﻿using Liga.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Liga.Infrastruture.Data.Mapping
{
    public class PlayerUsageMap : EntityTypeConfiguration<PlayerUsage>
    {
        public PlayerUsageMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("PlayerUsage");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ActualTeam).HasColumnName("ActualTeam");
            this.Property(t => t.season).HasColumnName("season");
            this.Property(t => t.PNTS).HasColumnName("PNTS");
            this.Property(t => t.FGA).HasColumnName("FGA");
            this.Property(t => t.FTA).HasColumnName("FTA");
            this.Property(t => t.F3A).HasColumnName("F3A");
            this.Property(t => t.REBS).HasColumnName("REBS");
            this.Property(t => t.ASS).HasColumnName("ASS");
            this.Property(t => t.STLS).HasColumnName("STLS");
            this.Property(t => t.TRNS).HasColumnName("TRNS");
            this.Property(t => t.BLKS).HasColumnName("BLKS");
        }
    }
}