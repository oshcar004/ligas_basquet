﻿using Liga.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Liga.Infrastruture.Data.Mapping
{
    public class ValoracionesMap : EntityTypeConfiguration<Valoraciones>
    {
        public ValoracionesMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.id)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("Valoraciones");
            this.Property(t => t.nota).HasColumnName("Nota");
            this.Property(t => t.idManagerOtorga).HasColumnName("idManagerOtorga");
            this.Property(t => t.idManagerRecibe).HasColumnName("idManagerRecibe");
            this.Property(t => t.idGame).HasColumnName("idGame");
            this.Property(t => t.gameDate).HasColumnName("gameDate");
            this.Property(t => t.season).HasColumnName("season");


            this.Property(t => t.id).HasColumnName("id");

            // Relationships
            this.HasRequired(t => t.ManagerInfoOtorgado)
                .WithMany(t => t.ValoracionesOtorga)
                .HasForeignKey(d => d.idManagerOtorga);

            this.HasRequired(t => t.ManagerInfoRecibido)
            .WithMany(t => t.ValoracionesRecibe)
            .HasForeignKey(d => d.idManagerRecibe);

            this.HasRequired(t => t.GameInfo)
                .WithMany(t=>t.ValoracionesGames)
                .HasForeignKey(d=>d.idGame);
        }
    }
}