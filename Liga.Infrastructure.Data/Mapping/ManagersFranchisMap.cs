using Liga.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Liga.Infrastruture.Data.Mapping
{
    public class ManagersFranchisMap : EntityTypeConfiguration<ManagersFranchises>
    {
        public ManagersFranchisMap()
        {
            // Primary Key
            this.HasKey(t => new { t.IdFranchise, t.IdManager, t.StartDate });

            // Properties
            this.Property(t => t.IdFranchise)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IdManager)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("ManagersFranchises");
            this.Property(t => t.IdFranchise).HasColumnName("IdFranchise");
            this.Property(t => t.IdManager).HasColumnName("IdManager");
            this.Property(t => t.StartDate).HasColumnName("StartDate");
            this.Property(t => t.EndDate).HasColumnName("EndDate");

            // Relationships
            this.HasRequired(t => t.LVNBAFranchis)
                .WithMany(t => t.ManagersFranchises)
                .HasForeignKey(d => d.IdFranchise);
            this.HasRequired(t => t.Manager)
                .WithMany(t => t.ManagersFranchises)
                .HasForeignKey(d => d.IdManager);

        }
    }
}
