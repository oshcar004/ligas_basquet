using Liga.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Liga.Infrastruture.Data.Mapping
{
    public class BoxScoreTeamsMap : EntityTypeConfiguration<BoxScoreTeams>
    {
        public BoxScoreTeamsMap()
        {
            // Primary Key
            HasKey(t => new { t.id, t.idScoreLocal, t.idScoreAway, t.LocalStadium, t.AwayStadium, t.LocalSidebar, t.AwaySidebar,t.TMIN, t.TPNTS, t.TRDEFS, t.TREBS, t.TROFS
                ,
                                   t.TASSS,
                                   t.TBLKS,
                                   t.TSTLS,
                                   t.TTRNS,
                                   t.TFOLS,
                                   t.TFGM,
                                   t.TFGA,
                                   t.TF3M,
                                   t.TF3A,
                                   t.TFTM,
                                   t.TFTA,
                                   t.idTeam
            });

            // Properties
            Property(t => t.id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.idScoreLocal)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.idScoreAway)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.LocalName)
                .HasMaxLength(30);

            Property(t => t.AwayName)
                .HasMaxLength(30);

            Property(t => t.AbreLocal)
                .HasMaxLength(4);

            Property(t => t.AbreAway)
                .HasMaxLength(4);

            Property(t => t.LocalLogo)
                .HasMaxLength(50);

            Property(t => t.AwayLogo)
                .HasMaxLength(50);

            Property(t => t.LocalStadium)
                .IsRequired()
                .HasMaxLength(50);

            Property(t => t.AwayStadium)
                .IsRequired()
                .HasMaxLength(50);

            Property(t => t.LocalSidebar)
                .IsRequired()
                .HasMaxLength(50);

            Property(t => t.AwaySidebar)
                .IsRequired()
                .HasMaxLength(50);

            Property(t => t.LocalIcono)
                .HasMaxLength(10);

            Property(t => t.AwayIcono)
                .HasMaxLength(10);

            Property(t => t.TMIN)
             .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.TPNTS)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.TRDEFS)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.TREBS)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.TROFS)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.TASSS)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.TBLKS)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.TSTLS)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.TTRNS)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.TFOLS)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.TFGM)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.TFGA)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.TF3M)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.TF3A)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.TFTM)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.TFTA)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.idTeam)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);




            // Table & Column Mappings
            ToTable("BoxScoreTeams");
            Property(t => t.id).HasColumnName("id");
            Property(t => t.idScoreLocal).HasColumnName("idScoreLocal");
            Property(t => t.idScoreAway).HasColumnName("idScoreAway");
            Property(t => t.gameDate).HasColumnName("gameDate");
            Property(t => t.LocalName).HasColumnName("LocalName");
            Property(t => t.AwayName).HasColumnName("AwayName");
            Property(t => t.AbreLocal).HasColumnName("AbreLocal");
            Property(t => t.AbreAway).HasColumnName("AbreAway");
            Property(t => t.IdLocal).HasColumnName("IdLocal");
            Property(t => t.IdAway).HasColumnName("IdAway");
            Property(t => t.LocalLogo).HasColumnName("LocalLogo");
            Property(t => t.AwayLogo).HasColumnName("AwayLogo");
            Property(t => t.LocalStadium).HasColumnName("LocalStadium");
            Property(t => t.AwayStadium).HasColumnName("AwayStadium");
            Property(t => t.LocalSidebar).HasColumnName("LocalSidebar");
            Property(t => t.AwaySidebar).HasColumnName("AwaySidebar");
            Property(t => t.LocalIcono).HasColumnName("LocalIcono");
            Property(t => t.AwayIcono).HasColumnName("AwayIcono");

            Property(t => t.TPNTS).HasColumnName("TMIN");
            Property(t => t.TPNTS).HasColumnName("TPNTS");
            Property(t => t.TRDEFS).HasColumnName("TRDEFS");
            Property(t => t.TREBS).HasColumnName("TREBS");
            Property(t => t.TROFS).HasColumnName("TROFS");
            Property(t => t.TASSS).HasColumnName("TASSS");
            Property(t => t.TBLKS).HasColumnName("TBLKS");
            Property(t => t.TSTLS).HasColumnName("TSTLS");
            Property(t => t.TTRNS).HasColumnName("TTRNS");
            Property(t => t.TFOLS).HasColumnName("TFOLS");
            Property(t => t.TFGM).HasColumnName("TFGM");
            Property(t => t.TFGA).HasColumnName("TFGA");
            Property(t => t.TF3M).HasColumnName("TF3M");
            Property(t => t.TF3A).HasColumnName("TF3A");
            Property(t => t.TFTM).HasColumnName("TFTM");
            Property(t => t.TFTA).HasColumnName("TFTA");

        }
    }
}
