using Liga.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Liga.Infrastruture.Data.Mapping
{
    public class AwardsClassMap : EntityTypeConfiguration<AwardsClass>
    {
        public AwardsClassMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.Id)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            Property(t => t.Class)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            ToTable("AwardsClasses");
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.Class).HasColumnName("Class");
        }
    }
}
