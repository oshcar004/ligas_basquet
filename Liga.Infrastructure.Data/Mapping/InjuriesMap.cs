using Liga.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Liga.Infrastruture.Data.Mapping
{
    public class InjuriesMap : EntityTypeConfiguration<Injuries>
    {
        public InjuriesMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("Injuries");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IdPlayer).HasColumnName("IdPlayer");
            this.Property(t => t.IdType).HasColumnName("IdType");
            this.Property(t => t.GamesOut).HasColumnName("GamesOut");
            this.Property(t => t.GamesRest).HasColumnName("GamesRest");

            // Relationships
            this.HasRequired(t => t.InjuryType)
                .WithMany(t => t.Injuries)
                .HasForeignKey(d => d.IdType);
            this.HasRequired(t => t.Player)
                .WithMany(t => t.Injuries)
                .HasForeignKey(d => d.IdPlayer);
            this.HasRequired(t => t.Game)
              .WithMany(t => t.InjuriesInGame)
              .HasForeignKey(d => d.IdGame);

        }
    }
}
