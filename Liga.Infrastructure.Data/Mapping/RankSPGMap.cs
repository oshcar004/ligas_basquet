﻿using Liga.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Liga.Infrastruture.Data.Mapping
{
    public class RankSPGMap : EntityTypeConfiguration<RankSPG>
    {
        public RankSPGMap()
        {
            // Primary Key
            HasKey(t => t.Player);

            // Properties
            Property(t => t.Player)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.season)
                .HasMaxLength(30);

            // Table & Column Mappings
            ToTable("RankSPG");
            Property(t => t.POS).HasColumnName("POS");
            Property(t => t.Player).HasColumnName("Player");
            Property(t => t.season).HasColumnName("season");
            Property(t => t.SPG).HasColumnName("SPG");
            Property(t => t.CompleteName).HasColumnName("CompleteName");
            Property(t => t.Logo).HasColumnName("Logo");
            Property(t => t.FaceImage).HasColumnName("FaceImage");
        }
    }
}