﻿using Liga.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Liga.Infrastruture.Data.Mapping
{
    public class FreeAgentOffersMap : EntityTypeConfiguration<FreeAgentOffers>
    {
        public FreeAgentOffersMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("FreeAgentOffers");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IdPlayer).HasColumnName("IdPlayer");
            this.Property(t => t.Salary1).HasColumnName("Salary1");
            this.Property(t => t.Salary2).HasColumnName("Salary2");
            this.Property(t => t.Salary3).HasColumnName("Salary3");
            this.Property(t => t.Salary4).HasColumnName("Salary4");
            this.Property(t => t.Salary5).HasColumnName("Salary5");
            this.Property(t => t.Accepted).HasColumnName("Accepted");
            this.Property(t => t.Rejected).HasColumnName("Rejected");
            this.Property(t => t.IdFranchise).HasColumnName("IdFranchise");
            this.Property(t => t.OfferDate).HasColumnName("OfferDate");
            this.Property(t => t.IdCondition1).HasColumnName("IdCondition1");
            this.Property(t => t.IdCondition2).HasColumnName("IdCondition2");
            this.Property(t => t.IdCondition3).HasColumnName("IdCondition3");
            this.Property(t => t.IdCondition4).HasColumnName("IdCondition4");
            this.Property(t => t.IdCondition5).HasColumnName("IdCondition5");

            // Relationships
            this.HasRequired(t => t.LVNBAFranchis)
                .WithMany(t => t.FreeAgentOffers)
                .HasForeignKey(d => d.IdFranchise);
            this.HasRequired(t => t.Player)
                .WithMany(t => t.FreeAgentOffers)
                .HasForeignKey(d => d.IdPlayer);

        }
    }
}