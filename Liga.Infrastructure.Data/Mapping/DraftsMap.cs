using Liga.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Liga.Infrastruture.Data.Mapping
{
    public class DraftsMap : EntityTypeConfiguration<Drafts>
    {
        public DraftsMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("Drafts");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IdPlayer).HasColumnName("IdPlayer");
            this.Property(t => t.Year).HasColumnName("Year");
            this.Property(t => t.IdRound).HasColumnName("IdRound");
            this.Property(t => t.DraftDate).HasColumnName("DraftDate");
            this.Property(t => t.Position).HasColumnName("Position");

            // Relationships
            this.HasRequired(t => t.Player)
                .WithMany(t => t.Drafts)
                .HasForeignKey(d => d.IdPlayer);
            this.HasOptional(t => t.Round)
                .WithMany(t => t.Drafts)
                .HasForeignKey(d => d.IdRound);

        }
    }
}
