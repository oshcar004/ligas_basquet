﻿using Liga.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Liga.Infrastruture.Data.Mapping
{
    public class PlazosSemanalesMap : EntityTypeConfiguration<PlazosSemanales>
    {
        public PlazosSemanalesMap()
        {
            // Primary Key
            this.HasKey(t => t.Plazo);

            // Properties
            // Table & Column Mappings
            this.ToTable("PlazosSemanales");
            this.Property(t => t.Inicio).HasColumnName("Inicio");
            this.Property(t => t.Fin).HasColumnName("Fin");
        }
    }
}