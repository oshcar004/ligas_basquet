﻿using Liga.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Liga.Infrastruture.Data.Mapping
{
    public class PlayersInfoListMap : EntityTypeConfiguration<PlayersInfoList>
    {
        public PlayersInfoListMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Player, t.IdTeam, t.Team, t.Age, t.Position, t.YearsPro });

            // Properties
            this.Property(t => t.Player)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);


            this.Property(t => t.CompleteName)
                .HasMaxLength(252);


            this.Property(t => t.IdTeam)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Team)
                .IsRequired()
                .HasMaxLength(4);

            this.Property(t => t.Logo)
                .HasMaxLength(50);

            this.Property(t => t.Age)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Position)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.YearsPro)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);


            // Table & Column Mappings
            this.ToTable("PlayersInfoList");
            this.Property(t => t.Player).HasColumnName("Player");
            this.Property(t => t.CompleteName).HasColumnName("CompleteName");
            this.Property(t => t.Weight).HasColumnName("Weight");
            this.Property(t => t.Height).HasColumnName("Height");
            this.Property(t => t.IdTeam).HasColumnName("IdTeam");
            this.Property(t => t.Team).HasColumnName("Team");
            this.Property(t => t.Logo).HasColumnName("Logo");
            this.Property(t => t.Age).HasColumnName("Age");
            this.Property(t => t.Position).HasColumnName("Position");
            this.Property(t => t.YearsPro).HasColumnName("YearsPro");
            this.Property(t => t.Followed).HasColumnName("Followed");
            this.Property(t => t.ContractY).HasColumnName("ContractY");
            this.Property(t => t.SalaryTeam).HasColumnName("SalaryTeam");
            this.Property(t => t.Level2k).HasColumnName("Level2k");
            this.Property(t => t.RookieRights).HasColumnName("RookieRights");

        }
    }
}