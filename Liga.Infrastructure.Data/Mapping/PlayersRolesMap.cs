using Liga.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Liga.Infrastruture.Data.Mapping
{
    public class PlayersRolesMap : EntityTypeConfiguration<PlayersRoles>
    {
        public PlayersRolesMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Rol)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Description)
                .HasMaxLength(100);

            this.Property(t => t.Abrev)
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("PlayersRoles");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Rol).HasColumnName("Rol");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Abrev).HasColumnName("Abrev");
        }
    }
}
