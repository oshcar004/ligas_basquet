using Liga.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Liga.Infrastruture.Data.Mapping
{
    public class SalaryConditionsMap : EntityTypeConfiguration<SalaryConditions>
    {
        public SalaryConditionsMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Condition)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Abrev)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.Colour)
                .IsFixedLength()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("SalaryConditions");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Condition).HasColumnName("Condition");
            this.Property(t => t.Abrev).HasColumnName("Abrev");
            this.Property(t => t.Colour).HasColumnName("Colour");
        }
    }
}
