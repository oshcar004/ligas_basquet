﻿using Liga.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Liga.Infrastruture.Data.Mapping
{
    public class ValMediaLocalVisitanteRecibidaMap : EntityTypeConfiguration<ValMediaLocalVisitanteRecibida>
    {
        public ValMediaLocalVisitanteRecibidaMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.LocalOVisitante)
                .IsRequired()
                .HasMaxLength(9);

            // Table & Column Mappings
            this.ToTable("ValMediaLocalVisitanteRecibida");
            this.Property(t => t.Media).HasColumnName("Media");
            this.Property(t => t.Partidos).HasColumnName("Partidos");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.idManager).HasColumnName("idManager");
            this.Property(t => t.LocalOVisitante).HasColumnName("LocalOVisitante");

            // Relationships
            this.HasRequired(t => t.ManagerInfo)
                .WithMany(t => t.ValLVRecibida)
                .HasForeignKey(d => d.idManager);

        }
    }
}