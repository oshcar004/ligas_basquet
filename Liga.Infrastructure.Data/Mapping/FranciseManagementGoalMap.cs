﻿using Liga.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Liga.Infrastruture.Data.Mapping
{
    public class FranchiseManagementGoalMap : EntityTypeConfiguration<FranchiseManagementGoal>
    {
        public FranchiseManagementGoalMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("FranchiseManagementGoals");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IdGoal).HasColumnName("IdGoal");
            this.Property(t => t.IdFranchise).HasColumnName("IdFranchise");

            // Relationships
            this.HasRequired(t => t.Goal)
                .WithMany(t => t.FranchiseManagementGoals)
                .HasForeignKey(d => d.IdGoal);
            this.HasRequired(t => t.LVNBAFranchis)
                .WithMany(t => t.FranchiseManagementGoals)
                .HasForeignKey(d => d.IdFranchise);

        }
    }
    
}