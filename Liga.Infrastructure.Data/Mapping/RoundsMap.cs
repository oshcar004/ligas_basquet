using Liga.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Liga.Infrastruture.Data.Mapping
{
    public class RoundsMap : EntityTypeConfiguration<Rounds>
    {
        public RoundsMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("Rounds");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.Year).HasColumnName("Year");
            this.Property(t => t.IdFranchise).HasColumnName("IdFranchise");
            this.Property(t => t.Owner).HasColumnName("Owner");
            this.Property(t => t.IdManager).HasColumnName("IdManager");

            // Relationships
            this.HasRequired(t => t.LVNBAFranchis)
                .WithMany(t => t.Rounds)
                .HasForeignKey(d => d.IdFranchise);
            this.HasRequired(t => t.LVNBAFranchisProp)
                .WithMany(t => t.Rounds1)
                .HasForeignKey(d => d.Owner);
            this.HasOptional(t => t.Manager)
                .WithMany(t => t.Rounds)
                .HasForeignKey(d => d.IdManager);
        }
    }
}
