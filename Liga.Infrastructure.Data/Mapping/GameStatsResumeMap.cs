using Liga.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Liga.Infrastruture.Data.Mapping
{
    public class GameStatsResumeMap : EntityTypeConfiguration<GameStatsResume>
    {
        public GameStatsResumeMap()
        {
            // Primary Key
            this.HasKey(t => new { t.idPlayer, t.CompleteName, t.Tipo, t.idGame, t.idTeam, t.Abbreviation, t.Color });

            // Properties
            this.Property(t => t.idPlayer)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CompleteName)
                .IsRequired()
                .HasMaxLength(252);

            this.Property(t => t.Tipo)
                .IsRequired()
                .HasMaxLength(5);

            this.Property(t => t.idGame)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.idTeam)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Abbreviation)
                .IsRequired()
                .HasMaxLength(4);

            // Table & Column Mappings
            this.ToTable("GameStatsResume");
            this.Property(t => t.idPlayer).HasColumnName("idPlayer");
            this.Property(t => t.CompleteName).HasColumnName("CompleteName");
            this.Property(t => t.stat).HasColumnName("stat");
            this.Property(t => t.Tipo).HasColumnName("Tipo");
            this.Property(t => t.idGame).HasColumnName("idGame");
            this.Property(t => t.idTeam).HasColumnName("idTeam");
            this.Property(t => t.Abbreviation).HasColumnName("Abbreviation");
            this.Property(t => t.Color).HasColumnName("Color");
        }
    }
}
