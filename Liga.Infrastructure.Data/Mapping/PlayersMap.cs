using Liga.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Liga.Infrastruture.Data.Mapping
{
    public class PlayersMap : EntityTypeConfiguration<Players>
    {
        public PlayersMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(100);

            Property(t => t.NickName)
                .HasMaxLength(50);

            Property(t => t.FirstName)
                .HasMaxLength(150);

            Property(t => t.FaceImage)
                .HasMaxLength(1000);

            Property(t => t.CompleteName)
                .HasMaxLength(252);

            Property(t => t.AbrevName)
                .HasMaxLength(153);

            Property(t => t.ManagerAdvert)
                .HasMaxLength(200);

            // Table & Column Mappings
            ToTable("Players");
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.Name).HasColumnName("Name");
            Property(t => t.NickName).HasColumnName("NickName");
            Property(t => t.FirstName).HasColumnName("FirstName");
            Property(t => t.Height).HasColumnName("Height");
            Property(t => t.Weight).HasColumnName("Weight");
            Property(t => t.ActualTeam).HasColumnName("ActualTeam");
            Property(t => t.ManagerOpinion).HasColumnName("ManagerOpinion");
            Property(t => t.IdRol).HasColumnName("IdRol");
            Property(t => t.IdPosition).HasColumnName("IdPosition");
            Property(t => t.BornDate).HasColumnName("BornDate");
            Property(t => t.IdState).HasColumnName("IdState");
            Property(t => t.Cutted).HasColumnName("Cutted");
            Property(t => t.Injuried).HasColumnName("Injuried");
            Property(t => t.BirdRights).HasColumnName("BirdRights");
            Property(t => t.Retired).HasColumnName("Retired");
            Property(t => t.FaceImage).HasColumnName("FaceImage");
            Property(t => t.MainImage).HasColumnName("MainImage");
            Property(t => t.CompleteName).HasColumnName("CompleteName");
            Property(t => t.Age).HasColumnName("Age");
            Property(t => t.Injury).HasColumnName("Injury");
            Property(t => t.YearsPro).HasColumnName("YearsPro");
            Property(t => t.AbrevName).HasColumnName("AbrevName");
            Property(t => t.Level2k).HasColumnName("Level2k");
            Property(t => t.Followers).HasColumnName("Followers");
            Property(t => t.ManagerAdvert).HasColumnName("ManagerAdvert");
            Property(t => t.IdCollege).HasColumnName("IdCollege");
            Property(t => t.FromAgency).HasColumnName("FromAgency");

            // Relationships
            HasOptional(t => t.LVNBAFranchis)
                .WithMany(t => t.Players)
                .HasForeignKey(d => d.ActualTeam);
            HasOptional(t => t.PlayersState)
                .WithMany(t => t.Players)
                .HasForeignKey(d => d.IdState);
            HasOptional(t => t.PlayersPosition)
                .WithMany(t => t.Players)
                .HasForeignKey(d => d.IdPosition);
            HasOptional(t => t.PlayersRole)
                .WithMany(t => t.Players)
                .HasForeignKey(d => d.IdRol);
            HasOptional(t => t.PlayersCollege)
                .WithMany(t => t.Players)
                .HasForeignKey(d => d.IdCollege);
       
        }
    }
}
