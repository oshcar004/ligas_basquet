﻿using Liga.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Liga.Infrastruture.Data.Mapping
{

    public class ResumenLesionesMap : EntityTypeConfiguration<ResumenLesiones>
    {
        public ResumenLesionesMap()
        {
            // Primary Key
            this.HasKey(t => t.IdPlayer);

            // Properties
            // Table & Column Mappings
            this.ToTable("ResumenLesiones");
            this.Property(t => t.IdPlayer).HasColumnName("IdPlayer");
            this.Property(t => t.NamePlayer).HasColumnName("NamePlayer");
            this.Property(t => t.AbrevTeam).HasColumnName("AbrevTeam");
            this.Property(t => t.PartidosLesionado).HasColumnName("PartidosLesionado");
            this.Property(t => t.Lesiones).HasColumnName("Lesiones");


        }
    }
}