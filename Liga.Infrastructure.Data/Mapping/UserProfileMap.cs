using Liga.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Liga.Infrastruture.Data.Mapping
{
    public class UserProfileMap : EntityTypeConfiguration<UserProfile>
    {
        public UserProfileMap()
        {
            // Primary Key
            HasKey(t => t.UserId);

            // Table & Column Mappings
            ToTable("UserProfile");
            Property(t => t.UserId).HasColumnName("UserId");
            Property(t => t.UserName).HasColumnName("UserName");
            Property(t => t.Email).HasColumnName("Email");

        }
    }
}
