﻿using Liga.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Liga.Infrastruture.Data.Mapping
{
    public class ResumenQuejaMap : EntityTypeConfiguration<ResumenQueja>
    {
        public ResumenQuejaMap()
        {
            // Primary Key
            HasKey(t => t.id);

            // Properties
            Property(t => t.id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.SenderTeam)
                .HasMaxLength(30);

            Property(t => t.SenderIcono)
                .HasMaxLength(10);

            Property(t => t.SenderGamertag)
                .HasMaxLength(50);

            Property(t => t.SenderMotivo)
                .HasMaxLength(200);

            Property(t => t.ReceiverTeam)
                .HasMaxLength(30);

            Property(t => t.ReceiverIcono)
                .HasMaxLength(10);

            Property(t => t.ReceiverGamertag)
                .HasMaxLength(50);

            // Table & Column Mappings
            ToTable("ResumenQuejas");
            Property(t => t.id).HasColumnName("id");
            Property(t => t.GameId).HasColumnName("GameId");
            Property(t => t.SenderIdTeam).HasColumnName("SenderIdTeam");
            Property(t => t.SenderTeam).HasColumnName("SenderTeam");
            Property(t => t.SenderIcono).HasColumnName("SenderIcono");
            Property(t => t.SenderIdManager).HasColumnName("SenderIdManager");
            Property(t => t.SenderGamertag).HasColumnName("SenderGamertag");
            Property(t => t.SenderMotivo).HasColumnName("SenderMotivo");
            Property(t => t.ReceiverIdTeam).HasColumnName("ReceiverIdTeam");
            Property(t => t.ReceiverTeam).HasColumnName("ReceiverTeam");
            Property(t => t.ReceiverIcono).HasColumnName("ReceiverIcono");
            Property(t => t.ReceiverIdManager).HasColumnName("ReceiverIdManager");
            Property(t => t.ReceiverGamertag).HasColumnName("ReceiverGamertag");
            Property(t => t.gameDate).HasColumnName("gameDate");
            Property(t => t.date2k).HasColumnName("date2k");

            HasRequired(t => t.GameInfo)
            .WithMany(t => t.Quejas)
            .HasForeignKey(d => d.GameId);
        }
    }

}