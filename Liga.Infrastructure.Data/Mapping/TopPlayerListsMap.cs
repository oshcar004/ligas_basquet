using Liga.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Liga.Infrastruture.Data.Mapping
{
    public class TopPlayerListsMap : EntityTypeConfiguration<TopPlayerLists>
    {
        public TopPlayerListsMap()
        {
            // Primary Key
            this.HasKey(t => new { t.id, t.Tipo });

            // Properties
            this.Property(t => t.id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CompleteName)
                .HasMaxLength(252);

            this.Property(t => t.FaceImage)
                .HasMaxLength(50);

            this.Property(t => t.Logo)
                .HasMaxLength(50);

            this.Property(t => t.Tipo)
                .IsRequired()
                .HasMaxLength(3);

            // Table & Column Mappings
            this.ToTable("TopPlayerList");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.CompleteName).HasColumnName("CompleteName");
            this.Property(t => t.FaceImage).HasColumnName("FaceImage");
            this.Property(t => t.Logo).HasColumnName("Logo");
            this.Property(t => t.STAT).HasColumnName("STAT");
            this.Property(t => t.Tipo).HasColumnName("Tipo");
        }
    }
}
