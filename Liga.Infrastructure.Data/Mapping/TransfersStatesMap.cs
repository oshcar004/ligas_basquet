using Liga.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Liga.Infrastruture.Data.Mapping
{
    public class TransfersStatesMap : EntityTypeConfiguration<TransfersStates>
    {
        public TransfersStatesMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.TransferState)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("TransfersStates");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.TransferState).HasColumnName("TransferState");
        }
    }
}
