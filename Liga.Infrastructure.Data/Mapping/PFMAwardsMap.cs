using Liga.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Liga.Infrastruture.Data.Mapping
{
    public class PFMAwardsMap : EntityTypeConfiguration<PFMAwards>
    {
        public PFMAwardsMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("PFMAwards");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IdAward).HasColumnName("IdAward");
            this.Property(t => t.IdFranchise).HasColumnName("IdFranchise");
            this.Property(t => t.IdManager).HasColumnName("IdManager");
            this.Property(t => t.IdPlayer).HasColumnName("IdPlayer");
            this.Property(t => t.Info).HasColumnName("Info");
            this.Property(t => t.Year).HasColumnName("Year");
            this.Property(t => t.AwardDate).HasColumnName("AwardDate");
            this.Property(t => t.MWY).HasColumnName("MWY");
            // Relationships
            this.HasRequired(t => t.Award)
                .WithMany(t => t.PFMAwards)
                .HasForeignKey(d => d.IdAward);
            this.HasOptional(t => t.LVNBAFranchis)
                .WithMany(t => t.PFMAwards)
                .HasForeignKey(d => d.IdFranchise);
            this.HasOptional(t => t.Manager)
                .WithMany(t => t.PFMAwards)
                .HasForeignKey(d => d.IdManager);
            this.HasOptional(t => t.Player)
                .WithMany(t => t.PFMAwards)
                .HasForeignKey(d => d.IdPlayer);

        }
    }
}
