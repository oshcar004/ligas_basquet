using Liga.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Liga.Infrastruture.Data.Mapping
{
    public class TransfersMap : EntityTypeConfiguration<Transfers>
    {
        public TransfersMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("Transfers");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IdFranchise1).HasColumnName("IdFranchise1");
            this.Property(t => t.IdFranchise2).HasColumnName("IdFranchise2");
            this.Property(t => t.ResolutionDate).HasColumnName("ResolutionDate");
            this.Property(t => t.IdFranchise3).HasColumnName("IdFranchise3");
            this.Property(t => t.IdFranchise4).HasColumnName("IdFranchise4");
            this.Property(t => t.TransferDate).HasColumnName("TransferDate");
            this.Property(t => t.IdTransferState).HasColumnName("IdTransferState");
            this.Property(t => t.ReasonDeny).HasColumnName("ReasonDeny");
            this.Property(t => t.Comentario).HasColumnName("Comentario");
            this.Property(t => t.Confirmed1).HasColumnName("Confirmed1");
            this.Property(t => t.Confirmed2).HasColumnName("Confirmed2");
            this.Property(t => t.Confirmed3).HasColumnName("Confirmed3");
            this.Property(t => t.Confirmed4).HasColumnName("Confirmed4");
            this.Property(t => t.ManagerCreator).HasColumnName("UserCreator");

            // Relationships
            this.HasOptional(t => t.Manager)
                .WithMany(t => t.Trades)
                .HasForeignKey(d => d.ManagerCreator);
            this.HasRequired(t => t.LVNBAFranchis)
                .WithMany(t => t.Transfers)
                .HasForeignKey(d => d.IdFranchise1);
            this.HasRequired(t => t.LVNBAFranchis1)
                .WithMany(t => t.Transfers1)
                .HasForeignKey(d => d.IdFranchise2);
            this.HasOptional(t => t.LVNBAFranchis2)
                .WithMany(t => t.Transfers2)
                .HasForeignKey(d => d.IdFranchise3);
            this.HasOptional(t => t.LVNBAFranchis3)
                .WithMany(t => t.Transfers3)
                .HasForeignKey(d => d.IdFranchise4);
            this.HasRequired(t => t.TransfersState)
                .WithMany(t => t.Transfers)
                .HasForeignKey(d => d.IdTransferState);

        }
    }
}
