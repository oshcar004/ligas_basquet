using Liga.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Liga.Infrastruture.Data.Mapping
{
    public class LVNBAFranchisesMap : EntityTypeConfiguration<LVNBAFranchises>
    {
        public LVNBAFranchisesMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(30);

            Property(t => t.Abbreviation)
                .IsRequired()
                .HasMaxLength(4);

            Property(t => t.NickName)
                .IsRequired()
                .HasMaxLength(50);

            Property(t => t.Icono)
                .HasMaxLength(10);

            Property(t => t.Logo)
                .HasMaxLength(50);

            Property(t => t.BigLogo)
                .HasMaxLength(50);

            Property(t => t.StadiumBoard)
                .HasMaxLength(50);

            Property(t => t.SideBar)
                .HasMaxLength(50);

            Property(t => t.Color)
                .HasMaxLength(10);

            Property(t => t.Needs)
                .HasMaxLength(250);

            Property(t => t.ManagerOpinion)
                .HasMaxLength(350);

            // Table & Column Mappings
            ToTable("LVNBAFranchises");
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.IdDivision).HasColumnName("IdDivision");
            Property(t => t.Name).HasColumnName("Name");
            Property(t => t.IdExterno).HasColumnName("IdExterno");
            Property(t => t.Abbreviation).HasColumnName("Abbreviation");
            Property(t => t.NickName).HasColumnName("NickName");
            Property(t => t.IdManager).HasColumnName("IdManager");
            Property(t => t.Icono).HasColumnName("Icono");
            Property(t => t.Logo).HasColumnName("Logo");
            Property(t => t.BigLogo).HasColumnName("BigLogo");
            Property(t => t.StadiumBoard).HasColumnName("StadiumBoard");
            Property(t => t.SideBar).HasColumnName("SideBar");
            Property(t => t.Color).HasColumnName("Color");
            Property(t => t.History).HasColumnName("History");
            Property(t => t.Category).HasColumnName("Category");
            Property(t => t.TotalBonus).HasColumnName("TotalBonus");
            Property(t => t.Needs).HasColumnName("Needs");
            Property(t => t.ManagerOpinion).HasColumnName("ManagerOpinion");

            // Relationships
            HasRequired(t => t.Division)
                .WithMany(t => t.LVNBAFranchises)
                .HasForeignKey(d => d.IdDivision);
            HasOptional(t => t.Manager)
                .WithMany(t => t.LVNBAFranchises)
                .HasForeignKey(d => d.IdManager);

        }
    }
}
