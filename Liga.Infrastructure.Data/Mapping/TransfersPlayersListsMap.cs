using Liga.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Liga.Infrastruture.Data.Mapping
{
    public class TransfersPlayersListsMap : EntityTypeConfiguration<TransfersPlayersLists>
    {
        public TransfersPlayersListsMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("TransfersPlayersList");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IdPlayer).HasColumnName("IdPlayer");
            this.Property(t => t.IdTransfer).HasColumnName("IdTransfer");
            this.Property(t => t.Destination).HasColumnName("Destination");
            this.Property(t => t.IdFranchise).HasColumnName("IdFranchise");

            // Relationships
            this.HasRequired(t => t.LVNBAFranchis)
                .WithMany(t => t.TransfersPlayersLists)
                .HasForeignKey(d => d.Destination);
            this.HasRequired(t => t.LVNBAFranchis1)
                .WithMany(t => t.TransfersPlayersLists1)
                .HasForeignKey(d => d.IdFranchise);
            this.HasRequired(t => t.LVNBAFranchis2)
                .WithMany(t => t.TransfersPlayersLists2)
                .HasForeignKey(d => d.Destination);
            this.HasRequired(t => t.Player)
                .WithMany(t => t.TransfersPlayersLists)
                .HasForeignKey(d => d.IdPlayer);
            this.HasRequired(t => t.Transfer)
                .WithMany(t => t.TransfersPlayersLists)
                .HasForeignKey(d => d.IdTransfer);

        }
    }
}
