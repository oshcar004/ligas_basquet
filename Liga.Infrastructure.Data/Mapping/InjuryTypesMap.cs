using Liga.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Liga.Infrastruture.Data.Mapping
{
    public class InjuryTypesMap : EntityTypeConfiguration<InjuryTypes>
    {
        public InjuryTypesMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("InjuryType");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.MinDuration).HasColumnName("MinDuration");
            this.Property(t => t.MaxDuration).HasColumnName("MaxDuration");
        }
    }
}
