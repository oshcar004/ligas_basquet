﻿using Liga.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Liga.Infrastruture.Data.Mapping
{
    public class CollegesMap : EntityTypeConfiguration<Colleges>
    {
        public CollegesMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.College)
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("Colleges");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.College).HasColumnName("College");
        }
    }
}