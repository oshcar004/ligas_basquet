using Liga.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Liga.Infrastruture.Data.Mapping
{
    public class ParametersMap : EntityTypeConfiguration<Parameters>
    {
        public ParametersMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("Parameters");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.LimiteSalarial).HasColumnName("LimiteSalarial");
            this.Property(t => t.ImpuestoLujo).HasColumnName("ImpuestoLujo");
            this.Property(t => t.MidLevel).HasColumnName("MidLevel");
            this.Property(t => t.MiniMidLevel).HasColumnName("MiniMidLevel");
            this.Property(t => t.NivelTraspaso).HasColumnName("NivelTraspaso");
            this.Property(t => t.SuperTasaLujo).HasColumnName("SuperTasaLujo");
            this.Property(t => t.Temporada).HasColumnName("Temporada");
            this.Property(t => t.Pretemporada).HasColumnName("Pretemporada");
            this.Property(t => t.Postemporada).HasColumnName("Postemporada");
            this.Property(t => t.Agencia).HasColumnName("Agencia");
            this.Property(t => t.RegularSeason).HasColumnName("RegularSeason");
            this.Property(t => t.Playoffs).HasColumnName("Playoffs");
            this.Property(t => t.MercadoAbierto).HasColumnName("MercadoAbierto");
            this.Property(t => t.OffSeasonMaxTradesIn).HasColumnName("OffSeasonMaxTradesIn");
            this.Property(t => t.OffSeasonMaxTradesOut).HasColumnName("OffSeasonMaxTradesOut");
            this.Property(t => t.OffSeasonMaxTrades).HasColumnName("OffSeasonMaxTrades");
            this.Property(t => t.SeasonMaxTrades).HasColumnName("SeasonMaxTrades");
            this.Property(t => t.SeasonMaxTradesIn).HasColumnName("SeasonMaxTradesIn");
            this.Property(t => t.SeasonMaxTradesOut).HasColumnName("SeasonMaxTradesOut");
            this.Property(t => t.WithoutSummerMaxTrades).HasColumnName("WithoutSummerMaxTrades");
            this.Property(t => t.WithoutSummerMaxTradesIn).HasColumnName("WithoutSummerMaxTradesIn");
            this.Property(t => t.WithoutSummerMaxTradesOut).HasColumnName("WithoutSummerMaxTradesOut");
            this.Property(t => t.PlazoMin).HasColumnName("PlazoMin");
            this.Property(t => t.PlazoMax).HasColumnName("PlazoMax");
            this.Property(t => t.CifraNoSuperable).HasColumnName("CifraNoSuperable");
        }
    }
}
