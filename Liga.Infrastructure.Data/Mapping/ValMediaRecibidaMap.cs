﻿using Liga.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Liga.Infrastruture.Data.Mapping
{
    public class ValMediaRecibidaMap : EntityTypeConfiguration<ValMediaRecibida>
    {
        public ValMediaRecibidaMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.id)
                .IsRequired();
            
            // Table & Column Mappings
            this.ToTable("ValMediaRecibida");
            this.Property(t => t.Media).HasColumnName("Media");
            this.Property(t => t.idManager).HasColumnName("idManager");
            this.Property(t => t.id).HasColumnName("id");

            // Relationships
            this.HasRequired(t => t.ManagerInfo)
                .WithMany(t => t.ValRecibidas)
                .HasForeignKey(d => d.idManager);

        }
    }
}