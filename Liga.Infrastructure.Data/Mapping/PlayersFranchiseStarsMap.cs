﻿using Liga.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Liga.Infrastruture.Data.Mapping
{
    public class PlayersFranchiseStarMap : EntityTypeConfiguration<PlayersFranchisesStars>
    {
        public PlayersFranchiseStarMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("PlayersFranchisesStars");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Type).HasColumnName("Type");

            // Relationships
            this.HasRequired(t => t.Player)
                .WithOptional(t => t.PlayersFranchisesStar);

        }
    }

}