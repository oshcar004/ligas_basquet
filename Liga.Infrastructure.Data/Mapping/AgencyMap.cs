using Liga.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Liga.Infrastruture.Data.Mapping
{
    public class AgencyMap : EntityTypeConfiguration<Agency>
    {
        public AgencyMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.Id)
                .IsRequired();

            // Table & Column Mappings
            ToTable("Agencia");
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.IdPlayer).HasColumnName("IdPlayer");
            Property(t => t.Tipo).HasColumnName("Tipo");
            Property(t => t.Quit).HasColumnName("Quit");
            Property(t => t.Ejercida).HasColumnName("Ejercida");
        }
    }
}
