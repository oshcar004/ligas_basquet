using Liga.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Liga.Infrastruture.Data.Mapping
{
    public class PlayersStatsGamesMap : EntityTypeConfiguration<PlayersStatsGames>
    {
        public PlayersStatsGamesMap()
        {
            // Primary Key
            this.HasKey(t => new { t.idGame, t.idPlayer });

            // Properties
            this.Property(t => t.idGame)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.idPlayer)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("PlayersStatsGame");
            this.Property(t => t.idGame).HasColumnName("idGame");
            this.Property(t => t.idPlayer).HasColumnName("idPlayer");
            this.Property(t => t.idTeam).HasColumnName("idTeam");
            this.Property(t => t.starter).HasColumnName("starter");
            this.Property(t => t.segs).HasColumnName("segs");
            this.Property(t => t.pnts).HasColumnName("pnts");
            this.Property(t => t.rofs).HasColumnName("rofs");
            this.Property(t => t.rdefs).HasColumnName("rdefs");
            this.Property(t => t.rebs).HasColumnName("rebs");
            this.Property(t => t.asss).HasColumnName("asss");
            this.Property(t => t.blks).HasColumnName("blks");
            this.Property(t => t.stls).HasColumnName("stls");
            this.Property(t => t.trns).HasColumnName("trns");
            this.Property(t => t.fols).HasColumnName("fols");
            this.Property(t => t.fga).HasColumnName("fga");
            this.Property(t => t.fgm).HasColumnName("fgm");
            this.Property(t => t.f3a).HasColumnName("f3a");
            this.Property(t => t.f3m).HasColumnName("f3m");
            this.Property(t => t.fta).HasColumnName("fta");
            this.Property(t => t.ftm).HasColumnName("ftm");
            this.Property(t => t.plusminus).HasColumnName("plusminus");

            // Relationships
            this.HasRequired(t => t.Game)
                .WithMany(t => t.PlayersStatsGames)
                .HasForeignKey(d => d.idGame);
            this.HasRequired(t => t.LVNBAFranchis)
                .WithMany(t => t.PlayersStatsGames)
                .HasForeignKey(d => d.idTeam);
            this.HasRequired(t => t.Player)
                .WithMany(t => t.PlayersStatsGames)
                .HasForeignKey(d => d.idPlayer);

        }
    }
}
