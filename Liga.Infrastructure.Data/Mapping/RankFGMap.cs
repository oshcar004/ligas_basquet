﻿using Liga.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Liga.Infrastruture.Data.Mapping
{
    public class RankFGMap : EntityTypeConfiguration<RankFG>
    {
        public RankFGMap()
        {
            // Primary Key
            HasKey(t => t.Player);

            // Properties
            Property(t => t.Player)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.season)
                .HasMaxLength(30);

            // Table & Column Mappings
            ToTable("RankFG");
            Property(t => t.POS).HasColumnName("POS");
            Property(t => t.Player).HasColumnName("Player");
            Property(t => t.season).HasColumnName("season");
            Property(t => t.FG).HasColumnName("FG");
            Property(t => t.CompleteName).HasColumnName("CompleteName");
            Property(t => t.Logo).HasColumnName("Logo");
            Property(t => t.FaceImage).HasColumnName("FaceImage");
        }
    }
}