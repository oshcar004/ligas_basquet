﻿using Liga.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Liga.Infrastruture.Data.Mapping
{
    public class PlayerStatisticsRSPOByTeamMap : EntityTypeConfiguration<PlayerStatisticsRSPOByTeam>
    {
        public PlayerStatisticsRSPOByTeamMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Player, t.IdTeam, t.Team, t.Age, t.Position, t.YearsPro, t.MPG, t.FG, t.F3G, t.FT, t.OFR, t.DEF, t.RPG, t.APG, t.SPG, t.BPG, t.TOO, t.PF, t.PPG });

            // Properties
            this.Property(t => t.Player)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.FaceImage)
                .HasMaxLength(50);

            this.Property(t => t.CompleteName)
                .HasMaxLength(252);

            this.Property(t => t.Season)
                .HasMaxLength(30);

            this.Property(t => t.Tipo)
                .HasMaxLength(30);

            this.Property(t => t.IdTeam)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Team)
                .IsRequired()
                .HasMaxLength(4);

            this.Property(t => t.Logo)
                .HasMaxLength(50);

            this.Property(t => t.Age)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Position)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.YearsPro)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.MPG)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.FG)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.F3G)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.FT)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.OFR)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.DEF)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.RPG)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.APG)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.SPG)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.BPG)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.TOO)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.PF)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.PPG)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Salary)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Salary2)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("PlayerStatisticsRSPOByTeam");
            this.Property(t => t.Player).HasColumnName("Player");
            this.Property(t => t.FaceImage).HasColumnName("FaceImage");
            this.Property(t => t.CompleteName).HasColumnName("CompleteName");
            this.Property(t => t.Season).HasColumnName("Season");
            this.Property(t => t.Tipo).HasColumnName("Tipo");
            this.Property(t => t.G).HasColumnName("G");
            this.Property(t => t.GS).HasColumnName("GS");
            this.Property(t => t.IdTeam).HasColumnName("IdTeam");
            this.Property(t => t.Team).HasColumnName("Team");
            this.Property(t => t.Logo).HasColumnName("Logo");
            this.Property(t => t.Age).HasColumnName("Age");
            this.Property(t => t.Position).HasColumnName("Position");
            this.Property(t => t.YearsPro).HasColumnName("YearsPro");
            this.Property(t => t.MPG).HasColumnName("MPG");
            this.Property(t => t.FG).HasColumnName("FG");
            this.Property(t => t.F3G).HasColumnName("F3G");
            this.Property(t => t.FT).HasColumnName("FT");
            this.Property(t => t.OFR).HasColumnName("OFR");
            this.Property(t => t.DEF).HasColumnName("DEF");
            this.Property(t => t.RPG).HasColumnName("RPG");
            this.Property(t => t.APG).HasColumnName("APG");
            this.Property(t => t.SPG).HasColumnName("SPG");
            this.Property(t => t.BPG).HasColumnName("BPG");
            this.Property(t => t.TOO).HasColumnName("TOO");
            this.Property(t => t.PF).HasColumnName("PF");
            this.Property(t => t.PPG).HasColumnName("PPG");
            this.Property(t => t.Salary).HasColumnName("Salary");
            this.Property(t => t.Salary2).HasColumnName("Salary2");
        }
    }

}