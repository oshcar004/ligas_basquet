﻿using Liga.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Liga.Infrastruture.Data.Mapping
{
    public class LastGameMap : EntityTypeConfiguration<LastGame>
    {
        public LastGameMap()
        {
            // Primary Key
            this.HasKey(t => new { t.idPlayer, t.Opponent });

            // Properties
            this.Property(t => t.idPlayer)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Opponent)
                .IsRequired()
                .HasMaxLength(5);

            this.Property(t => t.Result)
                .HasMaxLength(65);

            this.Property(t => t.FG)
                .HasMaxLength(61);

            this.Property(t => t.C3P)
                .HasMaxLength(61);

            this.Property(t => t.FT)
                .HasMaxLength(61);

            // Table & Column Mappings
            this.ToTable("LastGames");
            this.Property(t => t.idPlayer).HasColumnName("idPlayer");
            this.Property(t => t.season).HasColumnName("season");
            this.Property(t => t.gameDate).HasColumnName("gameDate");
            this.Property(t => t.Opponent).HasColumnName("Opponent");
            this.Property(t => t.Result).HasColumnName("Result");
            this.Property(t => t.MIN).HasColumnName("MIN");
            this.Property(t => t.FG).HasColumnName("FG");
            this.Property(t => t.C3P).HasColumnName("3P");
            this.Property(t => t.FT).HasColumnName("FT");
            this.Property(t => t.rofs).HasColumnName("rofs");
            this.Property(t => t.rdefs).HasColumnName("rdefs");
            this.Property(t => t.rebs).HasColumnName("rebs");
            this.Property(t => t.asss).HasColumnName("asss");
            this.Property(t => t.stls).HasColumnName("stls");
            this.Property(t => t.blks).HasColumnName("blks");
            this.Property(t => t.trns).HasColumnName("trns");
            this.Property(t => t.fols).HasColumnName("fols");
            this.Property(t => t.pnts).HasColumnName("pnts");
        }
    }
}