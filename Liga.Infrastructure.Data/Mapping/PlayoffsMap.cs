﻿using Liga.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Liga.Infrastruture.Data.Mapping
{
    public class PlayoffsMap : EntityTypeConfiguration<Playoffs>
    {
        public PlayoffsMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("Playoffs");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IdTeam1).HasColumnName("IdTeam1");
            this.Property(t => t.IdTeam2).HasColumnName("IdTeam2");
            this.Property(t => t.PosTeam1).HasColumnName("PosTeam1");
            this.Property(t => t.PosTeam2).HasColumnName("PosTeam2");
            this.Property(t => t.WinsTeam1).HasColumnName("WinsTeam1");
            this.Property(t => t.WinsTeam2).HasColumnName("WinsTeam2");
            this.Property(t => t.Position).HasColumnName("Position");
            this.Property(t => t.Season).HasColumnName("Season");

            // Relationships
            this.HasRequired(t => t.LVNBAFranchisLocal)
                .WithMany(t => t.PlayoffsLocal)
                .HasForeignKey(d => d.IdTeam1);
            this.HasRequired(t => t.LVNBAFranchisAway)
                .WithMany(t => t.PlayoffsAway)
                .HasForeignKey(d => d.IdTeam2);

        }
    }
}