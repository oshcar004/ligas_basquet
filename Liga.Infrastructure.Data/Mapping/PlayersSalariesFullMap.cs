using Liga.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Liga.Infrastruture.Data.Mapping
{
    public class PlayersSalariesFullMap : EntityTypeConfiguration<PlayersSalariesFull>
    {
        public PlayersSalariesFullMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Table & Column Mappings
            ToTable("PlayersSalariesFull");
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.IdPlayer).HasColumnName("PlayerId");
            Property(t => t.IdCondition).HasColumnName("IdCondition");
            Property(t => t.IdLVNBAFranchises).HasColumnName("IdLVNBAFranchises");
            Property(t => t.Salarie).HasColumnName("Salarie");
            Property(t => t.DateSalarie).HasColumnName("DateSalarie");
            // Relationships
            
            // Relationships
            HasRequired(t => t.LVNBAFranchis)
                .WithMany(t => t.PlayersSalaries)
                .HasForeignKey(d => d.IdLVNBAFranchises);

            HasRequired(t => t.Player)
                .WithMany(t => t.PlayersSalariesFull)
                .HasForeignKey(d => d.IdPlayer);

            HasRequired(t => t.SalaryCondition)
                .WithMany(t => t.PlayersSalariesFull)
                .HasForeignKey(d => d.IdCondition);

        }
    }
}
