using Liga.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Liga.Infrastruture.Data.Mapping
{
    public class CommentsMap : EntityTypeConfiguration<Comments>
    {
        public CommentsMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.Comment1)
                .IsRequired();

            // Table & Column Mappings
            ToTable("Comments");
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.PostId).HasColumnName("PostId");
            Property(t => t.DateTime).HasColumnName("DateTime");
            Property(t => t.Comment1).HasColumnName("Comment");
            Property(t => t.UserId).HasColumnName("UserId");
        }
    }
}
