using Liga.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Liga.Infrastruture.Data.Mapping
{
    public class GamesMap : EntityTypeConfiguration<Games>
    {
        public GamesMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("Games");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IdTeamLocal).HasColumnName("IdTeamLocal");
            this.Property(t => t.IdTeamAway).HasColumnName("IdTeamAway");
            this.Property(t => t.idScoreLocal).HasColumnName("idScoreLocal");
            this.Property(t => t.idScoreAway).HasColumnName("idScoreAway");
            this.Property(t => t.date2k).HasColumnName("date2k");
            this.Property(t => t.gameDate).HasColumnName("gameDate");
            this.Property(t => t.idleague).HasColumnName("idleague");
            this.Property(t => t.season).HasColumnName("season");
            this.Property(t => t.regularseason).HasColumnName("regularseason");

            this.Property(t => t.state).HasColumnName("state");
            this.Property(t => t.aVal).HasColumnName("aVal");
            this.Property(t => t.lVal).HasColumnName("lVal");
            this.Property(t => t.aQueja).HasColumnName("aQueja");
            this.Property(t => t.lQueja).HasColumnName("lQueja");
            this.Property(t => t.aMotivo).HasColumnName("aMotivo");
            this.Property(t => t.lMotivo).HasColumnName("lMotivo");
            this.Property(t => t.errores).HasColumnName("errores");
            this.Property(t => t.lidManager).HasColumnName("lidManager");
            this.Property(t => t.aidManager).HasColumnName("aidManager");
            this.Property(t => t.lMVP).HasColumnName("lMVP");
            this.Property(t => t.aMVP).HasColumnName("aMVP");
            this.Property(t => t.cMVP).HasColumnName("mvpPlayer");
            this.Property(t => t.withoutStatsLocal).HasColumnName("withoutStatsLocal");
            this.Property(t => t.withoutStatsAway).HasColumnName("withoutStatsAway");

            // Relationships
            this.HasRequired(t => t.LVNBAFranchisLocal)
                .WithMany(t => t.Games)
                .HasForeignKey(d => d.IdTeamLocal);
            this.HasRequired(t => t.LVNBAFranchisAway)
                .WithMany(t => t.Games1)
                .HasForeignKey(d => d.IdTeamAway);


            this.HasOptional(t => t.cMVPPlayerInfo)
                .WithMany(t => t.PlayersMVPCPU)
                .HasForeignKey(d => d.cMVP);
            this.HasOptional(t => t.aMVPPlayerInfo)
                .WithMany(t => t.PlayersMVPAway)
                .HasForeignKey(d => d.aMVP);
            this.HasOptional(t => t.lMVPPlayerInfo)
                .WithMany(t => t.PlayersMVPLocal)
                .HasForeignKey(d => d.lMVP);

            this.HasOptional(t => t.ManagerAway)
                .WithMany(t => t.GamesAway)
                .HasForeignKey(d => d.aidManager);
            this.HasOptional(t => t.ManagerLocal)
                .WithMany(t => t.GamesLocal)
                .HasForeignKey(d => d.lidManager);


        }
    }
}
