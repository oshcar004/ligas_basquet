﻿using Liga.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Liga.Infrastruture.Data.Mapping
{
    public class LVNBAFranchiseParamsMap: EntityTypeConfiguration<LVNBAFranchisesParams>
    {
        public LVNBAFranchiseParamsMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable(" LVNBAFranchisesParams");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.MidLevelWasted).HasColumnName("MidLevelWasted");
            this.Property(t => t.OffSeasonTradedIn).HasColumnName("OffSeasonTradedIn");
            this.Property(t => t.OfSeasonTradedOut).HasColumnName("OfSeasonTradedOut");
            this.Property(t => t.SeasonTradedIn).HasColumnName("SeasonTradedIn");
            this.Property(t => t.SeasonTradedOut).HasColumnName("SeasonTradedOut");
            this.Property(t => t.SeasonTrades).HasColumnName("SeasonTrades");
            this.Property(t => t.OffSeasonTrades).HasColumnName("OffSeasonTrades");

            // Relationships
            this.HasRequired(t => t.LVNBAFranchis)
                .WithOptional(t => t.LVNBAFranchisesParam);

        }
    }
}