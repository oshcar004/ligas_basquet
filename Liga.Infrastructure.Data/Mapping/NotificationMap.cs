﻿using Liga.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Liga.Infrastruture.Data.Mapping
{
    public class NotificationMap : EntityTypeConfiguration<Notification>
    {
        public NotificationMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.title)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.notification1)
                .IsRequired();

            this.Property(t => t.sendDate)
                .IsRequired();

            this.Property(t => t.season)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("Notifications");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.idtype).HasColumnName("idtype");
            this.Property(t => t.title).HasColumnName("title");
            this.Property(t => t.notification1).HasColumnName("notification");
            this.Property(t => t.idReceiver).HasColumnName("idReceiver");
            this.Property(t => t.idSender).HasColumnName("idSender");
            this.Property(t => t.sendDate).HasColumnName("sendDate");
            this.Property(t => t.season).HasColumnName("season");

            // Relationships
            this.HasRequired(t => t.NotificationType)
                .WithMany(t => t.Notifications)
                .HasForeignKey(d => d.idtype);

        }
    }
}