using Liga.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Liga.Infrastruture.Data.Mapping
{
    public class ManagerRequestMap : EntityTypeConfiguration<ManagerRequests>
    {
        public ManagerRequestMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Gamertag)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Email)
                .HasMaxLength(50);

            this.Property(t => t.Availability)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("ManagerRequests");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Gamertag).HasColumnName("Gamertag");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.BornDate).HasColumnName("BornDate");
            this.Property(t => t.Question1).HasColumnName("Question1");
            this.Property(t => t.FavouriteTeam).HasColumnName("FavouriteTeam");
            this.Property(t => t.Availability).HasColumnName("Availability");
            this.Property(t => t.KnowUs).HasColumnName("KnowUs");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.RequestDate).HasColumnName("RequestDate");
            this.Property(t => t.UserId).HasColumnName("UserId");

            // Relationships
            this.HasOptional(t => t.LVNBAFranchis)
                .WithMany(t => t.ManagerRequests)
                .HasForeignKey(d => d.FavouriteTeam);

        }
    }
}
