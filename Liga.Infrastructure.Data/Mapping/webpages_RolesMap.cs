using Liga.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Liga.Infrastruture.Data.Mapping
{
    public class Webpages_RolesMap : EntityTypeConfiguration<Webpages_Roles>
    {
        public Webpages_RolesMap()
        {
            // Primary Key
            HasKey(t => t.RoleId);

            // Table & Column Mappings
            ToTable("webpages_Roles");
            Property(t => t.RoleId).HasColumnName("RoleId");
            Property(t => t.RoleName).HasColumnName("RoleName");

        }
    }
}
