using Liga.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Liga.Infrastruture.Data.Mapping
{
    public class BoxScorePlayersMap : EntityTypeConfiguration<BoxScorePlayers>
    {
        public BoxScorePlayersMap()
        {
            // Primary Key
            HasKey(t => new { t.id, t.idScoreLocal, t.idScoreAway, t.LocalStadium, t.AwayStadium, t.LocalSidebar, t.AwaySidebar, t.IdPlayer, t.CompleteName, t.AbrevName, t.ActualTeam, t.TIT,t.MINU, t.PTS, t.REBO, t.REB, t.REBT, t.ASSISTS, t.BLOCKS, t.STEALS, t.TURNOVERS, t.FOULTS, t.FGA, t.FGM, t.P3GA, t.P3GM, t.FTA, t.FTM, t.PLUSMINUS });
                //t.TLPTS, t.TLREBO, t.TLREB, t.TLREBT, t.TLASSISTS, t.TLBLOCKS, t.TLSTEALS, t.TLTURNOVERS, t.TLFOULTS, t.TLFGA, t.TLFGM, t.TLP3GA, t.TLP3GM, t.TLFTA, t.TLFTM, t.TAPTS, t.TAREBO, t.TAREB, t.TAREBT, t.TAASSISTS, t.TABLOCKS, t.TASTEALS, t.TATURNOVERS, t.TAFOULTS, t.TAFGA, t.TAFGM, t.TAP3GA, t.TAP3GM, t.TAFTA, t.TAFTM });

            // Properties
            Property(t => t.id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.idScoreLocal)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.idScoreAway)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.LocalName)
                .HasMaxLength(30);

            Property(t => t.AwayName)
                .HasMaxLength(30);

            Property(t => t.AbreLocal)
                .HasMaxLength(4);

            Property(t => t.AbreAway)
                .HasMaxLength(4);

            Property(t => t.LocalLogo)
                .HasMaxLength(50);

            Property(t => t.AwayLogo)
                .HasMaxLength(50);

            Property(t => t.LocalStadium)
                .IsRequired()
                .HasMaxLength(50);

            Property(t => t.AwayStadium)
                .IsRequired()
                .HasMaxLength(50);

            Property(t => t.LocalSidebar)
                .IsRequired()
                .HasMaxLength(50);

            Property(t => t.AwaySidebar)
                .IsRequired()
                .HasMaxLength(50);

            Property(t => t.LocalIcono)
                .HasMaxLength(10);

            Property(t => t.AwayIcono)
                .HasMaxLength(10);

            Property(t => t.IdPlayer)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.CompleteName)
                .IsRequired()
                .HasMaxLength(252);

            Property(t => t.AbrevName)
                .IsRequired()
                .HasMaxLength(153);

            Property(t => t.ActualTeam)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.TIT)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.MINU)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.PTS)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.REBO)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.REB)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.REBT)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.ASSISTS)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.BLOCKS)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.STEALS)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.TURNOVERS)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.FOULTS)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.FGA)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.FGM)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.P3GA)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.P3GM)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.FTA)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.FTM)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.PLUSMINUS)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            ToTable("BoxScorePlayers");
            Property(t => t.id).HasColumnName("id");
            Property(t => t.idScoreLocal).HasColumnName("idScoreLocal");
            Property(t => t.idScoreAway).HasColumnName("idScoreAway");
            Property(t => t.gameDate).HasColumnName("gameDate");
            Property(t => t.LocalName).HasColumnName("LocalName");
            Property(t => t.AwayName).HasColumnName("AwayName");
            Property(t => t.AbreLocal).HasColumnName("AbreLocal");
            Property(t => t.AbreAway).HasColumnName("AbreAway");
            Property(t => t.IdLocal).HasColumnName("IdLocal");
            Property(t => t.IdAway).HasColumnName("IdAway");
            Property(t => t.LocalLogo).HasColumnName("LocalLogo");
            Property(t => t.AwayLogo).HasColumnName("AwayLogo");
            Property(t => t.LocalStadium).HasColumnName("LocalStadium");
            Property(t => t.AwayStadium).HasColumnName("AwayStadium");
            Property(t => t.LocalSidebar).HasColumnName("LocalSidebar");
            Property(t => t.AwaySidebar).HasColumnName("AwaySidebar");
            Property(t => t.LocalIcono).HasColumnName("LocalIcono");
            Property(t => t.AwayIcono).HasColumnName("AwayIcono");
            Property(t => t.IdPlayer).HasColumnName("IdPlayer");
            Property(t => t.CompleteName).HasColumnName("CompleteName");
            Property(t => t.AbrevName).HasColumnName("AbrevName");
            Property(t => t.ActualTeam).HasColumnName("ActualTeam");
            Property(t => t.TIT).HasColumnName("TIT");
            Property(t => t.MINU).HasColumnName("MINU");
            Property(t => t.PTS).HasColumnName("PTS");
            Property(t => t.REBO).HasColumnName("REBO");
            Property(t => t.REB).HasColumnName("REB");
            Property(t => t.REBT).HasColumnName("REBT");
            Property(t => t.ASSISTS).HasColumnName("ASSISTS");
            Property(t => t.BLOCKS).HasColumnName("BLOCKS");
            Property(t => t.STEALS).HasColumnName("STEALS");
            Property(t => t.TURNOVERS).HasColumnName("TURNOVERS");
            Property(t => t.FOULTS).HasColumnName("FOULTS");
            Property(t => t.FGA).HasColumnName("FGA");
            Property(t => t.FGM).HasColumnName("FGM");
            Property(t => t.P3GA).HasColumnName("P3GA");
            Property(t => t.P3GM).HasColumnName("P3GM");
            Property(t => t.FTA).HasColumnName("FTA");
            Property(t => t.FTM).HasColumnName("FTM");
            Property(t => t.PLUSMINUS).HasColumnName("PLUSMINUS");
           
        }
    }
}
