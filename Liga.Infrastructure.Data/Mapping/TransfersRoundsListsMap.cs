using Liga.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Liga.Infrastruture.Data.Mapping
{
    public class TransfersRoundsListsMap : EntityTypeConfiguration<TransfersRoundsLists>
    {
        public TransfersRoundsListsMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("TransfersRoundsList");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IdRound).HasColumnName("IdRound");
            this.Property(t => t.IdTransfer).HasColumnName("IdTransfer");
            this.Property(t => t.IdFranchise).HasColumnName("IdFranchise");
            this.Property(t => t.Destination).HasColumnName("Destination");

            // Relationships
            this.HasRequired(t => t.LVNBAFranchis)
                .WithMany(t => t.TransfersRoundsLists)
                .HasForeignKey(d => d.IdFranchise);
            this.HasRequired(t => t.LVNBAFranchis1)
                .WithMany(t => t.TransfersRoundsLists1)
                .HasForeignKey(d => d.Destination);
            this.HasRequired(t => t.Round)
                .WithMany(t => t.TransfersRoundsLists)
                .HasForeignKey(d => d.IdRound);
            this.HasRequired(t => t.Transfer)
                .WithMany(t => t.TransfersRoundsLists)
                .HasForeignKey(d => d.IdTransfer);

        }
    }
}
