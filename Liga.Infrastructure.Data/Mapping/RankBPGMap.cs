﻿using Liga.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Liga.Infrastruture.Data.Mapping
{
    public class RankBPGMap : EntityTypeConfiguration<RankBPG>
    {
        public RankBPGMap()
        {
            // Primary Key
            this.HasKey(t => t.Player);

            // Properties
            this.Property(t => t.Player)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.season)
                .HasMaxLength(30);

            // Table & Column Mappings
            this.ToTable("RankBPG");
            this.Property(t => t.POS).HasColumnName("POS");
            this.Property(t => t.Player).HasColumnName("Player");
            this.Property(t => t.season).HasColumnName("season");
            this.Property(t => t.BPG).HasColumnName("BPG");
        }
    }
}