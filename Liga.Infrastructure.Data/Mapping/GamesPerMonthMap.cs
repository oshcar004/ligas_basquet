using Liga.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Liga.Infrastruture.Data.Mapping
{
    public class GamesPerMonthMap : EntityTypeConfiguration<GamesPerMonth>
    {
        public GamesPerMonthMap()
        {
            // Primary Key
            this.HasKey(t => new { t.gameMonth, t.season });

            // Properties
            this.Property(t => t.gameMonth)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.season)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("GamesPerMonth");
            this.Property(t => t.gameMonth).HasColumnName("gameMonth");
            this.Property(t => t.season).HasColumnName("season");
            this.Property(t => t.gameTotal).HasColumnName("gameTotal");
        }
    }
}
