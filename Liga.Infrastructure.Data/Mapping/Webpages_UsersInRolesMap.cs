using Liga.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Liga.Infrastruture.Data.Mapping
{
    public class Webpages_UsersInRolesMap : EntityTypeConfiguration<Webpages_UsersInRoles>
    {
        public Webpages_UsersInRolesMap()
        {
            // Table & Column Mappings
            ToTable("Webpages_UsersInRoles");
            Property(t => t.UserId).HasColumnName("UserId");
            Property(t => t.RoleId).HasColumnName("RoleId");
        }
    }
}
