﻿using Liga.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Liga.Infrastruture.Data.Mapping
{
    public class StatsPlaeyerMap : EntityTypeConfiguration<StatsPlayer>
    {
        public StatsPlaeyerMap()
        {
            // Primary Key
            HasKey(t => t.PlayerId);

            // Properties
            Property(t => t.PlayerId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);


            // Table & Column Mappings
            ToTable("StatsPlayer");
            Property(t => t.PlayerId).HasColumnName("Player");
            Property(t => t.Season).HasColumnName("season");
            Property(t => t.APG).HasColumnName("APG");
            Property(t => t.PPG).HasColumnName("PPG");
            Property(t => t.BPG).HasColumnName("BPG");
            Property(t => t.FG).HasColumnName("FG");
            Property(t => t.MPG).HasColumnName("MPG");
            Property(t => t.RPG).HasColumnName("RPG");
            Property(t => t.SPG).HasColumnName("SPG");

        }
    }
}