using Liga.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Liga.Infrastruture.Data.Mapping
{
    public class TeamStandingsMap : EntityTypeConfiguration<TeamStandings>
    {
        public TeamStandingsMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Streak)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("TeamStanding");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Wins).HasColumnName("Wins");
            this.Property(t => t.Losses).HasColumnName("Losses");
            this.Property(t => t.Games).HasColumnName("Games");
            this.Property(t => t.Blocks).HasColumnName("Blocks");
            this.Property(t => t.Rebounds).HasColumnName("Rebounds");
            this.Property(t => t.Steals).HasColumnName("Steals");
            this.Property(t => t.PointsF).HasColumnName("PointsF");
            this.Property(t => t.PointsA).HasColumnName("PointsA");
            this.Property(t => t.GamesHome).HasColumnName("GamesHome");
            this.Property(t => t.GamesAway).HasColumnName("GamesAway");
            this.Property(t => t.WinsHome).HasColumnName("WinsHome");
            this.Property(t => t.LossHome).HasColumnName("LossHome");
            this.Property(t => t.WinsAway).HasColumnName("WinsAway");
            this.Property(t => t.LossAway).HasColumnName("LossAway");
            this.Property(t => t.Streak).HasColumnName("Streak");
            this.Property(t => t.Assists).HasColumnName("Assists");
            this.Property(t => t.ThreePointAttemptes).HasColumnName("ThreePointAttemptes");
            this.Property(t => t.ThreePointScored).HasColumnName("ThreePointScored");
            this.Property(t => t.FieldGoalAttempted).HasColumnName("FieldGoalAttempted");
            this.Property(t => t.FieldGoalScored).HasColumnName("FieldGoalScored");
            this.Property(t => t.FreeThrowAttempted).HasColumnName("FreeThrowAttempted");
            this.Property(t => t.FreeThrowScored).HasColumnName("FreeThrowScored");

            // Relationships
            this.HasRequired(t => t.LVNBAFranchis)
                .WithOptional(t => t.TeamStanding);

        }
    }
}
