using Liga.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Liga.Infrastruture.Data.Mapping
{
    public class ConferencesMap : EntityTypeConfiguration<Conferences>
    {
        public ConferencesMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.Conferencia)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            ToTable("Conferences");
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.Conferencia).HasColumnName("Conferencia");
        }
    }
}
