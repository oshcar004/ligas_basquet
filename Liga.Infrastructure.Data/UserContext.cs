﻿using System.Data.Entity;

namespace Liga.Infrastructure.Data
{
    public class UsersContext : DbContext
    {
        public UsersContext()
            : base("DefaultConnection")
        {
        }
    }
}
