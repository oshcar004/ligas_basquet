using Liga.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Liga.Infrastruture.Data.Mapping
{
    public class webpages_RolesMap : EntityTypeConfiguration<Webpages_Roles>
    {
        public webpages_RolesMap()
        {
            // Primary Key
            HasKey(t => t.RoleId);

            // Table & Column Mappings
            ToTable("webpages_Roles");
            Property(t => t.RoleId).HasColumnName("RoleId");
            Property(t => t.RoleName).HasColumnName("RoleName");

        }
    }
}
