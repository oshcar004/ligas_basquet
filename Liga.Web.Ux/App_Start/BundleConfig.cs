﻿using System.Web.Optimization;

namespace Liga.Web.UI
{
    public class BundleConfig
    {
        // Para obtener más información acerca de Bundling, consulte http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/jquery").Include(
            "~/Scripts/jquery-{version}.js",
            "~/Scripts/jquery-ui-1.10.3/jquery-1.9.1.js", "~/Scripts/jquery-ui-1.10.3/ui/jquery-ui.js",
            "~/Scripts/jquery-ui-1.10.3/ui/i18n/jquery-ui-i18n.js",
            "~/Scripts/autonumeric/autoNumeric.js",
            "~/Scripts/jquery.textfill.min.js",
            "~/Scripts/LVNBAFunctions.js"));

            bundles.Add(new ScriptBundle("~/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            // Utilice la versión de desarrollo de Modernizr para desarrollar y obtener información sobre los formularios. De este modo, estará
            // preparado para la producción y podrá utilizar la herramienta de creación disponible
            // en http://modernizr.com para seleccionar solo las pruebas que necesite.
            bundles.Add(new ScriptBundle("~/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/jqGrid").Include(
            "~/Scripts/jquery.jqGrid.min.js",
            "~/Scripts/i18n/grid.locale-es.js"
        ));
        }
    }
}