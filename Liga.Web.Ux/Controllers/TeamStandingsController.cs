﻿
using Liga.Domain.Entities;
using Liga.Infrastructure.Data;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;

namespace Liga.Web.UI.Controllers
{
    public class TeamStandingsController : Controller
    {
        //
        // GET: /TeamStandings/
        private Liga_DBContext db = new Liga_DBContext();

        public ActionResult Index(int? idSeason)
        {

            IQueryable<Clasificacion> listaTeams;

            if (idSeason == null)
                idSeason = db.Parameters.First().Temporada;

            string query = "ClasificacionesProc @Season ";
            SqlParameter SeasonId = new SqlParameter("@Season", idSeason);


            object[] SqlArr = new object[1];
            SqlArr[0] = SeasonId;

            listaTeams = db.Database.SqlQuery<Clasificacion>(query, SqlArr).ToList().AsQueryable();

            foreach (Clasificacion team in listaTeams)
            {
                team.TeamInfo = db.LVNBAFranchises.Where(x => x.Id == team.id).First();
            }


            List<Clasificacion> lAtlantic = new List<Clasificacion>();
            List<Clasificacion> lCentral = new List<Clasificacion>();
            List<Clasificacion> lSouthEast = new List<Clasificacion>();
            List<Clasificacion> lNorthwest = new List<Clasificacion>();
            List<Clasificacion> lPacific = new List<Clasificacion>();
            List<Clasificacion> lSouthWest = new List<Clasificacion>();




            lAtlantic = listaTeams.Where(x => x.TeamInfo.IdDivision == 1).OrderByDescending(x => x.Pct).ToList();
            lCentral = listaTeams.Where(x => x.TeamInfo.IdDivision == 2).OrderByDescending(x => x.Pct).ToList();
            lSouthEast = listaTeams.Where(x => x.TeamInfo.IdDivision == 3).OrderByDescending(x => x.Pct).ToList();
            lNorthwest = listaTeams.Where(x => x.TeamInfo.IdDivision == 4).OrderByDescending(x => x.Pct).ToList();
            lPacific = listaTeams.Where(x => x.TeamInfo.IdDivision == 5).OrderByDescending(x => x.Pct).ToList();
            lSouthWest = listaTeams.Where(x => x.TeamInfo.IdDivision == 6).OrderByDescending(x => x.Pct).ToList();

            List<List<Clasificacion>> listaClasificaciones = new List<List<Clasificacion>>();
            listaClasificaciones.Add(lAtlantic);
            listaClasificaciones.Add(lCentral);
            listaClasificaciones.Add(lSouthEast);
            listaClasificaciones.Add(lNorthwest);
            listaClasificaciones.Add(lPacific);
            listaClasificaciones.Add(lSouthWest);

            ViewBag.idSeason = idSeason;
            ViewBag.Seasons = db.Games.Where(x => x.Id > 0).Select(x => x.season).Distinct();
            return View(listaClasificaciones.AsEnumerable());
        }


    }
}
