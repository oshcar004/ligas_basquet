﻿
using Liga.Domain.Entities;
using Liga.Infrastructure.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Liga.Web.UI.Controllers
{
    public class ValoracionesController : BaseController
    {
        //
        // GET: /Valoraciones/
        private Liga_DBContext db = new Liga_DBContext();

        [Authorize(Roles = "Administrador, Comisionado")]
        public ActionResult Index()
        {
            
            IEnumerable<ValMediaLocalVisitanteOtorgada> listLVOtorgadas = db.ValMediaLocalVisitanteOtorgadas.AsEnumerable();
            IEnumerable<ValMediaLocalVisitanteRecibida> listLVRecibidas = db.ValMediaLocalVisitanteRecibidas.AsEnumerable();
            IEnumerable<ValMediaOtorgada> listOtorgadas = db.ValMediaOtorgadas.AsEnumerable();
            IEnumerable<ValMediaRecibida> listRecibidas = db.ValMediaRecibidas.AsEnumerable();
            IEnumerable<Valoraciones> allValoraciones = db.Valoraciones.AsEnumerable();

            ValoracionReport valoraciones = new ValoracionReport();

            valoraciones.ListaValoracionesRecibidas = listLVRecibidas;
            valoraciones.ListaValoracionesOtorgadas = listLVOtorgadas;
            valoraciones.MediasOtorgadas = listOtorgadas;
            valoraciones.MediasRecibidas = listRecibidas;
            valoraciones.Valoraciones = allValoraciones;


            return View(valoraciones);


        }

        [Authorize(Roles = "Administrador, Comisionado")]
        public ActionResult History()
        {

            IEnumerable<ValMediaLocalVisitanteOtorgada> listLVOtorgadas = db.ValMediaLocalVisitanteOtorgadas.AsEnumerable();
            IEnumerable<ValMediaLocalVisitanteRecibida> listLVRecibidas = db.ValMediaLocalVisitanteRecibidas.AsEnumerable();
            IEnumerable<ValMediaOtorgada> listOtorgadas = db.ValMediaOtorgadas.AsEnumerable();
            IEnumerable<ValMediaRecibida> listRecibidas = db.ValMediaRecibidas.AsEnumerable();
            IEnumerable<Valoraciones> allValoraciones = db.Valoraciones.AsEnumerable();

            ValoracionReport valoraciones = new ValoracionReport();

            valoraciones.ListaValoracionesRecibidas = listLVRecibidas;
            valoraciones.ListaValoracionesOtorgadas = listLVOtorgadas;
            valoraciones.MediasOtorgadas = listOtorgadas;
            valoraciones.MediasRecibidas = listRecibidas;
            valoraciones.Valoraciones = allValoraciones;


            return View(valoraciones);


        }

        [Authorize(Roles = "Administrador, Comisionado")]
        public ActionResult Season()
        {

            IEnumerable<ValMediaLocalVisitanteOtorgada> listLVOtorgadas = db.ValMediaLocalVisitanteOtorgadas.AsEnumerable();
            IEnumerable<ValMediaLocalVisitanteRecibida> listLVRecibidas = db.ValMediaLocalVisitanteRecibidas.AsEnumerable();
            IEnumerable<ValMediaOtorgada> listOtorgadas = db.ValMediaOtorgadas.AsEnumerable();
            IEnumerable<ValMediaRecibida> listRecibidas = db.ValMediaRecibidas.AsEnumerable();
            IEnumerable<Valoraciones> allValoraciones = db.Valoraciones.AsEnumerable();

            ValoracionReport valoraciones = new ValoracionReport();

            valoraciones.ListaValoracionesRecibidas = listLVRecibidas;
            valoraciones.ListaValoracionesOtorgadas = listLVOtorgadas;
            valoraciones.MediasOtorgadas = listOtorgadas;
            valoraciones.MediasRecibidas = listRecibidas;
            valoraciones.Valoraciones = allValoraciones;


            return View(valoraciones);


        }
    }
}
