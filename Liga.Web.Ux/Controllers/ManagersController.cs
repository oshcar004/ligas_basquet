﻿
using Liga.Domain.Entities;
using Liga.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;

namespace Liga.Web.UI.Controllers
{
    [Authorize(Roles = "Administrador")]
    public class ManagersController : BaseController
    {
        private Liga_DBContext db = new Liga_DBContext();

        //
        // GET: /Managers/

        public ActionResult Index()
        {
            var managers = db.Managers;
            return View(managers.ToList());
        }

        //
        // GET: /Managers/Details/5

        public ActionResult Details(int id = 0)
        {
            Managers managers = db.Managers.Find(id);
            if (managers == null)
            {
                return HttpNotFound();
            }
            return View(managers);
        }

        //
        // GET: /Managers/Create

        public ActionResult Create()
        {
            ViewBag.UserId = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /Managers/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Managers managers)
        {
            if (ModelState.IsValid)
            {
                db.Managers.Add(managers);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserId = new SelectList(db.UserProfiles, "UserId", "UserName", managers.UserId);
            return View(managers);
        }

        //
        // GET: /Managers/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Managers managers = db.Managers.Find(id);
            if (managers == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserId = new SelectList(db.UserProfiles, "UserId", "UserName", managers.UserId);
            return View(managers);
        }

        //
        // POST: /Managers/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Managers managers)
        {
            if (ModelState.IsValid)
            {
                db.Entry(managers).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserId = new SelectList(db.UserProfiles, "UserId", "UserName", managers.UserId);
            return View(managers);
        }

        //
        // GET: /Managers/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Managers managers = db.Managers.Find(id);
            if (managers == null)
            {
                return HttpNotFound();
            }
            return View(managers);
        }

        //
        // POST: /Managers/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Managers managers = db.Managers.Find(id);
            LVNBAFranchises franchises = db.LVNBAFranchises.Where(x => x.IdManager == id).FirstOrDefault();
            if(franchises != null)
            {
                franchises.IdManager = null;
                db.Entry(franchises).State = EntityState.Modified;
                db.SaveChanges();
            }
            IList<PFMAwards> awards = db.PFMAwards.Where(x => x.IdManager == id).ToList();
            db.PFMAwards.RemoveRange(awards);
            db.SaveChanges();
            db.Managers.Remove(managers);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }


        [Authorize(Roles = "Administrador")]
        public ActionResult AssignManagerUser()
        {


            ViewBag.IdManager = new SelectList(db.Managers.ToList().OrderBy(x => x.GamerTag), "Id", "GamerTag");

            List<UserProfile> listaUsers = db.UserProfiles.ToList();
            List<UserProfile> listaUsersNoAsociados = new List<UserProfile>();
            List<ManagersWithUsername> listaManagers = new List<ManagersWithUsername>();

            foreach (UserProfile iUser in listaUsers)
            {   // si el usuario no esta en ningun manager
                if (db.Managers.Where(x => x.UserId == iUser.UserId).FirstOrDefault() == null)
                {
                    listaUsersNoAsociados.Add(iUser);
                }
            }

            ViewBag.UserId = new SelectList(listaUsersNoAsociados.ToList(), "UserId", "UserName");
            foreach (Managers man in db.Managers)
            {
                ManagersWithUsername manUser = new ManagersWithUsername();
                manUser.Id = man.Id;
                manUser.UserId = man.UserId;
                manUser.Gamertag = man.GamerTag;
                if (man.UserId == null)
                {
                    manUser.Username = "";
                }
                else
                {
                    manUser.Username = db.UserProfiles.Where(x => x.UserId == man.UserId).First().UserName;
                }
                listaManagers.Add(manUser);

            }
            return View(listaManagers.OrderBy(x => x.Gamertag).ToList());
        }



        [HttpPost]
        [Authorize(Roles = "Administrador")]
        public ActionResult AssignManagerUser(FormCollection col)
        {
            string idManager = col[1];
            string idUser = col[2];
            List<UserProfile> listaUsers = db.UserProfiles.ToList();
            List<UserProfile> listaUsersNoAsociados = new List<UserProfile>();
            List<ManagersWithUsername> listaManagers = new List<ManagersWithUsername>();

            try
            {

                if (idManager == "")
                {
                    ModelState.AddModelError("", "Debe seleccionar un manager");
                    ViewBag.IdManager = new SelectList(db.Managers.ToList().OrderBy(x => x.GamerTag), "Id", "GamerTag");

                    foreach (UserProfile iUser in listaUsers)
                    {   // si el usuario no esta en ningun manager
                        if (db.Managers.Where(x => x.UserId == iUser.UserId).FirstOrDefault() == null)
                        {
                            listaUsersNoAsociados.Add(iUser);
                        }
                    }

                    ViewBag.UserId = new SelectList(listaUsersNoAsociados.ToList(), "UserId", "UserName");
                    foreach (Managers man in db.Managers)
                    {
                        ManagersWithUsername manUser = new ManagersWithUsername();
                        manUser.Id = man.Id;
                        manUser.UserId = man.UserId;
                        manUser.Gamertag = man.GamerTag;
                        if (man.UserId == null)
                        {
                            manUser.Username = "";
                        }
                        else
                        {
                            manUser.Username = db.UserProfiles.Where(x => x.UserId == man.UserId).First().UserName;
                        }
                        listaManagers.Add(manUser);

                    }
                    return View(listaManagers.OrderBy(x => x.Gamertag).ToList());



                }

                Managers manager = db.Managers.Find(Convert.ToInt32(idManager));
                if (manager != null)
                {
                    int? oldUser = manager.UserId;
                    
                    if (idUser == "")
                    {
                         manager.UserId = null;
                    }
                    else
                    {
                        manager.UserId = Convert.ToInt32(idUser.ToString());

                        int iUser = Convert.ToInt32(idUser);
                        string sUsername = db.UserProfiles.Where(x => x.UserId == iUser).First().UserName;
                        //hay que asignarle al nuevo usuario el rol de manager
                        Roles.AddUserToRole(sUsername, "Manager");
                     
                    }
                    if (oldUser != null)
                    {


                        int ioldUser = Convert.ToInt32(oldUser);
                        string sUsername = db.UserProfiles.Where(x => x.UserId == ioldUser).First().UserName;
                        Roles.RemoveUserFromRole(sUsername, "Manager");
                    }

                    db.SaveChanges();
                }


                ViewBag.IdManager = new SelectList(db.Managers.ToList().OrderBy(x => x.GamerTag), "Id", "GamerTag");


                foreach (UserProfile iUser in listaUsers)
                {   // si el usuario no esta en ningun manager
                    if (db.Managers.Where(x => x.UserId == iUser.UserId).FirstOrDefault() == null)
                    {
                        listaUsersNoAsociados.Add(iUser);
                    }
                }

                ViewBag.UserId = new SelectList(listaUsersNoAsociados.ToList(), "UserId", "UserName");
                foreach (Managers man in db.Managers)
                {
                    ManagersWithUsername manUser = new ManagersWithUsername();
                    manUser.Id = man.Id;
                    manUser.UserId = man.UserId;
                    manUser.Gamertag = man.GamerTag;
                    if (man.UserId == null)
                    {
                        manUser.Username = "";
                    }
                    else
                    {
                        manUser.Username = db.UserProfiles.Where(x => x.UserId == man.UserId).First().UserName;
                    }
                    listaManagers.Add(manUser);

                }
                return View(listaManagers.OrderBy(x => x.Gamertag).ToList());


            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "No se han podido guardar los cambios" + ex.ToString());
                ViewBag.IdManager = new SelectList(db.Managers.ToList().OrderBy(x => x.GamerTag), "Id", "GamerTag");

                foreach (UserProfile iUser in listaUsers)
                {   // si el usuario no esta en ningun manager
                    if (db.Managers.Where(x => x.UserId == iUser.UserId).FirstOrDefault() == null)
                    {
                        listaUsersNoAsociados.Add(iUser);
                    }
                }

                ViewBag.UserId = new SelectList(listaUsersNoAsociados.ToList(), "UserId", "UserName");
                foreach (Managers man in db.Managers)
                {
                    ManagersWithUsername manUser = new ManagersWithUsername();
                    manUser.Id = man.Id;
                    manUser.UserId = man.UserId;
                    manUser.Gamertag = man.GamerTag;
                    if (man.UserId == null)
                    {
                        manUser.Username = "";
                    }
                    else
                    {
                        manUser.Username = db.UserProfiles.Where(x => x.UserId == man.UserId).First().UserName;
                    }
                    listaManagers.Add(manUser);

                }
                return View(listaManagers.OrderBy(x => x.Gamertag).ToList());

            }
        }

        [HttpPost]
        public ActionResult GetUserByManager(int idManager)
        {

            if (db.Managers.Where(x => x.Id == idManager).FirstOrDefault() == null)
                return Json("");
            else
            {
                int? idUser = db.Managers.Where(x => x.Id == idManager).FirstOrDefault().UserId;
                if (idUser != null)
                {
                    return Json(db.UserProfiles.Where(x => x.UserId == idUser).FirstOrDefault().UserName.ToString());
                }
                else
                {
                    return Json("");
                }
            }
        }

        #region ManagerRequest
        [AllowAnonymous]
        public ActionResult ManagersRequest()
        {

            ViewData["question1"] = 1;
            ViewData["knowUs"] = 1;

            ViewBag.FavouriteTeam = new SelectList(db.LVNBAFranchises, "Id", "Name");

            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult ManagersRequest(ManagerRequests request)
        {
            if (ModelState.IsValid)
            {
                if (request.Gamertag == null || request.Email == null)
                {
                    ModelState.AddModelError("", "El Gamertag o la dirección de correo es obligatoria");
                    ViewBag.FavouriteTeam = new SelectList(db.LVNBAFranchises, "Id", "Name");
                    return View();
                }
                request.State = 0;
                request.RequestDate = DateTime.Now;
                string username = System.Threading.Thread.CurrentPrincipal.Identity.Name.ToString();
                request.UserId = db.UserProfiles.Where(x => x.UserName == username).FirstOrDefault().UserId;
                db.ManagerRequests.Add(request);
                db.SaveChanges();

                //enviar correo al usuario, y enviar correo a la comisión
                return RedirectToAction("Thanks");
            }
            ViewBag.FavouriteTeam = new SelectList(db.LVNBAFranchises, "Id", "Name");

            return View();

        }

        [Authorize(Roles = "Administrador,Comisionado")]
        public PartialViewResult ManagersRequestList()
        {
            var model = db.ManagerRequests.Where(x => x.State == 0).OrderByDescending(x => x.RequestDate).AsEnumerable();
            return PartialView("ManagersRequestList", model);

        }

        [AllowAnonymous]
        public ActionResult Thanks()
        {
            return View();
        }

        public ActionResult DetailsRequest(int id)
        {
            ManagerRequests request = db.ManagerRequests.Where(x => x.Id == id).First();
            return View(request);
        }


        [Authorize(Roles = "Administrador, Comisionado")]
        public ActionResult DeleteRequest(int id)
        {
            ManagerRequests request = db.ManagerRequests.Find(id);
            return View(request);
        }

        //
        [Authorize(Roles = "Administrador, Comisionado")]
        [HttpPost, ActionName("DeleteRequest")]
        public ActionResult DeleteRequestConfirmed(int id)
        {
            ManagerRequests request = db.ManagerRequests.Where(d => d.Id == id).First();
            db.ManagerRequests.Remove(request);
            db.SaveChanges();
            return RedirectToAction("ManagersRequestList");
        }

        [Authorize(Roles = "Administrador, Comisionado")]
        public ActionResult AcceptRequest(int id)
        {


            ManagerRequests request = db.ManagerRequests.Find(id);
            return View(request);
        }

        //
        [Authorize(Roles = "Administrador, Comisionado")]
        [HttpPost, ActionName("AcceptRequest")]
        public ActionResult AcceptRequestConfirmed(int id)
        {
            ManagerRequests request = db.ManagerRequests.Where(d => d.Id == id).First();
            UserProfile user = db.UserProfiles.Where(x => x.UserId == request.UserId).FirstOrDefault();
            if (user != null)
            {
                request.State = 1;
                Managers manager = new Managers
                {
                    GamerTag = request.Gamertag,
                    UserId = request.UserId,
                    BornDate = request.BornDate,
                    LVNBAAge = 0
                };
                db.Managers.Add(manager);
                if (!Roles.IsUserInRole(user.UserName, "Manager"))
                {
                    Roles.AddUserToRole(user.UserName, "Manager");
                }
                db.SaveChanges();
                return RedirectToAction("ManagersRequestList", "Managers", null);
            }

            return View();

        }




        [HttpPost]
        public ActionResult GetManagerCard(int idManager)
        {
            ManagerCardMO managerCard = new ManagerCardMO();
            managerCard.Manager = db.Managers.Where(x => x.Id == idManager).First();
            managerCard.ManagerAwards = db.PFMAwards.Where(x => x.IdManager == idManager).AsEnumerable();
            managerCard.ManagerRounds = db.Rounds.Where(x => x.IdManager == idManager).AsEnumerable();
            managerCard.ManagerTrayectoria = db.ManagersFranchises.Where(x => x.IdManager == idManager).AsEnumerable();

            return PartialView("ManagerCard", managerCard);
        }
        #endregion
    }
}