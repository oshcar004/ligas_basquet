﻿
using Liga.Domain.Entities;
using Liga.Infrastructure.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace Liga.Web.UI.Controllers
{
    [Authorize(Roles = "Administrador")]
    public class PlayerStatesController : BaseController
    {
        private Liga_DBContext db = new Liga_DBContext();

        //
        // GET: /PlayerStates/

        public ActionResult Index()
        {
            return View(db.PlayersStates.ToList());
        }

        //
        // GET: /PlayerStates/Details/5

        public ActionResult Details(int id = 0)
        {
            PlayersStates playersstates = db.PlayersStates.Find(id);
            if (playersstates == null)
            {
                return HttpNotFound();
            }
            return View(playersstates);
        }

        //
        // GET: /PlayerStates/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /PlayerStates/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PlayersStates playersstates)
        {
            if (ModelState.IsValid)
            {
                db.PlayersStates.Add(playersstates);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(playersstates);
        }

        //
        // GET: /PlayerStates/Edit/5

        public ActionResult Edit(int id = 0)
        {
            PlayersStates playersstates = db.PlayersStates.Find(id);
            if (playersstates == null)
            {
                return HttpNotFound();
            }
            return View(playersstates);
        }

        //
        // POST: /PlayerStates/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PlayersStates playersstates)
        {
            if (ModelState.IsValid)
            {
                db.Entry(playersstates).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(playersstates);
        }

        //
        // GET: /PlayerStates/Delete/5

        public ActionResult Delete(int id = 0)
        {
            PlayersStates playersstates = db.PlayersStates.Find(id);
            if (playersstates == null)
            {
                return HttpNotFound();
            }
            return View(playersstates);
        }

        //
        // POST: /PlayerStates/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PlayersStates playersstates = db.PlayersStates.Find(id);
            db.PlayersStates.Remove(playersstates);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}