﻿using Liga.Domain.Entities;
using Liga.Infrastructure.Data;
using Liga.Infrastructure.Data.Repository;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace Liga.Web.UI.Controllers
{
    public class QuejasController : BaseController
    {
        private Liga_DBContext db = new Liga_DBContext();

        //
        // GET: /Quejas/

        public ActionResult Index()
        {
            var model = db.ResumenQuejas.ToList();
            return View(model);
        }
        public ActionResult SendedDate()
        {
            IEnumerable<ResumenQueja> model = db.ResumenQuejas.OrderByDescending(x=>x.gameDate).Take(20).AsEnumerable();
            return View(model);
        }


        [Authorize(Roles = "Administrador")]
        public ActionResult SendWarning(int idManager = 0, string fromPage = "")
        {
            Managers manager =   db.Managers.Where(x => x.Id == idManager).First();
            ViewBag.FromPage = fromPage;

            if (manager == null)
            {
                return HttpNotFound();
            }
            return View(manager);
        }

        [HttpPost]
        public ActionResult SendWarning(Managers m, FormCollection r)
        {
            string sError = "";
            var mensaje = r.GetValues("mensaje");
            var fromPage = r.GetValues("fromPage");

            Managers manager = db.Managers.Find(m.Id);
            if (manager == null)
            {
                return HttpNotFound();
            }

            if (mensaje == null || mensaje[0].ToString() == "")
            {
                ModelState.AddModelError("", "El mensaje es obligatorio.");
                return View(m);
            }
            else
            {

                Notifications Notif = new Notifications();

                if (Notif.SendNotification(null, manager.Id, GlobalConstants.Ct_Notifications.NotificationsTypes.ManagerWarning, "", mensaje[0].ToString(), out sError))
                {
                    if (manager.Warnings == null)
                        manager.Warnings = 1;
                    else
                        manager.Warnings = manager.Warnings + 1;

                    db.Entry(manager).State = EntityState.Modified;

                    db.SaveChanges();



                    if (fromPage[0].ToString() == "Index")
                    {
                        IEnumerable<ResumenQueja> model = db.ResumenQuejas.AsEnumerable();
                        return View("Index", model);
                    }
                    else
                    {
                        IEnumerable<ResumenQueja> model = db.ResumenQuejas.OrderByDescending(x => x.gameDate).Take(20).AsEnumerable();
                        return View("SendedDate", model);
                    }

                }
                else
                {
                    ModelState.AddModelError("", "No se ha podido notificar ni contabilizar el aviso debido al siguiente error: " + sError );
                    return View(manager);
                }
               

          
            }
        }

    }


}
