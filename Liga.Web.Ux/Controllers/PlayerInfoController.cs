﻿
using Liga.Domain.Entities;
using Liga.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Liga.Web.UI.Controllers
{
    public class PlayerInfoController : BaseController
    {
        //
        // GET: /PlayerInfo/
        private Liga_DBContext db = new Liga_DBContext();
        
        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Retired()
        {
            var query = db.Players.Where(x=>x.Retired == true).GroupBy(player => player.FirstName.Substring(0, 1))
      .Select(group =>
            new AlphabeticalMapping<Players>
            {
                FirstLetter = group.Key,
                Items = group.OrderBy(x => x.FirstName)
            })
      .OrderBy(group => group.Items.FirstOrDefault().FirstName);



            return View(query);
        }

        [HttpGet]

       

        public ActionResult GetPlayerInfoSinModal(int idPlayer)
        {
            //Players player = db.Players.Where(x => x.Id == idPlayer).First();
            //return PartialView("PlayersInfo", player);
            string Season = db.Parameters.First().Temporada.ToString();
            ViewBag.Season = Convert.ToInt32(Season);
            return View("PlayersInfoModal", getPlayerCompleteInfo(idPlayer));
        }

        [AllowAnonymous]
        public ActionResult IndexAllPlayers()
        {
            var query = db.Players.Where(x => x.Retired == false).GroupBy(player => player.FirstName.Substring(0, 1))
                  .Select(group =>
                        new AlphabeticalMapping<Players>
                        {
                            FirstLetter = group.Key,
                            Items = group.OrderBy(x => x.FirstName)
                        })
                  .OrderBy(group => group.Items.FirstOrDefault().FirstName);



            return View(query);
        }

        [AllowAnonymous]
        public ActionResult GetPlayerInfoNoModal(int idPlayer)
        {
            return PartialView("PlayerInfoNoModal", getPlayerCompleteInfo(idPlayer));
        }

        public ActionResult GetUsagesPercent(int? playerID)
        {
            int iSeason = db.Parameters.First().Temporada;
            PlayerUsage pUsage = db.PlayerUsages.Where(x => x.Id == playerID && x.season == iSeason).First();

            int[] percents = new int[] { 0, Convert.ToInt32(pUsage.PNTS), 0, Convert.ToInt32(pUsage.FGA), 0, Convert.ToInt32(pUsage.F3A), 0, Convert.ToInt32(pUsage.FTA), 0, Convert.ToInt32(pUsage.REBS), 0, Convert.ToInt32(pUsage.ASS), 0, Convert.ToInt32(pUsage.TRNS), 0, Convert.ToInt32(pUsage.STLS), 0, Convert.ToInt32(pUsage.BLKS), 0 };
            return Json(percents, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetShootTendence(int? playerID)
        {
            PlayerStatisticsBySeason pStatistic = db.PlayerStatisticsBySeasons.Where(x => x.Player == playerID).First();

            int FG = Convert.ToInt32(pStatistic.FG);
            int F3 = Convert.ToInt32(pStatistic.F3G);
            int FT = Convert.ToInt32(pStatistic.FT);

            int[] percents = new int[] { FG, F3, FT };
            return Json(percents, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AdvancedStats(int idPlayer)
        {
            
           
            return PartialView("AdvStatsPlayers", getPlayerCompleteInfo(idPlayer));
        }

        private PlayerCompleteInfo getPlayerCompleteInfo(int idPlayer)
        {
            string Season = db.Parameters.First().Temporada.ToString();
            int iSeason = Convert.ToInt32 (Season);
            PlayerCompleteInfo completeInfoPlayer = new PlayerCompleteInfo();

            PosRanksPlayer pRankPlayer = new PosRanksPlayer();
            pRankPlayer.PosMPG = db.RankMPGs.Where(x => x.season == Season.ToString() && x.Player == idPlayer).FirstOrDefault();
            pRankPlayer.PosPPG = db.RankPPGs.Where(x => x.Season == Season && x.Player == idPlayer).FirstOrDefault();
            pRankPlayer.PosRPG = db.RankRPGs.Where(x => x.season == Season.ToString() && x.Player == idPlayer).FirstOrDefault();
            pRankPlayer.PosAPG = db.RankAPGs.Where(x => x.season == Season && x.Player == idPlayer).FirstOrDefault();
            pRankPlayer.PosSPG = db.RankSPGs.Where(x => x.season == Season.ToString() && x.Player == idPlayer).FirstOrDefault();
            pRankPlayer.PosBPG = db.RankBPGs.Where(x => x.season == Season.ToString() && x.Player == idPlayer).FirstOrDefault();
            if (db.PlayerStatisticsBySeasons.Where(x => x.Player == idPlayer) == null)
            {
                pRankPlayer.PlayerStat = new PlayerStatisticsBySeason();
            }
            else
            {
                pRankPlayer.PlayerStat = db.PlayerStatisticsBySeasons.Where(x => x.Player == idPlayer).FirstOrDefault();
            }
            completeInfoPlayer.Player = db.Players.Where(x => x.Id == idPlayer).First();
            completeInfoPlayer.PlayerStatThisRS = db.PlayerStatisticsBySeasons.Where(x => x.Player == idPlayer && x.Season == Season.ToString()).FirstOrDefault();
            completeInfoPlayer.PlayerStatThisPlayoffs = db.PlayerStatisticsByPofs.Where(x => x.Player == idPlayer && x.Season == Season.ToString()).FirstOrDefault();

            completeInfoPlayer.PlayerSalary = db.PlayersSalariesFull.Where(x => x.IdPlayer == idPlayer).FirstOrDefault();
            completeInfoPlayer.PlayerHistories = db.PlayersHistories.Where(x => x.IdPlayer == idPlayer).OrderByDescending(x => x.StartDate);
            completeInfoPlayer.PlayerAwards = db.PFMAwards.Where(x => x.IdPlayer == idPlayer).OrderByDescending(x => x.Year);
            completeInfoPlayer.PlayersGames = db.LastGames.Where(x => x.idPlayer == idPlayer && x.season == iSeason).OrderByDescending(x => x.gameDate);
            completeInfoPlayer.PositionRank = pRankPlayer;

            return completeInfoPlayer;
        }

        public ActionResult StatsBySeason(int idPlayer)
        {
            IEnumerable<PlayerStatisticsRSPO> player = db.PlayerStatisticsRSPOs.Where(x => x.Player == idPlayer).OrderByDescending(x=>x.Season).ThenByDescending(x => x.Tipo).AsEnumerable();
            return PartialView("StatsBySeason", player);

        }
        public ActionResult StatsByTeam(int idPlayer)
        {
            IEnumerable<PlayerStatisticsRSPOByTeam> player = db.PlayerStatisticsRSPOByTeams.Where(x => x.Player == idPlayer).OrderByDescending(x => x.Season).ThenByDescending(x => x.Tipo).AsEnumerable();
            return PartialView("StatsByTeam", player);

        }
        public ActionResult Last3Games(int idPlayer)
        {
            int Season =Convert.ToInt32( db.Parameters.First().Temporada.ToString());

            IEnumerable<LastGame> stats = db.LastGames.Where(x => x.idPlayer == idPlayer && x.season == Season).OrderByDescending(x => x.gameDate).Take(3).AsEnumerable();
            return PartialView("Last3Games", stats);

        }

        [HttpPost]
        public ActionResult GetPlayerCard(int idPlayer)
        {
            Players player = db.Players.Where(x => x.Id == idPlayer).First();

            //Games game = db.Games.Where(x => x.Id == idGame).First();

            return PartialView("MinPlayerCardForStatus", player);
        }
    }


}
