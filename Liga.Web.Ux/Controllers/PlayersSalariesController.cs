﻿using Liga.Domain.Entities;
using Liga.Infrastructure.Data;
using Liga.Infrastructure.Data.Repository;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Serialization;
using System.Web.Mvc;

namespace Liga.Web.UI.Controllers
{
    [Authorize(Roles = "Administrador,Comisionado")]
    public class PlayersSalariesController : BaseController
    {
        private Liga_DBContext db = new Liga_DBContext();
        private DateTime Today = DateTime.Today;

        //
        // GET: /PlayersSalaries/

        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = string.IsNullOrEmpty(sortOrder) ? "Name_desc" : "";
            ViewBag.TeamSortParm = sortOrder == "Team" ? "Team_desc" : "Team";
            ViewBag.Today = db.Parameters.First().Temporada;
            ViewBag.Today1 = db.Parameters.First().Temporada+1;
            ViewBag.Today2 = db.Parameters.First().Temporada+2;
            ViewBag.Today3 = db.Parameters.First().Temporada+3;
            ViewBag.Today4 = db.Parameters.First().Temporada+4;
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;

            var players = db.Players.Include(p => p.PlayersSalariesFull).Where(p => !p.Retired);
            if (!string.IsNullOrEmpty(searchString))
            {
                players = players.Where(s => s.CompleteName.ToUpper().Contains(searchString.ToUpper())
                                       || s.LVNBAFranchis.Name.ToUpper().Contains(searchString.ToUpper()));
            }
            switch (sortOrder)
            {
                case "Name_desc":
                    players = players.OrderByDescending(s => s.CompleteName);
                    break;
                case "Team":
                    players = players.OrderBy(s => s.LVNBAFranchis.Name);
                    break;
                case "Team_desc":
                    players = players.OrderByDescending(s => s.LVNBAFranchis.Name);
                    break;
                default:
                    players = players.OrderBy(s => s.CompleteName);
                    break;
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);

            return View(players.ToPagedList(pageNumber, pageSize));
        }

        public ActionResult IndexOld(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = string.IsNullOrEmpty(sortOrder) ? "Name_desc" : "";
            ViewBag.TeamSortParm = sortOrder == "Team" ? "Team_desc" : "Team";
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;

            var playerssalaries = db.PlayersSalariesFull.Include(p => p.Player).Include(p => p.SalaryCondition);

            if (!string.IsNullOrEmpty(searchString))
            {
                playerssalaries = playerssalaries.Where(s => s.Player.CompleteName.ToUpper().Contains(searchString.ToUpper())
                                       || s.LVNBAFranchis.Name.ToUpper().Contains(searchString.ToUpper()));
            }
            switch (sortOrder)
            {
                case "Name_desc":
                    playerssalaries = playerssalaries.OrderByDescending(s => s.Player.CompleteName);
                    break;
                case "Team":
                    playerssalaries = playerssalaries.OrderBy(s => s.LVNBAFranchis.Name);
                    break;
                case "Team_desc":
                    playerssalaries = playerssalaries.OrderByDescending(s => s.LVNBAFranchis.Name);
                    break;
                default:
                    playerssalaries = playerssalaries.OrderBy(s => s.Player.CompleteName);
                    break;
            }
            int pageSize = 30;
            int pageNumber = (page ?? 1);

            return View(playerssalaries.ToPagedList(pageNumber, pageSize));
        }

        //
        // GET: /PlayersSalaries/Details/5

        public ActionResult Details(int id = 0)
        {
            var playerssalaries = db.PlayersSalariesFull.Find(id);
            if (playerssalaries == null)
            {
                return HttpNotFound();
            }
            return View(playerssalaries);
        }

        //
        // GET: /PlayersSalaries/Create

        public ActionResult Create()
        {
            ViewBag.IdLVNBAFranchises = new SelectList(db.LVNBAFranchises.OrderBy(x => x.Name), "Id", "Name");
            ViewBag.Id = new SelectList(db.Players.OrderBy(x=>x.CompleteName), "Id", "CompleteName");
            return View();
        }

        //
        // POST: /PlayersSalaries/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PlayersSalariesFull playerssalaries)
        {
            try
            {
                ViewBag.Id = new SelectList(db.Players.OrderBy(x => x.CompleteName), "Id", "CompleteName", playerssalaries.Id);
                ViewBag.IdLVNBAFranchises = new SelectList(db.LVNBAFranchises.OrderBy(x => x.Name), "Id", "Name",playerssalaries.IdLVNBAFranchises);

                if (ModelState.IsValid)
                {
                    string sMessage;

                    if (db.PlayersSalariesFull.Find(playerssalaries.Id) != null)
                    {
                        sMessage = "Este jugador ya tiene una ficha de salarios";
                        ModelState.AddModelError("", sMessage);
                        return View(playerssalaries); 
                    }
                    sMessage = ValidaCondicionSinSalario(playerssalaries);
                    if (sMessage != "")
                    {
                        ModelState.AddModelError("", sMessage);
                        return View(playerssalaries);
                    }
                    db.PlayersSalariesFull.Add(playerssalaries);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException ex)
            {

                ModelState.AddModelError("", "No se ha podido guardar: " + ex.InnerException.ToString());
            }


                return View(playerssalaries);
        }

        //
        // GET: /PlayersSalaries/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Players player = db.Players.Find(id);
            if (player == null)
            {
                return HttpNotFound();
            }
            PlayerSalariesModel playerSalariesModel = new PlayerSalariesModel();
            int condition1, condition2, condition3, condition4, condition5 = 0;
            decimal salarie1, salarie2, salarie3, salarie4, salarie5;
            Today = new DateTime(db.Parameters.First().Temporada, 01, 01);
            PlayersSalariesFull playersSalariesFull1 = player.PlayersSalariesFull.ToList().Find(x => (x.IdPlayer == player.Id) && (x.DateSalarie.Year == Today.Year));
            PlayersSalariesFull playersSalariesFull2 = player.PlayersSalariesFull.ToList().Find(x => (x.IdPlayer == player.Id) && (x.DateSalarie.Year == Today.AddYears(1).Year));
            PlayersSalariesFull playersSalariesFull3 = player.PlayersSalariesFull.ToList().Find(x => (x.IdPlayer == player.Id) && (x.DateSalarie.Year == Today.AddYears(2).Year));
            PlayersSalariesFull playersSalariesFull4 = player.PlayersSalariesFull.ToList().Find(x => (x.IdPlayer == player.Id) && (x.DateSalarie.Year == Today.AddYears(3).Year));
            PlayersSalariesFull playersSalariesFull5 = player.PlayersSalariesFull.ToList().Find(x => (x.IdPlayer == player.Id) && (x.DateSalarie.Year == Today.AddYears(4).Year));

            if (playersSalariesFull1 != null)
            {
                condition1 = playersSalariesFull1.IdCondition;
                salarie1 = playersSalariesFull1.Salarie;
            }
            else
            {
                condition1 = 6;
                salarie1 = 0;
            }
            if (playersSalariesFull2 != null)
            {
                condition2 = playersSalariesFull2.IdCondition;
                salarie2 = playersSalariesFull2.Salarie;
            }
            else
            {
                condition2 = 6;
                salarie2 = 0;
            }
            if (playersSalariesFull3 != null)
            {
                condition3 = playersSalariesFull3.IdCondition;
                salarie3 = playersSalariesFull3.Salarie;
            }
            else
            {
                condition3 = 6;
                salarie3 = 0;
            }
            if (playersSalariesFull4 != null)
            {
                condition4 = playersSalariesFull4.IdCondition;
                salarie4 = playersSalariesFull4.Salarie;
            }
            else
            {
                condition4 = 6;
                salarie4 = 0;
            }
            if (playersSalariesFull5 != null)
            {
                condition5 = playersSalariesFull5.IdCondition;
                salarie5 = playersSalariesFull5.Salarie;
            }
            else
            {
                condition5 = 6;
                salarie5 = 0;
            }

            playerSalariesModel.IdCondition1 = condition1;
            playerSalariesModel.IdCondition2 = condition2;
            playerSalariesModel.IdCondition3 = condition3;
            playerSalariesModel.IdCondition4 = condition4;
            playerSalariesModel.IdCondition5 = condition5;

            playerSalariesModel.Salarie1 = salarie1;
            playerSalariesModel.Salarie2 = salarie2;
            playerSalariesModel.Salarie3 = salarie3;
            playerSalariesModel.Salarie4 = salarie4;
            playerSalariesModel.Salarie5 = salarie5;

            playerSalariesModel.Today = Today.Year;

            playerSalariesModel.ActualTeam = ((player.ActualTeam != null) ? (int)player.ActualTeam : -1);
            playerSalariesModel.CompleteName = player.CompleteName;

            ViewBag.Id = new SelectList(db.Players.OrderBy(x => x.CompleteName), "Id", "CompleteName", player.Id);
            ViewBag.IdCondition1 = new SelectList(db.SalaryConditions, "Id", "Condition", condition1);
            ViewBag.IdCondition2 = new SelectList(db.SalaryConditions, "Id", "Condition", condition2);
            ViewBag.IdCondition3 = new SelectList(db.SalaryConditions, "Id", "Condition", condition3);
            ViewBag.IdCondition4 = new SelectList(db.SalaryConditions, "Id", "Condition", condition4);
            ViewBag.IdCondition5 = new SelectList(db.SalaryConditions, "Id", "Condition", condition5);
            ViewBag.ActualTeam = new SelectList(db.LVNBAFranchises.OrderBy(x => x.Name), "Id", "Name", player.ActualTeam);
            //return View(player);
            return View(playerSalariesModel);
        }

        public ActionResult EditOld(int id = 0)
        {
            var playerssalaries = db.PlayersSalariesFull.Find(id);
            if (playerssalaries == null)
            {
                return HttpNotFound();
            }
            ViewBag.Id = new SelectList(db.Players.OrderBy(x => x.CompleteName), "Id", "CompleteName", playerssalaries.Id);
            ViewBag.IdLVNBAFranchises = new SelectList(db.LVNBAFranchises.OrderBy(x => x.Name), "Id", "Name", playerssalaries.IdLVNBAFranchises);

            return View(playerssalaries);
        }

        //
        // POST: /PlayersSalaries/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PlayerSalariesModel playerSalariesModel)
        {
            try
            {
                Players player_update = db.Players.Find(playerSalariesModel.Id);
                Today = new DateTime(db.Parameters.First().Temporada, 01, 01);
                if (player_update == null)
                {
                    return HttpNotFound();
                }
                PlayersSalariesFull playersSalariesFull1 = player_update.PlayersSalariesFull.ToList().Find(x => (x.IdPlayer == player_update.Id) && (x.DateSalarie.Year == Today.Year));
                PlayersSalariesFull playersSalariesFull2 = player_update.PlayersSalariesFull.ToList().Find(x => (x.IdPlayer == player_update.Id) && (x.DateSalarie.Year == Today.AddYears(1).Year));
                PlayersSalariesFull playersSalariesFull3 = player_update.PlayersSalariesFull.ToList().Find(x => (x.IdPlayer == player_update.Id) && (x.DateSalarie.Year == Today.AddYears(2).Year));
                PlayersSalariesFull playersSalariesFull4 = player_update.PlayersSalariesFull.ToList().Find(x => (x.IdPlayer == player_update.Id) && (x.DateSalarie.Year == Today.AddYears(3).Year));
                PlayersSalariesFull playersSalariesFull5 = player_update.PlayersSalariesFull.ToList().Find(x => (x.IdPlayer == player_update.Id) && (x.DateSalarie.Year == Today.AddYears(4).Year));

                if (player_update.ActualTeam != playerSalariesModel.ActualTeam && TryUpdateModel(player_update, "", new string[] { "ActualTeam" }))
                {
                    db.SaveChanges();
                }
                if (playersSalariesFull1 != null)
                {
                    if (playersSalariesFull1.Salarie != playerSalariesModel.Salarie1 || playersSalariesFull1.IdCondition != playerSalariesModel.IdCondition1 || playersSalariesFull1.IdLVNBAFranchises != playerSalariesModel.ActualTeam)
                    {
                        playersSalariesFull1.Salarie = playerSalariesModel.Salarie1;
                        playersSalariesFull1.IdCondition = playerSalariesModel.IdCondition1;
                        playersSalariesFull1.IdLVNBAFranchises = playerSalariesModel.ActualTeam;
                        if (TryUpdateModel(playersSalariesFull1, "", new string[] { "Salarie", "IdCondition", "IdLVNBAFranchises" }))
                        {
                            db.SaveChanges();
                        }
                    }
                }
                else if (playerSalariesModel.Salarie1 != 0)
                {
                    db.PlayersSalariesFull.Add(new PlayersSalariesFull
                        {
                            DateSalarie = Today,
                            Salarie = playerSalariesModel.Salarie1,
                            IdCondition = playerSalariesModel.IdCondition1,
                            IdLVNBAFranchises = playerSalariesModel.ActualTeam,
                            IdPlayer = playerSalariesModel.Id
                        }
                    );
                    db.SaveChanges();
                }

                if (playersSalariesFull2 != null)
                {
                    if (playersSalariesFull2.Salarie != playerSalariesModel.Salarie2 || playersSalariesFull2.IdCondition != playerSalariesModel.IdCondition2 || playersSalariesFull2.IdLVNBAFranchises != playerSalariesModel.ActualTeam)
                    {
                        playersSalariesFull2.Salarie = playerSalariesModel.Salarie2;
                        playersSalariesFull2.IdCondition = playerSalariesModel.IdCondition2;
                        playersSalariesFull2.IdLVNBAFranchises = playerSalariesModel.ActualTeam;
                        if (TryUpdateModel(playersSalariesFull2, "", new string[] { "Salarie", "IdCondition", "IdLVNBAFranchises" }))
                        {
                            db.SaveChanges();
                        }
                    }
                }
                else if (playerSalariesModel.Salarie2 != 0)
                {
                    db.PlayersSalariesFull.Add(new PlayersSalariesFull
                    {
                        DateSalarie = Today.AddYears(1),
                        Salarie = playerSalariesModel.Salarie2,
                        IdCondition = playerSalariesModel.IdCondition2,
                        IdLVNBAFranchises = playerSalariesModel.ActualTeam,
                        IdPlayer = playerSalariesModel.Id
                    }
                    );
                    db.SaveChanges();
                }

                if (playersSalariesFull3 != null)
                {
                    if (playersSalariesFull3.Salarie != playerSalariesModel.Salarie3 || playersSalariesFull3.IdCondition != playerSalariesModel.IdCondition3 || playersSalariesFull3.IdLVNBAFranchises != playerSalariesModel.ActualTeam)
                    {
                        playersSalariesFull3.Salarie = playerSalariesModel.Salarie3;
                        playersSalariesFull3.IdCondition = playerSalariesModel.IdCondition3;
                        playersSalariesFull3.IdLVNBAFranchises = playerSalariesModel.ActualTeam;
                        if (TryUpdateModel(playersSalariesFull3, "", new string[] { "Salarie", "IdCondition", "IdLVNBAFranchises" }))
                        {
                            db.SaveChanges();
                        }
                    }
                }
                else if (playerSalariesModel.Salarie3 != 0)
                {
                    db.PlayersSalariesFull.Add(new PlayersSalariesFull
                    {
                        DateSalarie = Today.AddYears(2),
                        Salarie = playerSalariesModel.Salarie3,
                        IdCondition = playerSalariesModel.IdCondition3,
                        IdLVNBAFranchises = playerSalariesModel.ActualTeam,
                        IdPlayer = playerSalariesModel.Id
                    }
                    );
                    db.SaveChanges();
                }

                if (playersSalariesFull4 != null)
                {
                    if (playersSalariesFull4.Salarie != playerSalariesModel.Salarie4 || playersSalariesFull4.IdCondition != playerSalariesModel.IdCondition4 || playersSalariesFull4.IdLVNBAFranchises != playerSalariesModel.ActualTeam)
                    {
                        playersSalariesFull4.Salarie = playerSalariesModel.Salarie4;
                        playersSalariesFull4.IdCondition = playerSalariesModel.IdCondition4;
                        playersSalariesFull4.IdLVNBAFranchises = playerSalariesModel.ActualTeam;
                        if (TryUpdateModel(playersSalariesFull4, "", new string[] { "Salarie", "IdCondition", "IdLVNBAFranchises" }))
                        {
                            db.SaveChanges();
                        }
                    }
                }
                else if (playerSalariesModel.Salarie4 != 0)
                {
                    db.PlayersSalariesFull.Add(new PlayersSalariesFull
                    {
                        DateSalarie = Today.AddYears(3),
                        Salarie = playerSalariesModel.Salarie4,
                        IdCondition = playerSalariesModel.IdCondition4,
                        IdLVNBAFranchises = playerSalariesModel.ActualTeam,
                        IdPlayer = playerSalariesModel.Id
                    }
                    );
                    db.SaveChanges();
                }

                if (playersSalariesFull5 != null)
                {
                    if (playersSalariesFull5.Salarie != playerSalariesModel.Salarie5 || playersSalariesFull5.IdCondition != playerSalariesModel.IdCondition5 || playersSalariesFull5.IdLVNBAFranchises != playerSalariesModel.ActualTeam)
                    {
                        playersSalariesFull5.Salarie = playerSalariesModel.Salarie5;
                        playersSalariesFull5.IdCondition = playerSalariesModel.IdCondition5;
                        playersSalariesFull5.IdLVNBAFranchises = playerSalariesModel.ActualTeam;
                        if (TryUpdateModel(playersSalariesFull5, "", new string[] { "Salarie", "IdCondition", "IdLVNBAFranchises" }))
                        {
                            db.SaveChanges();
                        }
                    }
                }
                else if (playerSalariesModel.Salarie5 != 0)
                {
                    db.PlayersSalariesFull.Add(new PlayersSalariesFull
                    {
                        DateSalarie = Today.AddYears(4),
                        Salarie = playerSalariesModel.Salarie5,
                        IdCondition = playerSalariesModel.IdCondition5,
                        IdLVNBAFranchises = playerSalariesModel.ActualTeam,
                        IdPlayer = playerSalariesModel.Id
                    }
                    );
                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index");
            }
        }

        //
        // POST: /PlayersSalaries/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditOld(PlayersSalariesFull playerssalaries)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(playerssalaries).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.Id = new SelectList(db.Players.OrderBy(x => x.CompleteName), "Id", "CompleteName", playerssalaries.Id);
                ViewBag.IdLVNBAFranchises = new SelectList(db.LVNBAFranchises.OrderBy(x => x.Name), "Id", "Name", playerssalaries.IdLVNBAFranchises);
                return View(playerssalaries);
            }
            catch (Exception ex)
            {
                ViewBag.Id = new SelectList(db.Players.OrderBy(x => x.CompleteName), "Id", "CompleteName", playerssalaries.Id);
                ViewBag.IdLVNBAFranchises = new SelectList(db.LVNBAFranchises.OrderBy(x => x.Name), "Id", "Name", playerssalaries.IdLVNBAFranchises);
                return View(playerssalaries);
            }
        }

        //
        // GET: /PlayersSalaries/Delete/5

        public ActionResult DeleteOld(int id = 0)
        {
            var playerssalaries = db.PlayersSalariesFull.Find(id);
            if (playerssalaries == null)
            {
                return HttpNotFound();
            }
            return View(playerssalaries);
        }

        public ActionResult Delete(int id = 0)
        {
            var player = db.Players.Find(id);
            if (player.PlayersSalariesFull == null || player.PlayersSalariesFull.Count() <= 0)
            {
                return RedirectToAction("Index");
            }
            return View(player);
        }

        //
        // POST: /PlayersSalaries/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmedOld(int id)
        {
            var playerssalaries = db.PlayersSalariesFull.Find(id);
            db.PlayersSalariesFull.Remove(playerssalaries);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost, ActionName("DeleteConfirmed")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var player = db.Players.Find(id);
            if (player.PlayersSalariesFull == null || player.PlayersSalariesFull.Count() <= 0)
            {
                return RedirectToAction("Index");
            }
            db.PlayersSalariesFull.RemoveRange(player.PlayersSalariesFull);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult GetTeamByPlayer(int idPlayer)
        {
            var idTeam = 0;
            idTeam = db.Players.Where(x => x.Id == idPlayer).FirstOrDefault().ActualTeam.Value;
            return Json(idTeam, JsonRequestBehavior.AllowGet);
        }
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        internal  string ValidaCondicionSinSalario(PlayersSalariesFull playerssalaries)
        {
            string sMessage = "";
            if (playerssalaries.IdCondition != 0 && playerssalaries.Salarie == 0 )
            {
                sMessage = "No se puede poner una condición sin salario. Salario" + System.Environment.NewLine;
            }

            return sMessage;
        }


        public ActionResult EditComission(int id = 0)
        {
            var playerssalaries = db.PlayersSalariesFull.Find(id);
            if (playerssalaries == null)
            {
                return HttpNotFound();
            }
            ViewBag.Id = new SelectList(db.Players.OrderBy(x => x.CompleteName), "Id", "CompleteName", playerssalaries.Id);
            ViewBag.IdLVNBAFranchises = new SelectList(db.LVNBAFranchises.OrderBy(x => x.Name), "Id", "Name", playerssalaries.IdLVNBAFranchises);

            return View(playerssalaries);
        }

        //
        // POST: /PlayersSalaries/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditComission(PlayersSalariesFull playerssalaries)
        {
            if (ModelState.IsValid)
            {
                db.Entry(playerssalaries).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("PlayerActions", "Players");
            }
            ViewBag.Id = new SelectList(db.Players.OrderBy(x => x.CompleteName), "Id", "CompleteName", playerssalaries.Id);
            ViewBag.IdLVNBAFranchises = new SelectList(db.LVNBAFranchises.OrderBy(x => x.Name), "Id", "Name", playerssalaries.IdLVNBAFranchises);

            return View(playerssalaries);
        }

        [AllowAnonymous]
        public ActionResult SalaryInfoTeam()
        {
            List<SalaryTeamInfo> listaSalariosEquipo = new List<SalaryTeamInfo>();
            Jugadores jugadores = new Jugadores();
            int iSeason = db.Parameters.First().Temporada;
            ViewBag.Today = iSeason;
            foreach (LVNBAFranchises franchise in db.LVNBAFranchises)
            {
                //List<Rounds> rounds = db.Rounds.Where(x => x.IdFranchise == franchise.Id).OrderByDescending(y => y.Year).ToList();
                SalaryTeamInfo IdLVNBAFranchises = new SalaryTeamInfo();
                IdLVNBAFranchises.FranchiseParams = db.LVNBAFranchisesParams.Where(x => x.ID == franchise.Id).First();
                //IdLVNBAFranchises.RoundsTeamInfo = db.Rounds.Where(x => x.Owner == franchise.Id && x.Year >= iSeason).OrderByDescending(x => x.Year).ThenBy(x => x.Type).AsEnumerable();
                IdLVNBAFranchises.Franchise = franchise;
                //IdLVNBAFranchises.ListPlayersRoster = jugadores.getPlayersSalaryInSalary(franchise.Id); // db.PlayersSalariesFull.Where(x => x.IdLVNBAFranchises == idTeam).OrderBy(x => x.IdCondition1).ThenByDescending(x => x.Salary1);
                listaSalariosEquipo.Add(IdLVNBAFranchises);
            }
            return View(listaSalariosEquipo.AsEnumerable());
        }

        [AllowAnonymous]
        public JsonResult GetR(int idFranchise)
        {
            int iSeason = db.Parameters.First().Temporada;
            LVNBAFranchises franchise = db.LVNBAFranchises.Find(idFranchise);
            var round = db.Rounds.Where(x => x.IdFranchise == idFranchise && x.Year == iSeason).Select(a => new
            {
                FranchiseName = franchise.NickName,
                IdFranchise = franchise.Id,
                Anhio = a.Year,
                Temporada = a.Type
            });
            return Json(round, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public JsonResult GetS(int idFranchise)
        {
            var players = db.Players.Where(x => (x.ActualTeam == idFranchise)).Select(a => new
            {
                IdFranchise = idFranchise,
                Nombre = a.CompleteName,
                Salario = a.PlayersSalariesFull.Select(b => new
                {
                    Anho = b.DateSalarie.Year.ToString(),
                    Monto = b.Salarie,
                    Color = b.SalaryCondition.Colour
                })
            });
            return Json(players, JsonRequestBehavior.AllowGet);
        }

    }
}