﻿
using Liga.Domain.Entities;
using Liga.Infrastructure.Data;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace Liga.Web.UI.Controllers
{
    [Authorize(Roles = "Administrador, Comisionado")]
    public class InjuriesController : BaseController
    {
        private Liga_DBContext db = new Liga_DBContext();

        //
        // GET: /Injuries/

        public ActionResult Index()
        {
            var injuries = db.Injuries;
            return View(injuries.ToList());
        }

        //
        // GET: /Injuries/Details/5

        public ActionResult Details(int id = 0)
        {
            Injuries injuries = db.Injuries.Find(id);
            if (injuries == null)
            {
                return HttpNotFound();
            }
            return View(injuries);
        }

        //
        // GET: /Injuries/Create

        public ActionResult Create()
        {
            ViewBag.IdPlayer = new SelectList(db.Players.OrderBy(x => x.CompleteName), "Id", "CompleteName");
            ViewBag.IdType = new SelectList(db.InjuryTypes, "Id", "Description");
            return View();
        }

        //
        // POST: /Injuries/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Injuries injuries)
        {
            if (ModelState.IsValid)
            {
                db.Injuries.Add(injuries);
                if (injuries.GamesRest > 0)
                {
                    var pl = db.Players.FirstOrDefault(x => x.Id == injuries.IdPlayer);
                    pl.Injuried = 1;
                    db.Entry(pl).State = EntityState.Modified;
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdPlayer = new SelectList(db.Players.OrderBy(x => x.CompleteName), "Id", "CompleteName", injuries.IdPlayer);
            ViewBag.IdType = new SelectList(db.InjuryTypes, "Id", "Description", injuries.IdType);
            return View(injuries);
        }

        //
        // GET: /Injuries/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Injuries injuries = db.Injuries.Find(id);
            if (injuries == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdPlayer = new SelectList(db.Players.OrderBy(x => x.CompleteName), "Id", "CompleteName", injuries.IdPlayer);
            ViewBag.IdType = new SelectList(db.InjuryTypes, "Id", "Description", injuries.IdType);
            return View(injuries);
        }

        //
        // POST: /Injuries/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Injuries injuries)
        {
            if (ModelState.IsValid)
            {
                db.Entry(injuries).State = EntityState.Modified;
                if(injuries.GamesRest == 0)
                {
                    var pl = db.Players.FirstOrDefault(x => x.Id == injuries.IdPlayer);
                    pl.Injuried = 0;
                    db.Entry(pl).State = EntityState.Modified;
                }
                else
                {
                    var pl = db.Players.FirstOrDefault(x => x.Id == injuries.IdPlayer);
                    pl.Injuried = 1;
                    db.Entry(pl).State = EntityState.Modified;
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdPlayer = new SelectList(db.Players.OrderBy(x=>x.CompleteName), "Id", "CompleteName", injuries.IdPlayer);
            ViewBag.IdType = new SelectList(db.InjuryTypes, "Id", "Description", injuries.IdType);
            return View(injuries);
        }

        //
        // GET: /Injuries/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Injuries injuries = db.Injuries.Find(id);
            if (injuries == null)
            {
                return HttpNotFound();
            }
            return View(injuries);
        }

        //
        // POST: /Injuries/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Injuries injuries = db.Injuries.Find(id);
            db.Injuries.Remove(injuries);
            var pl = db.Players.FirstOrDefault(x => x.Id == injuries.IdPlayer);
            pl.Injuried = 0;
            db.Entry(pl).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult GetIncompleteInjuries()
        {
            List<FlashInjuryReport> listaLesiones = new List<FlashInjuryReport>();
            List<Injuries> InjuryList = db.Injuries.Where(x => x.GamesRest > 0).Take(20).ToList();
            foreach (Injuries injury in InjuryList)
            {
                FlashInjuryReport lesion = new FlashInjuryReport();
                lesion.sLogo = (injury.Player != null && injury.Player.LVNBAFranchis != null ? injury.Player.LVNBAFranchis.Icono : ""); // injury.Player.LVNBAFranchis.Icono; 
                lesion.idPlayer = injury.IdPlayer;
                lesion.sCompleteName = (injury.Player != null ? injury.Player.AbrevName : "");
                lesion.iGamesRest = injury.GamesRest;
                listaLesiones.Add(lesion);
            }
            return Json(listaLesiones.ToList());
        }


        [AllowAnonymous]
        public ActionResult InjuryResume()
        {
            int iSeason = db.Parameters.First().Temporada;
            InjuryResume resumen = new InjuryResume
            {
                ListaLesionados = db.Injuries.Where(x => x.GamesRest > 0),
                ListaPartidosPerdidosPorLesion = db.InjuryGames.Where(x => x.Game.season == iSeason),
                ListaResumenLesiones = db.ResumenLesiones
            };
            return View(resumen);
        }

    }
    public class FlashInjuryReport
    {
        public string sLogo { get; set; }
        public int iGamesRest { get; set; }
        public string sCompleteName { get; set; }
        public int idPlayer { get; set; }

    }

    

    
}
