﻿
using Liga.Domain.Entities;
using Liga.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Mail;
using System.Text;
using System.Web;

namespace Liga.Infrastructure.Data.Repository
{
    public class SendingMails
    {
        public const string ComisionadoMail = "comisionado@lvnba.com";

        private static string getManagersMails()
        {
            Liga_DBContext dbS = new Liga_DBContext();
            string sMails = "";
            foreach (UserProfile user in dbS.UserProfiles)
            {
                sMails = sMails + user.Email + ";";
            }
            return sMails;
        }
        private Liga_DBContext db = new Liga_DBContext();
        
        private static SmtpClient getClient()
        {
            SmtpClient client = new SmtpClient();
            client.Port = 587;
            client.Host = "lvnba.com";
            client.Timeout = 10000;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            return client;

        }

        public  void pruebaMail()
        {

            SmtpClient client = getClient();
            client.Credentials = new System.Net.NetworkCredential("comisionado@lvnba.com", "123456");
            MailMessage mail = new MailMessage();
            mail.From = new MailAddress("comisionado@lvnba.com");
            mail.To.Add("dguzman1980@gmail.com");
            mail.Subject = "Prueba";
            mail.IsBodyHtml = true;
            var inlineLogo = new LinkedResource(HttpContext.Current.Server.MapPath("~/Images/Common/teams/Iconos/LAC.png"));
            var inlineLogo2 = new LinkedResource(HttpContext.Current.Server.MapPath("~/Images/Common/teams/Iconos/DEN.png"));
            inlineLogo.ContentId = Guid.NewGuid().ToString();
            inlineLogo2.ContentId = Guid.NewGuid().ToString();
            string body = string.Format(@"            <p>Lorum Ipsum Blah Blah</p>            <img src=""cid:{0}"" />            <p>   Lorum Ipsum Blah Blah</p><img src=""cid:{1}"" />        ", inlineLogo.ContentId,inlineLogo2.ContentId);
            var view = AlternateView.CreateAlternateViewFromString(body, null, "text/html");
            view.LinkedResources.Add(inlineLogo); view.LinkedResources.Add(inlineLogo2);
            mail.AlternateViews.Add(view);
            try
            {
                client.Send(mail);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception caught in CreateTestMessage2(): {0}",
                     ex.ToString());
            }
        }


        public static void enviaMail(string sSender,string sSenderPSW, string sReceiver, string sBCC, string sSubject,string sHeader, string sBody, string sFooter, string sFirma, AlternateView view)
        {

            SmtpClient client = getClient();
            client.Credentials = new System.Net.NetworkCredential(sSender, sSenderPSW);

            //MailMessage mesage = new MailMessage();
            //mesage.

            MailMessage mm = new MailMessage(sSender, sReceiver, sSubject, sHeader + sBody + sFooter + sFirma);
            mm.To.Add("darth0@gmail.com");
            mm.BodyEncoding = UTF8Encoding.UTF8;
            mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
            mm.IsBodyHtml = true;

            mm.AlternateViews.Add(view);
           
            try
            {
                client.Send(mm);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception caught in CreateTestMessage2(): {0}",
                     ex.ToString());
            }

        }


        /// <summary>
        /// Propuesta de traspaso
        /// </summary>
        /// <param name="idManagerPropone"></param>
        /// <param name="idTrade"></param>
        public void propuestaTraspaso(int idManagerPropone, int idTrade)
        {
            LinkedResource inlineLogo1; LinkedResource inlineLogo2; LinkedResource inlineLogo3; LinkedResource inlineLogo4;
            inlineLogo3 = new LinkedResource(HttpContext.Current.Server.MapPath("~/Images/Common/teams/Iconos/FA.gif"));
            inlineLogo4 = new LinkedResource(HttpContext.Current.Server.MapPath("~/Images/Common/teams/Iconos/FA.gif"));
            string sFirma = "";
            string sFooter = "";
            string sBody = "";
            string sSubject = "";
            string sEmailsTo;
            string sEmail1,sEmail2,sEmail3, sEmail4 = "";
            string sEquipo1 = "",sEquipo2 = "",sEquipo3 = "", sEquipo4 = "";
            int iEquipoId1 = 0; int iEquipoId2 = 0; ; int iEquipoId3 = 0; int iEquipoId4 = 0;
            string sTeamHeader = "";
            Managers manager = db.Managers.Where(x => x.Id == idManagerPropone).First();
            Transfers transfer =  db.Transfers.Where(x=>x.Id == idTrade).FirstOrDefault();
            if (transfer != null)
            {
                //obtenemos destinatarios
                sEmail1 = transfer.LVNBAFranchis.Manager.Email;
                sEmailsTo = sEmail1;
                sEmail2 = transfer.LVNBAFranchis1.Manager.Email;
                sEmailsTo = sEmailsTo + ";" + sEmail2;

                sEquipo1 = transfer.LVNBAFranchis.Abbreviation;
                iEquipoId1 = transfer.LVNBAFranchis.Id;
                sEquipo2 = transfer.LVNBAFranchis1.Abbreviation;
                sTeamHeader = sEquipo1 + " - " +  sEquipo2;
                iEquipoId2 = transfer.LVNBAFranchis1.Id;

                if (transfer.LVNBAFranchis2 != null)
                {
                    sEmail3 = transfer.LVNBAFranchis2.Manager.Email;
                    sEmailsTo = sEmailsTo + ";" + sEmail3;
                    sEquipo3 = transfer.LVNBAFranchis2.Abbreviation;
                    iEquipoId3 = transfer.LVNBAFranchis2.Id;
                    sTeamHeader = sTeamHeader + " - " + sEquipo3;
                }
                if (transfer.LVNBAFranchis3 != null)
                {
                    sEmail4 = transfer.LVNBAFranchis3.Manager.Email;
                    sEmailsTo = sEmailsTo + ";" + sEmail4;
                    sEquipo4 = transfer.LVNBAFranchis3.Abbreviation;
                    iEquipoId4 = transfer.LVNBAFranchis3.Id;
                    sTeamHeader = sTeamHeader + " - " + sEquipo4;
                }

                //montamos el asunto del mail
                sSubject = "Traspaso propuesto:  " + sTeamHeader + " - por " + manager.GamerTag;
                //montamos el cuerpo del mensaje

                sBody = "<br/><br/><b>" + manager.GamerTag + " (" + manager.LVNBAFranchises.First().Abbreviation + ")</b> ha propuesto el siguiente traspaso: <br/><br/> ";

                sBody = sBody + "Fecha propuesta: " + transfer.TransferDate.Value.ToShortDateString() + "<br/><br/>";


                List<TransfersPlayersLists> listaJugadoresDaEquipo1 = transfer.TransfersPlayersLists.Where(x => x.IdFranchise == iEquipoId1).ToList();
                List<TransfersPlayersLists> listaJugadoresRecibeEquipo1 = transfer.TransfersPlayersLists.Where(x => x.Destination == iEquipoId1).ToList();
                List<TransfersPlayersLists> listaJugadoresDaEquipo2 = transfer.TransfersPlayersLists.Where(x => x.IdFranchise == iEquipoId2).ToList();
                List<TransfersPlayersLists> listaJugadoresRecibeEquipo2 = transfer.TransfersPlayersLists.Where(x => x.Destination == iEquipoId2).ToList();
                List<TransfersPlayersLists> listaJugadoresDaEquipo3 = transfer.TransfersPlayersLists.Where(x => x.IdFranchise == iEquipoId3).ToList();
                List<TransfersPlayersLists> listaJugadoresRecibeEquipo3 = transfer.TransfersPlayersLists.Where(x => x.Destination == iEquipoId3).ToList();
                List<TransfersPlayersLists> listaJugadoresDaEquipo4 = transfer.TransfersPlayersLists.Where(x => x.IdFranchise == iEquipoId4).ToList();
                List<TransfersPlayersLists> listaJugadoresRecibeEquipo4 = transfer.TransfersPlayersLists.Where(x => x.Destination == iEquipoId4).ToList();

                List<TransfersRoundsLists> listaRondasDaEquipo1 = transfer.TransfersRoundsLists.Where(x => x.IdFranchise == iEquipoId1).ToList();
                List<TransfersRoundsLists> listaRondasRecibeEquipo1 = transfer.TransfersRoundsLists.Where(x => x.Destination == iEquipoId1).ToList();
                List<TransfersRoundsLists> listaRondasDaEquipo2 = transfer.TransfersRoundsLists.Where(x => x.IdFranchise == iEquipoId2).ToList();
                List<TransfersRoundsLists> listaRondasRecibeEquipo2 = transfer.TransfersRoundsLists.Where(x => x.Destination == iEquipoId2).ToList();
                List<TransfersRoundsLists> listaRondasDaEquipo3 = transfer.TransfersRoundsLists.Where(x => x.IdFranchise == iEquipoId3).ToList();
                List<TransfersRoundsLists> listaRondasRecibeEquipo3 = transfer.TransfersRoundsLists.Where(x => x.Destination == iEquipoId3).ToList();
                List<TransfersRoundsLists> listaRondasDaEquipo4 = transfer.TransfersRoundsLists.Where(x => x.IdFranchise == iEquipoId4).ToList();
                List<TransfersRoundsLists> listaRondasRecibeEquipo4 = transfer.TransfersRoundsLists.Where(x => x.Destination == iEquipoId4).ToList();

                sBody = sBody + MontaCadenaDaRecibe(sEquipo1, listaJugadoresDaEquipo1, listaRondasDaEquipo1, listaJugadoresRecibeEquipo1, listaRondasRecibeEquipo1,out inlineLogo1);
                sBody = sBody + MontaCadenaDaRecibe(sEquipo2, listaJugadoresDaEquipo2, listaRondasDaEquipo2, listaJugadoresRecibeEquipo2, listaRondasRecibeEquipo2, out inlineLogo2);
                if(iEquipoId3 != 0)
                    sBody = sBody + MontaCadenaDaRecibe(sEquipo3, listaJugadoresDaEquipo3, listaRondasDaEquipo3, listaJugadoresRecibeEquipo3, listaRondasRecibeEquipo3, out inlineLogo3);
                if(iEquipoId4 != 0)
                    sBody = sBody + MontaCadenaDaRecibe(sEquipo4, listaJugadoresDaEquipo4, listaRondasDaEquipo4, listaJugadoresRecibeEquipo4, listaRondasRecibeEquipo4, out inlineLogo4);


                sFooter = " Para poder visualizar y aceptar la propuesta accede a lvnba.com ";

                sFirma = " Firma ";


                AlternateView view = AlternateView.CreateAlternateViewFromString(sBody + sFooter + sFirma, null, "text/html");

                view.LinkedResources.Add(inlineLogo1); view.LinkedResources.Add(inlineLogo2); 
                if(iEquipoId3 != 0)
                    view.LinkedResources.Add(inlineLogo3);
                if(iEquipoId4 != 0)
                    view.LinkedResources.Add(inlineLogo4);
               

                enviaMail("comisionado@lvnba.com", "123456", "dguzman1980@gmail.com", "", sSubject, "", sBody, sFooter, sFirma, view);

            }
           
            
        }

        private string MontaCadenaDaRecibe(string sEquipo, List<TransfersPlayersLists> listaJugadoresDaEquipo, List<TransfersRoundsLists> listaRondasDaEquipo, List<TransfersPlayersLists> listaJugadoresRecibeEquipo, List<TransfersRoundsLists> listaRondasRecibeEquipo, out LinkedResource  inlineLogo)
        {
            inlineLogo = new LinkedResource(HttpContext.Current.Server.MapPath("~/Images/Common/teams/Iconos/" + sEquipo + ".png"));
            inlineLogo.ContentId = Guid.NewGuid().ToString();



            string sCadena = "";
            sCadena = sCadena + "<br/><span style='font-weight:bold;font-size:18px'>" + "" + string.Format(@"</span><img src=""cid:{0}"" /><br/>",inlineLogo.ContentId);
            sCadena = sCadena + "<table><tr><td style='padding:15px; vertical-align:top'>";

            sCadena = sCadena + "<span style='font-weight:bold;font-size:24px;color:red'> recibe: </span><br/>";
            foreach (TransfersPlayersLists jugador in listaJugadoresRecibeEquipo)
            {
                sCadena = sCadena + @"<img width=""50px"" height=""70"" src="  + jugador.Player.FaceImage + " />" + jugador.Player.CompleteName + @"<img width=""100px"" height=""30""  src=""http://lvnba.com/images/common/larrow.gif"" />" + @"<img width=""70px"" height=""70"" src=""http://lvnba.com/Images/Common/teams/Iconos/" + jugador.LVNBAFranchis1.Abbreviation + @".png""  /><br/>";
            }
            sCadena = sCadena + "<br/>";
            foreach (TransfersRoundsLists ronda in listaRondasRecibeEquipo)
            {
                sCadena = sCadena + "<span style='font-size:16px;font-weight:bold'>" + ronda.Round.Type + "ª Ronda  " + ronda.Round.Year + " (" + ronda.Round.LVNBAFranchis.Abbreviation + ")</span>" + @"<img width=""100px"" height=""30""  src=""http://lvnba.com/images/common/larrow.gif"" />" + @"<img width=""70px"" height=""70"" src=""http://lvnba.com/Images/Common/teams/Iconos/" + ronda.LVNBAFranchis.Abbreviation + @".png""  /><br/>";
            }


            sCadena = sCadena + "<br/></td><td style='padding:15px;  vertical-align:top'>";


            sCadena = sCadena + "<span style='font-weight:bold;font-size:24px;color:red'> da: </span> <br/>";
            foreach (TransfersPlayersLists jugador in listaJugadoresDaEquipo)
            {
                sCadena = sCadena + @"<img width=""50px"" height=""70"" src="  + jugador.Player.CompleteName + @"<img width=""100px"" height=""30""  src=""http://lvnba.com/images/common/rarrow.gif"" />" + @"<img width=""70px"" height=""70"" src=""http://lvnba.com/Images/Common/teams/Iconos/" + jugador.LVNBAFranchis2.Abbreviation + @".png""  /><br/>";
            }
            sCadena = sCadena + "<br/>";
            foreach (TransfersRoundsLists ronda in listaRondasDaEquipo)
            {
                sCadena = sCadena + "<span style='font-size:16px;font-weight:bold'>" + ronda.Round.Type + "ª Ronda " + ronda.Round.Year + " (" + ronda.Round.LVNBAFranchis.Abbreviation + ")</span>" + @"<img width=""100px"" height=""30""  src=""http://lvnba.com/images/common/rarrow.gif"" />" + @"<img width=""70px"" height=""70"" src=""http://lvnba.com/Images/Common/teams/Iconos/" + ronda.LVNBAFranchis1.Abbreviation + @".png""  /><br/>";
            }
            sCadena = sCadena + "<br/></td></tr></table><br/>";
            return sCadena;
        }


        /// <summary>
        /// Cambio de status mercantil
        /// </summary>
        /// <param name="idManagerPropone"></param>
        /// <param name="idTrade"></param>
        public void statusMercantil(int idTeam)
        {
            LinkedResource inlineLogo; 
            string sFirma = "";
            string sFooter = "";
            string sBody = "";
            string sSubject = "";
            string sEmailBCC = "";

            LVNBAFranchises franquicia = db.LVNBAFranchises.Where(x=>x.Id == idTeam).First();

            inlineLogo = new LinkedResource(HttpContext.Current.Server.MapPath("~/Images/Common/teams/Iconos/" + franquicia.Abbreviation + ".png"));
            inlineLogo.ContentId = Guid.NewGuid().ToString();

            sEmailBCC = SendingMails.getManagersMails();

            //montamos el asunto del mail
            sSubject = "Los " + franquicia.NickName + " han modificado su status mercantil";
            //montamos el cuerpo del mensaje

            var jugadores = new Jugadores().getPlayersRoster(idTeam); 
            string sElementoPlayer = "";
            foreach (var jugador in jugadores)
            {
                sElementoPlayer = "<br/><img src='" +  jugador.FaceImage + "' /><div>"  + jugador.CompleteName + "</div><br/>";
                sBody = sBody + sElementoPlayer;
            }
            sBody = sBody + "<br/><br/><br/>";
            sFooter = "  ";
            sFirma = " Firma ";
            AlternateView view = AlternateView.CreateAlternateViewFromString(sBody + sFooter + sFirma, null, "text/html");
            view.LinkedResources.Add(inlineLogo);
            enviaMail("comisionado@lvnba.com", "123456", "dguzman1980@gmail.com", "", sSubject, "Status Mercantil", sBody, sFooter, sFirma, view);
        }

    }
}