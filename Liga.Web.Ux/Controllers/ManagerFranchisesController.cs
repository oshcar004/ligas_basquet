﻿using Liga.Domain.Entities;
using Liga.Infrastructure.Data;
using Liga.Infrastructure.Data.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace Liga.Web.UI.Controllers
{
    [Authorize(Roles = "Administrador")]
    public class ManagerFranchisesController : BaseController
    {
        private Liga_DBContext db = new Liga_DBContext();

        //
        // GET: /ManagerFranchises/

        public ActionResult Index()
        {
            var managersfranchises = db.ManagersFranchises.Include(m => m.LVNBAFranchis).Include(m => m.Manager);
            return View(managersfranchises.ToList());
        }

        //
        // GET: /ManagerFranchises/Details/5

        public ActionResult Details(int id = 0)
        {
            ManagersFranchises managersfranchises = db.ManagersFranchises.Find(id);
            if (managersfranchises == null)
            {
                return HttpNotFound();
            }
            return View(managersfranchises);
        }

        //
        // GET: /ManagerFranchises/Create

        public ActionResult Create()
        {
            ViewBag.IdFranchise = new SelectList(db.LVNBAFranchises, "Id", "Name");
            ViewBag.IdManager = new SelectList(db.Managers, "Id", "GamerTag");
            return View();
        }

        //
        // POST: /ManagerFranchises/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ManagersFranchises managersfranchises)
        {
            if (ModelState.IsValid)
            {
                db.ManagersFranchises.Add(managersfranchises);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdFranchise = new SelectList(db.LVNBAFranchises, "Id", "Name", managersfranchises.IdFranchise);
            ViewBag.IdManager = new SelectList(db.Managers, "Id", "GamerTag", managersfranchises.IdManager);
            return View(managersfranchises);
        }

        //
        // GET: /ManagerFranchises/Edit/5

        public ActionResult Edit(int idfranchise, int idmanager , DateTime startdate)
        {
            ManagersFranchises managersfranchises = db.ManagersFranchises.Find(idfranchise,idmanager,startdate);
            if (managersfranchises == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdFranchise = new SelectList(db.LVNBAFranchises, "Id", "Name", managersfranchises.IdFranchise);
            ViewBag.IdManager = new SelectList(db.Managers, "Id", "GamerTag", managersfranchises.IdManager);
            return View(managersfranchises);
        }

        //
        // POST: /ManagerFranchises/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ManagersFranchises managersfranchises)
        {
            if (ModelState.IsValid)
            {
                db.Entry(managersfranchises).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdFranchise = new SelectList(db.LVNBAFranchises, "Id", "Name", managersfranchises.IdFranchise);
            ViewBag.IdManager = new SelectList(db.Managers, "Id", "GamerTag", managersfranchises.IdManager);
            return View(managersfranchises);
        }

        //
        // GET: /ManagerFranchises/Delete/5

        public ActionResult Delete(int id = 0)
        {
            ManagersFranchises managersfranchises = db.ManagersFranchises.Find(id);
            if (managersfranchises == null)
            {
                return HttpNotFound();
            }
            return View(managersfranchises);
        }

        //
        // POST: /ManagerFranchises/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ManagersFranchises managersfranchises = db.ManagersFranchises.Find(id);
            db.ManagersFranchises.Remove(managersfranchises);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }


        public ActionResult AssignManagerToFranchise()
        {
            ViewBag.IdManager = new SelectList(GetManagerExceptOne(), "Id", "GamerTag");
            ViewBag.IdFranchise = new SelectList(db.LVNBAFranchises.OrderBy(x => x.Name), "Id", "Name");
            return View(db.LVNBAFranchises.OrderBy(x => x.Name).ToList());
        }
        [HttpPost]
        public ActionResult AssignManagerToFranchise(FormCollection col)
        {
            string sFranquicia = col[1];
            string sManager = col[2];

            try
            {

                if (string.IsNullOrEmpty(sFranquicia))
                {
                    ModelState.AddModelError("", "Debe seleccionar una franquicia");
                    ViewBag.IdManager = new SelectList(GetManagerExceptOne(), "Id", "GamerTag");
                    ViewBag.IdFranchise = new SelectList(db.LVNBAFranchises.OrderBy(x => x.Name), "Id", "Name");
                    return View(db.LVNBAFranchises.OrderBy(x => x.Name).ToList());
                }

                AsignarManager(sFranquicia, sManager);

                ViewBag.IdManager = new SelectList(GetManagerExceptOne(), "Id", "GamerTag");
                ViewBag.IdFranchise = new SelectList(db.LVNBAFranchises.OrderBy(x => x.Name), "Id", "Name", sFranquicia);
                return View(db.LVNBAFranchises.OrderBy(x => x.Name).ToList());
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "No se han podido guardar los cambios" + ex.ToString());
                ViewBag.IdManager = new SelectList(GetManagerExceptOne(), "Id", "GamerTag");
                ViewBag.IdFranchise = new SelectList(db.LVNBAFranchises.OrderBy(x => x.Name), "Id", "Name", sFranquicia);
                return View(db.LVNBAFranchises.OrderBy(x => x.Name).ToList());

            }
        }


        public IEnumerable<Managers> GetManagerExceptOne()
        {
            var query = from c in db.Managers
                        where !(from o in db.LVNBAFranchises
                                select o.IdManager)
                       .Contains(c.Id)
                        select c;

            return (IEnumerable<Managers>)query;
        }

        public void AsignarManager(string sFranquicia, string sManager)
        {
            try
            {
                int iFranquicia = Convert.ToInt32(sFranquicia);
                int? iManager;
                string sLaFranquicia;
                int? oldMan;
                if (sManager == "")
                    iManager = null;
                else
                    iManager = Convert.ToInt32(sManager);

                Historico his = new Historico();
                his.ConsolidateHistoryManager(iFranquicia, iManager);
                LVNBAFranchises fran = db.LVNBAFranchises.Find(iFranquicia);
                if (fran != null)
                {
                    sLaFranquicia = fran.Name;
                    oldMan = fran.IdManager;
                    fran.IdManager = iManager;
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult ManagerByTeam(int idTeam)
        {
            if (db.LVNBAFranchises.Where(x => x.Id == idTeam).FirstOrDefault().Manager == null)
                return Json("");
            else
                return Json(db.LVNBAFranchises.Where(x => x.Id == idTeam).FirstOrDefault().Manager.GamerTag.ToString());
        }

    }
}