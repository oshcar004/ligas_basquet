﻿using Liga.Domain.Entities;
using Liga.Infrastructure.Data;
using Liga.Infrastructure.Data.Repository;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Liga.Web.UI.Controllers
{
    public class RostersController : BaseController
    {
        private Liga_DBContext db = new Liga_DBContext();

        //
        // GET: /Rosters/

        public ActionResult Index()
        {
            RosterPlayers rosters = new RosterPlayers();
            rosters.equipos = db.LVNBAFranchises.OrderBy(x => x.Division.Conference.Conferencia).OrderBy(x => x.IdDivision).ToList();
            rosters.jugadores = db.Players.ToList();

            return View(rosters);
        }

        public ActionResult TeamRosters(int? idTab, int? idTeam)
        {
            int iSeason = db.Parameters.First().Temporada;
            ViewBag.Season = iSeason;
            ViewBag.Tab = idTab;
            LVNBAFranchises franchise = db.LVNBAFranchises.Where(x => x.Id == idTeam).First();
            GeneralFranchiseInfo oFranchiseInfo = new GeneralFranchiseInfo();
            var playersTeamCuttedRights = new Jugadores().getPlayersRightsAndCutted(idTeam.Value);
            oFranchiseInfo.Today = new DateTime(db.Parameters.First().Temporada, 01, 01);
            oFranchiseInfo.Franchise = franchise;
            Managers manager = null;
            if (franchise.IdManager == null)
            {
                manager = new Managers
                {
                    GamerTag = "SIN MANAGER"
                };
            }
            else
            {
                manager = db.Managers.Where(x => x.Id == franchise.IdManager).FirstOrDefault();
            }
            int? idManagerFr = franchise.IdManager;
            var awards = db.PFMAwards;
            IEnumerable<PFMAwards> franchiseAwards = awards.Where(x => x.Award.Class == "E" && x.IdFranchise == idTeam).AsEnumerable();
            IEnumerable<PFMAwards> managerAwards = awards.Where(x => x.IdManager == franchise.IdManager).AsEnumerable();
            IEnumerable<Rounds> franchiseRounds = db.Rounds.Where(x => x.Owner == idTeam && x.Year >= iSeason).OrderBy(x => x.Year).ThenBy(x => x.Type).AsEnumerable();
            IEnumerable<Rounds> managerRounds = db.Rounds.Where(x => x.IdManager == franchise.IdManager).OrderBy(x => x.Year).ThenBy(x => x.Type).AsEnumerable();
            IEnumerable<ManagersFranchises> managerTrayectorias = db.ManagersFranchises.Where(x => x.IdManager == idManagerFr).AsEnumerable();
            IEnumerable<SalaryConditions> salaryCondition = db.SalaryConditions;

            oFranchiseInfo.TeamGoalsInfo = new GoalsTeam();
            oFranchiseInfo.PlotInfo = new PlotInfoSalary();
            oFranchiseInfo.ManagerCard = new ManagerCardMO();
            oFranchiseInfo.TeamCard = new TeamCardMO();
            oFranchiseInfo.RoundsTeamInfo = new RoundsTeam();

            //Grafico salarios
            var parameters = db.Parameters.First();
            oFranchiseInfo.PlotInfo.LuxuryTax = parameters.ImpuestoLujo.ToString();
            oFranchiseInfo.PlotInfo.SalaryTax = parameters.LimiteSalarial.ToString();

            var salaries = db.PlayersSalariesFull.Where(x => x.IdLVNBAFranchises == idTeam);

            decimal? dLujo = parameters.ImpuestoLujo;
            decimal? dSalary = parameters.LimiteSalarial;
            oFranchiseInfo.PlotInfo.LuxuryTaxForPlot = Convert.ToInt32(dLujo);
            oFranchiseInfo.PlotInfo.SalaryTaxForPlot = Convert.ToInt32(dSalary);
            oFranchiseInfo.PlotInfo = oFranchiseInfo.PlotInfo;
            oFranchiseInfo.TeamCard.Franchise = franchise;
            List<Players> playersList = new List<Players>();
            List<PlayersSalariesFull> playersSalariesFullList = new List<PlayersSalariesFull>();
            playersSalariesFullList = db.PlayersSalariesFull.Where(x => x.IdLVNBAFranchises == franchise.Id 
            && x.DateSalarie.Year >= parameters.Temporada && x.Salarie > 1).OrderBy(x => x.Salarie).ToList();
            foreach (PlayersSalariesFull playersSalariesFull in playersSalariesFullList)
            {
                if (!playersList.Contains(playersSalariesFull.Player))
                    playersList.Add(playersSalariesFull.Player); // Guarda los jugadores pertenecientes al equipo y los jugadores que fueron cortados por el equipo (que pueden pertenecer a otro equipo) pero se les debe pagar igual
            }
            oFranchiseInfo.ListPlayersRoster = playersList.Where(x => x.ActualTeam == franchise.Id && x.PlayersSalariesFull != null && x.PlayersSalariesFull.Count() > 0)
                .OrderByDescending(x => x.PlayersSalariesFull.First(p => p.DateSalarie.Year ==  parameters.Temporada).Salarie);
            oFranchiseInfo.ListPlayersFinantialInfo = playersList.AsEnumerable();
            decimal salarieTemp1 = 0;
            decimal salarieTemp2 = 0;
            decimal salarieTemp3 = 0;
            decimal salarieTemp4 = 0;
            decimal salarieTemp5 = 0;
            foreach (Players player in oFranchiseInfo.ListPlayersFinantialInfo)
            {
                IEnumerable<PlayersSalariesFull> salarie1 = player.PlayersSalariesFull.Where(x => x.DateSalarie.Year == iSeason);
                salarieTemp1 = salarieTemp1 + ((salarie1 != null && salarie1.Count() > 0) ? salarie1.First().Salarie : 0);

                IEnumerable<PlayersSalariesFull> salarie2 = player.PlayersSalariesFull.Where(x => x.DateSalarie.Year == iSeason + 1);
                salarieTemp2 = salarieTemp2 + ((salarie2 != null && salarie2.Count() > 0) ? salarie2.First().Salarie : 0);

                IEnumerable<PlayersSalariesFull> salarie3 = player.PlayersSalariesFull.Where(x => x.DateSalarie.Year == iSeason + 2);
                salarieTemp3 = salarieTemp3 + ((salarie3 != null && salarie3.Count() > 0) ? salarie3.First().Salarie : 0);

                IEnumerable<PlayersSalariesFull> salarie4 = player.PlayersSalariesFull.Where(x => x.DateSalarie.Year == iSeason + 3);
                salarieTemp4 = salarieTemp4 + ((salarie4 != null && salarie4.Count() > 0) ? salarie4.First().Salarie : 0);

                IEnumerable<PlayersSalariesFull> salarie5 = player.PlayersSalariesFull.Where(x => x.DateSalarie.Year == iSeason + 4);
                salarieTemp5 = salarieTemp5 + ((salarie5 != null && salarie5.Count() > 0) ? salarie5.First().Salarie : 0);
            }
            List<ChartData> Data = new List<ChartData>()
            {
                new ChartData(iSeason.ToString(), (double)salarieTemp1, (double)dSalary),
                new ChartData((iSeason + 1).ToString(), (double)salarieTemp2, (double)dSalary),
                new ChartData((iSeason + 2).ToString(), (double)salarieTemp3, (double)dSalary),
                new ChartData((iSeason + 3).ToString(), (double)salarieTemp4, (double)dSalary),
                new ChartData((iSeason + 4).ToString(), (double)salarieTemp5, (double)dSalary)
			};
            oFranchiseInfo.ChartData = Data;
            oFranchiseInfo.PlotInfo.SalaryTeam1 = salarieTemp1.ToString(); // ACA SE DEBEN SUMAR TODOS LOS SALARIOS QUE INVOLUCRAN AL EQUIPO
            oFranchiseInfo.ListPlayersRightsoCutted = playersTeamCuttedRights;
            oFranchiseInfo.ManagerCard.Manager = manager;
            oFranchiseInfo.TeamCard.FranchiseAwards = franchiseAwards;
            oFranchiseInfo.ManagerCard.ManagerAwards = managerAwards;
            oFranchiseInfo.ManagerCard.ManagerRounds = managerRounds;
            oFranchiseInfo.ManagerCard.ManagerTrayectoria = managerTrayectorias;
            oFranchiseInfo.RoundsTeamInfo.Rounds = franchiseRounds;
            oFranchiseInfo.TeamGoalsInfo.SportGoal = db.FranchiseSportGoals.Where(x => x.IdFranchise == idTeam).FirstOrDefault();
            oFranchiseInfo.TeamGoalsInfo.FinancialGoal = db.FranchiseManagementGoals.Where(x => x.IdFranchise == idTeam).FirstOrDefault();
            oFranchiseInfo.Conditions = salaryCondition;
            return View(oFranchiseInfo);
        }

        public ActionResult TeamPlayers(int? idTab, int? idTeam, int? size, string sortOrder, string currentFilter, string searchString, int? page, string message = "")
        {

            return View(getPlayersInfoList(idTab, idTeam, size, sortOrder, currentFilter, searchString, page, message));
        }

        public ActionResult PlayersStatsRedirect(int? idTab, int? idTeam, int? size, string sortOrder, string currentFilter, string searchString, int? page, string message = "")
        {
            return View(getPlayersInfoList(idTab, idTeam, size, sortOrder, currentFilter, searchString, page, message));
        }

        private IPagedList<PlayersInfoList> getPlayersInfoList(int? idTab, int? idTeam, int? size, string sortOrder, string currentFilter, string searchString, int? page, string message = "")
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = string.IsNullOrEmpty(sortOrder) ? "Name_desc" : "";
            ViewBag.Message = message;
            ViewBag.idTeam = idTeam;
            ViewBag.idTab = (idTab == null ? 1 : idTab);
            ViewBag.States = db.PlayersStates.Where(x => x.Id > 0);
            ViewBag.Roles = db.PlayersRoles.Where(x => x.Id > 0);
            IQueryable<PlayerStatisticsBySeason> playerStatisticsBySeasonList = null;
            if (idTeam != 0)
            {
                playerStatisticsBySeasonList = db.PlayerStatisticsBySeasons.Where(x => x.IdTeam == idTeam).OrderByDescending(x => x.G).AsQueryable();
            }
            else
            {
                playerStatisticsBySeasonList = db.PlayerStatisticsBySeasons.OrderByDescending(x => x.G).AsQueryable();
            }

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;
            //////
            IQueryable<PlayersInfoList> listaPlayers;
            List<PlayersInfoList> listaPlayersList = new List<PlayersInfoList>();
            IQueryable<Players> playersList;
            if (idTeam != 0)
            {
                playersList = db.Players.Where(x => x.ActualTeam == idTeam && x.Cutted != 1).OrderByDescending(x => x.Name).AsQueryable();
                int temporada = new Liga_DBContext().Parameters.First().Temporada;
                ViewBag.Season = temporada;
                foreach (Players player in playersList)
                {
                    PlayersInfoList playersInfoList = new PlayersInfoList();
                    IEnumerable<PlayersSalariesFull> salarios = player.PlayersSalariesFull.Where(x => x.DateSalarie.Year == temporada);
                    PlayersSalariesFull playerSalarie = ((salarios != null && salarios.Count() > 0) ? salarios.First() : null);
                    playersInfoList.idCondition = (playerSalarie != null ? playerSalarie.SalaryCondition.Id : 6);
                    playersInfoList.BirdRights = player.BirdRights;
                    playersInfoList.Logo = player.LVNBAFranchis.Logo;
                    playersInfoList.CompleteName = player.CompleteName;
                    playersInfoList.Age = (player.Age != null ? player.Age.Value : 0);
                    playersInfoList.Height = (player.Height != null ? player.Height.Value : 0);
                    playersInfoList.Weight = (player.Weight != null ? player.Weight.Value : 0);
                    playersInfoList.Position = player.PlayersPosition.Position;
                    playersInfoList.StateLong = player.PlayersState.State;
                    playersInfoList.IdState = player.PlayersState.Id;
                    playersInfoList.RolLong = player.PlayersRole.Rol;
                    playersInfoList.IdRol = player.PlayersRole.Id;
                    playersInfoList.Level2k = (player.Level2k != null ? player.Level2k.Value : 0);
                    playersInfoList.YearsPro = (player.YearsPro != null ? player.YearsPro.Value : 0);
                    playersInfoList.Salary = (playerSalarie != null ? playerSalarie.Salarie : 0);
                    playersInfoList.Followed = (player.Followers != null ? player.Followers.Value : 0);
                    playersInfoList.ContractY = 0; // preguntar
                    playersInfoList.SalaryTeam = ""; // preguntar
                    playersInfoList.RookieRights = 0; // preguntar
                    playersInfoList.PlayerEntity = player;
                    PlayerStatisticsBySeason playerStatisticsBySeason = null;
                    IQueryable<PlayerStatisticsBySeason> lista = playerStatisticsBySeasonList.Where(x => x.Player == player.Id);
                    playerStatisticsBySeason = ((lista != null && lista.Count() > 0) ? lista.First() : null);
                    if (playerStatisticsBySeason != null)
                    {
                        playersInfoList.G = playerStatisticsBySeason.G;
                        playersInfoList.GS = playerStatisticsBySeason.GS;
                        playersInfoList.MPG = playerStatisticsBySeason.MPG;
                        playersInfoList.FG = playerStatisticsBySeason.FG;
                        playersInfoList.F3G = playerStatisticsBySeason.F3G;
                        playersInfoList.FT = playerStatisticsBySeason.FT;
                        playersInfoList.OFR = playerStatisticsBySeason.OFR;
                        playersInfoList.DEF = playerStatisticsBySeason.DEF;
                        playersInfoList.RPG = playerStatisticsBySeason.RPG;
                        playersInfoList.APG = playerStatisticsBySeason.APG;
                        playersInfoList.SPG = playerStatisticsBySeason.SPG;
                        playersInfoList.TOO = playerStatisticsBySeason.TOO;
                        playersInfoList.PF = playerStatisticsBySeason.PF;
                        playersInfoList.PPG = playerStatisticsBySeason.PPG;
                    }
                    else
                    {
                        playersInfoList.G = 0;  // preguntar
                        playersInfoList.GS = 0;  // preguntar
                        playersInfoList.MPG = 0;  // preguntar
                        playersInfoList.FG = 0;  // preguntar
                        playersInfoList.F3G = 0;  // preguntar
                        playersInfoList.FT = 0;  // preguntar
                        playersInfoList.OFR = 0;  // preguntar
                        playersInfoList.DEF = 0;  // preguntar
                        playersInfoList.RPG = 0;  // preguntar
                        playersInfoList.APG = 0;  // preguntar
                        playersInfoList.SPG = 0;  // preguntar
                        playersInfoList.TOO = 0;  // preguntar
                        playersInfoList.PF = 0;  // preguntar
                        playersInfoList.PPG = 0;  // preguntar
                    }
                    listaPlayersList.Add(playersInfoList);
                }
                //listaPlayers = db.PlayersInfoList.Where(x => x.IdTeam == idTeam).OrderByDescending(x => x.Salary).AsQueryable();
            }
            else
            {
                //listaPlayers = db.PlayersInfoList.OrderByDescending(x => x.Salary).AsQueryable();
            }
            listaPlayers = listaPlayersList.AsQueryable();
            //////
            if (!string.IsNullOrEmpty(searchString))
            {
                listaPlayers = listaPlayers.Where(s => s.CompleteName.ToUpper().Contains(searchString.ToUpper()));
            }
            switch (sortOrder)
            {
                case "Name_desc":
                    listaPlayers = listaPlayers.OrderByDescending(s => s.CompleteName);
                    break;
                default:
                    listaPlayers = listaPlayers.OrderBy(s => s.CompleteName);
                    break;
            }
            int pageSize = (size == null ? 20 : (int)size);
            int pageNumber = (page ?? 1);
            return listaPlayers.ToPagedList(pageNumber, pageSize);
        }

        public ActionResult TeamCronics(int idTeam)
        {
            CronicasInfoList listaCronica = new CronicasInfoList();
            List<Cronicas> ListadeCronicas = new List<Cronicas>();
            listaCronica.ListCronicas = ListadeCronicas;
            listaCronica.IdTeam = idTeam;
            return View(listaCronica);
        }
    }

    public class RosterPlayers
    {
        public List<LVNBAFranchises> equipos;
        public List<Players> jugadores;
    }
}
