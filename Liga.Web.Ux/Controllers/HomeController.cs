﻿using Liga.Domain.Entities;
using Liga.Infrastructure.Data;
using Liga.Infrastructure.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Liga.Web.UI.Controllers
{
    public class HomeController : BaseController
    {
        private Liga_DBContext db = new Liga_DBContext();

        private int Season = new Liga_DBContext().Parameters.First().Temporada;


        public ActionResult Index()
        {
            ViewBag.Message = "Página principal!!!!!!";

            return View();
        }

        public ActionResult About()
        {

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


        public ActionResult GetStandings(int conference)
        {

            return Json(GetTeamStandings(conference));
        }

        public List<PreviewStanding> GetTeamStandings(int conference)
        {

            List<PreviewStanding> list = new List<PreviewStanding>();
            IEnumerable<Clasificacion> listTeam = db.Clasificacions.Where(x => x.TeamInfo.Division.IdConference == conference).OrderByDescending(x => x.Pct).ToList();
            foreach (Clasificacion team in listTeam)
            {
                PreviewStanding oneStanding = new PreviewStanding();
                oneStanding.Id = team.id;
                oneStanding.TeamName = team.TeamInfo.Name;
                oneStanding.Wins = (int)team.W;
                oneStanding.Losses = (int)team.L;
                oneStanding.Logo = team.TeamInfo.Icono;
                list.Add(oneStanding);

            }
            return list;
        }

        public PartialViewResult PlayerInfo(int idPlayer)
        {
            return PartialView("PlayersInfo", getPlayerCompleteInfo(idPlayer));
        }

        private PlayerCompleteInfo getPlayerCompleteInfo(int idPlayer)
        {
            string Season = db.Parameters.First().Temporada.ToString();
            int iSeason = Convert.ToInt32(Season);
            PlayerCompleteInfo completeInfoPlayer = new PlayerCompleteInfo();

            PosRanksPlayer pRankPlayer = new PosRanksPlayer();
            pRankPlayer.PosMPG = db.RankMPGs.Where(x => x.season == Season && x.Player == idPlayer).FirstOrDefault();
            pRankPlayer.PosPPG = db.RankPPGs.Where(x => x.Season == Season && x.Player == idPlayer).FirstOrDefault();
            pRankPlayer.PosRPG = db.RankRPGs.Where(x => x.season == Season && x.Player == idPlayer).FirstOrDefault();
            pRankPlayer.PosAPG = db.RankAPGs.Where(x => x.season == Season && x.Player == idPlayer).FirstOrDefault();
            pRankPlayer.PosSPG = db.RankSPGs.Where(x => x.season == Season && x.Player == idPlayer).FirstOrDefault();
            pRankPlayer.PosBPG = db.RankBPGs.Where(x => x.season == Season && x.Player == idPlayer).FirstOrDefault();
            if (db.PlayerStatisticsBySeasons.Where(x => x.Player == idPlayer) == null)
            {
                pRankPlayer.PlayerStat = new PlayerStatisticsBySeason();
            }
            else
            {
                pRankPlayer.PlayerStat = db.PlayerStatisticsBySeasons.Where(x => x.Player == idPlayer).FirstOrDefault();
            }
            completeInfoPlayer.Player = db.Players.Where(x => x.Id == idPlayer).First();
            completeInfoPlayer.PlayerStatThisRS = db.PlayerStatisticsBySeasons.Where(x => x.Player == idPlayer && x.Season == Season.ToString()).FirstOrDefault();
            completeInfoPlayer.PlayerStatThisPlayoffs = db.PlayerStatisticsByPofs.Where(x => x.Player == idPlayer && x.Season == Season.ToString()).FirstOrDefault();

            completeInfoPlayer.PlayerSalary = db.PlayersSalariesFull.Where(x => x.IdPlayer == idPlayer).FirstOrDefault();
            completeInfoPlayer.PlayerHistories = db.PlayersHistories.Where(x => x.IdPlayer == idPlayer).OrderByDescending(x => x.StartDate);
            completeInfoPlayer.PlayerAwards = db.PFMAwards.Where(x => x.IdPlayer == idPlayer).OrderByDescending(x => x.Year);
            completeInfoPlayer.PlayersGames = db.LastGames.Where(x => x.idPlayer == idPlayer && x.season == iSeason).OrderByDescending(x => x.gameDate);
            completeInfoPlayer.PositionRank = pRankPlayer;

            return completeInfoPlayer;
        }

        public ActionResult Test(int? idTeam)
        {
            if (idTeam == null) 
                idTeam = 9;
            Jugadores jug = new Jugadores();
            return View(jug.getPlayersRightsAndCutted(idTeam.Value));
        }
    }
}
