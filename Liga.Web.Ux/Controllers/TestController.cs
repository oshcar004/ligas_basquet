﻿using Liga.Service.Impl;
using System.Web.Http;
using System.Web.Mvc;
using AllowAnonymousAttribute = System.Web.Http.AllowAnonymousAttribute;
using HttpGetAttribute = System.Web.Http.HttpGetAttribute;

namespace LVNBA_1.Controllers
{
    [System.Web.Http.Route("api/test")]
    public class TestController : ApiController
    {
        readonly SeasonService serv = new SeasonService();

        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult TestAsync()
        {
            //int year = 2019;
            //serv.CreateNewSeasonAsync(year);
            return Ok();
        }
    }
}
