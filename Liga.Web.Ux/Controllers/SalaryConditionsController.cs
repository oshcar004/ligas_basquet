﻿
using Liga.Domain.Entities;
using Liga.Infrastructure.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace Liga.Web.UI.Controllers
{
    [Authorize(Roles = "Administrador, Comisionado")]
    public class SalaryConditionsController : BaseController
    {
        private Liga_DBContext db = new Liga_DBContext();

        //
        // GET: /SalaryConditions/

        public ActionResult Index()
        {
            return View(db.SalaryConditions.ToList());
        }

        //
        // GET: /SalaryConditions/Details/5

        public ActionResult Details(int id = 0)
        {
            SalaryConditions salaryconditions = db.SalaryConditions.Find(id);
            if (salaryconditions == null)
            {
                return HttpNotFound();
            }
            return View(salaryconditions);
        }

        //
        // GET: /SalaryConditions/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /SalaryConditions/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SalaryConditions salaryconditions)
        {
            if (ModelState.IsValid)
            {
                db.SalaryConditions.Add(salaryconditions);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(salaryconditions);
        }

        //
        // GET: /SalaryConditions/Edit/5

        public ActionResult Edit(int id = 0)
        {
            SalaryConditions salaryconditions = db.SalaryConditions.Find(id);
            if (salaryconditions == null)
            {
                return HttpNotFound();
            }
            return View(salaryconditions);
        }

        //
        // POST: /SalaryConditions/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(SalaryConditions salaryconditions)
        {
            if (ModelState.IsValid)
            {
                db.Entry(salaryconditions).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(salaryconditions);
        }

        //
        // GET: /SalaryConditions/Delete/5

        public ActionResult Delete(int id = 0)
        {
            SalaryConditions salaryconditions = db.SalaryConditions.Find(id);
            if (salaryconditions == null)
            {
                return HttpNotFound();
            }
            return View(salaryconditions);
        }

        //
        // POST: /SalaryConditions/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SalaryConditions salaryconditions = db.SalaryConditions.Find(id);
            db.SalaryConditions.Remove(salaryconditions);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}