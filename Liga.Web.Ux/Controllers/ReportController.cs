﻿using Liga.Domain.Entities;
using Liga.Infrastructure.Data;
using Liga.Infrastructure.Data.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Transactions;
using System.Web.Mvc;
using System.Web.Security;

namespace Liga.Web.UI.Controllers
{
    [Authorize(Roles = "Manager")]
    public class ReportController : BaseController
    {
        /// <summary>
        /// Obtiene el equipo del usuario conectado
        /// </summary>
        /// <returns></returns>
        public int? GetManagerTeam(out int? idManager)
        {
            idManager = null;
            int? idTeam = null;
            if (System.Threading.Thread.CurrentPrincipal.Identity.IsAuthenticated)
            {
                if (Roles.IsUserInRole("Manager"))
                {
                    string username = System.Threading.Thread.CurrentPrincipal.Identity.Name.ToString();

                    UserProfile user = db.UserProfiles.First(u => u.UserName == username);
                    Managers managers = db.Managers.FirstOrDefault(p => p.UserId == user.UserId);
                    idManager = managers.Id;
                    if (managers != null && managers.ManagersFranchises.Where(x => x.IdManager == managers.Id && x.EndDate == null).FirstOrDefault() != null)
                    {
                        idTeam = db.LVNBAFranchises.Where(x => x.IdManager == managers.Id).First().Id;
                    }

                }
            }
            return idTeam;
        }

        public bool bTienePermisosParaLaAccion(Games game,int iEstadoRequerido)
        {
            int? idManager = null;
            int? idTeam = GetManagerTeam(out idManager);
            if (game.IdTeamAway != idTeam && game.IdTeamLocal != idTeam) //no puedes reportar si no eres manager de uno de los dos equipos
            {
                return false;
            }
            if (game.state != iEstadoRequerido)
                return false;

            return true;
        }

        //
        // GET: /Report/
        private Liga_DBContext db = new Liga_DBContext();

        [Authorize(Roles = "Manager")]
        public ActionResult Index()
        {
            ViewBag.IdTeam1 = new SelectList(db.LVNBAFranchises.OrderBy(x => x.Name), "Id", "Name");
            ViewBag.IdTeam2 = new SelectList(db.LVNBAFranchises.OrderBy(x => x.Name), "Id", "Name");
            int? idManager = null;
            int iSeason = db.Parameters.First().Temporada;
            int? idTeam = GetManagerTeam(out idManager);
            ViewBag.Manager = idManager;
            if (idTeam != null)
            {
                var gamesTeam = db.Games.Where(x => x.season == iSeason && (x.IdTeamLocal == idTeam.Value || x.IdTeamAway == idTeam.Value)).OrderByDescending(x => x.gameDate);
                return View(gamesTeam.ToList());
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

        }

        [Authorize(Roles = "Administrador")]
        public ActionResult AdminReports()
        {
            ViewBag.IdTeam1 = new SelectList(db.LVNBAFranchises.OrderBy(x => x.Name), "Id", "Name");
            int iSeason = db.Parameters.First().Temporada;

            var gamesTeam = db.Games.Where(x => x.season == iSeason && x.gameDate != null).OrderByDescending(x => x.gameDate).ToList();
            return View(gamesTeam);

        }

        #region Reporte Inicial
        public ActionResult Report(int idGame = 0)
        {
            Games game = db.Games.Find(idGame);
            if (game == null)
            {
                return HttpNotFound();
            }


            ViewBag.mvpPlayer = new SelectList(db.Players.Where(x => x.ActualTeam == game.IdTeamLocal || x.ActualTeam == game.IdTeamAway && (x.Injuried == null || x.Injuried == 0)).OrderBy(x => x.ActualTeam).ThenBy(x => x.Name), "Id", "AbrevName", "");
            ViewBag.ManmvpPlayer = new SelectList(db.Players.Where(x => x.ActualTeam == game.IdTeamLocal || x.ActualTeam == game.IdTeamAway && (x.Injuried == null || x.Injuried == 0)).OrderBy(x => x.ActualTeam).ThenBy(x => x.Name), "Id", "AbrevName", "");
            

            ViewBag.EquipoLocal = db.Games.Where(x => x.Id == idGame).First().LVNBAFranchisLocal.Name;
            ViewBag.EquipoVisitante = db.Games.Where(x => x.Id == idGame).First().LVNBAFranchisAway.Name;


            return View(game);
        }

        [HttpPost]
        [Authorize(Roles = "Manager")]
        public ActionResult Report(Games g, FormCollection r)
        {
            int? idManager = null;
            bool bEsEquipoLocal = true;
            var valoracion = r.GetValues("valora1");
            var bQueja = r.GetValues("chQueja");
            var sMotivo = r.GetValues("idMotivo");

            var CPU_MVP = r.GetValues("mvpPlayer");
            var MAN_MVP = r.GetValues("ManmvpPlayer");

            ViewBag.EquipoLocal = db.LVNBAFranchises.Where(x => x.Id == g.IdTeamLocal).First().Name;
            ViewBag.EquipoVisitante = db.LVNBAFranchises.Where(x => x.Id == g.IdTeamAway).First().Name;
            ViewBag.ManagerLocal = db.LVNBAFranchises.Where(x => x.Id == g.IdTeamLocal).First().IdManager;
            ViewBag.ManagerVisitante = db.LVNBAFranchises.Where(x => x.Id == g.IdTeamAway).First().IdManager;

            ViewBag.mvpPlayer = new SelectList(db.Players.Where(x => x.ActualTeam == g.IdTeamLocal || x.ActualTeam == g.IdTeamAway).OrderBy(x => x.FirstName), "Id", "AbrevName", "");
            ViewBag.ManmvpPlayer = new SelectList(db.Players.Where(x => x.ActualTeam == g.IdTeamLocal || x.ActualTeam == g.IdTeamAway).OrderBy(x => x.FirstName), "Id", "AbrevName", "");
            bool bValid = true;
            List<string> listaErrores = new List<string>();
            int iSeason = db.Parameters.First().Temporada;
            int? idTeam = GetManagerTeam(out idManager);
            ViewBag.Manager = idManager;
            if (idTeam == null)
            {
                return RedirectToAction("Index", "Home");

            }

            if (idTeam == g.IdTeamLocal)
                bEsEquipoLocal = true;
            if (idTeam == g.IdTeamAway)
                bEsEquipoLocal = false;

            if (ModelState.IsValid)
            {
                if (g.IdTeamAway != idTeam && g.IdTeamLocal != idTeam)
                {
                    bValid = false;
                    listaErrores.Add("No puedes reportar un partido si no eres manager de uno de los dos equipos ");
                }
                if (g.idScoreLocal == g.idScoreAway)
                {
                    bValid = false;
                    listaErrores.Add("No puede haber empate  ");
                }
                if (g.idScoreLocal < 50 || g.idScoreAway < 50)
                {
                    bValid = false;
                    listaErrores.Add("No puede ser un resultado inferior a 50 ");
                }
                if (g.idScoreLocal > 160 || g.idScoreAway > 160)
                {
                    bValid = false;
                    listaErrores.Add("No puede ser un resultado superior a 160  ");
                }
                if (valoracion == null)
                {
                    bValid = false;
                    listaErrores.Add("La valoración es obligatoria  ");
                }
                if (Convert.ToBoolean(bQueja[0]) == true && Convert.ToString(sMotivo[0]) == "")
                {
                    bValid = false;
                    listaErrores.Add("Si pones queja, el motivo es obligatorio  ");
                }
                if (Convert.ToString(CPU_MVP[0]) == "")
                {
                    bValid = false;
                    listaErrores.Add("Debes poner el MVP de la CPU  ");
                }

                if (Convert.ToString(MAN_MVP[0]) == "")
                {
                    bValid = false;
                    listaErrores.Add("Debes elegir un MVP  ");
                }

                if (Convert.ToBoolean(bQueja[0]) == true && Convert.ToString(sMotivo[0]) == "")
                {
                    bValid = false;
                    listaErrores.Add("Si pones queja, el motivo es obligatorio  ");
                }


                if (bValid == false)
                {
                    foreach (string sError in listaErrores)
                    {
                        ModelState.AddModelError("", sError);
                    }
                    return View(g);
                }

                var scope = new TransactionScope(
                    // a new transaction will always be created
                        TransactionScopeOption.RequiresNew,
                    // we will allow volatile data to be read during transaction
                        new TransactionOptions()
                        {
                            IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                        }
                );
                using (scope)
                {
                    if (bEsEquipoLocal)
                    {
                        g.lQueja = Convert.ToBoolean(bQueja[0]);
                        g.lMotivo = Convert.ToString(sMotivo[0]);
                        g.lVal = Convert.ToInt32(valoracion[0]);
                        g.lidManager = ViewBag.ManagerLocal;
                        g.aidManager = ViewBag.ManagerVisitante;
                        g.lMVP = Convert.ToInt32(MAN_MVP[0]);
                    }
                    else
                    {
                        g.aQueja = Convert.ToBoolean(bQueja[0]);
                        g.aMotivo = Convert.ToString(sMotivo[0]);
                        g.aVal = Convert.ToInt32(valoracion[0]);
                        g.lidManager = ViewBag.ManagerLocal;
                        g.aidManager = ViewBag.ManagerVisitante;
                        g.aMVP = Convert.ToInt32(MAN_MVP[0]);

                    }
                    g.cMVP  = Convert.ToInt32(CPU_MVP[0]);
                    g.gameDate = DateTime.Now;
                    g.state = 0;
                    g.season = iSeason;
                    g.regularseason = db.Parameters.First().RegularSeason;
                    g.idleague = 1;
                    db.Entry(g).State = EntityState.Modified;
                    db.SaveChanges();

                    //estadisiticas de los que juegan solo
                    Jugadores jug = new Jugadores();
                    List<Players>listaRoster = jug.getPlayersRoster(g.IdTeamAway).ToList();
                    listaRoster.AddRange(jug.getPlayersRoster(g.IdTeamLocal).ToList());
                    try
                    {
                        var list = new List<PlayersStatsGames>();
                        foreach (Players player in listaRoster.Where(x => x.Injuried == 0 || x.Injuried == null))
                        {
                            PlayersStatsGames plSG = new PlayersStatsGames
                            {
                                idGame = g.Id,
                                idPlayer = player.Id,
                                idTeam = player.ActualTeam.Value
                            };
                            //ponemos todo lo demas a 0 pq sino da problemas ne las queries
                            plSG.asss = plSG.blks = plSG.f3a = plSG.f3m = plSG.fga = plSG.fgm = plSG.fols = plSG.fta = plSG.ftm = plSG.plusminus = plSG.pnts = plSG.rdefs = plSG.rebs = plSG.rofs = plSG.segs = plSG.stls = plSG.trns = plSG.starter = 0;
                            plSG.Game = null;
                            list.Add(plSG);
                        }

                        foreach(var pl in listaRoster.Where(x => x.Injuries.Where(d => d.GamesRest > 0).Count() > 0))
                        {
                            if(pl.Injuries.FirstOrDefault(x => x.GamesRest > 0).GamesRest > 0)
                            {
                                var le = pl.Injuries.FirstOrDefault();
                                le.GamesRest -= 1;
                                if(le.GamesRest == 0)
                                {
                                    var player = db.Players.Find(pl.Id);
                                    player.Injuried = 0;
                                    db.Entry(player).State = EntityState.Modified;
                                }
                                
                                var inj = db.Injuries.Find(le.Id);
                                inj.GamesRest = le.GamesRest;
                                db.Entry(inj).State = EntityState.Modified;
                            }
                        }
                        db.PlayersStatsGames.AddRange(list);
                        db.SaveChanges();
                        scope.Complete();
                    }
                    catch (Exception ex)
                    {
                        ModelState.AddModelError("", "No se puede reportar. Error:" + ex.Message);
                        return View(g);
                    }
                   
                }

                var gamesTeam = db.Games.Where(x => x.season == iSeason && (x.IdTeamLocal == idTeam.Value 
                || x.IdTeamAway == idTeam.Value)).OrderByDescending(x => x.gameDate);
                return View("Index", gamesTeam.ToList());
            }
            else
            {
                ModelState.AddModelError("", "No se puede reportar. Comprueba la fecha y el valor en los resultados." + System.Environment.NewLine);

                return View(g);
            }
        }

        #endregion


        #region Manager

        [Authorize(Roles = "Manager")]
        public ActionResult InjuryReport(int id = 0)
        {
            Games game = db.Games.Find(id);
            ViewBag.Fecha2K = game.date2k.Value.ToShortDateString();
            if (game == null)
            {
                return HttpNotFound();
            }

            ViewBag.EquipoLocal = db.LVNBAFranchises.Where(x => x.Id == game.IdTeamLocal).First().Abbreviation;
            ViewBag.EquipoVisitante = db.LVNBAFranchises.Where(x => x.Id == game.IdTeamAway).First().Abbreviation;
            ViewBag.EquipoLocalId = game.IdTeamLocal;
            ViewBag.EquipoVisitanteId = game.IdTeamAway;
            ViewBag.GameId = id;
            ViewBag.IconoVisitante = db.LVNBAFranchises.Where(x => x.Id == game.IdTeamAway).First().Icono;
            ViewBag.IconoLocal = db.LVNBAFranchises.Where(x => x.Id == game.IdTeamLocal).First().Icono;
            if (!bTienePermisosParaLaAccion(game,0))
            {
                return RedirectToAction("Index", "Home");
            }
            IEnumerable<PlayersStatsGames> plStatsList = db.PlayersStatsGames.Where(x => x.idGame == id);
            plStatsList = plStatsList.Where(x => x.Player.Injuries.Where(i => i.GamesRest > 0).Count() == 0);
            if (plStatsList == null)
            {
                return HttpNotFound();
            }
            return View(plStatsList);
        }

        [HttpPost]
        [Authorize(Roles = "Manager")]
        public ActionResult InjuryReport(FormCollection r)
        {

            int? idManager = null;
            var idPartido = r.GetValues("idPartido");
            int iGame = Convert.ToInt32(idPartido[0]);
            var TotalMinutosLocal = r.GetValues("sumLocal");
            var TotalMinutosVisitante = r.GetValues("sumVisitante");
            if (TotalMinutosLocal[0] == "" || TotalMinutosVisitante[0] == "")
            {
                ModelState.AddModelError("", "Debes rellenar los minutos antes de generar las lesiones!");
                IEnumerable<PlayersStatsGames> plStatsList = db.PlayersStatsGames.Where(x => x.idGame == iGame);
                ViewBag.EquipoLocal = db.Games.Where(x => x.Id == iGame).First().IdTeamLocal;
                ViewBag.EquipoVisitante = db.Games.Where(x => x.Id == iGame).First().IdTeamAway;
                return View(plStatsList);
            }
            int iTotalMinutosLocal = Convert.ToInt32(TotalMinutosLocal[0]);
            int iTotalMinutosVisitante = Convert.ToInt32(TotalMinutosVisitante[0]);



            if (iTotalMinutosLocal < 240 || iTotalMinutosVisitante < 240)
            {
                ModelState.AddModelError("", "Los minutos de cada equipo deben sumar almenos 240 en total!");
                IEnumerable<PlayersStatsGames> plStatsList = db.PlayersStatsGames.Where(x => x.idGame == iGame);
                ViewBag.EquipoLocal = db.Games.Where(x => x.Id == iGame).First().IdTeamLocal;
                ViewBag.EquipoVisitante = db.Games.Where(x => x.Id == iGame).First().IdTeamAway;
                return View(plStatsList);
            }
            else
            {
                List<JugadorMinuto> listaPlayers = new List<JugadorMinuto>();
                List<Players> playerList = new List<Players>();
                for (int i = 0; i < r.Count; i++)
                {
                    if (r.GetKey(i).ToString().StartsWith("p")) //es un jugador
                    {
                        string idPlayer = r.GetKey(i).ToString().Substring(1);
                        var Minutos = r.GetValues(i);
                        int iMinutos = 0;
                        if (Minutos[0] != "")
                            iMinutos = Convert.ToInt32(Minutos[0]);

                        JugadorMinuto jM = new JugadorMinuto
                        {
                            Minuto = iMinutos,
                            Player = idPlayer
                        };
                        Players player = db.Players.Find(Convert.ToInt32(idPlayer));
                        if (player != null && iMinutos > 0)
                            playerList.Add(player);
                        listaPlayers.Add(jM);
                    }
                }
                // GeneraLesiones();
                Boolean errorInjuryPlayer = false;
                string msj = "";
                foreach (var plyr in playerList)
                {
                    if (plyr.Injuries.Count > 0) // Si no es null, esta lesionado
                    {
                        msj = "No puede sumar minutos al jugador '"+ plyr.FirstName + "' porque esta lesionado.";
                        errorInjuryPlayer = true;
                        break;
                    }
                }

                if (errorInjuryPlayer)
                {
                    ModelState.AddModelError("", msj);
                    IEnumerable<PlayersStatsGames> plStatsL = db.PlayersStatsGames.Where(x => x.idGame == iGame);
                    ViewBag.EquipoLocal = db.Games.Where(x => x.Id == iGame).First().IdTeamLocal;
                    ViewBag.EquipoVisitante = db.Games.Where(x => x.Id == iGame).First().IdTeamAway;
                    return View(plStatsL);
                }

                ReportarMinutos(listaPlayers, iGame);
                return RedirectToAction("Index", "Report");
            }
        }

        /// <summary>
        /// Borrar el reporte desde la fase inicial
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = "Manager")]
        public ActionResult DeleteF1(int id = 0)
        {
            Games game = db.Games.Find(id);

            if (game == null)
            {
                return HttpNotFound();
            }

            if (!bTienePermisosParaLaAccion(game, 0))
            {
                return RedirectToAction("Index", "Home");
            }

            return View(game);
        }

        [HttpPost, ActionName("DeleteF1")]
        [Authorize(Roles = "Manager")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteF1Confirmed(int id)
        {

            Games game = db.Games.Find(id);
            game = ReseteaObjetoGame(game);

            game.PlayersStatsGames.Clear();

            db.Entry(game).State = EntityState.Modified;
            db.SaveChanges();


            return RedirectToAction("Index");
        }

        #endregion

        #region Administrador

        [Authorize(Roles = "Administrador")]
        public ActionResult AdminInjuryReport(int id = 0)
        {
            ViewBag.EquipoLocal = db.Games.Where(x => x.Id == id).First().IdTeamLocal;
            ViewBag.EquipoVisitante = db.Games.Where(x => x.Id == id).First().IdTeamAway;

            IEnumerable<PlayersStatsGames> plStatsList = db.PlayersStatsGames.Where(x => x.idGame == id);
            if (plStatsList == null)
            {
                return HttpNotFound();
            }
            return View(plStatsList);
        }

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        public ActionResult AdminInjuryReport(FormCollection r)
        {

            int? idManager = null;
            var idPartido = r.GetValues("idPartido");
            int iGame = Convert.ToInt32(idPartido[0]);
            var TotalMinutosLocal = r.GetValues("sumLocal");
            var TotalMinutosVisitante = r.GetValues("sumVisitante");
            if (TotalMinutosLocal[0] == "" || TotalMinutosVisitante[0] == "")
            {
                ModelState.AddModelError("", "Debes rellenar los minutos antes de generar las lesiones!");
                IEnumerable<PlayersStatsGames> plStatsList = db.PlayersStatsGames.Where(x => x.idGame == iGame);
                ViewBag.EquipoLocal = db.Games.Where(x => x.Id == iGame).First().IdTeamLocal;
                ViewBag.EquipoVisitante = db.Games.Where(x => x.Id == iGame).First().IdTeamAway;
                return View(plStatsList);
            }
            int iTotalMinutosLocal = Convert.ToInt32(TotalMinutosLocal[0]);
            int iTotalMinutosVisitante = Convert.ToInt32(TotalMinutosVisitante[0]);



            if (iTotalMinutosLocal < 240 || iTotalMinutosVisitante < 240)
            {
                ModelState.AddModelError("", "Los minutos de cada equipo deben sumar almenos 240 en total!");
                IEnumerable<PlayersStatsGames> plStatsList = db.PlayersStatsGames.Where(x => x.idGame == iGame);
                ViewBag.EquipoLocal = db.Games.Where(x => x.Id == iGame).First().IdTeamLocal;
                ViewBag.EquipoVisitante = db.Games.Where(x => x.Id == iGame).First().IdTeamAway;
                return View(plStatsList);
            }
            else
            {
                List<JugadorMinuto> listaPlayers = new List<JugadorMinuto>();
                for (int i = 0; i < r.Count; i++)
                {
                    if (r.GetKey(i).ToString().StartsWith("p")) //es un jugador
                    {
                        string idPlayer = r.GetKey(i).ToString().Substring(1);
                        var Minutos = r.GetValues(i);
                        int iMinutos = 0;
                        if (Minutos[0] != "")
                            iMinutos = Convert.ToInt32(Minutos[0]);

                        JugadorMinuto jM = new JugadorMinuto();
                        jM.Minuto = iMinutos;
                        jM.Player = idPlayer;
                        listaPlayers.Add(jM);
                    }
                }
                ReportarMinutos(listaPlayers, iGame);

                // GeneraLesiones();
                ViewBag.IdTeam1 = new SelectList(db.LVNBAFranchises.OrderBy(x => x.Name), "Id", "Name");
                ViewBag.IdTeam2 = new SelectList(db.LVNBAFranchises.OrderBy(x => x.Name), "Id", "Name");

                int iSeason = db.Parameters.First().Temporada;

                int? idTeam = GetManagerTeam(out idManager);
                ViewBag.Manager = idManager;
                ViewBag.EquipoLocal = db.Games.Where(x => x.Id == iGame).First().IdTeamLocal;
                ViewBag.EquipoVisitante = db.Games.Where(x => x.Id == iGame).First().IdTeamAway;
                IEnumerable<PlayersStatsGames> plStatsList = db.PlayersStatsGames.Where(x => x.idGame == iGame);
                return View(plStatsList);
            }

        }

        /// <summary>
        /// Borrar el reporte desde la fase inicial
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = "Administrador")]
        public ActionResult AdminDeleteF1(int id = 0)
        {
            Games game = db.Games.Find(id);

            if (game == null)
            {
                return HttpNotFound();
            }
            return View(game);
        }

        /// <summary>
        /// Borra el reporte en fases mas avanzadas
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = "Administrador")]
        public ActionResult AdminDeleteF2(int id = 0)
        {
            Games game = db.Games.Find(id);

            if (game == null)
            {
                return HttpNotFound();
            }
            return View(game);
        }


        [HttpPost, ActionName("AdminDeleteF1")]
        [Authorize(Roles = "Administrador")]
        [ValidateAntiForgeryToken]
        public ActionResult AdminDeleteF1Confirmed(int id)
        {
            Games game = db.Games.Find(id);
            game = ReseteaObjetoGame(game);

            game.PlayersStatsGames.Clear();

            db.Entry(game).State = EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        [HttpPost, ActionName("AdminDeleteF2")]
        [Authorize(Roles = "Administrador")]
        [ValidateAntiForgeryToken]
        public ActionResult AdminDeleteF2Confirmed(int id)
        {
            var scope = new TransactionScope(
                // a new transaction will always be created
                        TransactionScopeOption.RequiresNew,
                // we will allow volatile data to be read during transaction
                        new TransactionOptions()
                        {
                            IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                        }
            );
            using (scope)
            {

                Games game = db.Games.Find(id);
                game = ReseteaObjetoGame(game);

                game.PlayersStatsGames.Clear();
                db.Entry(game).State = EntityState.Modified;

                //--si un partido se borra (después de generar lesiones):
                //--si un lesionado existe con idgame del partido que se borra, significa que no debio estar lesionado (ya que se lesiono en ese partido) y se pone gamerest a 0
                List<Injuries> listaLesionadosEnEsePartido = db.Injuries.Where(x => x.IdGame == id).ToList();
                foreach (Injuries inj in listaLesionadosEnEsePartido)
                {
                    Injuries injur = db.Injuries.Where(x => x.IdPlayer == inj.IdPlayer && x.IdGame == inj.IdGame).First();
                    injur.GamesRest = 0;
                    db.Entry(injur).State = EntityState.Modified;
                }
                //--si un lesionado existe con idgame en Injury-Game, significa que se le resto 1 a la lesion por lo que hay que sumarle uno al registro que tenga en Injury(si existe)
                //--o bien buscar uno con gamerest = 0(deberia en algun partido haberse lesionado) y ponerle 1
                List<InjuryGames> listaLesionadosRestadosEnEsePartido = db.InjuryGames.Where(x => x.IdGame == id).ToList();
                foreach (InjuryGames inj in listaLesionadosRestadosEnEsePartido)
                {
                    Injuries injur =  db.Injuries.Where(x => x.IdPlayer == inj.IdPlayer && x.IdGame == inj.IdGame).FirstOrDefault();
                    if (injur == null) //caso raro que no existe, pero se crea
                    {
                        injur = db.Injuries.Where(x => x.IdPlayer == inj.IdPlayer && x.GamesRest == 0).FirstOrDefault();
                        if (injur == null)
                        {
                            injur = new Injuries();
                            injur.IdGame = inj.IdGame;
                            injur.GamesOut = injur.GamesRest = 1;
                            injur.IdPlayer = inj.IdPlayer;
                            injur.IdType = 0;
                            db.Injuries.Add(injur);
                            db.SaveChanges();
                        }
                        else
                        {
                            injur.GamesRest = injur.GamesRest + 1;
                            db.Entry(injur).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                    else
                    {
                        injur.GamesRest = injur.GamesRest + 1;
                        db.Entry(injur).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }
                db.SaveChanges();
                scope.Complete();
            }
            return RedirectToAction("AdminReports");
        }

        private Games ReseteaObjetoGame(Games game)
        {
            game.gameDate = null;
            game.state = null;
            game.idScoreLocal = 0;
            game.idScoreAway = 0;
            game.lQueja = false;
            game.aQueja = false;
            game.lVal = null;
            game.aVal = null;
            game.lMotivo = null;
            game.aMotivo = null;
            game.errores = null;
            game.date2k = null;
            game.aMVP = null;
            game.lMVP = null;
            game.cMVP = null;
            game.aCountGame = null;
            game.lCountGame = null;
            game.aidManager = null;
            game.lidManager = null;

            return game;
        }


        #endregion

        #region Fase edición reporte

        #region Manager

        [Authorize(Roles = "Manager")]
        public ActionResult EditReportCellEdit(int idGame = 0, int idTeam = 0)
        {
            Games game = db.Games.Find(idGame);
            if (game == null)
            {
                return HttpNotFound();
            }

            if(!bTienePermisosParaLaAccion(game,1))
            {
                return RedirectToAction("Index", "Home");
            }

            ViewBag.IdTeam = idTeam;
            ViewBag.Icono = db.LVNBAFranchises.Where(x => x.Id == idTeam).First().Icono;

            return View(game);
        }

        [HttpPost]
        [Authorize(Roles = "Manager")]
        public ActionResult ValidateReport(int idGame = 0, int idTeam = 0)
        {
            List<ErrorReporte> listaErrores = new List<ErrorReporte>();
            try
            {
                dameErroresEquipoReporte(idGame, idTeam, ref listaErrores);
                dameErroresJugadoresReporte(idGame, idTeam, ref listaErrores);
                damePosibleErroresJugadoresReporte(idGame, idTeam, ref listaErrores);
                return Json(listaErrores.ToList());
            }
            catch (Exception ex)
            {
                ErrorReporte err = new ErrorReporte();
                err.Mensaje = "No se ha podido validar. Ha habido un error. Comuníquelo a un administrador. ";
                err.Color = "red";
                listaErrores.Clear();
                listaErrores.Add(err);
                return Json(listaErrores.ToList());
            }

        }

        #endregion

        #region Administrador

        [Authorize(Roles = "Administrador")]
        public ActionResult AdminEditReportCellEdit(int idGame = 0, int idTeam = 0)
        {
            ViewBag.IdTeam = idTeam;
            ViewBag.Icono = db.LVNBAFranchises.Where(x => x.Id == idTeam).First().Icono;
            Games Game = db.Games.Where(x => x.Id == idGame).First();
            return View(Game);
        }

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        public ActionResult AdminValidateReport(int idGame = 0)
        {
            List<ErrorReporte> listaErrores = new List<ErrorReporte>();
            try
            {
                Games game = db.Games.Find(idGame);
                if (game == null)
                {
                    return HttpNotFound();
                }
                dameErroresEquipoReporte(idGame, game.IdTeamAway, ref listaErrores);
                dameErroresJugadoresReporte(idGame, game.IdTeamAway, ref listaErrores);
                damePosibleErroresJugadoresReporte(idGame, game.IdTeamAway, ref listaErrores);
                ErrorReporte err = new ErrorReporte
                {
                    Mensaje = "<hr/>",
                    Color = "white"
                };
                listaErrores.Add(err);
                dameErroresEquipoReporte(idGame, game.IdTeamLocal, ref listaErrores);
                dameErroresJugadoresReporte(idGame, game.IdTeamLocal, ref listaErrores);
                damePosibleErroresJugadoresReporte(idGame, game.IdTeamLocal, ref listaErrores);

                if (listaErrores.Count() == 1) //no hay errores posibles, solo la linea de separacion blanca
                {
                    game.errores = 0;
                    db.Entry(game).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return Json(listaErrores.ToList());
            }
            catch (Exception ex)
            {  
                listaErrores.Clear();
                ErrorReporte err = new ErrorReporte();
                err.Mensaje = "";
                err.Color = "white";
              
                listaErrores.Add(err);
                err = new ErrorReporte();
                err.Mensaje = "No se ha podido validar. Ha habido un error. Comuníquelo a un administrador. " + System.Environment.NewLine + ex.ToString();
                err.Color = "red";
            
                listaErrores.Add(err);
                return Json(listaErrores.ToList());
            }

        }

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        public ActionResult NotifyError(int idGame = 0)
        {
            Games game = db.Games.Find(idGame);
            if (game == null)
            {
                return HttpNotFound();
            }

            game.errores = 1;
            db.Entry(game).State = EntityState.Modified;
            db.SaveChanges();
            return Json(true);

        }



        #endregion

        #endregion

        #region Acciones

        [HttpPost]

        public ActionResult GetInjuriesOfGame(int idGame = 0)
        {
            var game = db.Games.FirstOrDefault(x => x.Id == idGame);
            List<FlashInjuryReport> listaLesiones = new List<FlashInjuryReport>();
            List<Injuries> InjuryList = db.Injuries.Where(x => (x.Player.ActualTeam == game.IdTeamAway || x.Player.ActualTeam == game.IdTeamLocal) && x.GamesRest > 0).AsEnumerable().ToList();
            foreach (Injuries injury in InjuryList)
            {
                FlashInjuryReport lesion = new FlashInjuryReport();
                lesion.sLogo = injury.Player.LVNBAFranchis.Icono;
                lesion.idPlayer = injury.IdPlayer;
                lesion.sCompleteName = injury.Player.AbrevName;
                lesion.iGamesRest = injury.GamesRest;
                listaLesiones.Add(lesion);
            }
            return Json(listaLesiones.ToList());
        }

        public void ReportarMinutos(List<JugadorMinuto> minutosJugador, int idGame)
        {
            var scope = new TransactionScope(
                // a new transaction will always be created
                          TransactionScopeOption.RequiresNew,
                // we will allow volatile data to be read during transaction
                          new TransactionOptions()
                          {
                              IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                          }
                  );
            using (scope)
            {
                foreach (JugadorMinuto jM in minutosJugador)
                {
                    int idPlayer = Convert.ToInt32(jM.Player);
                    PlayersStatsGames plstG = db.PlayersStatsGames.Where(x => x.idGame == idGame && x.idPlayer == idPlayer).First();
                    if (plstG != null)
                    {
                        plstG.segs = jM.Minuto;
                        db.Entry(plstG).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }

                Games theGame = db.Games.Where(x => x.Id == idGame).First();
                theGame.state = 1;
                db.Entry(theGame).State = EntityState.Modified;
                db.SaveChanges();
                int idAway = theGame.IdTeamAway;
                int idHome = theGame.IdTeamLocal;
                //generamos las lesiones
                List<Injuries> listaLesionados = new List<Injuries>();

                //obtengo los jugadores de ambos equipos
                Jugadores jug = new Jugadores();

                List<Players> lista = jug.getPlayersRoster(idAway).ToList();
                lista.AddRange(jug.getPlayersRoster(idHome).ToList());
                List<int> recorrido = new List<int>();

                foreach (Players player in lista)
                {
                    if (!recorrido.Contains(player.Id))
                    {
                        recorrido.Add(player.Id);
                        Lesiones les = new Lesiones();
                        //descontamos partidos de lesion a los jugadores que no han jugado y estan lesionados
                        if (minutosJugador.Where(x => x.Player == player.Id.ToString() && x.Minuto == 0).FirstOrDefault() != null) //si es un jugador que no ha jugado en este partido
                        {
                            Injuries jugLesion = db.Injuries.Where(x => x.IdPlayer == player.Id && x.GamesRest > 0).FirstOrDefault();
                            if (jugLesion != null)
                            {
                                Players jugador = jugLesion.Player;
                                jugLesion.GamesRest -= 1;
                                db.Entry(jugLesion).State = EntityState.Modified;
                                db.SaveChanges();
                                if (jugLesion.GamesRest == 0) // Se termina la lesion
                                {
                                    jugador.Injuried = 0; // Se informa que el jugador ya no esta lesionado
                                    db.Entry(jugador).State = EntityState.Deleted;
                                    db.SaveChanges();
                                }
                                else // Se perdio este partido
                                {
                                    InjuryGames injuryGame = new InjuryGames()
                                    {
                                        IdGame = idGame,
                                        IdPlayer = jugLesion.Player.Id
                                    };
                                    db.InjuryGames.Add(injuryGame);
                                    db.SaveChanges();
                                }
                            }
                        }
                        IEnumerable<JugadorMinuto> jugado = minutosJugador.Where(x => x.Player == player.Id.ToString());
                        Injuries injury = null;
                        if (jugado != null && jugado.Count() > 0)
                            injury = les.SeLesiona(player.Id, jugado.First(), db);
                        if (injury != null) //si no devuelve nulo es que hay lesion
                        {
                            injury.IdGame = idGame;
                            db.Injuries.Add(injury);
                            listaLesionados.Add(injury);

                            injury.Player.Injuried = 1; // Se informa que el jugador esta lesionado
                            db.Entry(injury.Player).State = EntityState.Modified;
                        }
                    }
                }
                db.SaveChanges();
                scope.Complete();
            }
        }

        [HttpPost]
        public ActionResult SaveReportCell(int idGame, FormCollection postData)
        {
            bool isValidNumber = true;
            int iResult;
            bool success = true;
            int idPlayer = Convert.ToInt32(postData["id"]);
            PlayersStatsGames plStatGame = db.PlayersStatsGames.Where(x => x.idPlayer == idPlayer && x.idGame == idGame).First();
            if (postData[1].ToString() == "")
            {
                iResult = 0;
            }
            else
            {
                isValidNumber = int.TryParse(postData[1], out iResult);
            }

            bool bFinded = false;

            if (isValidNumber)
            {
                if (plStatGame != null)
                {
                    switch (postData[0]) //el nombre
                    {
                        case "MIN":
                            plStatGame.segs = iResult;
                            bFinded = true;
                            break;
                        case "REBS":
                            plStatGame.rebs = iResult;
                            plStatGame.rdefs = plStatGame.rebs - plStatGame.rofs;
                            bFinded = true;
                            break;
                        case "AST":
                            plStatGame.asss = iResult;
                            bFinded = true;
                            break;
                        case "PNTS":
                            plStatGame.pnts = iResult;
                            bFinded = true;
                            break;
                        case "ROB":
                            plStatGame.stls = iResult;
                            bFinded = true;
                            break;
                        case "TAP":
                            plStatGame.blks = iResult;
                            bFinded = true;
                            break;
                        case "PER":
                            plStatGame.trns = iResult;
                            bFinded = true;
                            break;
                        case "TCM":
                            plStatGame.fgm = iResult;
                            bFinded = true;
                            break;
                        case "TCA":
                            plStatGame.fga = iResult;
                            bFinded = true;
                            break;
                        case "C3TCM":
                            plStatGame.f3m = iResult;
                            bFinded = true;
                            break;
                        case "C3TCA":
                            plStatGame.f3a = iResult;
                            bFinded = true;
                            break;
                        case "TLM":
                            plStatGame.ftm = iResult;
                            bFinded = true;
                            break;
                        case "TLA":
                            plStatGame.fta = iResult;
                            bFinded = true;
                            break;
                        case "RO":
                            plStatGame.rofs = iResult;
                            plStatGame.rdefs = plStatGame.rebs - iResult;
                            bFinded = true;
                            break;
                        case "FP":
                            plStatGame.fols = iResult;
                            bFinded = true;
                            break;
                    }
                    if (bFinded)
                    {
                        db.Entry(plStatGame).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    return Json(success);
                }
                else
                {
                    return Json(false);
                }
            }
            else
                return Json(false);


        }

        [HttpPost]
        public ActionResult SaveStarter(int idGame,int idPlayer, bool check)
        {
            PlayersStatsGames plSt = db.PlayersStatsGames.Where(x => x.idGame == idGame & x.idPlayer == idPlayer).First();
            if (check)
                plSt.starter = 1;
            else
                plSt.starter = 0;

            db.SaveChanges();
            return Json(true);
        }



        #region Manager
        [Authorize(Roles = "Manager")]
        public ActionResult SetMVP(int idGame = 0, int idManager = 0)
        {
            int? idManagerConnected = null;
            Games game = db.Games.Find(idGame);
            int? idTeam = GetManagerTeam(out idManagerConnected);
            ViewBag.Manager = idManager;
            if (idManagerConnected != idManager)
            {
                return RedirectToAction("Index", "Home");
            }
            if (game == null)
            {
                return HttpNotFound();
            }

            if(game.idScoreAway > game.idScoreLocal)
            {
                ViewBag.MVPList = new SelectList(db.Players.Where(x => x.ActualTeam == game.IdTeamAway).OrderBy(x => x.ActualTeam).ThenBy(x => x.Name), "Id", "AbrevName", "");

            }
            else
            {
                ViewBag.MVPList = new SelectList(db.Players.Where(x => x.ActualTeam == game.IdTeamLocal).OrderBy(x => x.ActualTeam).ThenBy(x => x.Name), "Id", "AbrevName", "");
            }
            ViewBag.MVPCPU = game.cMVPPlayerInfo.AbrevName;

            if (idManagerConnected == game.aidManager)
            {
                ViewBag.MVPRival = game.lMVPPlayerInfo.AbrevName;

            }
            else
            {
                ViewBag.MVPRival = game.aMVPPlayerInfo.AbrevName;

            }

            return View(game);
        }

        [HttpPost]
        public ActionResult SetMVP(GameMVP g, FormCollection r)
        {
            int iSeason = db.Parameters.First().Temporada;
            int? idManagerConnected = null;

            int? idTeam = GetManagerTeam(out idManagerConnected);
            var mvpSelected = r.GetValues("MVPList");
            if (mvpSelected == null || mvpSelected[0] == "")
            {
                Games game = db.Games.Find(g.id);

                ModelState.AddModelError("", "Debes escoger un jugador.");
                if (game.idScoreAway > game.idScoreLocal)
                {
                    ViewBag.MVPList = new SelectList(db.Players.Where(x => x.ActualTeam == game.IdTeamAway).OrderBy(x => x.ActualTeam).ThenBy(x => x.Name), "Id", "AbrevName", "");

                }
                else
                {
                    ViewBag.MVPList = new SelectList(db.Players.Where(x => x.ActualTeam == game.IdTeamLocal).OrderBy(x => x.ActualTeam).ThenBy(x => x.Name), "Id", "AbrevName", "");
                }
                ViewBag.MVPCPU = game.cMVPPlayerInfo.AbrevName;

                if (idManagerConnected == game.aidManager)
                {
                    ViewBag.MVPRival = game.lMVPPlayerInfo.AbrevName;

                }
                else
                {
                    ViewBag.MVPRival = game.aMVPPlayerInfo.AbrevName;

                }
                return View(game);
            }
            else
            {
                Games game = db.Games.Find(g.id);

                if (idManagerConnected == game.aidManager)
                {
                    game.aMVP = Convert.ToInt32(mvpSelected[0]);
                }
                else
                {
                    game.lMVP = Convert.ToInt32(mvpSelected[0]);
                }
                if (game.aMVP == game.lMVP) //si ambos managers opinan lo mismo, manda sobre CPU
                    game.cMVP = game.aMVP;

                db.Entry(game).State = EntityState.Modified;
                db.SaveChanges();
                var gamesTeam = db.Games.Where(x => x.season == iSeason && (x.IdTeamLocal == idTeam.Value || x.IdTeamAway == idTeam.Value)).OrderByDescending(x => x.gameDate);

                return RedirectToAction("Index", "Report");
            }
        }

        [Authorize(Roles = "Manager")]
        public ActionResult SetVal(int idGame = 0, int idManager = 0)
        {
            int? idManagerConnected = null;
            Games game = db.Games.Find(idGame);
            int? idTeam = GetManagerTeam(out idManagerConnected);
            ViewBag.Manager = idManager;
            if (idManagerConnected != idManager)
            {
                return RedirectToAction("Index", "Home");
            }
            if (game == null)
            {
                return HttpNotFound();
            }
            return View(game);
        }

      
        [HttpPost]
        public ActionResult SetVal(GameVal g, FormCollection r)
        {
            int iSeason = db.Parameters.First().Temporada;
            int? idManagerConnected = null;
            int? idTeam = GetManagerTeam(out idManagerConnected);
            var valoracion = r.GetValues("valora1");
            if (valoracion == null)
            {
                ModelState.AddModelError("", "La valoración es obligatoria.");
                return View(g);
            }
            else
            {
                Games game = db.Games.Find(g.id);

                if (idManagerConnected == game.aidManager)
                {
                    game.aVal = Convert.ToInt32(valoracion[0]);
                }
                else
                {
                    game.lVal = Convert.ToInt32(valoracion[0]);

                }
                db.Entry(game).State = EntityState.Modified;
                db.SaveChanges();
                var gamesTeam = db.Games.Where(x => x.season == iSeason && (x.IdTeamLocal == idTeam.Value || x.IdTeamAway == idTeam.Value)).OrderByDescending(x => x.gameDate);

                return RedirectToAction("Index", "Report");
            }
        }

        [Authorize(Roles = "Manager")]
        public ActionResult AddQueja(int idGame = 0, int idManager = 0)
        {
            int? idManagerConnected = null;
            Games game = db.Games.Find(idGame);
            int? idTeam = GetManagerTeam(out idManagerConnected);
            ViewBag.Manager = idManager;
            if (idManagerConnected != idManager)
            {
                return RedirectToAction("Index", "Home");
            }
            if (game == null)
            {
                return HttpNotFound();
            }
            return View(game);
        }

        [HttpPost]
        public ActionResult AddQueja(GameQueja g, FormCollection r)
        {
            int iSeason = db.Parameters.First().Temporada;
            int? idManagerConnected = null;
            int? idTeam = GetManagerTeam(out idManagerConnected);
            var motivo = r.GetValues("aMotivo");
            if (motivo == null || motivo[0].ToString() == "")
            {
                ModelState.AddModelError("", "El motivo  es obligatorio.");
                return View(g);
            }
            else
            {
                Games game = db.Games.Find(g.id);

                if (idManagerConnected == game.aidManager)
                {
                    game.aQueja = true;
                    game.aMotivo = motivo[0].ToString().Replace("\r\n", "<br/>");
                }
                else
                {
                    game.lQueja = true;
                    game.lMotivo = motivo[0].ToString().Replace("\r\n", "<br/>");

                }
                db.Entry(game).State = EntityState.Modified;
                db.SaveChanges();
                var gamesTeam = db.Games.Where(x => x.season == iSeason && (x.IdTeamLocal == idTeam.Value || x.IdTeamAway == idTeam.Value)).OrderByDescending(x => x.gameDate);

                return RedirectToAction("Index", "Report");
            }
        }

        [Authorize(Roles = "Manager")]
        public ActionResult DeleteQueja(int idGame = 0, int idManager = 0)
        {
            int? idManagerConnected = null;
            Games game = db.Games.Find(idGame);
            int? idTeam = GetManagerTeam(out idManagerConnected);
            ViewBag.Manager = idManager;
            if (idManagerConnected != idManager)
            {
                return RedirectToAction("Index", "Home");
            }
            if (game == null)
            {
                return HttpNotFound();
            }
            return View(game);
        }

        [HttpPost]
        public ActionResult DeleteQueja(GameQueja g, FormCollection r)
        {
            int iSeason = db.Parameters.First().Temporada;
            int? idManagerConnected = null;
            int? idTeam = GetManagerTeam(out idManagerConnected);

            Games game = db.Games.Find(g.id);

            if (idManagerConnected == game.aidManager)
            {
                game.aQueja = false;
                game.aMotivo = "";
            }
            else
            {
                game.lQueja = false;
                game.lMotivo = "";

            }
            db.Entry(game).State = EntityState.Modified;
            db.SaveChanges();
            var gamesTeam = db.Games.Where(x => x.season == iSeason && (x.IdTeamLocal == idTeam.Value || x.IdTeamAway == idTeam.Value)).OrderByDescending(x => x.gameDate);

            return RedirectToAction("Index", "Report");

        }


        #endregion

        #region Administrador

        [Authorize(Roles = "Administrador")]
        public ActionResult AdminAddQueja(int idGame = 0, int idManager = 0)
        {
            ViewBag.Manager = idManager;
            Games game = db.Games.Find(idGame);
            if (game == null)
            {
                return HttpNotFound();
            }
            return View(game);
        }

        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public ActionResult AdminAddQueja(GameQueja g, FormCollection r)
        {
            int iSeason = db.Parameters.First().Temporada;
            var motivo = r.GetValues("aMotivo");
            var idManager = r.GetValues("idManager");

            if (motivo == null || motivo[0].ToString() == "")
            {
                ModelState.AddModelError("", "El motivo  es obligatorio.");
                return View(g);
            }
            else
            {
                Games game = db.Games.Find(g.id);

                if (Convert.ToInt32(idManager[0].ToString()) == game.aidManager)
                {
                    game.aQueja = true;
                    game.aMotivo = motivo[0].ToString().Replace("\r\n", "<br/>");
                }
                else
                {
                    game.lQueja = true;
                    game.lMotivo = motivo[0].ToString().Replace("\r\n", "<br/>");

                }
                db.Entry(game).State = EntityState.Modified;
                db.SaveChanges();
                var gamesTeam = db.Games.Where(x => x.season == iSeason && x.gameDate != null).OrderByDescending(x => x.gameDate);
                return RedirectToAction("AdminReports", "Report");
            }
        }


        [Authorize(Roles = "Administrador")]
        public ActionResult AdminDeleteQueja(int idGame = 0, int idManager = 0)
        {
            ViewBag.Manager = idManager;
            Games game = db.Games.Find(idGame);
            if (game == null)
            {
                return HttpNotFound();
            }
            return View(game);
        }

        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public ActionResult AdminDeleteQueja(GameQueja g, FormCollection r)
        {
            int iSeason = db.Parameters.First().Temporada;
            var idManager = r.GetValues("idManager");
            Games game = db.Games.Find(g.id);
            if (Convert.ToInt32(idManager[0].ToString()) == game.aidManager)
            {
                game.aQueja = false;
                game.aMotivo = "";
            }
            else
            {
                game.lQueja = false;
                game.lMotivo = "";

            }
            db.Entry(game).State = EntityState.Modified;
            db.SaveChanges();
            var gamesTeam = db.Games.Where(x => x.season == iSeason && x.gameDate != null).OrderByDescending(x => x.gameDate);
            return RedirectToAction("AdminReports", "Report");

        }

        [Authorize(Roles = "Administrador")]
        public ActionResult CloseGame(int idGame = 0)
        {
            Games game = db.Games.Find(idGame);
            if (game == null)
            {
                return HttpNotFound();
            }
            return View(game);
        }

        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public ActionResult CloseGame(GameVal g)
        {

            int iSeason = db.Parameters.First().Temporada;
            Games game = db.Games.Find(g.id);
            game.state = 3;
            int iGamesPlayed = getCountGameFrom(game.IdTeamAway);
            game.aCountGame = iGamesPlayed + 1;
            iGamesPlayed = getCountGameFrom(game.IdTeamLocal);
            game.lCountGame = iGamesPlayed + 1;

            db.Entry(game).State = EntityState.Modified;
            db.SaveChanges();
            var gamesTeam = db.Games.Where(x => x.season == iSeason && x.gameDate != null).OrderByDescending(x => x.gameDate);

            return RedirectToAction("AdminReports", "Report");
        }

        private int getCountGameFrom(int idTeam)
        {
            int iCount = db.Games.Where(x => (x.IdTeamLocal == idTeam || x.IdTeamAway == idTeam) && x.gameDate != null && x.state == 3).Count();
            return iCount;
            //int iMaxLocal = db.Games.Where(x => x.IdTeamLocal == idTeam && x.gameDate != null && x.state == 3).Max().lCountGame.Value;
            //int iMaxAway = db.Games.Where(x => x.IdTeamAway == idTeam && x.gameDate != null && x.state == 3).Max().aCountGame.Value;

            //if (iMaxAway > iMaxLocal)
            //    return iMaxAway;
            //else
            //    return iMaxLocal;
        }

        [Authorize(Roles = "Administrador")]
        public ActionResult OpenGame(int idGame = 0)
        {
            Games game = db.Games.Find(idGame);
            if (game == null)
            {
                return HttpNotFound();
            }
            return View(game);
        }

        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public ActionResult OpenGame(GameVal g)
        {

            int iSeason = db.Parameters.First().Temporada;
            Games game = db.Games.Find(g.id);
            game.state = 1;
            db.Entry(game).State = EntityState.Modified;
            db.SaveChanges();
            var gamesTeam = db.Games.Where(x => x.season == iSeason && x.gameDate != null).OrderByDescending(x => x.gameDate);

            return RedirectToAction("AdminReports", "Report");
        }

        #endregion
        #endregion

        #region Validaciones Reporte

        public void dameErroresEquipoReporte(int idGame, int idTeam, ref List<ErrorReporte> listaErrores)
        {
            string sImage = "<img width='20px' height='20px' src='../Images/Common/teams/Iconos/";

            Games Game = db.Games.Where(x => x.Id == idGame).First();

            int iResultado = 0;
            string logo = "";
            if (idTeam == Game.IdTeamLocal)
            {
                logo = Game.LVNBAFranchisLocal.Icono;
                iResultado = Game.idScoreLocal;

            }
            else
            {
                logo = Game.LVNBAFranchisAway.Icono;
                iResultado = Game.idScoreAway;

            }


            IQueryable<PlayersStatsGames> GameStats = db.PlayersStatsGames.Where(x => x.idGame == idGame && x.idTeam == idTeam);

            if (GameStats.Where(x => x.starter == 1).Count() != 5)
            {
                ErrorReporte err = new ErrorReporte();
                err.Mensaje = sImage + logo + "'/>" + "Deben haber cinco titulares en el equipo. ";
                err.Color = "red";
                listaErrores.Add(err);    

            }

            int ST_totalPuntosSumandoPlayers = GameStats.Sum(x => x.pnts).Value;

            int ST_totalMIN = GameStats.Sum(x => x.segs).Value;

            int ST_totalTCIntentados  = GameStats.Sum(x => x.fga).Value;   
            int ST_totalTCMetidos = GameStats.Sum(x => x.fgm).Value;

            int ST_total3TCIntentados = GameStats.Sum(x => x.f3a).Value;
            int ST_total3TCMetidos = GameStats.Sum(x => x.f3m).Value;

            int ST_totalTLIntentados = GameStats.Sum(x => x.fta).Value;
            int ST_totalTLMetidos = GameStats.Sum(x => x.ftm).Value;

            int totalPuntosContandoTiros = ((ST_totalTCMetidos - ST_total3TCMetidos) * 2) + (ST_totalTLMetidos) + (ST_total3TCMetidos * 3);

            if (ST_totalMIN < 240 || ST_totalMIN > 290)
            {
                ErrorReporte err = new ErrorReporte();
                err.Mensaje = sImage + logo + "'/>" + "La suma de los minutos del equipo es inferior a 240 o superior a 290. Revisa los minutos de los jugadores";
                err.Color = "red";
                listaErrores.Add(err);
            }
            if (ST_totalPuntosSumandoPlayers != iResultado)
            {
                ErrorReporte err = new ErrorReporte();
                err.Mensaje = sImage + logo + "'/>" + "La suma de los puntos anotados de los jugadores y el marcador no es igual. Revisa puntos de jugadores";
                err.Color = "red";
                listaErrores.Add(err);
            }
            if (totalPuntosContandoTiros != iResultado)
            {
                ErrorReporte err = new ErrorReporte();
                err.Mensaje = sImage + logo + "'/>" + "El conteo de tiros anotados de los jugadores y el marcador no es igual. Revisa los tiros de campo de los jugadores";
                err.Color = "red";
                listaErrores.Add(err);
            }
            if ( ST_totalTLIntentados < ST_totalTLMetidos)
            {
                ErrorReporte err = new ErrorReporte();
                err.Mensaje = sImage + logo + "'/>" + "Tiros libres: Más anotados que intentados. Revísalo ";
                err.Color = "red";
                listaErrores.Add(err);
            }
            if ( ST_totalTCIntentados < ST_totalTCMetidos )
            {
                ErrorReporte err = new ErrorReporte();
                err.Mensaje = sImage + logo + "'/>" + "Tiros de dos: Más anotados que intentados. Revísalo ";
                err.Color = "red";
                listaErrores.Add(err);
            }
            if (ST_total3TCIntentados < ST_total3TCMetidos)
            {
                ErrorReporte err = new ErrorReporte();
                err.Mensaje = sImage + logo + "'/>" + "Tiros de tres: Más anotados que intentados. Revísalo ";
                err.Color = "red";
                listaErrores.Add(err);
            }
   
            return;
        }

        public void dameErroresJugadoresReporte(int idGame, int idTeam, ref List<ErrorReporte> listaErrores)
        {
            string sImage = "<img width='20px' height='20px' src='../Images/Common/teams/Iconos/";
            List<PlayersStatsGames> lista = new List<PlayersStatsGames>();

            IQueryable<PlayersStatsGames> GameStats = db.PlayersStatsGames.Where(x => x.idGame == idGame && x.idTeam == idTeam);
            lista = GameStats.Where(x => x.ftm > x.fta).AsEnumerable().ToList();
            foreach (PlayersStatsGames pl in lista)
            {
                ErrorReporte err = new ErrorReporte();
                err.Mensaje = pl.Player.CompleteName + sImage + pl.LVNBAFranchis.Icono + "'/>" + "(" + pl.LVNBAFranchis.Abbreviation + ") tiene más tiros libres anotados que intentados. Revísalo";
                err.Color = "red";
                listaErrores.Add(err);
            }
            lista = GameStats.Where(x => x.fgm > x.fga).AsEnumerable().ToList();
            foreach (PlayersStatsGames pl in lista)
            {
                ErrorReporte err = new ErrorReporte();
                err.Mensaje = pl.Player.CompleteName + sImage + pl.LVNBAFranchis.Icono + "'/>" + " (" + pl.LVNBAFranchis.Abbreviation + ") tiene más tiros de campo anotados que intentados. Revísalo";
                err.Color = "red";
                listaErrores.Add(err);
            }
            lista = GameStats.Where(x => x.f3m > x.f3a).AsEnumerable().ToList();
            foreach (PlayersStatsGames pl in lista)
            {
                ErrorReporte err = new ErrorReporte();
                err.Mensaje = pl.Player.CompleteName + sImage + pl.LVNBAFranchis.Icono + "'/>" + " (" + pl.LVNBAFranchis.Abbreviation + ") tiene más tiros de tres anotados que intentados. Revísalo";
                err.Color = "red";
                listaErrores.Add(err);
            }

            lista = GameStats.Where(x => x.f3m > x.fgm || x.f3a > x.fga).AsEnumerable().ToList();
            foreach (PlayersStatsGames pl in lista)
            {
                ErrorReporte err = new ErrorReporte();
                err.Mensaje = pl.Player.CompleteName + sImage + pl.LVNBAFranchis.Icono + "'/>" + " (" + pl.LVNBAFranchis.Abbreviation + ") tiene más tiros de tres que de campo. Revísalo";
                err.Color = "red";
                listaErrores.Add(err);
            } 
            
            lista = GameStats.Where(x => x.fols > 6).AsEnumerable().ToList();
            foreach (PlayersStatsGames pl in lista)
            {
                ErrorReporte err = new ErrorReporte();
                err.Mensaje = pl.Player.CompleteName + sImage + pl.LVNBAFranchis.Icono + "'/>" + " (" + pl.LVNBAFranchis.Abbreviation + ") no debería tener más de 6 faltas. Revísalo";
                err.Color = "red";
                listaErrores.Add(err);
            }

            lista = GameStats.Where(x => x.segs == 0 && ( x.asss > 0 || x.blks > 0 || x.f3a > 0 || x.f3m > 0 || x.fga > 0 || x.fgm > 0 || x.fols >0 ||
                    x.fta > 0 || x.ftm > 0 || x.plusminus > 0 || x.pnts > 0 || x.rdefs > 0 || x.rebs > 0 || x.rofs > 0 || x.starter > 0 || x.stls > 0 || x.trns > 0 )).AsEnumerable().ToList();         
            foreach (PlayersStatsGames pl in lista)
            {
                ErrorReporte err = new ErrorReporte();
                err.Mensaje = pl.Player.CompleteName + sImage + pl.LVNBAFranchis.Icono + "'/>" + " (" + pl.LVNBAFranchis.Abbreviation + ") no ha jugado y tiene stats imputadas. Revísalo";
                err.Color = "red";
                listaErrores.Add(err);
            }

            lista = GameStats.AsEnumerable().ToList();
            foreach (PlayersStatsGames pl in lista)
            {
                int puntos = 0;
                int itotalDeUno = 0, itotalDeDos = 0, itotalDeTres = 0;
                if (pl.pnts == null)
                    puntos = 0;
                else
                    puntos = Convert.ToInt32(pl.pnts);
                int itotalPuntos = puntos;
                if (pl.fta == null)
                    itotalDeUno = 0;
                else
                    itotalDeUno = Convert.ToInt32(pl.ftm);

                if (pl.fga == null)
                    itotalDeDos = 0;
                else
                    itotalDeDos = Convert.ToInt32(pl.fgm);

                if (pl.f3a == null)
                    itotalDeTres = 0;
                else
                    itotalDeTres = Convert.ToInt32(pl.f3m);

                 //los de campo son la suma de los dos
                 itotalDeDos = (itotalDeDos - itotalDeTres) * 2;
                 itotalDeTres = itotalDeTres * 3;

                if (itotalPuntos != (itotalDeUno + itotalDeDos + itotalDeTres))
                {
                    ErrorReporte err = new ErrorReporte();
                    err.Mensaje = pl.Player.CompleteName + sImage + pl.LVNBAFranchis.Icono + "'/>" + " (" + pl.LVNBAFranchis.Abbreviation + ") los puntos anotados y su relación de tiros no cuadra. Revísalo";
                    err.Color = "red";
                    listaErrores.Add(err);
                }
            }


            return;
        }

        public void damePosibleErroresJugadoresReporte(int idGame, int idTeam, ref List<ErrorReporte> listaErrores)
        {
            List<PlayersStatsGames> lista = new List<PlayersStatsGames>();
            string sImage = "<img width='20px' height='20px' src='../Images/Common/teams/Iconos/";

            IQueryable<PlayersStatsGames> GameStats = db.PlayersStatsGames.Where(x => x.idGame == idGame && x.idTeam == idTeam);
            lista = GameStats.Where(x => x.pnts  > 30).AsEnumerable().ToList();
            foreach (PlayersStatsGames pl in lista)
            {
                ErrorReporte err = new ErrorReporte();
                err.Mensaje = pl.Player.CompleteName + sImage + pl.LVNBAFranchis.Icono + "'/>" + " (" + pl.LVNBAFranchis.Abbreviation + ") ha anotado más de 30 puntos. Es correcto?";
                err.Color = "yellow";
                listaErrores.Add(err);
            }
            lista = GameStats.Where(x => x.pnts > 20 && x.segs < 10).AsEnumerable().ToList();
            foreach (PlayersStatsGames pl in lista)
            {
                ErrorReporte err = new ErrorReporte();
                err.Mensaje = pl.Player.CompleteName + sImage + pl.LVNBAFranchis.Icono + "'/>" + " (" + pl.LVNBAFranchis.Abbreviation + ") ha anotado más de 20 puntos en menos de 10 minutos. Es correcto?";
                err.Color = "yellow";
                listaErrores.Add(err);
            }

            lista = GameStats.Where(x => x.asss > 15).AsEnumerable().ToList();
            foreach (PlayersStatsGames pl in lista)
            {
                ErrorReporte err = new ErrorReporte();
                err.Mensaje = pl.Player.CompleteName + sImage + pl.LVNBAFranchis.Icono + "'/>" + " (" + pl.LVNBAFranchis.Abbreviation + ") ha hecho más de 15 asistencias. Es correcto?";
                err.Color = "yellow";
                listaErrores.Add(err);
            }
            lista = GameStats.Where(x => x.asss > 10 &&  x.Player.IdPosition > 4  ).AsEnumerable().ToList();
            foreach (PlayersStatsGames pl in lista)
            {
                ErrorReporte err = new ErrorReporte();
                err.Mensaje = pl.Player.CompleteName + sImage + pl.LVNBAFranchis.Icono + "'/>" + " (" + pl.LVNBAFranchis.Abbreviation + ") ha hecho más de 10 asistencias y no es un exterior. Es correcto?";
                err.Color = "yellow";
                listaErrores.Add(err);
            }

            lista = GameStats.Where(x => x.rebs > 20).AsEnumerable().ToList();
            foreach (PlayersStatsGames pl in lista)
            {
                ErrorReporte err = new ErrorReporte();
                err.Mensaje = pl.Player.CompleteName + sImage + pl.LVNBAFranchis.Icono + "'/>" + " (" + pl.LVNBAFranchis.Abbreviation + ") ha hecho más de 20 rebotes. Es correcto?";
                err.Color = "yellow";
                listaErrores.Add(err);
            }

            lista = GameStats.Where(x => x.rofs > 10).AsEnumerable().ToList();
            foreach (PlayersStatsGames pl in lista)
            {
                ErrorReporte err = new ErrorReporte();
                err.Mensaje = pl.Player.CompleteName + sImage + pl.LVNBAFranchis.Icono + "'/>" + " (" + pl.LVNBAFranchis.Abbreviation + ") ha hecho más de 10 rebotes ofensivos. Es correcto?";
                err.Color = "yellow";
                listaErrores.Add(err);
            }

            lista = GameStats.Where(x => x.rofs > 5 && x.Player.IdPosition < 7).AsEnumerable().ToList();
            foreach (PlayersStatsGames pl in lista)
            {
                ErrorReporte err = new ErrorReporte();
                err.Mensaje = pl.Player.CompleteName + sImage + pl.LVNBAFranchis.Icono + "'/>" + " (" + pl.LVNBAFranchis.Abbreviation + ") ha hecho más de 5 rebotes ofensivos y no es un interior. Es correcto?";
                err.Color = "yellow";
                listaErrores.Add(err);
            }
            lista = GameStats.Where(x => x.blks > 5 ).AsEnumerable().ToList();
            foreach (PlayersStatsGames pl in lista)
            {
                ErrorReporte err = new ErrorReporte();
                err.Mensaje = pl.Player.CompleteName + sImage + pl.LVNBAFranchis.Icono + "'/>" + " (" + pl.LVNBAFranchis.Abbreviation + ") ha hecho más de 5 tapones. Es correcto?";
                err.Color = "yellow";
                listaErrores.Add(err);
            }
            lista = GameStats.Where(x => x.trns > 5).AsEnumerable().ToList();
            foreach (PlayersStatsGames pl in lista)
            {
                ErrorReporte err = new ErrorReporte();
                err.Mensaje = pl.Player.CompleteName + sImage + pl.LVNBAFranchis.Icono + "'/>" + " (" + pl.LVNBAFranchis.Abbreviation + ") ha hecho más de 5 pérdidas. Es correcto?";
                err.Color = "yellow";
                listaErrores.Add(err);
            }
            lista = GameStats.Where(x => x.stls > 5).AsEnumerable().ToList();
            foreach (PlayersStatsGames pl in lista)
            {
                ErrorReporte err = new ErrorReporte();
                err.Mensaje = pl.Player.CompleteName + sImage + pl.LVNBAFranchis.Icono + "'/>" + " (" + pl.LVNBAFranchis.Abbreviation + ") ha hecho más de 5 robos. Es correcto?";
                err.Color = "yellow";
                listaErrores.Add(err);
            }





            return;
        }

        #endregion
    }
}
