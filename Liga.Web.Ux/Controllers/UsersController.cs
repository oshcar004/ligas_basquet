﻿
using Liga.Domain.Entities;
using Liga.Infrastructure.Data;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace Liga.Web.UI.Controllers
{
    [Authorize(Roles = "Administrador")]
    public class UsersController : BaseController
    {
        private Liga_DBContext db = new Liga_DBContext();

        //
        // GET: /Goals/

        public ActionResult Index()
        {
            List<UserProfile> users = db.UserProfiles.ToList();

            return View(users);
        }

        //
        // GET: /Goals/Details/5

        public ActionResult Details(int id = 0)
        {
            return RedirectToAction("Index");
        }

        //
        // GET: /Goals/Create

        public ActionResult Create()
        {
            return RedirectToAction("Index");
        }

        //
        // POST: /Goals/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UserProfile userProfile)
        {
            return RedirectToAction("Index");
        }

        //
        // GET: /Goals/Edit/5

        public ActionResult Edit(int id = 0)
        {
            UserProfile userProfile = db.UserProfiles.Find(id);
            ViewBag.Roles = db.Webpages_Roles.ToList();
            if (userProfile == null)
            {
                return RedirectToAction("Index");
            }
            return View(userProfile);
        }

        //
        // POST: /Goals/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int UserId, string UserName, string Email, int[] roles)
        {
            UserProfile userProfile = db.UserProfiles.Find(UserId);
            if (userProfile != null)
            {
                userProfile.UserName = UserName;
                userProfile.Email = Email;
                if (roles != null && roles.Count() > 0)
                {
                    if (userProfile.Webpages_UsersInRoles != null && userProfile.Webpages_UsersInRoles.Count() > 0)
                    {
                        db.Webpages_UsersInRoles.RemoveRange(userProfile.Webpages_UsersInRoles);
                        db.SaveChanges();
                    }
                    userProfile.Webpages_UsersInRoles = new List<Webpages_UsersInRoles>();
                    foreach (var rolId in roles)
                    {
                        userProfile.Webpages_UsersInRoles.Add(new Webpages_UsersInRoles(userProfile.UserId, rolId));
                    }
                    db.SaveChanges();
                }
                else
                {
                    if (userProfile.Webpages_UsersInRoles != null && userProfile.Webpages_UsersInRoles.Count() > 0)
                    {
                        db.Webpages_UsersInRoles.RemoveRange(userProfile.Webpages_UsersInRoles);
                        db.SaveChanges();
                    }
                }
            }
            return RedirectToAction("Index");
        }

        //
        // GET: /Goals/Delete/5

        public ActionResult Delete(int id = 0)
        {
            UserProfile userProfile = db.UserProfiles.Find(id);
            if (userProfile == null)
            {
                return RedirectToAction("Index");
            }
            return View(userProfile);
        }

        //
        // POST: /Goals/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UserProfile userProfile = db.UserProfiles.Find(id);
            db.UserProfiles.Remove(userProfile);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

    }
}