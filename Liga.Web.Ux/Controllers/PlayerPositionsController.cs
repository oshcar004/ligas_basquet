﻿
using Liga.Domain.Entities;
using Liga.Infrastructure.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace Liga.Web.UI.Controllers
{
    [Authorize(Roles = "Administrador")]
    public class PlayerPositionsController : BaseController
    {
        private Liga_DBContext db = new Liga_DBContext();

        //
        // GET: /PlayerPositions/

        public ActionResult Index()
        {
            return View(db.PlayersPositions.ToList());
        }

        //
        // GET: /PlayerPositions/Details/5

        public ActionResult Details(int id = 0)
        {
            PlayersPositions playerspositions = db.PlayersPositions.Find(id);
            if (playerspositions == null)
            {
                return HttpNotFound();
            }
            return View(playerspositions);
        }

        //
        // GET: /PlayerPositions/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /PlayerPositions/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PlayersPositions playerspositions)
        {
            if (ModelState.IsValid)
            {
                db.PlayersPositions.Add(playerspositions);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(playerspositions);
        }

        //
        // GET: /PlayerPositions/Edit/5

        public ActionResult Edit(int id = 0)
        {
            PlayersPositions playerspositions = db.PlayersPositions.Find(id);
            if (playerspositions == null)
            {
                return HttpNotFound();
            }
            return View(playerspositions);
        }

        //
        // POST: /PlayerPositions/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PlayersPositions playerspositions)
        {
            if (ModelState.IsValid)
            {
                db.Entry(playerspositions).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(playerspositions);
        }

        //
        // GET: /PlayerPositions/Delete/5

        public ActionResult Delete(int id = 0)
        {
            PlayersPositions playerspositions = db.PlayersPositions.Find(id);
            if (playerspositions == null)
            {
                return HttpNotFound();
            }
            return View(playerspositions);
        }

        //
        // POST: /PlayerPositions/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PlayersPositions playerspositions = db.PlayersPositions.Find(id);
            db.PlayersPositions.Remove(playerspositions);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}