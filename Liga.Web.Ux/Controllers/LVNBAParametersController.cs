﻿
using Liga.Domain.Entities;
using Liga.Infrastructure.Data;
using Liga.Service.Impl;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace Liga.Web.UI.Controllers
{
    [Authorize(Roles = "Administrador")]
    public class LVNBAParametersController : BaseController
    {
        private Liga_DBContext db = new Liga_DBContext();
        readonly SeasonService serv = new SeasonService();
        //
        // GET: /LVNBAParameters/

        public ActionResult Index()
        {
            return View(db.Parameters.First());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(Parameters parameters)
        {
            if (ModelState.IsValid)
            {
                db.Entry(parameters).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "Admin", null);
            }

            return View(parameters);
        }

        public ActionResult EditParameters()
        {

            return View(db.Parameters.First());
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditParameters(Parameters parameters)
        {
            if (ModelState.IsValid)
            {
                var act = db.Parameters.FirstOrDefault();
                db.Entry(act).State = EntityState.Detached;
                db.Entry(parameters).State = EntityState.Modified;
                db.SaveChanges();
                if(act.Temporada != parameters.Temporada ){
                    serv.CreateNewSeasonAsync(parameters.Temporada);
                    serv.GenerateRounds(parameters.Temporada);
                }
                return RedirectToAction("EditParameters", "LVNBAParameters", null);
            }
            return View(parameters);
        }


    }
}
