﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Liga.Web.UI.Controllers
{
    [Authorize(Roles = "Administrador")]
    public class AdminController : BaseController
    {
        //
        // GET: /Admin/
        public ActionResult Index()
        {
            ViewBag.Title = "Administración de usuarios";
            ViewBag.Message = "Página principal para los Administradores y Comisionado.";
            return View();
        }

    }
}
