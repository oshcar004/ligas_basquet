﻿using Liga.Domain.Entities;
using Liga.Infrastructure.Data;
using Liga.Infrastructure.Data.Repository;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;

namespace Liga.Web.UI.Controllers
{
    public class StatsController : BaseController
    {
        private readonly int Season = new Liga_DBContext().Parameters.First().Temporada;
        private Liga_DBContext db = new Liga_DBContext();

        //RankStats in HomePage
        [HttpPost]
        public ActionResult GetTopFivePPGPlayers()
        {
            try
            {
                var res = db.RankPPGs.Take(5).ToList();
                var lista = new List<TopPlayerLists>();
                foreach(var pl in res)
                {
                    var top = new TopPlayerLists
                    {
                        CompleteName = pl.CompleteName,
                        FaceImage = pl.FaceImage,
                        Logo = pl.Logo,
                        STAT = pl.PPG,
                        id = pl.Player,
                        Tipo = "PPG"
                    };
                    lista.Add(top);
                }
                return Json(lista.ToList());
            }
            catch(Exception ex)
            {
                return null;
            }
        }
        [HttpPost]
        public ActionResult GetTopFiveAPGPlayers()
        {
            try
            {
                var res = db.RankAPGs.Take(5).ToList();
                var lista = new List<TopPlayerLists>();
                foreach (var pl in res)
                {
                    var top = new TopPlayerLists
                    {
                        CompleteName = pl.CompleteName,
                        FaceImage = pl.FaceImage,
                        Logo = pl.Logo,
                        STAT = pl.APG,
                        id = pl.Player,
                        Tipo = "APG"
                    };
                    lista.Add(top);
                }
                return Json(lista.ToList());
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        [HttpPost]
        public ActionResult GetTopFiveFGPlayers()
        {
            try
            {
                var res = db.RankFGs.Take(5).ToList();
                var lista = new List<TopPlayerLists>();
                foreach (var pl in res)
                {
                    var top = new TopPlayerLists
                    {
                        CompleteName = pl.CompleteName,
                        FaceImage = pl.FaceImage,
                        Logo = pl.Logo,
                        STAT = pl.FG,
                        id = pl.Player,
                        Tipo = "FG"
                    };
                    lista.Add(top);
                }
                return Json(lista.ToList());
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        [HttpPost]
        public ActionResult GetTopFiveRPGPlayers()
        {
            try
            {
                var res = db.RankRPGs.Take(5).ToList();
                var lista = new List<TopPlayerLists>();
                foreach (var pl in res)
                {
                    var top = new TopPlayerLists
                    {
                        CompleteName = pl.CompleteName,
                        FaceImage = pl.FaceImage,
                        Logo = pl.Logo,
                        STAT = pl.RPG,
                        id = pl.Player,
                        Tipo = "RPG"
                    };
                    lista.Add(top);
                }
                return Json(lista.ToList());
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        [HttpPost]
        public ActionResult GetTopFiveSPGPlayers()
        {
            try
            {
                var res = db.RankSPGs.Take(5).ToList();
                var lista = new List<TopPlayerLists>();
                foreach (var pl in res)
                {
                    var top = new TopPlayerLists
                    {
                        CompleteName = pl.CompleteName,
                        FaceImage = pl.FaceImage,
                        Logo = pl.Logo,
                        STAT = pl.SPG,
                        id = pl.Player,
                        Tipo = "SPG"
                    };
                    lista.Add(top);
                }
                return Json(lista.ToList());
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        [HttpPost]
        public ActionResult GetTopFiveBPGPlayers()
        {
            try
            {
                var res = db.RankBPGs.Take(5).ToList();
                var lista = new List<TopPlayerLists>();
                foreach (var pl in res)
                {
                    var top = new TopPlayerLists
                    {
                        CompleteName = pl.CompleteName,
                        FaceImage = pl.FaceImage,
                        Logo = pl.Logo,
                        STAT = pl.BPG,
                        id = pl.Player,
                        Tipo = "BPG"
                    };
                    lista.Add(top);
                }
                return Json(lista.ToList());
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public ActionResult StatsPlayer(string orderBy, string currentFilter, string searchString, int? page, string message = "")
        {
            ViewBag.CurrentSort = orderBy;
            ViewBag.Message = message;
            var temporada = db.Parameters.FirstOrDefault().Temporada;

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            IQueryable<PlayerStatisticsBySeason> listaPlayers =
            db.PlayerStatisticsBySeasons.Where(x => x.Season == temporada.ToString()).AsQueryable();

            switch (orderBy)
            {
                case "PPG":
                    listaPlayers = listaPlayers.OrderByDescending(s => s.PPG);
                    break;
                case "RPG":
                    listaPlayers = listaPlayers.OrderByDescending(s => s.RPG);
                    break;
                case "SPG":
                    listaPlayers = listaPlayers.OrderByDescending(s => s.SPG);
                    break;
                case "APG":
                    listaPlayers = listaPlayers.OrderByDescending(s => s.APG);
                    break;
                case "F3G":
                    listaPlayers = listaPlayers.OrderByDescending(s => s.F3G);
                    break;
                case "TOO":
                    listaPlayers = listaPlayers.OrderByDescending(s => s.TOO);
                    break;
                case "MPG":
                    listaPlayers = listaPlayers.OrderByDescending(s => s.MPG);
                    break;
                default:
                    listaPlayers = listaPlayers.OrderByDescending(s => s.PPG);
                    break;
            };
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(listaPlayers.ToPagedList(pageNumber, pageSize));
        }

        public ActionResult StatsRookies(int? idSeason, string orderBy, string currentFilter, string searchString, int? page, string message = "")
        {
            ViewBag.CurrentSort = orderBy;
            ViewBag.NameSortParm = string.IsNullOrEmpty(orderBy) ? "PPG" : "";
            ViewBag.Message = message;
            ViewBag.idSeason = (idSeason == null ? Season : idSeason);
            ViewBag.Seasons = db.Games.Where(x => x.Id > 0).Select(x => x.season).Distinct();
            string temporada = (idSeason == null ? Season.ToString() : idSeason.ToString());

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;
            //////
            IQueryable<PlayerStatisticsBySeason> listaPlayers =
                //db.PlayerStatisticsBySeasons.Where(x => x.Season == temporada).OrderByDescending(x => x.CompleteName).AsQueryable();
                db.PlayerStatisticsBySeasons.Where(x => x.Season == temporada && x.YearsPro <= 4).OrderByDescending(x => x.CompleteName).AsQueryable();
            //listaPlayers = listaPlayersList.AsQueryable();
            //////
            if (!string.IsNullOrEmpty(searchString))
            {
                listaPlayers = listaPlayers.Where(s => s.CompleteName.ToUpper().Contains(searchString.ToUpper()));
            }
            switch (orderBy)
            {
                case "PPG":
                    listaPlayers = listaPlayers.OrderByDescending(s => s.PPG);
                    break;
                case "RPG":
                    listaPlayers = listaPlayers.OrderByDescending(s => s.RPG);
                    break;
                case "SPG":
                    listaPlayers = listaPlayers.OrderByDescending(s => s.SPG);
                    break;
                case "APG":
                    listaPlayers = listaPlayers.OrderByDescending(s => s.APG);
                    break;
                case "F3G":
                    listaPlayers = listaPlayers.OrderByDescending(s => s.F3G);
                    break;
                case "TOO":
                    listaPlayers = listaPlayers.OrderByDescending(s => s.TOO);
                    break;
                case "MPG":
                    listaPlayers = listaPlayers.OrderByDescending(s => s.MPG);
                    break;
                default:
                    listaPlayers = listaPlayers.OrderByDescending(s => s.PPG);
                    break;
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(listaPlayers.ToPagedList(pageNumber, pageSize));
        }
        public ActionResult StatsExpirings(int? idSeason, string sortOrder, string currentFilter, string searchString, int? page, string message = "")
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = string.IsNullOrEmpty(sortOrder) ? "Name_desc" : "";
            ViewBag.Message = message;
            ViewBag.idSeason = (idSeason == null ? Season : idSeason);
            ViewBag.Seasons = db.Games.Where(x => x.Id > 0).Select(x => x.season).Distinct();
            string temporada = (idSeason == null ? Season.ToString() : idSeason.ToString());

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;
            //////
            IQueryable<PlayerStatisticsBySeason> listaPlayersTemp =
                //db.PlayerStatisticsBySeasons.Where(x => x.Season == temporada).OrderByDescending(x => x.CompleteName).AsQueryable();
                db.PlayerStatisticsBySeasons.Where(x => x.Season == temporada).OrderByDescending(x => x.CompleteName).AsQueryable();
            //IQueryable<Players> jugadores = db.Players.Where(x => x.Id > 0);
            List<Players> jugadoress = db.Players.Where(x => x.PlayersSalariesFull.Where(y => y.DateSalarie.Year >= Season).Count() == 1).ToList();
            IQueryable<Players> jugadores = db.Players.Where(x => x.PlayersSalariesFull.Where(y => y.DateSalarie.Year >= Season).Count() == 1);
            List<PlayerStatisticsBySeason> lPlayers = new List<PlayerStatisticsBySeason>();
            foreach (PlayerStatisticsBySeason player in listaPlayersTemp)
            {
                foreach (Players jugador in jugadores)
                {
                    if (jugador.Id == player.Player)
                    {
                        lPlayers.Add(player);
                        break;
                    }
                }
            }
            IQueryable<PlayerStatisticsBySeason> listaPlayers = lPlayers.AsQueryable();
            //listaPlayers = listaPlayersList.AsQueryable();
            //////
            if (!string.IsNullOrEmpty(searchString))
            {
                listaPlayers = listaPlayers.Where(s => s.CompleteName.ToUpper().Contains(searchString.ToUpper()));
            }
            switch (sortOrder)
            {
                case "Name_desc":
                    listaPlayers = listaPlayers.OrderByDescending(s => s.CompleteName);
                    break;
                default:
                    listaPlayers = listaPlayers.OrderBy(s => s.CompleteName);
                    break;
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(listaPlayers.ToPagedList(pageNumber, pageSize));
        }

        public ActionResult StatsTransferibles(string sortOrder, string currentFilter, string searchString, int? page, string message = "")
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = string.IsNullOrEmpty(sortOrder) ? "Name_desc" : "";
            ViewBag.Message = message;

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;
            //////
            IQueryable<PlayerStatisticsBySeason> listaPlayers =
                //db.PlayerStatisticsBySeasons.Where(x => x.Season == temporada).OrderByDescending(x => x.CompleteName).AsQueryable();
                db.PlayerStatisticsBySeasons.Where(x => x.IdState == 1).OrderByDescending(x => x.CompleteName).AsQueryable();
            //listaPlayers = listaPlayersList.AsQueryable();
            //////
            if (!string.IsNullOrEmpty(searchString))
            {
                listaPlayers = listaPlayers.Where(s => s.CompleteName.ToUpper().Contains(searchString.ToUpper()));
            }
            switch (sortOrder)
            {
                case "Name_desc":
                    listaPlayers = listaPlayers.OrderByDescending(s => s.CompleteName);
                    break;
                default:
                    listaPlayers = listaPlayers.OrderBy(s => s.CompleteName);
                    break;
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(listaPlayers.ToPagedList(pageNumber, pageSize));
        }

        public ActionResult Level2kCards()
        {
            return View();
        }
        public ActionResult Top10Week(int? idWeek, string sortOrder, string currentFilter, string searchString, int? page, string message = "")
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = string.IsNullOrEmpty(sortOrder) ? "Name_desc" : "";
            ViewBag.Message = message;
            ViewBag.Weeks = db.PlazosSemanales.Where(x => x.Plazo > 0);
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            if (idWeek == null || idWeek < 1)
            {
                idWeek = 1;
            }
            ViewBag.idWeek = idWeek;
            ViewBag.CurrentFilter = searchString;
            //////
            IQueryable<PlayerStatisticsBySeason> listaPlayers;
            int Season = new Liga_DBContext().Parameters.First().Temporada;
            PlazosSemanales plazo = db.PlazosSemanales.Where(x => x.Plazo == idWeek).First();
            int IPlazo = plazo.Inicio;
            int FPlazo = plazo.Fin;


            string query = "TopWeekMonth @Season, @IniPlazo, @FinPlazo ";
            SqlParameter SeasonId = new SqlParameter("@Season", Season);
            SqlParameter IniPlazo = new SqlParameter("@IniPlazo", IPlazo);
            SqlParameter FinPlazo = new SqlParameter("@FinPlazo", FPlazo);

            //SqlParameterCollection SqlArray;
            //SqlArray.Add(SeasonId);
            //SqlArray.Add(IniPlazo);
            //SqlArray.Add(FinPlazo);

            object[] SqlArr = new object[3];
            SqlArr[0] = SeasonId;
            SqlArr[1] = IniPlazo;
            SqlArr[2] = FinPlazo;


            listaPlayers = db.Database.SqlQuery<PlayerStatisticsBySeason>(query, SqlArr).ToList().AsQueryable();
            //listaPlayers = listaPlayersList.AsQueryable();
            //////
            if (!string.IsNullOrEmpty(searchString))
            {
                listaPlayers = listaPlayers.Where(s => s.CompleteName.ToUpper().Contains(searchString.ToUpper()));
            }
            switch (sortOrder)
            {
                case "Name_desc":
                    listaPlayers = listaPlayers.OrderByDescending(s => s.CompleteName);
                    break;
                default:
                    listaPlayers = listaPlayers.OrderBy(s => s.CompleteName);
                    break;
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(listaPlayers.ToPagedList(pageNumber, pageSize));
        }
        public ActionResult Top10Month(int? idMonth, string sortOrder, string currentFilter, string searchString, int? page, string message = "")
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = string.IsNullOrEmpty(sortOrder) ? "Name_desc" : "";
            ViewBag.Message = message;
            ViewBag.Months = db.PlazosMensuales.Where(x => x.Plazo > 0);
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            if (idMonth == null || idMonth < 1)
            {
                idMonth = 1;
            }
            ViewBag.idMonth = idMonth;
            ViewBag.CurrentFilter = searchString;
            //////
            IQueryable<PlayerStatisticsBySeason> listaPlayers;
            int Season = new Liga_DBContext().Parameters.First().Temporada;
            PlazosMensuales plazo = db.PlazosMensuales.Where(x => x.Plazo == idMonth).First();
            int IPlazo = plazo.Inicio;
            int FPlazo = plazo.Fin;


            string query = "TopWeekMonth @Season, @IniPlazo, @FinPlazo ";
            SqlParameter SeasonId = new SqlParameter("@Season", Season);
            SqlParameter IniPlazo = new SqlParameter("@IniPlazo", IPlazo);
            SqlParameter FinPlazo = new SqlParameter("@FinPlazo", FPlazo);

            //SqlParameterCollection SqlArray;
            //SqlArray.Add(SeasonId);
            //SqlArray.Add(IniPlazo);
            //SqlArray.Add(FinPlazo);

            object[] SqlArr = new object[3];
            SqlArr[0] = SeasonId;
            SqlArr[1] = IniPlazo;
            SqlArr[2] = FinPlazo;


            listaPlayers = db.Database.SqlQuery<PlayerStatisticsBySeason>(query, SqlArr).ToList().AsQueryable();
            //listaPlayers = listaPlayersList.AsQueryable();
            //////
            if (!string.IsNullOrEmpty(searchString))
            {
                listaPlayers = listaPlayers.Where(s => s.CompleteName.ToUpper().Contains(searchString.ToUpper()));
            }
            switch (sortOrder)
            {
                case "Name_desc":
                    listaPlayers = listaPlayers.OrderByDescending(s => s.CompleteName);
                    break;
                default:
                    listaPlayers = listaPlayers.OrderBy(s => s.CompleteName);
                    break;
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(listaPlayers.ToPagedList(pageNumber, pageSize));
        }
        public ActionResult StatsHistory()
        {
            return View();
        }
    }
}