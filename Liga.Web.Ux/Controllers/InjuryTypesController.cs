﻿
using Liga.Domain.Entities;
using Liga.Infrastructure.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace Liga.Web.UI.Controllers
{
    [Authorize(Roles = "Administrador, Comisionado")]
    public class InjuryTypesController : BaseController
    {
        private Liga_DBContext db = new Liga_DBContext();

        //
        // GET: /InjuryTypes/

        public ActionResult Index()
        {
            return View(db.InjuryTypes.ToList());
        }

        //
        // GET: /InjuryTypes/Details/5

        public ActionResult Details(int id = 0)
        {
            InjuryTypes injurytypes = db.InjuryTypes.Find(id);
            if (injurytypes == null)
            {
                return HttpNotFound();
            }
            return View(injurytypes);
        }

        //
        // GET: /InjuryTypes/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /InjuryTypes/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(InjuryTypes injurytypes)
        {
            if (ModelState.IsValid)
            {
                db.InjuryTypes.Add(injurytypes);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(injurytypes);
        }

        //
        // GET: /InjuryTypes/Edit/5

        public ActionResult Edit(int id = 0)
        {
            InjuryTypes injurytypes = db.InjuryTypes.Find(id);
            if (injurytypes == null)
            {
                return HttpNotFound();
            }
            return View(injurytypes);
        }

        //
        // POST: /InjuryTypes/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(InjuryTypes injurytypes)
        {
            if (ModelState.IsValid)
            {
                db.Entry(injurytypes).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(injurytypes);
        }

        //
        // GET: /InjuryTypes/Delete/5

        public ActionResult Delete(int id = 0)
        {
            InjuryTypes injurytypes = db.InjuryTypes.Find(id);
            if (injurytypes == null)
            {
                return HttpNotFound();
            }
            return View(injurytypes);
        }

        //
        // POST: /InjuryTypes/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            InjuryTypes injurytypes = db.InjuryTypes.Find(id);
            db.InjuryTypes.Remove(injurytypes);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}