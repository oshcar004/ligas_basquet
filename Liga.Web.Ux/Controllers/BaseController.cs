﻿
using Liga.Domain.Entities;
using Liga.Infrastructure.Data;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;

namespace Liga.Web.UI.Controllers
{
    public class BaseController : Controller
    {


        private Liga_DBContext dbUsers = new Liga_DBContext();
        private Liga_DBContext db = new Liga_DBContext();
        public BaseController()
        {
            AccountController ac = new AccountController();

            ViewBag.IsAdmin = IsAdmin();
            ViewBag.IsComissioner = IsComissioner();
            ViewBag.IsAnonymous = IsAnonymous();
            ViewBag.IsJournalist = IsJournalist();
            ViewBag.IsManager = IsManager();
            int? idManager = null;
            ViewBag.IsManagerWithTeam = IsManagerWithTeam(out idManager);
            ViewBag.IdManager = idManager;
        }
        public dynamic IsAdmin()
        {
            try
            {
                if (System.Threading.Thread.CurrentPrincipal.Identity.IsAuthenticated)
                {
                    
                    string[]roles =  Roles.GetAllRoles();
                    if (Roles.IsUserInRole("Administrador"))
                        return true;

                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public dynamic IsManagerWithTeam(out int? idManager)
        {
            try
            {
                idManager = null;
                if (System.Threading.Thread.CurrentPrincipal.Identity.IsAuthenticated)
                {
                    if (Roles.IsUserInRole("Manager"))
                    {
                        string username = System.Threading.Thread.CurrentPrincipal.Identity.Name.ToString();

                        UserProfile user = db.UserProfiles.First(u => u.UserName == username);
                        Managers managers = db.Managers.FirstOrDefault(p => p.UserId == user.UserId);
                        if (managers != null && managers.ManagersFranchises.Where(x => x.IdManager == managers.Id && x.EndDate == null).FirstOrDefault() != null)
                        {
                            idManager = managers.Id;
                            return true;
                        }

                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public dynamic IsManager()
        {
            try
            {
                if (System.Threading.Thread.CurrentPrincipal.Identity.IsAuthenticated)
                {
                    if (Roles.IsUserInRole("Manager"))
                        return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public dynamic IsComissioner()
        {
            try
            {
                if (System.Threading.Thread.CurrentPrincipal.Identity.IsAuthenticated)
                {
                    if (Roles.IsUserInRole("Comisionado"))
                        return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public dynamic IsJournalist()
        {
            try
            {
                if (System.Threading.Thread.CurrentPrincipal.Identity.IsAuthenticated)
                {
                    if (Roles.IsUserInRole("Periodista"))
                        return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public dynamic IsAnonymous()
        {
            try
            {
                if (System.Threading.Thread.CurrentPrincipal.Identity.IsAuthenticated)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public dynamic GetTeamIdOfManager(int? idManager)
        {
            try
            {
                idManager = null;
                if (System.Threading.Thread.CurrentPrincipal.Identity.IsAuthenticated)
                {
                    if (Roles.IsUserInRole("Manager"))
                    {

                        string username = System.Threading.Thread.CurrentPrincipal.Identity.Name.ToString();
                        UserProfile user = db.UserProfiles.First(u => u.UserName == username);
                        Managers managers = db.Managers.FirstOrDefault(p => p.UserId == user.UserId);
                        if (managers != null)
                        {
                            idManager = managers.Id;
                            LVNBAFranchises franquicia = db.LVNBAFranchises.SingleOrDefault(x => x.IdManager == idManager.Value);
                            return franquicia.Id;

                        }

                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public dynamic GetTeamAbrevOfManager()
        {
            try
            {
                int? idManager = null;
                if (System.Threading.Thread.CurrentPrincipal.Identity.IsAuthenticated)
                {
                    if (Roles.IsUserInRole("Manager"))
                    {

                        string username = System.Threading.Thread.CurrentPrincipal.Identity.Name.ToString();
                        UserProfile user = db.UserProfiles.First(u => u.UserName == username);
                        Managers managers = db.Managers.FirstOrDefault(p => p.UserId == user.UserId);
                        if (managers != null)
                        {
                            idManager = managers.Id;
                            LVNBAFranchises franquicia = db.LVNBAFranchises.SingleOrDefault(x => x.IdManager == idManager.Value);
                            if (franquicia != null)
                            {
                                return franquicia.Abbreviation;
                            }

                        }

                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }

}

