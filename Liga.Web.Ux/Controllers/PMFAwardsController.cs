﻿
using Liga.Domain.Entities;
using Liga.Infrastructure.Data;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace Liga.Web.UI.Controllers
{
    [Authorize(Roles = "Administrador, Comisionado")]
    public class PMFAwardsController : BaseController
    {
        private Liga_DBContext db = new Liga_DBContext();

        //
        // GET: /PMFAwards/

        public ActionResult Index()
        {
            var pfmawards = db.PFMAwards.Include(p => p.Award).Include(p => p.LVNBAFranchis).Include(p => p.Manager).Include(p => p.Player);
            return View(pfmawards.ToList());
        }

        //
        // GET: /PMFAwards/Details/5

        public ActionResult Details(int id = 0)
        {
            PFMAwards pfmawards = db.PFMAwards.Find(id);
            if (pfmawards == null)
            {
                return HttpNotFound();
            }
            return View(pfmawards);
        }

        //
        // GET: /PMFAwards/Create

        public ActionResult Create()
        {
            ViewBag.IdAward = new SelectList(db.Awards, "Id", "Description");
            ViewBag.IdManager = new SelectList(db.Managers.OrderBy(x=>x.GamerTag), "Id", "GamerTag");
            ViewBag.IdPlayer = new SelectList(db.Players.OrderBy(x => x.CompleteName), "Id", "CompleteName");
            ViewBag.IdFranchise = new SelectList(db.LVNBAFranchises.OrderBy(x => x.Name), "Id", "Name");

            return View();
        }

        //
        // POST: /PMFAwards/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PFMAwards pfmawards)
        {
            if (ModelState.IsValid)
            {
                db.PFMAwards.Add(pfmawards);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdAward = new SelectList(db.Awards, "Id", "Description", pfmawards.IdAward);
            ViewBag.IdManager = new SelectList(db.Managers.OrderBy(x => x.GamerTag), "Id", "GamerTag", pfmawards.IdManager);
            ViewBag.IdPlayer = new SelectList(db.Players.OrderBy(x => x.CompleteName), "Id", "CompleteName", pfmawards.IdPlayer);
            ViewBag.IdFranchise = new SelectList(db.LVNBAFranchises.OrderBy(x=>x.Name), "Id", "Name", pfmawards.IdFranchise);

            return View(pfmawards);
        }

        //
        // GET: /PMFAwards/Edit/5

        public ActionResult Edit(int id = 0)
        {
            PFMAwards pfmawards = db.PFMAwards.Find(id);
            if (pfmawards == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdAward = new SelectList(db.Awards, "Id", "Description", pfmawards.IdAward);
            ViewBag.IdFranchise = new SelectList(db.LVNBAFranchises.OrderBy(x => x.Name), "Id", "Name", pfmawards.IdFranchise);
            ViewBag.IdManager = new SelectList(db.Managers.OrderBy(x => x.GamerTag), "Id", "GamerTag", pfmawards.IdManager);
            ViewBag.IdPlayer = new SelectList(db.Players.OrderBy(x => x.CompleteName), "Id", "CompleteName", pfmawards.IdPlayer);
            return View(pfmawards);
        }

        //
        // POST: /PMFAwards/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PFMAwards pfmawards)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pfmawards).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdAward = new SelectList(db.Awards, "Id", "Description", pfmawards.IdAward);
            ViewBag.IdFranchise = new SelectList(db.LVNBAFranchises.OrderBy(x => x.Name), "Id", "Name", pfmawards.IdFranchise);
            ViewBag.IdManager = new SelectList(db.Managers.OrderBy(x => x.GamerTag), "Id", "GamerTag", pfmawards.IdManager);
            ViewBag.IdPlayer = new SelectList(db.Players.OrderBy(x => x.CompleteName), "Id", "CompleteName", pfmawards.IdPlayer);
            return View(pfmawards);
        }

        //
        // GET: /PMFAwards/Delete/5

        public ActionResult Delete(int id = 0)
        {
            PFMAwards pfmawards = db.PFMAwards.Find(id);
            if (pfmawards == null)
            {
                return HttpNotFound();
            }
            return View(pfmawards);
        }

        //
        // POST: /PMFAwards/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PFMAwards pfmawards = db.PFMAwards.Find(id);
            db.PFMAwards.Remove(pfmawards);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}