﻿
using Liga.Domain.Entities;
using System.Linq;
using System.Web.Mvc;
using Liga.Infrastructure.Data;
using Liga.Infrastructure.Data.Repository;
using System.Data.SqlClient;
using PagedList;

namespace Liga.Web.UI.Controllers
{
    public class StatsTeamsController : Controller
    {

        private readonly int Season = new Liga_DBContext().Parameters.First().Temporada;
        private Liga_DBContext db = new Liga_DBContext();
        //
        // GET: /StatsTeams/

        public ActionResult StatsTeamOld()
        {
            return View();
        }

        public ActionResult StatsTeam(int? idSeason, string orderBy, string currentFilter, string searchString, string message = "")
        {
            ViewBag.CurrentSort = orderBy;
            ViewBag.NameSortParm = string.IsNullOrEmpty(orderBy) ? "Name_desc" : "";
            ViewBag.Message = message;
            ViewBag.idSeason = (idSeason == null ? Season : idSeason);
            ViewBag.Seasons = db.Games.Where(x => x.Id > 0).Select(x => x.season).Distinct();
            string temporada = (idSeason == null ? Season.ToString() : idSeason.ToString());

            if (searchString == null)
            { 
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;
            //////
            IQueryable<TeamStats> listaTeams;


            string query = "TeamStatsProc @Season ";
            SqlParameter SeasonId = new SqlParameter("@Season", temporada);


            object[] SqlArr = new object[1];
            SqlArr[0] = SeasonId;

            listaTeams = db.Database.SqlQuery<TeamStats>(query, SqlArr).ToList().AsQueryable();

            switch (orderBy)
            {
                case "FGM":
                    listaTeams = listaTeams.OrderByDescending(s => s.FGM);
                    break;
                case "FGA":
                    listaTeams = listaTeams.OrderByDescending(s => s.FGA);
                    break;
                case "FG":
                    listaTeams = listaTeams.OrderByDescending(s => s.FG);
                    break;
                case "FTM":
                    listaTeams = listaTeams.OrderByDescending(s => s.FTM);
                    break;
                case "FTA":
                    listaTeams = listaTeams.OrderByDescending(s => s.FTA);
                    break;
                case "FT":
                    listaTeams = listaTeams.OrderByDescending(s => s.FT);
                    break;
                case "F3M":
                    listaTeams = listaTeams.OrderByDescending(s => s.F3M);
                    break;
                case "F3A":
                    listaTeams = listaTeams.OrderByDescending(s => s.F3A);
                    break;
                case "F3":
                    listaTeams = listaTeams.OrderByDescending(s => s.F3);
                    break;
                case "RPG":
                    listaTeams = listaTeams.OrderByDescending(s => s.RPG);
                    break;
                case "RDPG":
                    listaTeams = listaTeams.OrderByDescending(s => s.RDPG);
                    break;
                case "ROPG":
                    listaTeams = listaTeams.OrderByDescending(s => s.ROPG);
                    break;
                case "ASPG":
                    listaTeams = listaTeams.OrderByDescending(s => s.ASPG);
                    break;
                case "TOPG":
                    listaTeams = listaTeams.OrderByDescending(s => s.TOPG);
                    break;
                case "STPG":
                    listaTeams = listaTeams.OrderByDescending(s => s.STPG);
                    break;
                case "BLPG":
                    listaTeams = listaTeams.OrderByDescending(s => s.BLPG);
                    break;
                case "FOPG":
                    listaTeams = listaTeams.OrderByDescending(s => s.FOPG);
                    break;
                case "PNTS":
                    listaTeams = listaTeams.OrderByDescending(s => s.PNTS);
                    break;
                default:
                    listaTeams = listaTeams.OrderByDescending(s => s.Name);
                    break;
            }
            return View(listaTeams.ToList());
        }

        public ActionResult StatsOpponent()
        {
            return View();
        }
        public ActionResult StatsDiff()
        {
            return View();
        }

    }
}
