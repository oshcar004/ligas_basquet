﻿using Liga.Domain.Entities;
using Liga.Infrastructure.Data;
using Liga.Infrastructure.Data.Repository;
using Liga.Web.UI.Controllers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;

namespace LVNBA_WEB.Controllers
{
    [Authorize(Roles = "Manager")]
    public class ManagersWithTeamController : BaseController
    {
        //
        // GET: /ManagersWithTeam/
        private Liga_DBContext  db = new Liga_DBContext();

        public ActionResult Index()
        {
            int? idTeam = GetManagerTeam();
            if (idTeam == null)
                return View("Index");
            return View();
        }

        public ActionResult MenuGestion()
        {
            int? idTeam = GetManagerTeam();
            if (idTeam == null)
                return View("Index");
            return View();
        }


        public int? GetManagerTeam()
        {
            int? idTeam = null;
            if (System.Threading.Thread.CurrentPrincipal.Identity.IsAuthenticated)
            {
                if (Roles.IsUserInRole("Manager"))
                {
                    string username = System.Threading.Thread.CurrentPrincipal.Identity.Name.ToString();

                    UserProfile user = db.UserProfiles.First(u => u.UserName == username);
                    Managers managers = db.Managers.FirstOrDefault(p => p.UserId == user.UserId);
                    if (managers != null && managers.ManagersFranchises.Where(x => x.IdManager == managers.Id && x.EndDate == null).FirstOrDefault() != null)
                    {
                        idTeam = db.LVNBAFranchises.Where(x => x.IdManager == managers.Id).First().Id;
                    }

                }
            }
            return idTeam;
        }

        public ActionResult Franchise(int? idTab)
        {
            int? idTeam = GetManagerTeam();
            int iSeason = db.Parameters.First().Temporada;
            ViewBag.Season = iSeason;
            ViewBag.Tab = idTab - 1;
            LVNBAFranchises franchise = db.LVNBAFranchises.Where(x => x.Id == idTeam).First();
            GeneralFranchiseInfo oFranchiseInfo = new GeneralFranchiseInfo();
            var playersTeamCuttedRights = new Jugadores().getPlayersRightsAndCutted(idTeam.Value);
            oFranchiseInfo.Today = new DateTime(db.Parameters.First().Temporada, 01, 01);
            oFranchiseInfo.Franchise = franchise;
            Managers manager = null;
            if (franchise.IdManager == null)
            {
                manager = new Managers
                {
                    GamerTag = "SIN MANAGER"
                };
            }
            else
            {
                manager = db.Managers.Where(x => x.Id == franchise.IdManager).FirstOrDefault();
            }
            int? idManagerFr = franchise.IdManager;
            var awards = db.PFMAwards;
            IEnumerable<PFMAwards> franchiseAwards = awards.Where(x => x.Award.Class == "E" && x.IdFranchise == idTeam).AsEnumerable();
            IEnumerable<PFMAwards> managerAwards = awards.Where(x => x.IdManager == franchise.IdManager).AsEnumerable();
            IEnumerable<Rounds> franchiseRounds = db.Rounds.Where(x => x.Owner == idTeam && x.Year >= iSeason).OrderBy(x => x.Year).ThenBy(x => x.Type).AsEnumerable();
            IEnumerable<Rounds> managerRounds = db.Rounds.Where(x => x.IdManager == franchise.IdManager).OrderBy(x => x.Year).ThenBy(x => x.Type).AsEnumerable();
            IEnumerable<ManagersFranchises> managerTrayectorias = db.ManagersFranchises.Where(x => x.IdManager == idManagerFr).AsEnumerable();
            IEnumerable<SalaryConditions> salaryCondition = db.SalaryConditions;

            oFranchiseInfo.TeamGoalsInfo = new GoalsTeam();
            oFranchiseInfo.PlotInfo = new PlotInfoSalary();
            oFranchiseInfo.ManagerCard = new ManagerCardMO();
            oFranchiseInfo.TeamCard = new TeamCardMO();
            oFranchiseInfo.RoundsTeamInfo = new RoundsTeam();

            //Grafico salarios
            var parameters = db.Parameters.First();
            oFranchiseInfo.PlotInfo.LuxuryTax = parameters.ImpuestoLujo.ToString();
            oFranchiseInfo.PlotInfo.SalaryTax = parameters.LimiteSalarial.ToString();

            var salaries = db.PlayersSalariesFull.Where(x => x.IdLVNBAFranchises == idTeam);

            decimal? dLujo = parameters.ImpuestoLujo;
            decimal? dSalary = parameters.LimiteSalarial;
            oFranchiseInfo.PlotInfo.LuxuryTaxForPlot = Convert.ToInt32(dLujo);
            oFranchiseInfo.PlotInfo.SalaryTaxForPlot = Convert.ToInt32(dSalary);
            oFranchiseInfo.PlotInfo = oFranchiseInfo.PlotInfo;
            oFranchiseInfo.TeamCard.Franchise = franchise;
            //oFranchiseInfo.ListPlayersRoster = franchise.Players; // Trae todos los jugadores pertenecientes al equipo
            List<Players> playersList = new List<Players>();
            List<PlayersSalariesFull> playersSalariesFullList = new List<PlayersSalariesFull>();
            playersSalariesFullList = db.PlayersSalariesFull.Where(x => x.IdLVNBAFranchises == franchise.Id).OrderBy(x => x.Salarie).ToList();
            foreach (PlayersSalariesFull playersSalariesFull in playersSalariesFullList)
            {
                if (!playersList.Contains(playersSalariesFull.Player))
                    playersList.Add(playersSalariesFull.Player); // Guarda los jugadores pertenecientes al equipo y los jugadores que fueron cortados por el equipo (que pueden pertenecer a otro equipo) pero se les debe pagar igual
            }
            List<Players> ListPlayersFromPlayers = franchise.Players.ToList();
            foreach (Players player in ListPlayersFromPlayers)
            {
                if (!playersList.Contains(player))
                    playersList.Add(player); // Guarda los jugadores del equipo que no tienen sueldo
            }
            oFranchiseInfo.ListPlayersRoster = playersList.Where(x => x.ActualTeam == franchise.Id && x.PlayersSalariesFull != null && x.PlayersSalariesFull.Count() > 0).OrderByDescending(x => x.PlayersSalariesFull.First().Salarie);
            decimal salarieTemp1 = 0;
            decimal salarieTemp2 = 0;
            decimal salarieTemp3 = 0;
            decimal salarieTemp4 = 0;
            decimal salarieTemp5 = 0;
            foreach (Players player in oFranchiseInfo.ListPlayersRoster)
            {
                IEnumerable<PlayersSalariesFull> salarie1 = player.PlayersSalariesFull.Where(x => x.DateSalarie.Year == iSeason);
                salarieTemp1 = salarieTemp1 + ((salarie1 != null && salarie1.Count() > 0) ? salarie1.First().Salarie : 0);

                IEnumerable<PlayersSalariesFull> salarie2 = player.PlayersSalariesFull.Where(x => x.DateSalarie.Year == iSeason + 1);
                salarieTemp2 = salarieTemp2 + ((salarie2 != null && salarie2.Count() > 0) ? salarie2.First().Salarie : 0);

                IEnumerable<PlayersSalariesFull> salarie3 = player.PlayersSalariesFull.Where(x => x.DateSalarie.Year == iSeason + 2);
                salarieTemp3 = salarieTemp3 + ((salarie3 != null && salarie3.Count() > 0) ? salarie3.First().Salarie : 0);

                IEnumerable<PlayersSalariesFull> salarie4 = player.PlayersSalariesFull.Where(x => x.DateSalarie.Year == iSeason + 3);
                salarieTemp4 = salarieTemp4 + ((salarie4 != null && salarie4.Count() > 0) ? salarie4.First().Salarie : 0);

                IEnumerable<PlayersSalariesFull> salarie5 = player.PlayersSalariesFull.Where(x => x.DateSalarie.Year == iSeason + 4);
                salarieTemp5 = salarieTemp5 + ((salarie5 != null && salarie5.Count() > 0) ? salarie5.First().Salarie : 0);
            }
            List<ChartData> Data = new List<ChartData>()
            {
                new ChartData(iSeason.ToString(), (double)salarieTemp1, (double)dSalary),
                new ChartData((iSeason + 1).ToString(), (double)salarieTemp2, (double)dSalary),
                new ChartData((iSeason + 2).ToString(), (double)salarieTemp3, (double)dSalary),
                new ChartData((iSeason + 3).ToString(), (double)salarieTemp4, (double)dSalary),
                new ChartData((iSeason + 4).ToString(), (double)salarieTemp5, (double)dSalary)
            };
            oFranchiseInfo.ChartData = Data;
            oFranchiseInfo.PlotInfo.SalaryTeam1 = salarieTemp1.ToString(); // ACA SE DEBEN SUMAR TODOS LOS SALARIOS QUE INVOLUCRAN AL EQUIPO
            oFranchiseInfo.ListPlayersRightsoCutted = playersTeamCuttedRights;
            oFranchiseInfo.ManagerCard.Manager = manager;
            oFranchiseInfo.TeamCard.FranchiseAwards = franchiseAwards;
            oFranchiseInfo.ManagerCard.ManagerAwards = managerAwards;
            oFranchiseInfo.ManagerCard.ManagerRounds = managerRounds;
            oFranchiseInfo.ManagerCard.ManagerTrayectoria = managerTrayectorias;
            oFranchiseInfo.RoundsTeamInfo.Rounds = franchiseRounds;
            oFranchiseInfo.TeamGoalsInfo.SportGoal = db.FranchiseSportGoals.Where(x => x.IdFranchise == idTeam).FirstOrDefault();
            oFranchiseInfo.TeamGoalsInfo.FinancialGoal = db.FranchiseManagementGoals.Where(x => x.IdFranchise == idTeam).FirstOrDefault();
            oFranchiseInfo.Conditions = salaryCondition;
            return View(oFranchiseInfo);
        }

        public ActionResult Players(int? idTab)
        {
            int? idTeam = GetManagerTeam();
            if (idTeam == null)
            {
                return View("Index");
            }

            ViewBag.Tab = idTab;
            IEnumerable<Players> playersTeam = new Jugadores().getPlayersRoster(idTeam.Value);

            IEnumerable<PFMAwards> playersAwards = db.PFMAwards.Where(x => x.Award.Class == "J" && x.IdFranchise == idTeam).AsEnumerable();
            GeneralPlayersInfo oPlayersInfo = new GeneralPlayersInfo();
            oPlayersInfo.ListPlayers = playersTeam;
            oPlayersInfo.PlayersAwards = playersAwards;
            oPlayersInfo.idTeam = idTeam.Value;
            string Season = db.Parameters.First().Temporada.ToString();
            ViewBag.Season = Convert.ToInt32(Season);
            return View(oPlayersInfo);
        }

        private PlayerCompleteInfo getPlayerCompleteInfo(int idPlayer)
        {
            string Season = db.Parameters.First().Temporada.ToString();
            int iSeason = Convert.ToInt32(Season);
            PlayerCompleteInfo completeInfoPlayer = new PlayerCompleteInfo();

            PosRanksPlayer pRankPlayer = new PosRanksPlayer();
            pRankPlayer.PosMPG = db.RankMPGs.Where(x => x.season == Season.ToString() && x.Player == idPlayer).FirstOrDefault();
            pRankPlayer.PosPPG = db.RankPPGs.Where(x => x.Season == Season && x.Player == idPlayer).FirstOrDefault();
            pRankPlayer.PosRPG = db.RankRPGs.Where(x => x.season == Season.ToString() && x.Player == idPlayer).FirstOrDefault();
            pRankPlayer.PosAPG = db.RankAPGs.Where(x => x.season == Season && x.Player == idPlayer).FirstOrDefault();
            pRankPlayer.PosSPG = db.RankSPGs.Where(x => x.season == Season.ToString() && x.Player == idPlayer).FirstOrDefault();
            pRankPlayer.PosBPG = db.RankBPGs.Where(x => x.season == Season.ToString() && x.Player == idPlayer).FirstOrDefault();
            if (db.PlayerStatisticsBySeasons.Where(x => x.Player == idPlayer) == null)
            {
                pRankPlayer.PlayerStat = new PlayerStatisticsBySeason();
            }
            else
            {
                pRankPlayer.PlayerStat = db.PlayerStatisticsBySeasons.Where(x => x.Player == idPlayer).FirstOrDefault();
            }
            completeInfoPlayer.Player = db.Players.Where(x => x.Id == idPlayer).First();
            completeInfoPlayer.PlayerStatThisRS = db.PlayerStatisticsBySeasons.Where(x => x.Player == idPlayer && x.Season == Season.ToString()).FirstOrDefault();
            completeInfoPlayer.PlayerStatThisPlayoffs = db.PlayerStatisticsByPofs.Where(x => x.Player == idPlayer && x.Season == Season.ToString()).FirstOrDefault();

            completeInfoPlayer.PlayerSalary = db.PlayersSalariesFull.Where(x => x.IdPlayer == idPlayer).FirstOrDefault();
            completeInfoPlayer.PlayerHistories = db.PlayersHistories.Where(x => x.IdPlayer == idPlayer).OrderByDescending(x => x.StartDate);
            completeInfoPlayer.PlayerAwards = db.PFMAwards.Where(x => x.IdPlayer == idPlayer).OrderByDescending(x => x.Year);
            completeInfoPlayer.PlayersGames = db.LastGames.Where(x => x.idPlayer == idPlayer && x.season == iSeason).OrderByDescending(x => x.gameDate);
            completeInfoPlayer.PositionRank = pRankPlayer;

            return completeInfoPlayer;
        }

        public ActionResult GestRoles()
        {
            int? idTeam = GetManagerTeam();
            if (idTeam == null)
            {
                return View("Index");
            }
            IEnumerable<Players> listaPlayers = new Jugadores().getPlayersRoster(idTeam.Value);
            IEnumerable<PlayersRoles> rolePlayers = db.PlayersRoles.AsEnumerable();
            PlayersAndRoles plRo = new PlayersAndRoles();
            plRo.ListPlayers = listaPlayers;
            plRo.PlayersRoles = rolePlayers;
            ViewBag.IdState = new SelectList(db.PlayersRoles, "Id", "Rol");

            return View(plRo);
        }

        public ActionResult GestStatus()
        {
            int? idTeam = GetManagerTeam();
            if (idTeam == null)
            {
                return View("Index");
            }
            IEnumerable<Players> listaPlayers = new Jugadores().getPlayersRoster(idTeam.Value); 
            IEnumerable<PlayersStates> statePlayers = db.PlayersStates.AsEnumerable();
            PlayersAndStates plSt = new PlayersAndStates();
            plSt.ListPlayers = listaPlayers;
            plSt.PlayersStates = statePlayers;
            plSt.Team = db.LVNBAFranchises.Find(idTeam);

            ViewBag.IdState = new SelectList(db.PlayersStates, "Id", "State");

            return View(plSt);
        }

        [HttpPost]
        [Authorize(Roles = "Manager")]
        public ActionResult GestStatus(FormCollection r)
        {
            IEnumerable<Players> listaPlayers = null;
            PlayersAndStates plSt = null;
            int iContadorCambios = 0;
            List<int> listaJugadoresModificados = new List<int>();

            int? idTeam = GetManagerTeam();
            if (idTeam == null)
            {
                return View("Index");
            }
            var team = db.LVNBAFranchises.Find(idTeam);

            int i = 0;
            IEnumerable<PlayersStates> statePlayers;
            if (ModelState.IsValid)
            {
                var PlayerIdArray = r.GetValues("item.Id");
                var PlayerStatusIdArray = r.GetValues("item.IdState");
                var PlayerManagerOpinion = r.GetValues("item.ManagerAdvert");
                var Needs = r.GetValues("Team.Needs");

                for (i = 0; i < PlayerIdArray.Length; i++)
                {
                    int iNewState = Convert.ToInt32(PlayerStatusIdArray[i]);
                    //string sManagerOpinion = PlayerManagerOpinion[i];
                    Players player = db.Players.Find(Convert.ToInt32(PlayerIdArray[i]));
                    if ((player.IdState != 4 && player.IdState != 6) && (iNewState == 4 || iNewState == 6))
                    {
                        ModelState.AddModelError("", "No puedes declarar un jugador 'Legalmente intransferible' o 'Temporalmente intransferible' ");
                        listaPlayers = new Jugadores().getPlayersRoster(idTeam.Value); // db.Players.Where(x => x.ActualTeam == idTeam).AsEnumerable();
                        statePlayers = db.PlayersStates.AsEnumerable();

                        plSt = new PlayersAndStates();
                        plSt.ListPlayers = listaPlayers;
                        plSt.PlayersStates = statePlayers;
                        plSt.Team = team;
                        return View(plSt);
                    }

                    //if (player.ManagerAdvert != sManagerOpinion)
                    //{
                    //    player.ManagerAdvert = sManagerOpinion;
                    //}
                    if (player.IdState != iNewState)
                    {
                        listaJugadoresModificados.Add(player.Id);
                        iContadorCambios++;
                        if (iNewState == 2 || iNewState == 5)
                            player.ManagerAdvert = "";
                    }

                    player.IdState = iNewState;
                    db.Entry(player).State = EntityState.Modified;

                }
                if (Needs != null)
                {
                    team.Needs = Needs[0].ToString();
                    db.Entry(team).State = EntityState.Modified;

                }

                db.SaveChanges();
            }
            listaPlayers = new Jugadores().getPlayersRoster(idTeam.Value);
            statePlayers = db.PlayersStates.AsEnumerable();

            plSt = new PlayersAndStates
            {
                ListPlayers = listaPlayers,
                PlayersStates = statePlayers,
                Team = team
            };
            return View(plSt);
        }

        [HttpPost]
        [Authorize(Roles = "Manager")]
        public ActionResult GestRoles(FormCollection r)
        {
            IEnumerable<Players> listaPlayers = null;
            IEnumerable<PlayersRoles> rolePlayers = null;
            PlayersAndRoles plRo = null;
            int iContadorCambios = 0;
            List<int> listaJugadoresModificados = new List<int>();

            int? idTeam = GetManagerTeam();
            if (idTeam == null)
            {
                return View("Index");
            }
            int i = 0;
            if (ModelState.IsValid)
            {
                var PlayerIdArray = r.GetValues("item.Id");
                var PlayerRolesIdArray = r.GetValues("item.IdRol");
                /*TODO: Se saca por ahora.
                if (!cumpleRequisitos(PlayerRolesIdArray))
                {
                    ModelState.AddModelError("", "Para grabar los datos debes cumplir los requisitos.");
                    listaPlayers = new Jugadores().getPlayersOfTeamByRolPriority(idTeam.Value); // db.Players.Where(x => x.ActualTeam == idTeam).AsEnumerable();
                    rolePlayers = db.PlayersRoles.AsEnumerable();

                    plRo = new PlayersAndRoles();
                    plRo.ListPlayers = listaPlayers;
                    plRo.PlayersRoles = rolePlayers;
                    return View(plRo);

                }
                */
                for (i = 0; i < PlayerIdArray.Length; i++)
                {
                    int iNewRol = Convert.ToInt32(PlayerRolesIdArray[i]);
                    Players player = db.Players.Find(Convert.ToInt32(PlayerIdArray[i]));
                    if (iNewRol == 7 && player.IdRol != 7)
                    {
                        if (db.Players.Where(x => x.ActualTeam == idTeam && x.IdRol == 7).Count() > 0)
                        {
                            ModelState.AddModelError("", "No puedes declarar un jugador 'Jugador Franquicia' porque ya tienes ese rol asignado.");
                            listaPlayers = new Jugadores().getPlayersRoster(idTeam.Value);
                            rolePlayers = db.PlayersRoles.AsEnumerable();

                            plRo = new PlayersAndRoles();
                            plRo.ListPlayers = listaPlayers;
                            plRo.PlayersRoles = rolePlayers;
                            return View(plRo);
                        }
                    }

                    if (player.IdRol != iNewRol)
                    {
                        listaJugadoresModificados.Add(player.Id);
                        iContadorCambios++;
                    }

                    player.IdRol = iNewRol;
                    db.Entry(player).State = EntityState.Modified;

                }
                db.SaveChanges();
            }
            listaPlayers = new Jugadores().getPlayersRoster(idTeam.Value); // db.Players.Where(x => x.ActualTeam == idTeam).AsEnumerable();
            rolePlayers = db.PlayersRoles.AsEnumerable();

            plRo = new PlayersAndRoles();
            plRo.ListPlayers = listaPlayers;
            plRo.PlayersRoles = rolePlayers;

            return View(plRo);
        }

        [HttpPost]
        [Authorize(Roles = "Manager")]
        public ActionResult EditFranchise(FormCollection r)
        {
            int? idTeam = GetManagerTeam();
            if (idTeam == null)
            {
                return View("Index");
            }

            if (ModelState.IsValid)
            {
                string id = r.GetValue("Id").AttemptedValue;
                string opinion = r.GetValue("ManagerOpinion").AttemptedValue;
                LVNBAFranchises fra = db.LVNBAFranchises.Find(Convert.ToInt32(id));
                if (fra != null)
                {
                    fra.ManagerOpinion = opinion.ToString();
                    db.Entry(fra).State = EntityState.Modified;
                    db.SaveChanges();
                    return View(fra);

                }
            }
            return View("Index");
        }
        private bool cumpleRequisitos(string[] PlayerRolesIdArray)
        {
            int iSinAsignar, iSuplente, iPromesa, iSextoHombre, iMinutosBasura, iTitular, iJugadorFranquicia, iJugadorClave;
            iSinAsignar = iSuplente = iPromesa = iSextoHombre = iMinutosBasura = iTitular = iJugadorFranquicia = iJugadorClave = 0;

            foreach (string idRol in PlayerRolesIdArray)
            {
                switch (idRol)
                {
                    case "1":
                        iSinAsignar++;
                        break;
                    case "2":
                        iSuplente++;
                        break;
                    case "3":
                        iPromesa++;
                        break;
                    case "4":
                        iSextoHombre++;
                        break;
                    case "5":
                        iMinutosBasura++;
                        break;
                    case "6":
                        iTitular++;
                        break;
                    case "7":
                        iJugadorFranquicia++;
                        break;
                    case "8":
                        iJugadorClave++;
                        break;
                    default:
                        iSinAsignar++;
                        break;
                }
            }
            if (iJugadorFranquicia > 1 || iSextoHombre > 1 || iJugadorClave < 3 || iTitular != 4)
            {
                return false;
            }
            return true;

        }
        public virtual ActionResult ListEditRoles(string sidx, string sord, int page, int rows)
        {
            var players = db.Players.Where(x => x.ActualTeam == 1).AsEnumerable();

            var pageIndex = Convert.ToInt32(page) - 1;
            var pageSize = rows;
            var totalRecords = players.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);

            players = players.Skip(pageIndex * pageSize).Take(pageSize);

            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from contact in players
                    select new
                    {
                        i = contact.Id.ToString(),
                        cell = new string[] {
                            contact.CompleteName.ToString()
                        }
                    }).ToArray()
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
    }
}
