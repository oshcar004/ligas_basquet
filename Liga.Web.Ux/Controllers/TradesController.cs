﻿using Liga.Domain.Entities;
using Liga.Infrastructure.Data;
using Liga.Infrastructure.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;

namespace Liga.Web.UI.Controllers
{
    public class TradesController : BaseController
    {
        //
        // GET: /Trades/
        private Liga_DBContext db = new Liga_DBContext();
        

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult TransfersInfo()
        {
            List<LVNBAFranchises> LVNBAFranchisesList = db.LVNBAFranchises.ToList();
            return View(LVNBAFranchisesList);
        }

        public JsonResult GetTransferibles(int idFranchise)
        {
            var transferibles = db.Players.Where(x => x.IdState == 1 && x.ActualTeam == idFranchise).Select(x => new
            {
                Jugador = x.CompleteName,
                JugadorImagen = x.FaceImage,
                Equipo = x.LVNBAFranchis.Name,
                EquipoImagen = x.LVNBAFranchis.Icono.Replace(".png", ".jpg")
            });
            return Json(transferibles, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTransferiblesOld(int idFranchise)
        {
            var franchises = db.TransfersPlayersLists.Where(x => x.IdFranchise == idFranchise).Select(a => new
            {
                Jugador = a.Player.CompleteName
            });
            return Json(franchises, JsonRequestBehavior.AllowGet);
        }

        public ActionResult TransfersInfoOld()
        {            
            TradatorModel tModel = new TradatorModel();
            Salarios sal = new Salarios();
            if (ViewBag.IsManagerWithTeam == true)
            {
                int? idTeam = GetManagerTeam();
               

                ViewBag.IdTeam1 = new SelectList(db.LVNBAFranchises.OrderBy(x => x.Name), "Id", "Name", idTeam);
                tModel.ListPlayersTeam1 = new ListPlayersTradeTeam();


                tModel.ListPlayersTeam1.ListPlayersTeam = sal.getSalaryPlayers(idTeam);
                //db.PlayersSalariesFull.Where(x => x.IdLVNBAFranchises == idTeam && x.Player.IdState != 4);
                //foreach (PlayersSalaries salario in tModel.ListPlayersTeam1.ListPlayersTeam)
                //{
                //    if(salario.RookieRights == 1)
                //        salario.Salary1 = 0;
                //}
                tModel.ListPlayersTeam1.ListRoundsTeam = GetRounds(Convert.ToInt32(idTeam));
                tModel.Team1 = db.LVNBAFranchises.Where(x => x.Id == idTeam).First();
               
                tModel.ListPlayersTeam1.PresupuestoActual = sal.getSalaryTeam(Convert.ToInt32(idTeam));
            }
            else
            {
                ViewBag.IdTeam1 = new SelectList(db.LVNBAFranchises.OrderBy(x => x.Name), "Id", "Name", "");
                tModel.ListPlayersTeam1 = new ListPlayersTradeTeam();
            }
            ViewBag.IdTeam2 = new SelectList(db.LVNBAFranchises.OrderBy(x => x.Name), "Id", "Name", "");
            ViewBag.IdTeam3 = new SelectList(db.LVNBAFranchises.OrderBy(x => x.Name), "Id", "Name", "");
            ViewBag.IdTeam4 = new SelectList(db.LVNBAFranchises.OrderBy(x => x.Name), "Id", "Name", "");
            tModel.ListPlayersTeam2 = new ListPlayersTradeTeam();
            tModel.ListPlayersTeam3 = new ListPlayersTradeTeam();
            tModel.ListPlayersTeam4 = new ListPlayersTradeTeam();


            tModel.ListPlayersTeam1.idPosTeam = 1;
            tModel.ListPlayersTeam2.idPosTeam = 2;
            tModel.ListPlayersTeam3.idPosTeam = 3;
            tModel.ListPlayersTeam4.idPosTeam = 4;
            return View(tModel);
        }

        public IEnumerable<Rounds> GetRounds(int idOwner)
        {
            int iSeason = db.Parameters.First().Temporada;
            return db.Rounds.Where(x => x.Owner == idOwner && x.Year == iSeason).AsEnumerable();

        }
        public ActionResult PartialTradeTeam1(int id)
        {
            Salarios sal = new Salarios();
            // TODO: Populate the model (viewmodel) here using the id
            TradatorModel tModel = new TradatorModel();
            tModel.ListPlayersTeam1 = new ListPlayersTradeTeam();
            tModel.ListPlayersTeam1.ListPlayersTeam = sal.getSalaryPlayers(id); // db.PlayersSalariesFull.Where(x => x.IdLVNBAFranchises == id && x.Player.IdState != 4);
            tModel.ListPlayersTeam1.ListRoundsTeam = GetRounds(id);
            tModel.ListPlayersTeam1.idPosTeam = 1;
            //tModel.ListPlayersTeam2.idPosTeam = 2;
            //tModel.ListPlayersTeam3.idPosTeam = 3;
            //tModel.ListPlayersTeam4.idPosTeam = 4;
            return PartialView("TransferTeam", tModel.ListPlayersTeam1);
        }
        public ActionResult PartialRoundTradeTeam1(int id)
        {
            // TODO: Populate the model (viewmodel) here using the id
            TradatorModel tModel = new TradatorModel();
            tModel.ListPlayersTeam1 = new ListPlayersTradeTeam();
            tModel.ListPlayersTeam1.ListPlayersTeam = db.PlayersSalariesFull.Where(x => x.IdLVNBAFranchises == id && x.Player.IdState != 4);
            tModel.ListPlayersTeam1.ListRoundsTeam = GetRounds(id);
            tModel.ListPlayersTeam1.idPosTeam = 1;
            return PartialView("TransferRound", tModel.ListPlayersTeam1);
        }
        public ActionResult PartialTradeTeam2(int id)
        {
            Salarios sal = new Salarios();
            // TODO: Populate the model (viewmodel) here using the id
            TradatorModel tModel = new TradatorModel();
            tModel.ListPlayersTeam2 = new ListPlayersTradeTeam();
            tModel.ListPlayersTeam2.ListPlayersTeam = sal.getSalaryPlayers(id); // db.PlayersSalariesFull.Where(x => x.IdLVNBAFranchises == id && x.Player.IdState != 4);
            tModel.ListPlayersTeam2.ListRoundsTeam = GetRounds(id);
            tModel.ListPlayersTeam2.idPosTeam = 2;
            return PartialView("TransferTeam", tModel.ListPlayersTeam2);
        }
        public ActionResult PartialRoundTradeTeam2(int id)
        {
            // TODO: Populate the model (viewmodel) here using the id
            TradatorModel tModel = new TradatorModel();
            tModel.ListPlayersTeam2 = new ListPlayersTradeTeam();
            tModel.ListPlayersTeam2.ListPlayersTeam = db.PlayersSalariesFull.Where(x => x.IdLVNBAFranchises == id && x.Player.IdState != 4);
            tModel.ListPlayersTeam2.ListRoundsTeam = GetRounds(id);
            tModel.ListPlayersTeam2.idPosTeam =2;
            return PartialView("TransferRound", tModel.ListPlayersTeam2);
        }
        public ActionResult PartialTradeTeam3(int id)
        {
            Salarios sal = new Salarios();
            // TODO: Populate the model (viewmodel) here using the id
            TradatorModel tModel = new TradatorModel();
            tModel.ListPlayersTeam3 = new ListPlayersTradeTeam();
            tModel.ListPlayersTeam3.ListPlayersTeam = sal.getSalaryPlayers(id); // db.PlayersSalariesFull.Where(x => x.IdLVNBAFranchises == id && x.Player.IdState != 4);
            tModel.ListPlayersTeam3.ListRoundsTeam = GetRounds(id);
            tModel.ListPlayersTeam3.idPosTeam = 3;
            return PartialView("TransferTeam", tModel.ListPlayersTeam3);
        }
        public ActionResult PartialRoundTradeTeam3(int id)
        {
            // TODO: Populate the model (viewmodel) here using the id
            TradatorModel tModel = new TradatorModel();
            tModel.ListPlayersTeam3 = new ListPlayersTradeTeam();
            tModel.ListPlayersTeam3.ListPlayersTeam = db.PlayersSalariesFull.Where(x => x.IdLVNBAFranchises == id && x.Player.IdState != 4);
            tModel.ListPlayersTeam3.ListRoundsTeam = GetRounds(id);
            tModel.ListPlayersTeam3.idPosTeam = 3;

            return PartialView("TransferRound", tModel.ListPlayersTeam3);
        }
        public ActionResult PartialTradeTeam4(int id)
        {
            Salarios sal = new Salarios();
            // TODO: Populate the model (viewmodel) here using the id
            TradatorModel tModel = new TradatorModel();
            tModel.ListPlayersTeam4 = new ListPlayersTradeTeam();
            tModel.ListPlayersTeam4.ListPlayersTeam = sal.getSalaryPlayers(id); // db.PlayersSalariesFull.Where(x => x.IdLVNBAFranchises == id && x.Player.IdState != 4);
            tModel.ListPlayersTeam4.ListRoundsTeam = GetRounds(id);
            tModel.ListPlayersTeam4.idPosTeam = 4;
            return PartialView("TransferTeam", tModel.ListPlayersTeam4);
        }
        public ActionResult PartialRoundTradeTeam4(int id)
        {
            // TODO: Populate the model (viewmodel) here using the id
            TradatorModel tModel = new TradatorModel();
            tModel.ListPlayersTeam4 = new ListPlayersTradeTeam();
            tModel.ListPlayersTeam4.ListPlayersTeam = db.PlayersSalariesFull.Where(x => x.IdLVNBAFranchises == id && x.Player.IdState != 4);
            tModel.ListPlayersTeam4.ListRoundsTeam = GetRounds(id);
            tModel.ListPlayersTeam4.idPosTeam = 4;
            return PartialView("TransferRound", tModel.ListPlayersTeam4);
        }


        public ActionResult PartialSalaryTradeTeam1(int id)
        {
            Salarios sal = new Salarios();
            // TODO: Populate the model (viewmodel) here using the id
            TradatorModel tModel = new TradatorModel();
            tModel.ListPlayersTeam1 = new ListPlayersTradeTeam();
            tModel.ListPlayersTeam1.ListPlayersTeam = sal.getSalaryPlayers(id); // db.PlayersSalariesFull.Where(x => x.IdLVNBAFranchises == id && x.Player.IdState != 4);
            tModel.ListPlayersTeam1.ListRoundsTeam = GetRounds(id);
            tModel.ListPlayersTeam1.idPosTeam = 1;
            tModel.ListPlayersTeam1.PresupuestoActual = sal.getSalaryTeam(Convert.ToInt32(id));
            return PartialView("TransferSalary", tModel.ListPlayersTeam1);
        }
        public ActionResult PartialSalaryTradeTeam2(int id)
        {
          
            // TODO: Populate the model (viewmodel) here using the id
            TradatorModel tModel = new TradatorModel();
            tModel.ListPlayersTeam2 = new ListPlayersTradeTeam();
            Salarios sal = new Salarios();
            tModel.ListPlayersTeam2.PresupuestoActual = sal.getSalaryTeam(Convert.ToInt32(id));
            return PartialView("TransferSalary", tModel.ListPlayersTeam2);
        }
        public ActionResult PartialSalaryTradeTeam3(int id)
        {
            // TODO: Populate the model (viewmodel) here using the id
            TradatorModel tModel = new TradatorModel();
            tModel.ListPlayersTeam3 = new ListPlayersTradeTeam();
            Salarios sal = new Salarios();
            tModel.ListPlayersTeam3.PresupuestoActual = sal.getSalaryTeam(Convert.ToInt32(id));
            return PartialView("TransferSalary", tModel.ListPlayersTeam3);
        }
        public ActionResult PartialSalaryTradeTeam4(int id)
        {
            // TODO: Populate the model (viewmodel) here using the id
            TradatorModel tModel = new TradatorModel();
            tModel.ListPlayersTeam4 = new ListPlayersTradeTeam();
            Salarios sal = new Salarios();
            tModel.ListPlayersTeam4.PresupuestoActual = sal.getSalaryTeam(Convert.ToInt32(id));
            return PartialView("TransferSalary", tModel.ListPlayersTeam4);
        }

        public string ReturnImage(int id)
        {
            string icono = "../Images/Common/teams/Iconos/" + db.LVNBAFranchises.Where(x => x.Id == id).First().Icono;
            return icono;
        }
        public int? GetManagerTeam()
        {
            int? idTeam = null;
            if (System.Threading.Thread.CurrentPrincipal.Identity.IsAuthenticated)
            {
                if (Roles.IsUserInRole("Manager"))
                {
                    string username = System.Threading.Thread.CurrentPrincipal.Identity.Name.ToString();

                    UserProfile user = db.UserProfiles.First(u => u.UserName == username);
                    Managers managers = db.Managers.FirstOrDefault(p => p.UserId == user.UserId);
                    if (managers != null && managers.ManagersFranchises.Where(x => x.IdManager == managers.Id && x.EndDate == null).FirstOrDefault() != null)
                    {
                        idTeam = db.LVNBAFranchises.Where(x => x.IdManager == managers.Id).First().Id;
                    }

                }
            }
            return idTeam;
        }


        public ActionResult ValidateTransferInfo(string equipo1, string equipo2, string equipo3, string equipo4, string infoJug1, string infoRound1, string infoJug2, string infoRound2, string infoJug3, string infoRound3, string infoJug4, string infoRound4)
        {
            TradeatorInfo tradeator = new TradeatorInfo();
            string sMotinoNoValido = "";
            int equipo = 0;
            decimal dif = 0;
            try
            {
                Salarios sal = new Salarios();
                

                tradeator.listaPlayers1 = new List<Players>(); tradeator.listaRondasTeam1 = new List<Rounds>();
                tradeator.listaPlayers2 = new List<Players>(); tradeator.listaRondasTeam2 = new List<Rounds>();
                tradeator.listaPlayers3 = new List<Players>(); tradeator.listaRondasTeam3 = new List<Rounds>();
                tradeator.listaPlayers4 = new List<Players>(); tradeator.listaRondasTeam4 = new List<Rounds>();
                tradeator.listaPlayersDa1 = new List<Players>(); tradeator.listaPlayersDa2 = new List<Players>();
                tradeator.listaPlayersDa3 = new List<Players>(); tradeator.listaPlayersDa4 = new List<Players>();
                tradeator.listaRondasDa1 = new List<Rounds>(); tradeator.listaRondasDa2 = new List<Rounds>();
                tradeator.listaRondasDa3 = new List<Rounds>(); tradeator.listaRondasDa4 = new List<Rounds>();

                //obtenemos lo que recibe cada equipo en funcion de rondas y jugadores
                if (equipo1 == "" || equipo2 == "")
                {
                    tradeator.esValido = 0;
                    tradeator.sMotivoNoValido = "En un traspaso deben estar informados los dos primeros equipos como mínimo";
                    return PartialView("TradeInfo");
                }
                else
                {
                    tradeator.listaPlayers1 = dameListaTrade(infoJug1);
                    tradeator.listaRondasTeam1 = dameRondasTrade(infoRound1);
                    equipo = Convert.ToInt32(equipo1);
                    tradeator.Equipo1 = db.LVNBAFranchises.Where(x => x.Id == equipo).First();
                    tradeator.PresupuestoAntes1 = sal.getSalaryTeam(equipo);

                    tradeator.listaPlayers2 = dameListaTrade(infoJug2);
                    tradeator.listaRondasTeam2 = dameRondasTrade(infoRound2);
                    equipo = Convert.ToInt32(equipo2);
                    tradeator.Equipo2 = db.LVNBAFranchises.Where(x => x.Id == equipo).First();
                    tradeator.PresupuestoAntes2 = sal.getSalaryTeam(equipo);

                }
                if (equipo3 != "")
                {
                    tradeator.listaPlayers3 = dameListaTrade(infoJug3);
                    tradeator.listaRondasTeam3 = dameRondasTrade(infoRound3);
                    equipo = Convert.ToInt32(equipo3);
                    tradeator.Equipo3 = db.LVNBAFranchises.Where(x => x.Id == equipo).First();
                    tradeator.PresupuestoAntes3 = sal.getSalaryTeam(equipo);

                }
                if (equipo4 != "")
                {
                    tradeator.listaPlayers4 = dameListaTrade(infoJug4);
                    tradeator.listaRondasTeam4 = dameRondasTrade(infoRound4);
                    equipo = Convert.ToInt32(equipo4);
                    tradeator.Equipo4 = db.LVNBAFranchises.Where(x => x.Id == equipo).First();
                    tradeator.PresupuestoAntes4 = sal.getSalaryTeam(equipo);

                }
                //una vez sabemos lo que recibe cada uno , sabemos que es lo que da cada uno

                tradeator.listaPlayersDa1 = dameLosQueDa(1, tradeator.Equipo1.Id, tradeator.listaPlayers1, tradeator.listaPlayers2, tradeator.listaPlayers3, tradeator.listaPlayers4);
                tradeator.listaPlayersDa2 = dameLosQueDa(2, tradeator.Equipo2.Id, tradeator.listaPlayers1, tradeator.listaPlayers2, tradeator.listaPlayers3, tradeator.listaPlayers4);
                tradeator.listaRondasDa1 = dameLasRondaQueDa(1, tradeator.Equipo1.Id, tradeator.listaRondasTeam1, tradeator.listaRondasTeam2, tradeator.listaRondasTeam3, tradeator.listaRondasTeam4);
                tradeator.listaRondasDa2 = dameLasRondaQueDa(2, tradeator.Equipo2.Id, tradeator.listaRondasTeam1, tradeator.listaRondasTeam2, tradeator.listaRondasTeam3, tradeator.listaRondasTeam4);
                if (equipo3 != "")
                {
                    tradeator.listaPlayersDa3 = dameLosQueDa(3, tradeator.Equipo3.Id, tradeator.listaPlayers1, tradeator.listaPlayers2, tradeator.listaPlayers3, tradeator.listaPlayers4);
                    tradeator.listaRondasDa3 = dameLasRondaQueDa(3, tradeator.Equipo3.Id, tradeator.listaRondasTeam1, tradeator.listaRondasTeam2, tradeator.listaRondasTeam3, tradeator.listaRondasTeam4);

                }
                if (equipo4 != "")
                {
                    tradeator.listaPlayersDa4 = dameLosQueDa(4, tradeator.Equipo4.Id, tradeator.listaPlayers1, tradeator.listaPlayers2, tradeator.listaPlayers3, tradeator.listaPlayers4);
                    tradeator.listaRondasDa4 = dameLasRondaQueDa(4, tradeator.Equipo4.Id, tradeator.listaRondasTeam1, tradeator.listaRondasTeam2, tradeator.listaRondasTeam3, tradeator.listaRondasTeam4);

                }

                //Calculamos el presupuesto antes del traspaso y después 
                //OJO RELACIONES (JUGADORES CON DOS SALARIOS!!)
               

                dif = Convert.ToDecimal(tradeator.listaPlayers1.Sum(x => x.PlayersSalariesFull.First().Salarie)) - Convert.ToDecimal(tradeator.listaPlayersDa1.Sum(x => x.PlayersSalariesFull.First().Salarie));
                tradeator.PresupuestoDespues1 = tradeator.PresupuestoAntes1 + dif;

                dif = Convert.ToDecimal(tradeator.listaPlayers2.Sum(x => x.PlayersSalariesFull.First().Salarie)) - Convert.ToDecimal(tradeator.listaPlayersDa2.Sum(x => x.PlayersSalariesFull.First().Salarie));
                tradeator.PresupuestoDespues2 = tradeator.PresupuestoAntes2 + dif;
                if (equipo3 != "")
                {
                    dif = Convert.ToDecimal(tradeator.listaPlayers3.Sum(x => x.PlayersSalariesFull.First().Salarie)) - Convert.ToDecimal(tradeator.listaPlayersDa3.Sum(x => x.PlayersSalariesFull.First().Salarie));
                    tradeator.PresupuestoDespues3 = tradeator.PresupuestoAntes3 + dif;
                }
                if (equipo4 != "")
                {
                    dif = Convert.ToDecimal(tradeator.listaPlayers4.Sum(x => x.PlayersSalariesFull.First().Salarie)) - Convert.ToDecimal(tradeator.listaPlayersDa4.Sum(x => x.PlayersSalariesFull.First().Salarie));
                    tradeator.PresupuestoDespues4 = tradeator.PresupuestoAntes4 + dif;
                }

                //en este punto tenemos la informacion relacionada con el traspaso
                //necesitamos validar si es valido un traspaso o no
                Traspasos traspasosLogica = new Traspasos();
                tradeator.esValido = traspasosLogica.IsTradeValid(tradeator, out sMotinoNoValido);
                if (tradeator.esValido == 1)
                {
                    tradeator.sMotivoNoValido = "El traspaso es válido";
                }
                else
                {
                    tradeator.sMotivoNoValido = sMotinoNoValido;
                }

                return PartialView("TradeInfo", tradeator);
            }
            catch (Exception ex)
            {
                tradeator.esValido = 0;
                tradeator.sMotivoNoValido = "Ha ocurrido el siguiente error: " + ex.ToString();
                return PartialView("TradeInfo");
            }
        }



        public List<Players> dameListaTrade(string sCadenaPlayers)
        {
            List<Players> jugadores = new List<Players>();
            if (sCadenaPlayers != "")
            {
                sCadenaPlayers = sCadenaPlayers.Substring(0, sCadenaPlayers.Length - 1);
                string[] playerId = sCadenaPlayers.Split(';');
                foreach (var player in playerId)
                {
                    int idPlayer = Convert.ToInt32(player);
                    Players jug = db.Players.Where(x => x.Id == idPlayer).First();
                    jugadores.Add(jug);
                }
            }
            return jugadores;
        }
        public List<Rounds> dameRondasTrade(string sCadenaRondas)
        {
            List<Rounds> rondas = new List<Rounds>();
            if (sCadenaRondas != "")
            {
                sCadenaRondas = sCadenaRondas.Substring(0, sCadenaRondas.Length - 1);
                string[] roundId = sCadenaRondas.Split(';');
                foreach (var rounda in roundId)
                {
                    int idRonda = Convert.ToInt32(rounda);
                    Rounds ron = db.Rounds.Where(x => x.Id == idRonda).First();
                    rondas.Add(ron);
                }
            }
            return rondas;
        }


        public List<Players> dameLosQueDa(int queEquipo,int idEquipo, List<Players> listaRecibe1, List<Players> listaRecibe2, List<Players> listaRecibe3, List<Players> listaRecibe4)
        {
            List<Players> jugadores = new List<Players>();
            switch (queEquipo)
            {
                case 1:
                    jugadores.AddRange(listaRecibe2.Where(x => x.PlayersSalariesFull.Any(d => d.IdLVNBAFranchises== idEquipo)).ToList());
                    jugadores.AddRange(listaRecibe3.Where(x => x.PlayersSalariesFull.Any(d => d.IdLVNBAFranchises == idEquipo)).ToList());
                    jugadores.AddRange(listaRecibe4.Where(x => x.PlayersSalariesFull.Any(d => d.IdLVNBAFranchises == idEquipo)).ToList());
                    break;
                case 2:
                    jugadores.AddRange(listaRecibe1.Where(x => x.PlayersSalariesFull.Any(d => d.IdLVNBAFranchises == idEquipo)).ToList());
                    jugadores.AddRange(listaRecibe3.Where(x => x.PlayersSalariesFull.Any(d => d.IdLVNBAFranchises == idEquipo)).ToList());
                    jugadores.AddRange(listaRecibe4.Where(x => x.PlayersSalariesFull.Any(d => d.IdLVNBAFranchises == idEquipo)).ToList());
                    break;
                case 3:
                    jugadores.AddRange(listaRecibe1.Where(x => x.PlayersSalariesFull.Any(d => d.IdLVNBAFranchises == idEquipo)).ToList());
                    jugadores.AddRange(listaRecibe2.Where(x => x.PlayersSalariesFull.Any(d => d.IdLVNBAFranchises == idEquipo)).ToList());
                    jugadores.AddRange(listaRecibe4.Where(x => x.PlayersSalariesFull.Any(d => d.IdLVNBAFranchises == idEquipo)).ToList());
                    break;
                case 4:
                    jugadores.AddRange(listaRecibe1.Where(x => x.PlayersSalariesFull.Any(d => d.IdLVNBAFranchises == idEquipo)).ToList());
                    jugadores.AddRange(listaRecibe2.Where(x => x.PlayersSalariesFull.Any(d => d.IdLVNBAFranchises == idEquipo)).ToList());
                    jugadores.AddRange(listaRecibe3.Where(x => x.PlayersSalariesFull.Any(d => d.IdLVNBAFranchises == idEquipo)).ToList());
                    break;
            }
            return jugadores;
        }
        public List<Rounds> dameLasRondaQueDa(int queEquipo, int idEquipo, List<Rounds> listaRecibe1, List<Rounds> listaRecibe2, List<Rounds> listaRecibe3, List<Rounds> listaRecibe4)
        {
            List<Rounds> rondas = new List<Rounds>();
            switch (queEquipo)
            {
                case 1:
                    rondas.AddRange(listaRecibe2.Where(x => x.Owner == idEquipo).ToList());
                    rondas.AddRange(listaRecibe3.Where(x => x.Owner == idEquipo).ToList());
                    rondas.AddRange(listaRecibe4.Where(x => x.Owner == idEquipo).ToList());
                    break;
                case 2:
                    rondas.AddRange(listaRecibe1.Where(x => x.Owner == idEquipo).ToList());
                    rondas.AddRange(listaRecibe3.Where(x => x.Owner == idEquipo).ToList());
                    rondas.AddRange(listaRecibe4.Where(x => x.Owner == idEquipo).ToList());
                    break;
                case 3:
                    rondas.AddRange(listaRecibe1.Where(x => x.Owner == idEquipo).ToList());
                    rondas.AddRange(listaRecibe2.Where(x => x.Owner == idEquipo).ToList());
                    rondas.AddRange(listaRecibe4.Where(x => x.Owner == idEquipo).ToList());
                    break;
                case 4:
                    rondas.AddRange(listaRecibe1.Where(x => x.Owner == idEquipo).ToList());
                    rondas.AddRange(listaRecibe2.Where(x => x.Owner == idEquipo).ToList());
                    rondas.AddRange(listaRecibe3.Where(x => x.Owner == idEquipo).ToList());
                    break;
            }
            return rondas;
        }
       


        [Authorize(Roles = "Manager")]
        [HttpPost]
        public ActionResult NotifyTrade(TradeatorInfo g, FormCollection r, string submitButton)
        {
 
            int iEstado = 0;
            switch (submitButton)
            {
                case "Notificar":
                    iEstado = 1;
                    break;
                case "Guardar Borrador":
                    iEstado =0;
                    break;
            }

            Transfers traspaso = new Transfers();
            traspaso.ManagerCreator = @ViewBag.IdManager;
            traspaso.TransferDate = DateTime.Now;
            traspaso.IdTransferState = iEstado;
            traspaso.Confirmed1 = true;

            traspaso.IdFranchise1 = g.Equipo1.Id;
            traspaso.IdFranchise2 = g.Equipo2.Id;
            if(g.Equipo3 != null)
                traspaso.IdFranchise3 = g.Equipo3.Id;
            if(g.Equipo4 != null)
                traspaso.IdFranchise4 = g.Equipo4.Id;

            string []sCom = r.GetValues("aComentario");
            string sComen = sCom[0];
            if (r.GetValues("ListaRecibe1") != null)
            {
                foreach (string sJugador in r.GetValues("ListaRecibe1"))
                {
                    int idJugador = Convert.ToInt32(sJugador);
                    TransfersPlayersLists playerList = new TransfersPlayersLists();
                    playerList.IdFranchise = db.PlayersSalariesFull.Where(x => x.IdPlayer == idJugador).First().IdLVNBAFranchises;
                    playerList.IdPlayer = idJugador;
                    playerList.Destination = g.Equipo1.Id;
                    traspaso.TransfersPlayersLists.Add(playerList);
                }
            }
            if (r.GetValues("RondasRecibe1") != null)
            {
                foreach (string sRonda in r.GetValues("RondasRecibe1"))
                {
                    int idRonda = Convert.ToInt32(sRonda);
                    TransfersRoundsLists roundList = new TransfersRoundsLists();
                    roundList.IdFranchise = db.Rounds.Where(x => x.Id == idRonda).First().Owner;
                    roundList.IdRound = idRonda;
                    roundList.Destination = g.Equipo1.Id;
                    traspaso.TransfersRoundsLists.Add(roundList);
                }
            }
            if (r.GetValues("ListaRecibe2") != null)
            {
                foreach (string sJugador in r.GetValues("ListaRecibe2"))
                {
                    int idJugador = Convert.ToInt32(sJugador);
                    TransfersPlayersLists playerList = new TransfersPlayersLists();
                    playerList.IdFranchise = db.PlayersSalariesFull.Where(x => x.Id == idJugador).First().IdLVNBAFranchises;
                    playerList.IdPlayer = idJugador;
                    playerList.Destination = g.Equipo2.Id;
                    traspaso.TransfersPlayersLists.Add(playerList);
                }
            }
            if (r.GetValues("RondasRecibe2") != null)
            {
                foreach (string sRonda in r.GetValues("RondasRecibe2"))
                {
                    int idRonda = Convert.ToInt32(sRonda);
                    TransfersRoundsLists roundList = new TransfersRoundsLists();
                    roundList.IdFranchise = db.Rounds.Where(x => x.Id == idRonda).First().Owner;
                    roundList.IdRound = idRonda;
                    roundList.Destination = g.Equipo2.Id;
                    traspaso.TransfersRoundsLists.Add(roundList);
                }
            }
            if (g.Equipo3 != null)
            {
                if (r.GetValues("ListaRecibe3") != null)
                {
                    foreach (string sJugador in r.GetValues("ListaRecibe3"))
                    {
                        int idJugador = Convert.ToInt32(sJugador);
                        TransfersPlayersLists playerList = new TransfersPlayersLists();
                        playerList.IdFranchise = db.PlayersSalariesFull.Where(x => x.Id == idJugador).First().IdLVNBAFranchises;
                        playerList.IdPlayer = idJugador;
                        playerList.Destination = g.Equipo3.Id;
                        traspaso.TransfersPlayersLists.Add(playerList);
                    }
                }
                if (r.GetValues("RondasRecibe3") != null)
                {
                    foreach (string sRonda in r.GetValues("RondasRecibe3"))
                    {
                        int idRonda = Convert.ToInt32(sRonda);
                        TransfersRoundsLists roundList = new TransfersRoundsLists();
                        roundList.IdFranchise = db.Rounds.Where(x => x.Id == idRonda).First().Owner;
                        roundList.IdRound = idRonda;
                        roundList.Destination = g.Equipo3.Id;
                        traspaso.TransfersRoundsLists.Add(roundList);
                    }
                }


            }
            if (g.Equipo4 != null)
            {
                if (r.GetValues("ListaRecibe4") != null)
                {
                    foreach (string sJugador in r.GetValues("ListaRecibe4"))
                    {
                        int idJugador = Convert.ToInt32(sJugador);
                        TransfersPlayersLists playerList = new TransfersPlayersLists();
                        playerList.IdFranchise = db.PlayersSalariesFull.Where(x => x.Id == idJugador).First().IdLVNBAFranchises;
                        playerList.IdPlayer = idJugador;
                        playerList.Destination = g.Equipo4.Id;
                        traspaso.TransfersPlayersLists.Add(playerList);
                    }
                }
                if (r.GetValues("RondasRecibe4") != null)
                {
                    foreach (string sRonda in r.GetValues("RondasRecibe4"))
                    {
                        int idRonda = Convert.ToInt32(sRonda);
                        TransfersRoundsLists roundList = new TransfersRoundsLists();
                        roundList.IdFranchise = db.Rounds.Where(x => x.Id == idRonda).First().Owner;
                        roundList.IdRound = idRonda;
                        roundList.Destination = g.Equipo4.Id;
                        traspaso.TransfersRoundsLists.Add(roundList);
                    }
                }


            }
            db.Transfers.Add(traspaso);
            db.SaveChanges();

            SendingMails oMailer = new SendingMails();
            oMailer.propuestaTraspaso(traspaso.ManagerCreator.Value, traspaso.Id);
            return RedirectToAction("TransfersInfo", "Trades");
        }





    }
}
