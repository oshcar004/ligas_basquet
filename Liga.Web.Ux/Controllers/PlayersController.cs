﻿using Liga.Domain.Entities;
using Liga.Infrastructure.Data;
using Liga.Infrastructure.Data.Repository;
using Omu.ValueInjecter;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI.WebControls;

namespace Liga.Web.UI.Controllers
{
    
    public class PlayersController : BaseController
    {
        private Liga_DBContext db = new Liga_DBContext();
        private PlayerRepository playerRepository = new PlayerRepository();

        //
        // GET: /Players/

        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = string.IsNullOrEmpty(sortOrder) ? "Name_desc" : "";
            ViewBag.TeamSortParm = sortOrder == "Team" ? "Team_desc" : "Team";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;

            var players = db.Players.Include(p => p.LVNBAFranchis).Include(p => p.PlayersState).Include(p => p.PlayersPosition).Include(p => p.PlayersRole);
            if (!string.IsNullOrEmpty(searchString))
            {
                players = players.Where(s => s.CompleteName.ToUpper().Contains(searchString.ToUpper())
                                       || s.LVNBAFranchis.Name.ToUpper().Contains(searchString.ToUpper()));
            }
            switch (sortOrder)
            {
                case "Name_desc":
                    players = players.OrderByDescending(s => s.CompleteName);
                    break;
                case "Team":
                    players = players.OrderBy(s => s.LVNBAFranchis.Name);
                    break;
                case "Team_desc":
                    players = players.OrderByDescending(s => s.LVNBAFranchis.Name);
                    break;
                default:
                    players = players.OrderBy(s => s.CompleteName);
                    break;
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);

            return View(players.ToPagedList(pageNumber, pageSize));
        }
        public ActionResult FreeAgent(string sortOrder, string currentFilter, string searchString, int? page, string message = "")
        {
            var par = db.Parameters.FirstOrDefault();
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = string.IsNullOrEmpty(sortOrder) ? "Name_desc" : "";
            ViewBag.Message = message;
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;
            var players = db.Players.Include(p => p.LVNBAFranchis).
                    Include(p => p.PlayersState).
                    Include(p => p.PlayersPosition).
                    Include(p => p.PlayersRole)
                    .Where(x => (x.ActualTeam == null || x.Cutted == 1 || x.PlayersSalariesFull.Where(pa => pa.DateSalarie.Year >= par.Temporada && pa.Salarie > 1).Count() == 0) && x.Retired == false );

            if (!string.IsNullOrEmpty(searchString))
            {
                players = players.Where(s => s.CompleteName.ToUpper().Contains(searchString.ToUpper()));
            }
            switch (sortOrder)
            {
                case "Name_desc":
                    players = players.OrderByDescending(s => s.CompleteName);
                    break;
                default:
                    players = players.OrderBy(s => s.CompleteName);
                    break;
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(players.ToPagedList(pageNumber, pageSize));
        }
        public ActionResult RestrictedAgent(string sortOrder, string currentFilter, string searchString, int? page, string message = "")
        {
            int temporada = db.Parameters.First().Temporada;
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = string.IsNullOrEmpty(sortOrder) ? "Name_desc" : "";
            ViewBag.Message = message;
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;
            var players = db.Players
                    .Where(x => x.PlayersSalariesFull.Where(y => y.DateSalarie.Year >= temporada && (y.IdCondition == 1 || y.IdCondition == 3)).Count() > 0);
            if (!string.IsNullOrEmpty(searchString))
            {
                players = players.Where(s => s.CompleteName.ToUpper().Contains(searchString.ToUpper()));
            }
            switch (sortOrder)
            {
                case "Name_desc":
                    players = players.OrderByDescending(s => s.CompleteName);
                    break;
                default:
                    players = players.OrderBy(s => s.CompleteName);
                    break;
            }
            int pageSize = 30;
            int pageNumber = (page ?? 1);
            return View(players.ToPagedList(pageNumber, pageSize));
        }
        //
        // GET: /Players/Details/5

        public ActionResult Details(int id = 0)
        {
            Players players = db.Players.Find(id);
            if (players == null)
            {
                return HttpNotFound();
            }
            return View(players);
        }

        //
        // GET: /Players/Create
        [Authorize(Roles = "Administrador,Comisionado")]
        public ActionResult Create()
        {
            ViewBag.ActualTeam = new SelectList(db.LVNBAFranchises, "Id", "Name");
            ViewBag.IdState = new SelectList(db.PlayersStates, "Id", "State");
            ViewBag.IdPosition = new SelectList(db.PlayersPositions, "Id", "Position");
            ViewBag.IdRol = new SelectList(db.PlayersRoles, "Id", "Rol");
            ViewBag.Id = new SelectList(db.PlayersSalariesFull, "Id", "EuropeTeam");
            return View();
        }

        //
        // POST: /Players/Create
        [Authorize(Roles = "Administrador,Comisionado")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Players players)
        {

            if (ModelState.IsValid)
            {
                db.Players.Add(players);

                //creamos el salario del jugador por ahora vacio
                if(db.PlayersSalariesFull.Where(x=>x.IdPlayer == players.Id).FirstOrDefault() == null)
                {
                    DateTime fecha = DateTime.Now;
                    for (int i = 0; i < 5; i++)
                    {
                        var playerSalary = new PlayersSalariesFull
                        {
                            IdPlayer = players.Id,
                            IdLVNBAFranchises = players.ActualTeam.Value,
                            Salarie = 0,
                            DateSalarie = fecha,
                            IdCondition = 6
                        };
                        db.PlayersSalariesFull.Add(playerSalary);
                        fecha = new DateTime(fecha.Year+1, fecha.Month, fecha.Day);
                    }

                }
                PlayersHistories playerHistory = new PlayersHistories();
                if(db.PlayersHistories.Find(players.Id) == null && players.ActualTeam != null)
                {
                    playerHistory.IdPlayer = players.Id;
                    playerHistory.IdFranchise = players.ActualTeam.Value;
                    playerHistory.StartDate = DateTime.Now;
                    db.PlayersHistories.Add(playerHistory);
                }
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            ViewBag.ActualTeam = new SelectList(db.LVNBAFranchises.OrderBy(x => x.Name), "Id", "Name", players.ActualTeam);
            ViewBag.IdState = new SelectList(db.PlayersStates, "Id", "State", players.IdState);
            ViewBag.IdPosition = new SelectList(db.PlayersPositions, "Id", "Position", players.IdPosition);
            ViewBag.IdRol = new SelectList(db.PlayersRoles, "Id", "Rol", players.IdRol);
            ViewBag.Id = new SelectList(db.PlayersSalariesFull, "Id", "EuropeTeam", players.Id);
            return View(players);
        }

        //
        // GET: /Players/Tender/5
        
        public ActionResult Tender(int id = 0)
        {
            int franchise2 = 0;
            franchise2 = GetManagerTeam();
            if (franchise2 < 1)
                return RedirectToAction("Index");
            string salary = db.PlayersSalariesFull.Where(x => x.IdLVNBAFranchises == franchise2).Sum(x => x.Salarie).ToString();
            string LuxuryTax = db.Parameters.First().ImpuestoLujo.ToString();
            string SalaryTax = db.Parameters.First().LimiteSalarial.ToString();
            if (salary == null || salary == "") salary = "0";
            List<PlayersSalariesFull> playersSalariesFull = db.PlayersSalariesFull.Where(x => (x.IdLVNBAFranchises == franchise2 && x.IdPlayer == id && x.IdCondition == 4)).ToList();
            if (playersSalariesFull == null || playersSalariesFull.Count > 0)
            {
                return RedirectToAction("FreeAgent", new { message = "No puede volver a ofertar por este jugador." });
            }
            // Busca que la franquicia no tenga oferta activa de un jugador(oferta activa: Accepted y Rejected deben ser null, significa que aun esta sin resolucion desde el comisionado) 
            FreeAgentOffers freeAgentOffer = db.FreeAgentOffers.Where(x => x.IdFranchise == franchise2).Where(x => x.IdPlayer == id).Where(x => x.Accepted == null).Where(x => x.Rejected == null).FirstOrDefault();
            if (freeAgentOffer != null)
            {
                return RedirectToAction("FreeAgent", new { message = "Usted ya posee una oferta activa de este jugador." });
            }
            Players players = db.Players.Find(id);
            if (players == null)
            {
                return HttpNotFound();
            }
            ViewBag.ActualTeam = new SelectList(db.LVNBAFranchises.OrderBy(x => x.Name), "Id", "Name", players.ActualTeam);
            ViewBag.IdState = new SelectList(db.PlayersStates, "Id", "State", players.IdState);
            ViewBag.IdPosition = new SelectList(db.PlayersPositions, "Id", "Position", players.IdPosition);
            ViewBag.IdRol = new SelectList(db.PlayersRoles, "Id", "Rol", players.IdRol);
            ViewBag.Id = new SelectList(db.PlayersSalariesFull, "Id", "EuropeTeam", players.Id);
            ViewBag.Conditions = db.SalaryConditions.ToList();
            PlayerOfertInfo playerofertInfo = new PlayerOfertInfo
            {
                Salary = salary,
                Player = players,
                LuxuryTax = LuxuryTax,
                LimSalary = SalaryTax
            };
            return View(playerofertInfo);
        }

        //
        // GET: /Players/Edit/5
        [Authorize(Roles = "Administrador,Comisionado")]
        public ActionResult Edit(int id = 0)
        {
            Players players = db.Players.Find(id);
            if (players == null)
            {
                return HttpNotFound();
            }
            var equipos =  new SelectList(db.LVNBAFranchises.OrderBy(x => x.Name), "Id", "Name", players.ActualTeam);
            var li = new SelectListItem
            {
                Value = "-1",
                Text = "Ninguna"
            };
            equipos.InjectFrom(li);
            ViewBag.ActualTeam = equipos;
            ViewBag.IdState = new SelectList(db.PlayersStates, "Id", "State", players.IdState);
            ViewBag.IdPosition = new SelectList(db.PlayersPositions, "Id", "Position", players.IdPosition);
            ViewBag.IdRol = new SelectList(db.PlayersRoles, "Id", "Rol", players.IdRol);
            ViewBag.Id = new SelectList(db.PlayersSalariesFull, "Id", "EuropeTeam", players.Id);
            return View(players);
        }

        public int GetManagerTeam()
        {
            int idTeam = 0;
            if (System.Threading.Thread.CurrentPrincipal.Identity.IsAuthenticated)
            {
                if (Roles.IsUserInRole("Manager"))
                {
                    string username = System.Threading.Thread.CurrentPrincipal.Identity.Name.ToString();

                    UserProfile user = db.UserProfiles.First(u => u.UserName == username);
                    Managers managers = db.Managers.FirstOrDefault(p => p.UserId == user.UserId);
                    if (managers != null && managers.ManagersFranchises.Where(x => x.IdManager == managers.Id && x.EndDate == null).FirstOrDefault() != null)
                    {
                        idTeam = db.LVNBAFranchises.Where(x => x.IdManager == managers.Id).First().Id;
                    }

                }
            }
            return idTeam;
        }

        //
        // GET: /Players/Tender/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Tender(int id = 0, int? franchise1 = null, decimal ofert1 = 0, decimal ofert2 = 0, decimal ofert3 = 0, decimal ofert4 = 0, decimal ofert5 = 0, int c1 = 0, int c2 = 0, int c3 = 0, int c4 = 0, int c5 = 0)
        {
            int franchise2 = GetManagerTeam();
            if (franchise2 < 1)
                return RedirectToAction("Index");
            //franchise1 = 1;
            // Busca que la franquicia no tenga oferta activa de un jugador(oferta activa: Accepted y Rejected deben ser null, significa que aun esta sin resolucion desde el comisionado) 
            int temporada = db.Parameters.First().Temporada;
            FreeAgentOffers freeAgentOffer = new FreeAgentOffers
            {
                IdPlayer = id,
                IdFranchise = franchise2,
                Salary1 = 0,
                Salary2 = 0,
                Salary3 = 0,
                Salary4 = 0,
                Salary5 = 0,
                IdCondition1 = 6,
                IdCondition2 = 6,
                IdCondition3 = 6,
                IdCondition4 = 6,
                IdCondition5 = 6
            };
            if (ofert1 > 0)
            {
                freeAgentOffer.Salary1 = ofert1;
                freeAgentOffer.IdCondition1 = c1;
            }
            if (ofert2 > 0)
            {
                freeAgentOffer.Salary2 = ofert2;
                freeAgentOffer.IdCondition2 = c2;
            }
            if (ofert3 > 0)
            {
                freeAgentOffer.Salary3 = ofert3;
                freeAgentOffer.IdCondition3 = c3;
            }
            if (ofert4 > 0)
            {
                freeAgentOffer.Salary4 = ofert4;
                freeAgentOffer.IdCondition4 = c4;
            }
            if (ofert5 > 0)
            {
                freeAgentOffer.Salary5 = ofert5;
                freeAgentOffer.IdCondition5 = c5;
            }
            var date = DateTime.Now;
            date = new DateTime(db.Parameters.FirstOrDefault().Temporada, date.Month, date.Day);
            freeAgentOffer.OfferDate = date;
            try
            {
                int id_freeAgentOffer = playerRepository.SaveFreeAgentOfert(freeAgentOffer);
                Transfers transfer;
                if (franchise1 != null)
                {
                    transfer = new Transfers
                    {
                        IdFranchise1 = franchise1,
                        IdFranchise2 = franchise2,
                        IdTransferState = 3,
                        TransferDate = DateTime.Now
                    };
                }
                else
                {
                transfer = new Transfers
                {
                    IdFranchise2 = franchise2,
                    IdTransferState = 3,
                    TransferDate = DateTime.Now
                    };
                }
                db.Transfers.Add(transfer);
                db.SaveChanges();
                int id_transfer = transfer.Id;
                TransfersPlayersLists transfersPlayersLists;
                if (franchise1 != null)
                {
                    transfersPlayersLists = new TransfersPlayersLists
                    {
                        IdPlayer = id,
                        IdTransfer = id_transfer,
                        IdFranchise = franchise1,
                        Destination = franchise2
                    };
                }
                else
                {
                    transfersPlayersLists = new TransfersPlayersLists
                    {
                        IdPlayer = id,
                        IdTransfer = id_transfer,
                        Destination = franchise2
                    };
                }
                db.TransfersPlayersLists.Add(transfersPlayersLists);
                db.SaveChanges();
                int id_transferPlayersLists = transfersPlayersLists.Id;
                return RedirectToAction("FreeAgent");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //
        // POST: /Players/Edit/5
        [Authorize(Roles = "Administrador,Comisionado")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Players players)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(players).State = EntityState.Modified;
                    //modificamos el equipo de la tabla salarios o creamos el salario del jugador
                    if(players.ActualTeam == 0)
                    {
                        players.ActualTeam = null;
                    }
                    var pSal = db.PlayersSalariesFull.Where(x => x.IdPlayer == players.Id).FirstOrDefault();
                    if (pSal != null)
                    {
                        if (pSal.IdLVNBAFranchises != players.ActualTeam)
                        {
                            pSal.IdLVNBAFranchises = players.ActualTeam.Value;
                        }
                    }
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.ActualTeam = new SelectList(db.LVNBAFranchises.OrderBy(x => x.Name), "Id", "Name", players.ActualTeam);
                ViewBag.IdState = new SelectList(db.PlayersStates, "Id", "State", players.IdState);
                ViewBag.IdPosition = new SelectList(db.PlayersPositions, "Id", "Position", players.IdPosition);
                ViewBag.IdRol = new SelectList(db.PlayersRoles, "Id", "Rol", players.IdRol);
                ViewBag.Id = new SelectList(db.PlayersSalariesFull, "Id", "EuropeTeam", players.Id);
                return View(players);
            }
            catch(Exception ex)
            {
                ViewBag.ActualTeam = new SelectList(db.LVNBAFranchises.OrderBy(x => x.Name), "Id", "Name", players.ActualTeam);
                ViewBag.IdState = new SelectList(db.PlayersStates, "Id", "State", players.IdState);
                ViewBag.IdPosition = new SelectList(db.PlayersPositions, "Id", "Position", players.IdPosition);
                ViewBag.IdRol = new SelectList(db.PlayersRoles, "Id", "Rol", players.IdRol);
                ViewBag.Id = new SelectList(db.PlayersSalariesFull, "Id", "EuropeTeam", players.Id);
                return View(players);
            }
        }

        //
        // GET: /Players/Delete/5
        [Authorize(Roles = "Administrador,Comisionado")]
        public ActionResult Delete(int id = 0)
        {
            Players players = db.Players.Find(id);
            if (players == null)
            {
                return HttpNotFound();
            }
            return View(players);
        }

        //
        // POST: /Players/Delete/5
        [Authorize(Roles = "Administrador")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Players players = db.Players.Find(id);
            //Borramos salarios
            foreach (var pSal in db.PlayersSalariesFull.Where(x => x.IdPlayer == id))
            {
                db.PlayersSalariesFull.Remove(pSal);
            }
            PlayersHistories playerHistory = db.PlayersHistories.Find(id);
            if (playerHistory != null)
            {
                playerHistory.EndDate = DateTime.Now;
            }
            foreach(var agencia in db.Agencys.Where(x => x.IdPlayer == id))
            {
                db.Agencys.Remove(agencia);
            }
            db.Players.Remove(players);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }


        #region PlayerActions
        public ActionResult PlayerActions(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "Name_desc" : "";
            ViewBag.TeamSortParm = sortOrder == "Team" ? "Team_desc" : "Team";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;

            var players = db.Players.Where(x => x.Retired == false).Include(p => p.LVNBAFranchis).Include(p => p.PlayersState).Include(p => p.PlayersPosition).Include(p => p.PlayersRole);
            if (!String.IsNullOrEmpty(searchString))
            {
                players = players.Where(s => s.CompleteName.ToUpper().Contains(searchString.ToUpper())
                                       || s.LVNBAFranchis.Name.ToUpper().Contains(searchString.ToUpper()));
            }
            switch (sortOrder)
            {
                case "Name_desc":
                    players = players.OrderByDescending(s => s.CompleteName);
                    break;
                case "Team":
                    players = players.OrderBy(s => s.LVNBAFranchis.Name);
                    break;
                case "Team_desc":
                    players = players.OrderByDescending(s => s.LVNBAFranchis.Name);
                    break;
                default:
                    players = players.OrderBy(s => s.CompleteName);
                    break;
            }
            int pageSize = 30;
            int pageNumber = (page ?? 1);

            return View(players.ToPagedList(pageNumber, pageSize));

        }

        public ActionResult CutPlayer(int id = 0)
        {
            Players players = db.Players.Find(id);
            if (players == null)
            {
                return HttpNotFound();
            }
            else
            {
                if (players.Retired == true)
                {
                    ModelState.AddModelError("", "No se puede cortar a un jugador retirado.");
                    return View("PlayerActions");
                }
            }
            return View(players);
        }
        [HttpPost, ActionName("CutPlayer")]
        [ValidateAntiForgeryToken]
        public ActionResult CutPlayerConfirmed(int id)
        {
            try
            {
                Players player = db.Players.Find(id);
                int salaryConditionCuttedId = db.SalaryConditions.Where(x => x.Abrev == "CU").First().Id;
                foreach (PlayersSalariesFull salario in player.PlayersSalariesFull.ToList())
                {
                    PlayersSalariesFull salario_to_update = db.PlayersSalariesFull.Find(salario.Id);
                    salario_to_update.IdCondition = salaryConditionCuttedId;
                    if (TryUpdateModel(salario_to_update, "", new string[] { "IdCondition" }))
                    {
                        db.SaveChanges();
                    }
                }
                player.IdRol = 1; //sin rol
                player.IdState = 7; //sin estado
                player.Cutted = 1; // Cortado
                if (TryUpdateModel(player, "", new string[] { "ActualTeam", "IdRol", "IdState" }))
                {
                    db.SaveChanges(); // poner al jugador actualteam = null
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error al cortar el jugador: " + ex.InnerException.ToString());
            }
            return RedirectToAction("PlayerActions");
        }

        public ActionResult ConvertFA(int id = 0)
        {
            Players players = db.Players.Find(id);
            if (players == null)
            {
                return HttpNotFound();
            }
            else
            {
                if (players.Retired == true)
                {
                    ModelState.AddModelError("", "No se puede convertir en agente libre a un jugador retirado.");
                    return View("PlayerActions");
                }
            }
            return View(players);
        }

        [HttpPost, ActionName("ConvertFA")]
        [ValidateAntiForgeryToken]
        public ActionResult ConvertFAConfirmed(int id)
        {
            try
            {
                Jugadores accJug = new Jugadores();
                //Borramos salarios
                accJug.ConvertirEnFA(id);

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error al convertir a FA.: " + ex.InnerException.ToString());
            }
            return RedirectToAction("PlayerActions");
        }

        public ActionResult RetirePlayer(int id = 0)
        {
            Players players = db.Players.Find(id);
            if (players == null)
            {
                return HttpNotFound();
            }
            return View(players);
        }
        [HttpPost, ActionName("RetirePlayer")]
        [ValidateAntiForgeryToken]
        public ActionResult RetirePlayerConfirmed(int id)
        {
            try
            {
                Jugadores accJug = new Jugadores();
                //Borramos salarios
                accJug.RetirePlayer(id);

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error al retirar el jugador: " + ex.InnerException.ToString());
            }
            return RedirectToAction("PlayerActions");
        }

        public ActionResult ChangePlayerTeam(int id = 0)
        {

            var player = db.Players.Single(r => r.Id == id);

            PlayersForChangeTeam viewModel = new PlayersForChangeTeam();
            viewModel.InjectFrom(player);
            if (player.ActualTeam != null)
                viewModel.FranchiseName = player.LVNBAFranchis.Name;

            ViewBag.ActualTeam = new SelectList(db.LVNBAFranchises, "Id", "Name", player.ActualTeam);

            return View(viewModel);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePlayerTeam(PlayersForChangeTeam players)
        {

            var player = db.Players.Single(r => r.Id == players.Id);   // Grab the Article from the DB to update
            //miramos si es LEGALMENTE INTRANSFERIBLE
            if (player.IdState == 4)
            {
                ModelState.AddModelError("", "El jugador es LEGALMENTE INTRANSFERIBLE. No se puede cambiar de equipo desde aquí. Sólo desde administración");
                ViewBag.ActualTeam = new SelectList(db.LVNBAFranchises, "Id", "Name", player.ActualTeam);
                return View(players);
            }
            player.InjectFrom(players);      // Inject updated values from the viewModel into the Article stored in the DB

            // Fill in missing pieces
            player.ActualTeam = players.ActualTeam;

            if (ModelState.IsValid)
            {
                Jugadores jug = new Jugadores();
                player.IdRol = 1; //sin rol
                //MIRO SI PASA A INTRANSFERIBLE
                if (jug.esLI(player.Id))
                {
                    player.IdState = 4;
                }
                else
                {
                    player.IdState = 7; //sin estado
                }
                //CAMBIO EQUIPO DE SALARIO

                var playerSalary = db.PlayersSalariesFull.Where(x => x.IdPlayer == player.Id && x.DateSalarie.Year >= db.Parameters.FirstOrDefault().Temporada).ToList();
                if (playerSalary != null)
                {
                    foreach(var ps in playerSalary)
                    {
                        ps.IdLVNBAFranchises = players.ActualTeam;
                        db.Entry(ps).State = EntityState.Modified;
                    }
                    db.SaveChanges();
                }
                else
                {
                    var ps2 = new PlayersSalariesFull
                    {
                        IdPlayer = player.Id,
                        Salarie = 0,
                        IdLVNBAFranchises = player.ActualTeam.Value
                    };
                    db.PlayersSalariesFull.Add(ps2);
                }


                //ABRE y CIERRA CICLO
                PlayersHistories pHist = db.PlayersHistories.Where(x => x.IdPlayer == player.Id && x.EndDate == null).FirstOrDefault();
                if (pHist != null)
                    pHist.EndDate = DateTime.Now;
                PlayersHistories pHistNew = new PlayersHistories();
                pHistNew.IdPlayer = player.Id;
                pHistNew.IdFranchise = player.ActualTeam.Value;
                pHistNew.StartDate = DateTime.Now;
                db.PlayersHistories.Add(pHistNew);

                db.Entry(player).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("PlayerActions");
            }
            return View(players);    // Something went wrong
        }
        public ActionResult EditComission(int id = 0)
        {
            Players players = db.Players.Find(id);
            if (players == null)
            {
                return HttpNotFound();
            }
            ViewBag.ActualTeam = new SelectList(db.LVNBAFranchises, "Id", "Name", players.ActualTeam);
            ViewBag.IdState = new SelectList(db.PlayersStates, "Id", "State", players.IdState);
            ViewBag.IdPosition = new SelectList(db.PlayersPositions, "Id", "Position", players.IdPosition);
            ViewBag.IdRol = new SelectList(db.PlayersRoles, "Id", "Rol", players.IdRol);
            ViewBag.IdCollege = new SelectList(db.Colleges, "Id", "College", players.IdCollege);
            return View(players);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditComission(Players players)
        {
            if (ModelState.IsValid)
            {

                db.Entry(players).State = EntityState.Modified;
                //modificamos el equipo de la tabla salarios o creamos el salario del jugador
                var pSal = db.PlayersSalariesFull.Where(x => x.IdPlayer == players.Id).FirstOrDefault();
                if (pSal != null)
                {
                    if (pSal.IdLVNBAFranchises != players.ActualTeam)
                    {
                        //pSal.IdLVNBAFranchises = players.ActualTeam;
                    }
                }
                else
                {
                    pSal = new PlayersSalariesFull();
                    pSal.Salarie = 0;
                    pSal.IdPlayer = players.Id;
                    pSal.IdLVNBAFranchises = players.ActualTeam.Value;
                    db.PlayersSalariesFull.Add(pSal);
                }

                db.SaveChanges();
                return RedirectToAction("PlayerActions");
            }
            ViewBag.ActualTeam = new SelectList(db.LVNBAFranchises, "Id", "Name", players.ActualTeam);
            ViewBag.IdState = new SelectList(db.PlayersStates, "Id", "State", players.IdState);
            ViewBag.IdPosition = new SelectList(db.PlayersPositions, "Id", "Position", players.IdPosition);
            ViewBag.IdRol = new SelectList(db.PlayersRoles, "Id", "Rol", players.IdRol);
            ViewBag.IdCollege = new SelectList(db.Colleges, "Id", "College", players.IdCollege);


            return View(players);
        }


        public ActionResult OutOfNBA(int id = 0)
        {
            PlayersForOutNBA viewModel = new PlayersForOutNBA();
            int temporada = db.Parameters.First().Temporada;
            var player = db.PlayersSalariesFull.FirstOrDefault(r => r.IdPlayer == id);
            if (player == null)
            {
                return View(viewModel);
            }

            viewModel.InjectFrom(player);
            viewModel.CompleteName = player.Player.CompleteName;
            viewModel.Id = player.Player.Id;
            viewModel.SalaryTeam = player.Salarie.ToString();
            viewModel.EuropeTeam = player.Player.LVNBAFranchis.Name;

            return View(viewModel);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult OutOfNBA(PlayersForOutNBA players)
        {
            int temporada = db.Parameters.First().Temporada;
            var player = db.PlayersSalariesFull.FirstOrDefault(r => r.IdPlayer == players.Id);   // Grab the Article from the DB to update
            if (player == null)
            {
                ModelState.AddModelError("", "Debe crearle una hoja de salario al jugador primero. Sólo desde administración");
                return View(players);
            }

            player.IdCondition = 5;

            if (ModelState.IsValid)
            {
                Players jug = db.Players.FirstOrDefault(x => x.Id == player.IdPlayer);
                if (jug != null)
                {
                    jug.IdRol = 1; // sin rol
                    jug.IdState = 7; //sin estado
                }
                //CIERRA CICLO
                PlayersHistories pHist = db.PlayersHistories.Where(x => x.IdPlayer == player.Id && x.EndDate == null).FirstOrDefault();
                if (pHist != null)
                    pHist.EndDate = DateTime.Now;

                db.SaveChanges();
                return RedirectToAction("PlayerActions");
            }

            return View(players);    // Something went wrong


        }
        #endregion



    }
    public class AlphabeticalMapping<T>
    {
        public string FirstLetter { get; set; }
        public IEnumerable<T> Items { get; set; }
        public string FirstName { get; set; }
    }
}