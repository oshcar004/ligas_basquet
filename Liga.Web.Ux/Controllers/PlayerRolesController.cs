﻿
using Liga.Domain.Entities;
using Liga.Infrastructure.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace Liga.Web.UI.Controllers
{
    [Authorize(Roles = "Administrador")]
    public class PlayerRolesController : BaseController
    {
        private Liga_DBContext db = new Liga_DBContext();

        //
        // GET: /PlayerRoles/

        public ActionResult Index()
        {
            return View(db.PlayersRoles.ToList());
        }

        //
        // GET: /PlayerRoles/Details/5

        public ActionResult Details(int id = 0)
        {
            PlayersRoles playersroles = db.PlayersRoles.Find(id);
            if (playersroles == null)
            {
                return HttpNotFound();
            }
            return View(playersroles);
        }

        //
        // GET: /PlayerRoles/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /PlayerRoles/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PlayersRoles playersroles)
        {
            if (ModelState.IsValid)
            {
                db.PlayersRoles.Add(playersroles);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(playersroles);
        }

        //
        // GET: /PlayerRoles/Edit/5

        public ActionResult Edit(int id = 0)
        {
            PlayersRoles playersroles = db.PlayersRoles.Find(id);
            if (playersroles == null)
            {
                return HttpNotFound();
            }
            return View(playersroles);
        }

        //
        // POST: /PlayerRoles/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PlayersRoles playersroles)
        {
            if (ModelState.IsValid)
            {
                db.Entry(playersroles).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(playersroles);
        }

        //
        // GET: /PlayerRoles/Delete/5

        public ActionResult Delete(int id = 0)
        {
            PlayersRoles playersroles = db.PlayersRoles.Find(id);
            if (playersroles == null)
            {
                return HttpNotFound();
            }
            return View(playersroles);
        }

        //
        // POST: /PlayerRoles/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PlayersRoles playersroles = db.PlayersRoles.Find(id);
            db.PlayersRoles.Remove(playersroles);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}