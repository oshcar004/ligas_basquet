﻿
using Liga.Domain.Entities;
using Liga.Infrastructure.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace Liga.Web.UI.Controllers
{
    [Authorize(Roles = "Administrador, Comisionado")]
    public class RoundsController : BaseController
    {
        private Liga_DBContext db = new Liga_DBContext();

        //
        // GET: /Rounds/

        public ActionResult Index()
        {
            int temporada = db.Parameters.First().Temporada;
            var rounds = db.Rounds.Include(r => r.LVNBAFranchis).Include(r => r.LVNBAFranchisProp).Where(r => r.Year == temporada || r.Year == (temporada+1));
            return View(rounds.ToList());
        }

        //
        // GET: /Rounds/Details/5

        public ActionResult Details(int id = 0)
        {
            Rounds rounds = db.Rounds.Find(id);
            if (rounds == null)
            {
                return HttpNotFound();
            }
            return View(rounds);
        }

        //
        // GET: /Rounds/Create

        public ActionResult Create()
        {
            ViewBag.IdFranchise = new SelectList(db.LVNBAFranchises, "Id", "Name");
            ViewBag.Owner = new SelectList(db.LVNBAFranchises, "Id", "Name");
            return View();
        }

        //
        // POST: /Rounds/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Rounds rounds)
        {
            if (ModelState.IsValid)
            {
                db.Rounds.Add(rounds);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdFranchise = new SelectList(db.LVNBAFranchises, "Id", "Name", rounds.IdFranchise);
            ViewBag.Owner = new SelectList(db.LVNBAFranchises, "Id", "Name", rounds.Owner);
            return View(rounds);
        }

        //
        // GET: /Rounds/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Rounds rounds = db.Rounds.Find(id);
            if (rounds == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdFranchise = new SelectList(db.LVNBAFranchises, "Id", "Name", rounds.IdFranchise);
            ViewBag.Owner = new SelectList(db.LVNBAFranchises, "Id", "Name", rounds.Owner);
            return View(rounds);
        }

        //
        // POST: /Rounds/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Rounds rounds)
        {
            if (ModelState.IsValid)
            {
                db.Entry(rounds).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdFranchise = new SelectList(db.LVNBAFranchises, "Id", "Name", rounds.IdFranchise);
            ViewBag.Owner = new SelectList(db.LVNBAFranchises, "Id", "Name", rounds.Owner);
            return View(rounds);
        }

        //
        // GET: /Rounds/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Rounds rounds = db.Rounds.Find(id);
            if (rounds == null)
            {
                return HttpNotFound();
            }
            return View(rounds);
        }

        //
        // POST: /Rounds/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Rounds rounds = db.Rounds.Find(id);
            db.Rounds.Remove(rounds);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}