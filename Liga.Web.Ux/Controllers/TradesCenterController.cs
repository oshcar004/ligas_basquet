﻿
using Liga.Domain.Entities;
using Liga.Infrastructure.Data;
using System.Linq;
using System.Web.Mvc;
using System;
using System.Collections;
using System.Web.Security;
using System.Collections.Generic;

namespace Liga.Web.UI.Controllers
{
    [Authorize(Roles = "Comisionado")]
    public class TradesCenterController : BaseController
    {
        private Liga_DBContext db = new Liga_DBContext();

        //
        // GET: /TradesCenter/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Trade(int idState)
        {
            ViewBag.TransferState = idState;
            int idTeamOfManager = GetManagerTeam();
            ViewBag.Franchise = idTeamOfManager;
            var trades = db.Transfers.Where(x => x.IdTransferState == idState).OrderByDescending(x => x.TransferDate).ToList();
            if (trades != null)
            {
                foreach (var trade in trades)
                {
                    var player = trade.TransfersPlayersLists.First().Player;
                    player.FreeAgentOffers = db.FreeAgentOffers
                        .Where(x => x.IdPlayer == player.Id && x.LVNBAFranchis.Id == trade.LVNBAFranchis1.Id).ToList();
                }
            }
            return PartialView(trades);
        }

        public int GetManagerTeam()
        {
            int idTeam = 0;
            if (System.Threading.Thread.CurrentPrincipal.Identity.IsAuthenticated)
            {
                if (Roles.IsUserInRole("Manager"))
                {
                    string username = System.Threading.Thread.CurrentPrincipal.Identity.Name.ToString();

                    UserProfile user = db.UserProfiles.First(u => u.UserName == username);
                    Managers managers = db.Managers.FirstOrDefault(p => p.UserId == user.UserId);
                    if (managers != null && managers.ManagersFranchises.Where(x => x.IdManager == managers.Id && x.EndDate == null).FirstOrDefault() != null)
                    {
                        idTeam = db.LVNBAFranchises.Where(x => x.IdManager == managers.Id).First().Id;
                    }

                }
            }
            return idTeam;
        }

        public ActionResult Accept(int id)
        {
            Transfers transfer = db.Transfers.Where(x => x.Id == id).FirstOrDefault();
            string date = DateTime.Now.ToString("yyyy-MM-dd ") + DateTime.Now.ToString("hh:mm:ss");
            transfer.IdTransferState = 5;
            transfer.ResolutionDate = DateTime.Now;
            if (TryUpdateModel(transfer, "", new string[] { "IdTransferState", "ResolutionDate" }))
            {
                db.SaveChanges();
                TransfersPlayersLists transfersPlayersList = db.TransfersPlayersLists.Where(x => x.IdTransfer == transfer.Id).FirstOrDefault();
                FreeAgentOffers freeAgentOffer = db.FreeAgentOffers.Where(x => x.IdPlayer == transfersPlayersList.IdPlayer && x.IdFranchise == transfersPlayersList.Destination && x.Accepted == null && x.Rejected == null).FirstOrDefault();
                freeAgentOffer.Accepted = true;
                freeAgentOffer.Rejected = false;
                List<PlayersSalariesFull> myAL = new List<PlayersSalariesFull> ();
                if (freeAgentOffer.Salary1 > 0)
                {
                    PlayersSalariesFull playersSalariesFull = new PlayersSalariesFull();
                    playersSalariesFull.IdPlayer = freeAgentOffer.IdPlayer;
                    playersSalariesFull.IdLVNBAFranchises = freeAgentOffer.IdFranchise;
                    playersSalariesFull.Salarie = (decimal)freeAgentOffer.Salary1;
                    playersSalariesFull.DateSalarie = freeAgentOffer.OfferDate;
                    playersSalariesFull.IdCondition = (int)(freeAgentOffer.IdCondition1 != null ? freeAgentOffer.IdCondition1 : 6);
                    myAL.Add(playersSalariesFull);
                }
                if (freeAgentOffer.Salary2 > 0)
                {
                    PlayersSalariesFull playersSalariesFull = new PlayersSalariesFull();
                    playersSalariesFull.IdPlayer = freeAgentOffer.IdPlayer;
                    playersSalariesFull.IdLVNBAFranchises = freeAgentOffer.IdFranchise;
                    playersSalariesFull.Salarie = (decimal)freeAgentOffer.Salary2;
                    playersSalariesFull.DateSalarie = freeAgentOffer.OfferDate.AddYears(1);
                    playersSalariesFull.IdCondition = (int)(freeAgentOffer.IdCondition2 != null ? freeAgentOffer.IdCondition2 : 6);
                    myAL.Add(playersSalariesFull);
                }
                if (freeAgentOffer.Salary3 > 0)
                {
                    PlayersSalariesFull playersSalariesFull = new PlayersSalariesFull();
                    playersSalariesFull.IdPlayer = freeAgentOffer.IdPlayer;
                    playersSalariesFull.IdLVNBAFranchises = freeAgentOffer.IdFranchise;
                    playersSalariesFull.Salarie = (decimal)freeAgentOffer.Salary3;
                    playersSalariesFull.DateSalarie = freeAgentOffer.OfferDate.AddYears(2);
                    playersSalariesFull.IdCondition = (int)(freeAgentOffer.IdCondition3 != null ? freeAgentOffer.IdCondition3 : 6);
                    myAL.Add(playersSalariesFull);
                }
                if (freeAgentOffer.Salary4 > 0)
                {
                    PlayersSalariesFull playersSalariesFull = new PlayersSalariesFull();
                    playersSalariesFull.IdPlayer = freeAgentOffer.IdPlayer;
                    playersSalariesFull.IdLVNBAFranchises = freeAgentOffer.IdFranchise;
                    playersSalariesFull.Salarie = (decimal)freeAgentOffer.Salary4;
                    playersSalariesFull.DateSalarie = freeAgentOffer.OfferDate.AddYears(3);
                    playersSalariesFull.IdCondition = (int)(freeAgentOffer.IdCondition4 != null ? freeAgentOffer.IdCondition4 : 6);
                    myAL.Add(playersSalariesFull);
                }
                if (freeAgentOffer.Salary5 > 0)
                {
                    PlayersSalariesFull playersSalariesFull = new PlayersSalariesFull();
                    playersSalariesFull.IdPlayer = freeAgentOffer.IdPlayer;
                    playersSalariesFull.IdLVNBAFranchises = freeAgentOffer.IdFranchise;
                    playersSalariesFull.Salarie = (decimal)freeAgentOffer.Salary5;
                    playersSalariesFull.DateSalarie = freeAgentOffer.OfferDate.AddYears(4);
                    playersSalariesFull.IdCondition = (int)(freeAgentOffer.IdCondition5 != null ? freeAgentOffer.IdCondition5 : 6);
                    myAL.Add(playersSalariesFull);
                }
                if (TryUpdateModel(freeAgentOffer, "", new string[] { "Accepted", "Rejected" }))
                {
                    db.SaveChanges();
                    Players player = db.Players.Where(x => x.Id == freeAgentOffer.IdPlayer).FirstOrDefault();
                    player.ActualTeam = freeAgentOffer.IdFranchise;
                    player.Cutted = 0;
                    if (TryUpdateModel(player, "", new string[] { "ActualTeam" }))
                    {
                        db.SaveChanges();
                        /*
                        var playersSalaries = new PlayersSalariesFull();
                        playersSalaries.IdPlayer = player.Id;
                        playersSalaries.Salarie = freeAgentOffer.Salary1.Value;
                        playersSalaries.IdLVNBAFranchises = freeAgentOffer.IdFranchise;
                        db.PlayersSalariesFull.Add(playersSalaries);
                        db.SaveChanges();
                        */
                        /*foreach(PlayersSalariesFull playersSalariesFull in myAL)
                        {
                            db.PlayersSalariesFull.Add(playersSalariesFull);
                        }*/
                        db.PlayersSalariesFull.AddRange(myAL);
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                }
            }
            // FALTA: Debe retornar que se produjo un error
            return HttpNotFound();
        }

        public ActionResult Deny(int id)
        {
            Transfers transfer = db.Transfers.Where(x => x.Id == id).FirstOrDefault();
            string date = DateTime.Now.ToString("yyyy-MM-dd ") + DateTime.Now.ToString("hh:mm:ss");
            transfer.IdTransferState = 4;
            transfer.ResolutionDate = Convert.ToDateTime(date);
            if (TryUpdateModel(transfer, "", new string[] { "IdTransferState", "ResolutionDate" }))
            {
                db.SaveChanges();
                TransfersPlayersLists transfersPlayersList = db.TransfersPlayersLists.Where(x => x.IdTransfer == transfer.Id).FirstOrDefault();
                FreeAgentOffers freeAgentOffer = db.FreeAgentOffers.Where(x => x.IdPlayer == transfersPlayersList.IdPlayer && x.IdFranchise == transfersPlayersList.Destination && x.Accepted == null && x.Rejected == null).FirstOrDefault();
                freeAgentOffer.Accepted = false;
                freeAgentOffer.Rejected = true;
                if (TryUpdateModel(freeAgentOffer, "", new string[] { "Accepted", "Rejected" }))
                {
                    db.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Edit", new { id = 99 });
            }
        }

        public ActionResult Delete(int id)
        {
            Transfers transfer = db.Transfers.Where(x => x.Id == id).FirstOrDefault();
            //Borramos Players List
            foreach (TransfersPlayersLists pList in db.TransfersPlayersLists.Where(x => x.IdTransfer == id))
            {
                FreeAgentOffers freeAgentOffer = db.FreeAgentOffers.Where(x => x.IdPlayer == pList.IdPlayer && x.IdFranchise == pList.Destination && x.Accepted == null && x.Rejected == null).FirstOrDefault();
                db.FreeAgentOffers.Remove(freeAgentOffer);
                db.TransfersPlayersLists.Remove(pList);
            }
            //Borramos Rounds List
            foreach (TransfersRoundsLists pList in db.TransfersRoundsLists.Where(x => x.IdTransfer == id))
            {
                db.TransfersRoundsLists.Remove(pList);
            }
            db.Transfers.Remove(transfer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult TradeInfo(int idTransfer)
        {
            Transfers transfer = db.Transfers.Where(x => x.Id == idTransfer).First();
            return View("TradeInfo", transfer);
        }

    }
}
