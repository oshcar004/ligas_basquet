﻿
using Liga.Domain.Entities;
using Liga.Infrastructure.Data;
using Liga.Infrastructure.Data.Repository;
using MvcJqGrid;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;

namespace Liga.Web.UI.Controllers
{
    public class GridListController : Controller
    {
        //
        // GET: /GridList/
        private Liga_DBContext db = new Liga_DBContext();

     
        public ActionResult PositionsSelect()
        {
            Dictionary<int, string> positions = new Dictionary<int, string>();
            foreach (PlayersPositions position in db.PlayersPositions)
                positions.Add(position.Id, position.Position);
            return PartialView("SelectPartial", positions);
        }
        public ActionResult RolesSelect()
        {
            Dictionary<int, string> roles = new Dictionary<int, string>();
            foreach (PlayersRoles  rol in db.PlayersRoles)
                roles.Add(rol.Id, rol.Rol);
            return PartialView("SelectPartial", roles);
        }
        public ActionResult StatesSelect()
        {
            Dictionary<int, string> states = new Dictionary<int, string>();
            foreach (PlayersStates state in db.PlayersStates)
                states.Add(state.Id, state.State);
            return PartialView("SelectPartial", states);
        }

        public ActionResult Index()
        {
            return View();
        }
        public JsonResult GetPlayersSalariesByTeam(int idTeam, GridSettings settings)
        {
            Jugadores jugadores = new Jugadores();
            var listPlayers = jugadores.getPlayersSalaryInSalary(idTeam); // db.PlayersSalariesFull.Where(x => x.IdLVNBAFranchises == idTeam).OrderBy(x => x.IdCondition1).ThenByDescending(x => x.Salary1);
            int totalRecords = listPlayers.Count();
            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)settings.PageSize);
            int page = settings.PageIndex;

            int startRow = (settings.PageIndex - 1) * settings.PageSize;

            listPlayers = listPlayers.Skip(startRow).Take(settings.PageSize);

            foreach (var player in listPlayers) // con esto evitamos que de error en aquellos jugadores cortados y sin equipo
            {
                if (player.Player.ActualTeam == null)
                {
                    player.Player.LVNBAFranchis = player.LVNBAFranchis;
                }
            }


            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from players in listPlayers
                    select new
                    {
                        id = players.Player.Id,
                        cell = new string[]{
                            players.Player.Id.ToString(),
                            players.LVNBAFranchis.Abbreviation,
                            players.Player.CompleteName ,
                            players.Player.BirdRights.ToString(),
                            players.Player.PlayersPosition.Position,
                            players.Player.PlayersState.Abrev,
                            players.Player.PlayersRole.Abrev,
                            players.Player.LVNBAFranchis.Abbreviation.ToString()
                        }

                    }
            ).ToArray()
            };
            //     return Json(jsonData);
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }
        public JsonResult GetPlayersOutOfSalariesByTeam(int idTeam, GridSettings settings)
        {
            Jugadores jugadores = new Jugadores();
            var listPlayers = jugadores.getPlayersOutOfSalaryInSalary(idTeam); // db.PlayersSalariesFull.Where(x => x.IdLVNBAFranchises == idTeam).OrderBy(x => x.IdCondition1).ThenByDescending(x => x.Salary1);
            int totalRecords = listPlayers.Count();
            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)settings.PageSize);
            int page = settings.PageIndex;

            int startRow = (settings.PageIndex - 1) * settings.PageSize;

            listPlayers = listPlayers.Skip(startRow).Take(settings.PageSize);

            foreach (var player in listPlayers) // con esto evitamos que de error en aquellos jugadores cortados y sin equipo
            {
                if (player.Player.ActualTeam == null)
                {
                    player.Player.LVNBAFranchis = player.LVNBAFranchis;
                }
            }


            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from players in listPlayers
                    select new
                    {
                        id = players.Player.Id,
                        cell = new string[]{
                            players.Player.Id.ToString(),
                            players.LVNBAFranchis.Abbreviation,
                            players.Player.CompleteName ,
                            players.Player.BirdRights.ToString(),
                            players.Player.PlayersPosition.Position,
                            players.Player.PlayersState.Abrev,
                            players.Player.PlayersRole.Abrev,
                            players.Player.LVNBAFranchis.Abbreviation.ToString()
                        }

                    }
            ).ToArray()
            };
            //     return Json(jsonData);
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }

        #region PlayersInfoList
        public IEnumerable<PlayersInfoList> GetPlayersInfoList(GridSettings gridSettings, IQueryable<PlayersInfoList> players, out int totalPlayers)
        {
            var orderedPlayers = OrderPlayersInfoList(players, gridSettings.SortColumn, gridSettings.SortOrder);

            if (gridSettings.IsSearch)
            {
                orderedPlayers = gridSettings.Where.rules.Aggregate(orderedPlayers, FilterPlayersInfoList);
            }
            totalPlayers = orderedPlayers.Count();

            return orderedPlayers.Skip((gridSettings.PageIndex - 1) * gridSettings.PageSize).Take(gridSettings.PageSize).AsEnumerable();
        }
        private static IQueryable<PlayersInfoList> FilterPlayersInfoList(IQueryable<PlayersInfoList> players, Rule rule)
        {
            switch (rule.field)
            {
                case "IdCondition":
                    int idCo = 0;
                    if (Int32.TryParse(rule.data.Replace(".", ",").ToString(), out idCo))
                        return players.Where(c => c.idCondition == idCo);
                    return players;
                case "BirdRights":
                    int idBi = 0;
                    if (Int32.TryParse(rule.data.Replace(".", ",").ToString(), out idBi))
                        return players.Where(c => c.BirdRights == idBi);
                    return players;
                case "Logo":
                    return players.Where(c => c.Team.ToLower().Contains(rule.data.ToLower()));
                case "CompleteName":
                    return players.Where(c => c.CompleteName.ToLower().Contains(rule.data.ToLower()));
                case "IdLVNBAFranchises":
                    return players.Where(c => c.SalaryTeam.ToLower().Contains(rule.data.ToLower()));
                case "Height":
                    int idHeight = 0;
                    if (Int32.TryParse(rule.data.Replace(".", ",").ToString(), out idHeight))
                    {

                        switch (rule.op)
                        {

                            case "eq":
                                players = players.Where(x => x.Height == idHeight);
                                break;
                            case "lt":
                                players = players.Where(x => x.Height < idHeight);
                                break;
                            case "le":
                                players = players.Where(x => x.Height <= idHeight);
                                break;
                            case "gt":
                                players = players.Where(x => x.Height > idHeight);
                                break;
                            case "ge":
                                players = players.Where(x => x.Height >= idHeight);
                                break;
                            case "cn":
                                players = players.Where(x => x.Height == idHeight);
                                break;
                        }
                    }
                    return players;
                case "Weight":
                    decimal  idWeight = 0;
                    if (Decimal.TryParse(rule.data.Replace(".", ",").ToString(), out idWeight))
                    {

                        switch (rule.op)
                        {

                            case "eq":
                                players = players.Where(x => x.Weight == idWeight);
                                break;
                            case "lt":
                                players = players.Where(x => x.Weight < idWeight);
                                break;
                            case "le":
                                players = players.Where(x => x.Weight <= idWeight);
                                break;
                            case "gt":
                                players = players.Where(x => x.Weight > idWeight);
                                break;
                            case "ge":
                                players = players.Where(x => x.Weight >= idWeight);
                                break;
                            case "cn":
                                players = players.Where(x => x.Weight == idWeight);
                                break;
                        }
                    }
                    return players;
                case "Age":
                    int idAge = 0;
                    if (Int32.TryParse(rule.data.Replace(".", ",").ToString(), out idAge))
                    {

                        switch (rule.op)
                        {

                            case "eq":
                                players = players.Where(x => x.Age == idAge);
                                break;
                            case "lt":
                                players = players.Where(x => x.Age < idAge);
                                break;
                            case "le":
                                players = players.Where(x => x.Age <= idAge);
                                break;
                            case "gt":
                                players = players.Where(x => x.Age > idAge);
                                break;
                            case "ge":
                                players = players.Where(x => x.Age >= idAge);
                                break;
                            case "cn":
                                players = players.Where(x => x.Age == idAge);
                                break;
                        }
                    }
                    return players;
                case "Followed":
                    int idFollowed = 0;
                    if (Int32.TryParse(rule.data.Replace(".", ",").ToString(), out idFollowed))
                    {

                        switch (rule.op)
                        {

                            case "eq":
                                players = players.Where(x => x.Followed == idFollowed);
                                break;
                            case "lt":
                                players = players.Where(x => x.Followed < idFollowed);
                                break;
                            case "le":
                                players = players.Where(x => x.Followed <= idFollowed);
                                break;
                            case "gt":
                                players = players.Where(x => x.Followed > idFollowed);
                                break;
                            case "ge":
                                players = players.Where(x => x.Followed >= idFollowed);
                                break;
                            case "cn":
                                players = players.Where(x => x.Followed == idFollowed);
                                break;
                        }
                    }
                    return players;
                case "ContractY":
                    int idCY = 0;
                    if (Int32.TryParse(rule.data.Replace(".", ",").ToString(), out idCY))
                    {

                        switch (rule.op)
                        {

                            case "eq":
                                players = players.Where(x => x.ContractY == idCY);
                                break;
                            case "lt":
                                players = players.Where(x => x.ContractY < idCY);
                                break;
                            case "le":
                                players = players.Where(x => x.ContractY <= idCY);
                                break;
                            case "gt":
                                players = players.Where(x => x.ContractY > idCY);
                                break;
                            case "ge":
                                players = players.Where(x => x.ContractY >= idCY);
                                break;
                            case "cn":
                                players = players.Where(x => x.ContractY == idCY);
                                break;
                        }
                    }
                    return players;
                case "Position":
                    int idPosition = 0;
                    if (Int32.TryParse(rule.data.Replace(".", ",").ToString(), out idPosition))
                    {
                        return players.Where(c => c.IdPosition == idPosition);
                    }
                    return players;

                //return players.Where(c => c.Position.ToLower().Contains(rule.data.ToLower()));

                case "StateLong":
                    int idState = 0;
                    if (Int32.TryParse(rule.data.Replace(".", ",").ToString(), out idState))
                    {
                        return players.Where(c => c.IdState == idState);
                    }
                    return players;

                //return players.Where(c => c.State.ToLower().Contains(rule.data.ToLower()));

                case "RolLong":
                    int idRol = 0;
                    if (Int32.TryParse(rule.data.Replace(".", ",").ToString(), out idRol))
                    {
                        return players.Where(c => c.IdRol == idRol);
                    }
                    return players;

                //return players.Where(c => c.Rol.ToLower().Contains(rule.data.ToLower()));

                case "Salary":
                    int idSalary = 0;
                    if (Int32.TryParse(rule.data.Replace(".", ",").ToString(), out idSalary))
                    {

                        switch (rule.op)
                        {

                            case "eq":
                                players = players.Where(x => x.Salary == idSalary);
                                break;
                            case "lt":
                                players = players.Where(x => x.Salary < idSalary);
                                break;
                            case "le":
                                players = players.Where(x => x.Salary <= idSalary);
                                break;
                            case "gt":
                                players = players.Where(x => x.Salary > idSalary);
                                break;
                            case "ge":
                                players = players.Where(x => x.Salary >= idSalary);
                                break;
                            case "cn":
                                players = players.Where(x => x.Salary == idSalary);
                                break;
                        }
                    }
                    return players;
                case "Level2k":
                    int idLevel2k = 0;
                    if (Int32.TryParse(rule.data.Replace(".", ",").ToString(), out idLevel2k))
                    {

                        switch (rule.op)
                        {

                            case "eq":
                                players = players.Where(x => x.Level2k == idLevel2k);
                                break;
                            case "lt":
                                players = players.Where(x => x.Level2k < idLevel2k);
                                break;
                            case "le":
                                players = players.Where(x => x.Level2k <= idLevel2k);
                                break;
                            case "gt":
                                players = players.Where(x => x.Level2k > idLevel2k);
                                break;
                            case "ge":
                                players = players.Where(x => x.Level2k >= idLevel2k);
                                break;
                            case "cn":
                                players = players.Where(x => x.Level2k == idLevel2k);
                                break;
                        }
                    }
                    return players;
                case "YearsPro":
                    int idYearsPro = 0;
                    if (Int32.TryParse(rule.data.Replace(".", ",").ToString(), out idYearsPro))
                    {

                        switch (rule.op)
                        {

                            case "eq":
                                players = players.Where(x => x.YearsPro == idYearsPro);
                                break;
                            case "lt":
                                players = players.Where(x => x.YearsPro < idYearsPro);
                                break;
                            case "le":
                                players = players.Where(x => x.YearsPro <= idYearsPro);
                                break;
                            case "gt":
                                players = players.Where(x => x.YearsPro > idYearsPro);
                                break;
                            case "ge":
                                players = players.Where(x => x.YearsPro >= idYearsPro);
                                break;
                            case "cn":
                                players = players.Where(x => x.YearsPro == idYearsPro);
                                break;
                        }
                    }
                    return players;


                default:
                    return players;
            }
        }
        private static IQueryable<PlayersInfoList> OrderPlayersInfoList(IQueryable<PlayersInfoList> players, string sortColumn, string sortOrder)
        {
 

            switch (sortColumn)
            {
                case "IdCondition":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.idCondition) : players.OrderBy(c => c.idCondition);
                case "BirdRights":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.BirdRights) : players.OrderBy(c => c.BirdRights);
                case "Logo":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.Team) : players.OrderBy(c => c.Team);
                case "Age":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.Age) : players.OrderBy(c => c.Age);
                case "CompleteName":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.CompleteName) : players.OrderBy(c => c.CompleteName);
                case "Height":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.Height) : players.OrderBy(c => c.Height);
                case "Weight":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.Weight) : players.OrderBy(c => c.Weight);
                case "Position":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.Position) : players.OrderBy(c => c.Position);
                case "State":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.State) : players.OrderBy(c => c.State);
                case "Rol":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.Rol) : players.OrderBy(c => c.Rol);
                case "YearsPro":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.YearsPro) : players.OrderBy(c => c.YearsPro);
                case "Level2k":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.Level2k) : players.OrderBy(c => c.Level2k);
                case "Salary":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.Salary) : players.OrderBy(c => c.Salary);
                case "IdLVNBAFranchises":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.SalaryTeam) : players.OrderBy(c => c.SalaryTeam);
                case "Followed":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.Followed) : players.OrderBy(c => c.Followed);
                case "ContractY":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.ContractY) : players.OrderBy(c => c.ContractY);

                default:
                    return players;
            }
        }

        public JsonResult GetPlayersInfoListByTeamOld(int idTeam, GridSettings settings)
        {
            int countPlayers = 0;
            IQueryable<PlayersInfoList> listaPlayers;
            if (idTeam != 0)
            {
                listaPlayers = db.PlayersInfoList.Where(x => x.IdTeam == idTeam).OrderByDescending(x => x.Salary).AsQueryable();
            }
            else
            {
                listaPlayers = db.PlayersInfoList.OrderByDescending(x => x.Salary).AsQueryable();
            }

            IEnumerable<PlayersInfoList> listPlayers = GetPlayersInfoList(settings, listaPlayers, out countPlayers);

            int totalRecords = countPlayers;
            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)settings.PageSize);
            int page = settings.PageIndex;

            int startRow = (settings.PageIndex - 1) * settings.PageSize;


            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from players in listPlayers
                    select new
                    {
                        id = players.Player,
                        cell = new string[]{
                            players.idCondition.ToString(),
                            players.BirdRights.ToString(),
                            players.Logo,
                            players.CompleteName ,
                            players.Age.ToString() ,
                            players.Height.ToString() ,
                            players.Weight.ToString() ,
                            players.Position,
                            players.StateLong,
                            players.RolLong,
                            players.Level2k.ToString(),
                            players.YearsPro.ToString(),
                            players.Salary.ToString().Replace(",","."),
                            players.Followed.ToString(),
                            players.ContractY.ToString(),
                            players.SalaryTeam.ToString(),
                            players.RookieRights.ToString()
                        }

                    }
            ).ToArray()
            };
            //     return Json(jsonData);
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }

        public JsonResult GetPlayersInfoListByTeam(int idTeam, GridSettings settings)
        {
            int countPlayers = 0;
            IQueryable<PlayersInfoList> listaPlayers;
            List<PlayersInfoList> listaPlayersList = new List<PlayersInfoList>();
            IQueryable<Players> playersList;
            if (idTeam != 0)
            {
                playersList = db.Players.Where(x => x.ActualTeam == idTeam).OrderByDescending(x => x.Name).AsQueryable();
                int temporada = new Liga_DBContext().Parameters.First().Temporada;
                foreach (Players player in playersList)
                {
                    PlayersInfoList playersInfoList = new PlayersInfoList();
                    playersInfoList.idCondition = player.PlayersSalariesFull.Where(x => x.DateSalarie.Year == temporada).First().SalaryCondition.Id;
                    playersInfoList.BirdRights = player.BirdRights;
                    playersInfoList.Logo = player.LVNBAFranchis.Logo;
                    playersInfoList.CompleteName = player.CompleteName;
                    playersInfoList.Age = (player.Age != null ? player.Age.Value : 0);
                    playersInfoList.Height = (player.Height != null ? player.Height.Value : 0);
                    playersInfoList.Weight = (player.Weight != null ? player.Weight.Value : 0);
                    playersInfoList.Position = player.PlayersPosition.Position;
                    playersInfoList.StateLong = player.PlayersState.State;
                    playersInfoList.RolLong = player.PlayersRole.Rol;
                    playersInfoList.Level2k = (player.Level2k != null ? player.Level2k.Value : 0);
                    playersInfoList.YearsPro = (player.YearsPro != null ? player.YearsPro.Value : 0);
                    playersInfoList.Salary = player.PlayersSalariesFull.Where(x => x.DateSalarie.Year == temporada).First().Salarie;
                    playersInfoList.Followed = (player.Followers != null ? player.Followers.Value : 0);
                    playersInfoList.ContractY = 1; // preguntar
                    playersInfoList.SalaryTeam = ""; // preguntar
                    playersInfoList.RookieRights = 1; // preguntar
                    listaPlayersList.Add(playersInfoList);
                }
                //listaPlayers = db.PlayersInfoList.Where(x => x.IdTeam == idTeam).OrderByDescending(x => x.Salary).AsQueryable();
            }
            else
            {
                //listaPlayers = db.PlayersInfoList.OrderByDescending(x => x.Salary).AsQueryable();
            }
            listaPlayers = listaPlayersList.AsQueryable();
            IEnumerable<PlayersInfoList> listPlayers = GetPlayersInfoList(settings, listaPlayers, out countPlayers);

            int totalRecords = countPlayers;
            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)settings.PageSize);
            int page = settings.PageIndex;

            int startRow = (settings.PageIndex - 1) * settings.PageSize;


            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from players in listPlayers
                    select new
                    {
                        id = players.Player,
                        cell = new string[]{
                            players.idCondition.ToString(),
                            players.BirdRights.ToString(),
                            players.Logo,
                            players.CompleteName ,
                            players.Age.ToString() ,
                            players.Height.ToString() ,
                            players.Weight.ToString() ,
                            players.Position,
                            players.StateLong,
                            players.RolLong,
                            players.Level2k.ToString(),
                            players.YearsPro.ToString(),
                            players.Salary.ToString().Replace(",","."),
                            players.Followed.ToString(),
                            players.ContractY.ToString(),
                            players.SalaryTeam.ToString(),
                            players.RookieRights.ToString()
                        }

                    }
            ).ToArray()
            };
            //     return Json(jsonData);
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }
        #endregion

        #region Grid PlayerStats
        public IEnumerable<PlayerStatisticsBySeason> GetPlayersStats(GridSettings gridSettings,IQueryable<PlayerStatisticsBySeason> players, out int totalPlayers)
        {
            var orderedPlayers = OrderPlayersStats(players, gridSettings.SortColumn, gridSettings.SortOrder);

            if (gridSettings.IsSearch)
            {
                orderedPlayers = gridSettings.Where.rules.Aggregate(orderedPlayers, FilterPlayersStats);
            }
            totalPlayers = orderedPlayers.Count();

            return orderedPlayers.Skip((gridSettings.PageIndex - 1) * gridSettings.PageSize).Take(gridSettings.PageSize).AsEnumerable();
        }
      

        private static IQueryable<PlayerStatisticsBySeason> FilterPlayersStats(IQueryable<PlayerStatisticsBySeason> players, Rule rule)
        {                    
            

            switch (rule.field)
            {
                case "IdCondition":
                    int idCo = 0;
                    if (Int32.TryParse(rule.data.Replace(".", ",").ToString(), out idCo))
                        return players.Where(c => c.idCondition == idCo);
                    return players;
                case "BirdRights":
                    int idBi = 0;
                    if (Int32.TryParse(rule.data.Replace(".", ",").ToString(), out idBi))
                        return players.Where(c => c.BirdRights == idBi);
                    return players;
                case "Logo":
                    return players.Where(c => c.Team.ToLower().Contains(rule.data.ToLower()));
                case "CompleteName":
                    return players.Where(c => c.CompleteName.ToLower().Contains(rule.data.ToLower()));
                case "Position":
                    int idPosition = 0;
                    if (Int32.TryParse(rule.data.Replace(".", ",").ToString(), out idPosition))
                    {
                        return players.Where(c => c.IdPosition == idPosition);
                    }
                    return players;

                    //return players.Where(c => c.Position.ToLower().Contains(rule.data.ToLower()));

                case "State":
                    int idState = 0;
                    if (Int32.TryParse(rule.data.Replace(".", ",").ToString(), out idState))
                    {
                        return players.Where(c => c.IdState  == idState);
                    }
                    return players;

                    //return players.Where(c => c.State.ToLower().Contains(rule.data.ToLower()));

                case "Rol":
                    int idRol = 0;
                    if (Int32.TryParse(rule.data.Replace(".", ",").ToString(), out idRol))
                    {
                        return players.Where(c => c.IdRol == idRol);
                    }
                    return players;

                    //return players.Where(c => c.Rol.ToLower().Contains(rule.data.ToLower()));

                case "Salary":
                    int idSalary = 0;
                    if (Int32.TryParse(rule.data.Replace(".", ",").ToString(), out idSalary))
                    {

                        switch (rule.op)
                        {
     
                            case "eq":
                                players = players.Where(x => x.Salary == idSalary);
                                break;
                            case "lt":
                                players = players.Where(x => x.Salary < idSalary);
                                break;
                            case "le":
                                players = players.Where(x => x.Salary <= idSalary);
                                break;
                            case "gt":
                                players = players.Where(x => x.Salary > idSalary);
                                break;
                            case "ge":
                                players = players.Where(x => x.Salary >= idSalary);
                                break;
                            case "cn":
                                players = players.Where(x => x.Salary == idSalary);
                                break;
                        }
                    }
                    return players;
                case "Level2k":
                    int idLevel2k = 0;
                    if (Int32.TryParse(rule.data.Replace(".", ",").ToString(), out idLevel2k))
                    {

                        switch (rule.op)
                        {

                            case "eq":
                                players = players.Where(x => x.Level2k == idLevel2k);
                                break;
                            case "lt":
                                players = players.Where(x => x.Level2k < idLevel2k);
                                break;
                            case "le":
                                players = players.Where(x => x.Level2k <= idLevel2k);
                                break;
                            case "gt":
                                players = players.Where(x => x.Level2k > idLevel2k);
                                break;
                            case "ge":
                                players = players.Where(x => x.Level2k >= idLevel2k);
                                break;
                            case "cn":
                                players = players.Where(x => x.Level2k == idLevel2k);
                                break;
                        }
                    }
                    return players;
                case "YearsPro":
                    int idYearsPro = 0;
                    if (Int32.TryParse(rule.data.Replace(".", ",").ToString(), out idYearsPro))
                    {

                        switch (rule.op)
                        {
     
                            case "eq":
                                players = players.Where(x => x.YearsPro == idYearsPro);
                                break;
                            case "lt":
                                players = players.Where(x => x.YearsPro < idYearsPro);
                                break;
                            case "le":
                                players = players.Where(x => x.YearsPro <= idYearsPro);
                                break;
                            case "gt":
                                players = players.Where(x => x.YearsPro > idYearsPro);
                                break;
                            case "ge":
                                players = players.Where(x => x.YearsPro >= idYearsPro);
                                break;
                            case "cn":
                                players = players.Where(x => x.YearsPro == idYearsPro);
                                break;
                        }
                    }
                    return players;
                case "G":
                    int idG = 0;
                    if (Int32.TryParse(rule.data.Replace(".", ",").ToString(), out idG))
                    {

                        switch (rule.op)
                        {
                            case "eq":
                                players = players.Where(x => x.G == idG);
                                break;
                            case "lt":
                                players = players.Where(x => x.G < idG);
                                break;
                            case "le":
                                players = players.Where(x => x.G <= idG);
                                break;
                            case "gt":
                                players = players.Where(x => x.G > idG);
                                break;
                            case "ge":
                                players = players.Where(x => x.G >= idG);
                                break;
                            case "cn":
                                players = players.Where(x => x.G == idG);
                                break;
                        }
                    }
                    return players;
                case "GS":
                    int idGS = 0;
                    if (Int32.TryParse(rule.data.Replace(".", ",").ToString(), out idGS))
                    {

                        switch (rule.op)
                        {
                            case "eq":
                                players = players.Where(x => x.GS == idGS);
                                break;
                            case "lt":
                                players = players.Where(x => x.GS < idGS);
                                break;
                            case "le":
                                players = players.Where(x => x.GS <= idGS);
                                break;
                            case "gt":
                                players = players.Where(x => x.GS > idGS);
                                break;
                            case "ge":
                                players = players.Where(x => x.GS >= idGS);
                                break;
                            case "cn":
                                players = players.Where(x => x.GS == idGS);
                                break;
                        }
                    }
                    return players;
                case "MPG":
                    decimal idMPG = 0;
                    if (Decimal.TryParse(rule.data.Replace(".", ",").ToString(), out idMPG))
                    {
                        switch (rule.op)
                        {
                            case "eq":
                                players = players.Where(x => x.MPG == idMPG);
                                break;
                            case "lt":
                                players = players.Where(x => x.MPG < idMPG);
                                break;
                            case "le":
                                players = players.Where(x => x.MPG <= idMPG);
                                break;
                            case "gt":
                                players = players.Where(x => x.MPG > idMPG);
                                break;
                            case "ge":
                                players = players.Where(x => x.MPG >= idMPG);
                                break;
                            case "cn":
                                players = players.Where(x => x.MPG == idMPG);
                                break;
                        }
                    }
                    return players;
                case "FG":
                    decimal idFG = 0;
                    if (Decimal.TryParse(rule.data.Replace(".", ",").ToString(), out idFG))
                    {
                        switch (rule.op)
                        {
                            case "eq":
                                players = players.Where(x => x.FG == idFG);
                                break;
                            case "lt":
                                players = players.Where(x => x.FG < idFG);
                                break;
                            case "le":
                                players = players.Where(x => x.FG <= idFG);
                                break;
                            case "gt":
                                players = players.Where(x => x.FG > idFG);
                                break;
                            case "ge":
                                players = players.Where(x => x.FG >= idFG);
                                break;
                            case "cn":
                                players = players.Where(x => x.FG == idFG);
                                break;
                        }
                    }
                    return players;
                case "F3G":
                    decimal idF3G = 0;
                    if (Decimal.TryParse(rule.data.Replace(".", ",").ToString(), out idF3G))
                    {
                        switch (rule.op)
                        {
                            case "eq":
                                players = players.Where(x => x.F3G == idF3G);
                                break;
                            case "lt":
                                players = players.Where(x => x.F3G < idF3G);
                                break;
                            case "le":
                                players = players.Where(x => x.F3G <= idF3G);
                                break;
                            case "gt":
                                players = players.Where(x => x.F3G > idF3G);
                                break;
                            case "ge":
                                players = players.Where(x => x.F3G >= idF3G);
                                break;
                            case "cn":
                                players = players.Where(x => x.F3G == idF3G);
                                break;
                        }
                    }
                    return players;
                case "FT":
                    decimal idFT = 0;
                    if (Decimal.TryParse(rule.data.Replace(".", ",").ToString(), out idFT))
                    {
                        switch (rule.op)
                        {
                            case "eq":
                                players = players.Where(x => x.FT == idFT);
                                break;
                            case "lt":
                                players = players.Where(x => x.FT < idFT);
                                break;
                            case "le":
                                players = players.Where(x => x.FT <= idFT);
                                break;
                            case "gt":
                                players = players.Where(x => x.FT > idFT);
                                break;
                            case "ge":
                                players = players.Where(x => x.FT >= idFT);
                                break;
                            case "cn":
                                players = players.Where(x => x.FT == idFT);
                                break;
                        }
                    }
                    return players;
                case "OFR":
                    decimal idOFR = 0;
                    if (Decimal.TryParse(rule.data.Replace(".", ",").ToString(), out idOFR))
                    {
                        switch (rule.op)
                        {
                            case "eq":
                                players = players.Where(x => x.OFR == idOFR);
                                break;
                            case "lt":
                                players = players.Where(x => x.OFR < idOFR);
                                break;
                            case "le":
                                players = players.Where(x => x.OFR <= idOFR);
                                break;
                            case "gt":
                                players = players.Where(x => x.OFR > idOFR);
                                break;
                            case "ge":
                                players = players.Where(x => x.OFR >= idOFR);
                                break;
                            case "cn":
                                players = players.Where(x => x.OFR == idOFR);
                                break;
                        }
                    }
                    return players;
                case "DEF":
                    decimal idDEF = 0;
                    if (Decimal.TryParse(rule.data.Replace(".", ",").ToString(), out idDEF))
                    {
                        switch (rule.op)
                        {
                            case "eq":
                                players = players.Where(x => x.DEF == idDEF);
                                break;
                            case "lt":
                                players = players.Where(x => x.DEF < idDEF);
                                break;
                            case "le":
                                players = players.Where(x => x.DEF <= idDEF);
                                break;
                            case "gt":
                                players = players.Where(x => x.DEF > idDEF);
                                break;
                            case "ge":
                                players = players.Where(x => x.DEF >= idDEF);
                                break;
                            case "cn":
                                players = players.Where(x => x.DEF == idDEF);
                                break;
                        }
                    }
                    return players;
                case "RPG":
                    decimal idRPG = 0;
                    if (Decimal.TryParse(rule.data.Replace(".", ",").ToString(), out idRPG))
                    {
                        switch (rule.op)
                        {
                            case "eq":
                                players = players.Where(x => x.RPG == idRPG);
                                break;
                            case "lt":
                                players = players.Where(x => x.RPG < idRPG);
                                break;
                            case "le":
                                players = players.Where(x => x.RPG <= idRPG);
                                break;
                            case "gt":
                                players = players.Where(x => x.RPG > idRPG);
                                break;
                            case "ge":
                                players = players.Where(x => x.RPG >= idRPG);
                                break;
                            case "cn":
                                players = players.Where(x => x.RPG == idRPG);
                                break;
                        }
                    }
                    return players;
                case "APG":
                    decimal idAPG = 0;
                    if (Decimal.TryParse(rule.data.Replace(".", ",").ToString(), out idAPG))
                    {
                        switch (rule.op)
                        {
                            case "eq":
                                players = players.Where(x => x.APG == idAPG);
                                break;
                            case "lt":
                                players = players.Where(x => x.APG < idAPG);
                                break;
                            case "le":
                                players = players.Where(x => x.APG <= idAPG);
                                break;
                            case "gt":
                                players = players.Where(x => x.APG > idAPG);
                                break;
                            case "ge":
                                players = players.Where(x => x.APG >= idAPG);
                                break;
                            case "cn":
                                players = players.Where(x => x.APG == idAPG);
                                break;
                        }
                    }
                    return players;
                case "SPG":
                    decimal idSPG = 0;
                    if (Decimal.TryParse(rule.data.Replace(".", ",").ToString(), out idSPG))
                    {
                        switch (rule.op)
                        {
                            case "eq":
                                players = players.Where(x => x.SPG == idSPG);
                                break;
                            case "lt":
                                players = players.Where(x => x.SPG < idSPG);
                                break;
                            case "le":
                                players = players.Where(x => x.SPG <= idSPG);
                                break;
                            case "gt":
                                players = players.Where(x => x.SPG > idSPG);
                                break;
                            case "ge":
                                players = players.Where(x => x.SPG >= idSPG);
                                break;
                            case "cn":
                                players = players.Where(x => x.SPG == idSPG);
                                break;
                        }
                    }
                    return players;
                case "BPG":
                    decimal idBPG = 0;
                    if (Decimal.TryParse(rule.data.Replace(".", ",").ToString(), out idBPG))
                    {
                        switch (rule.op)
                        {
                            case "eq":
                                players = players.Where(x => x.BPG == idBPG);
                                break;
                            case "lt":
                                players = players.Where(x => x.BPG < idBPG);
                                break;
                            case "le":
                                players = players.Where(x => x.BPG <= idBPG);
                                break;
                            case "gt":
                                players = players.Where(x => x.BPG > idBPG);
                                break;
                            case "ge":
                                players = players.Where(x => x.BPG >= idBPG);
                                break;
                            case "cn":
                                players = players.Where(x => x.BPG == idBPG);
                                break;
                        }
                    }
                    return players;
                case "TOO":
                    decimal idTOO = 0;
                    if (Decimal.TryParse(rule.data.Replace(".", ",").ToString(), out idTOO))
                    {
                        switch (rule.op)
                        {
                            case "eq":
                                players = players.Where(x => x.TOO == idTOO);
                                break;
                            case "lt":
                                players = players.Where(x => x.TOO < idTOO);
                                break;
                            case "le":
                                players = players.Where(x => x.TOO <= idTOO);
                                break;
                            case "gt":
                                players = players.Where(x => x.TOO > idTOO);
                                break;
                            case "ge":
                                players = players.Where(x => x.TOO >= idTOO);
                                break;
                            case "cn":
                                players = players.Where(x => x.TOO == idTOO);
                                break;
                        }
                    }
                    return players;
                case "PF":
                    decimal idPF = 0;
                    if (Decimal.TryParse(rule.data.Replace(".", ",").ToString(), out idPF))
                    {
                        switch (rule.op)
                        {
                            case "eq":
                                players = players.Where(x => x.PF == idPF);
                                break;
                            case "lt":
                                players = players.Where(x => x.PF < idPF);
                                break;
                            case "le":
                                players = players.Where(x => x.PF <= idPF);
                                break;
                            case "gt":
                                players = players.Where(x => x.PF > idPF);
                                break;
                            case "ge":
                                players = players.Where(x => x.PF >= idPF);
                                break;
                            case "cn":
                                players = players.Where(x => x.PF == idPF);
                                break;
                        }
                    }
                    return players;
                case "PPG":
                    decimal idPPG = 0;
                    if (Decimal.TryParse(rule.data.Replace(".", ",").ToString(), out idPPG))
                    {
                        switch (rule.op)
                        {
                            case "eq":
                                players = players.Where(x => x.PPG == idPPG);
                                break;
                            case "lt":
                                players = players.Where(x => x.PPG < idPPG);
                                break;
                            case "le":
                                players = players.Where(x => x.PPG <= idPPG);
                                break;
                            case "gt":
                                players = players.Where(x => x.PPG > idPPG);
                                break;
                            case "ge":
                                players = players.Where(x => x.PPG >= idPPG);
                                break;
                            case "cn":
                                players = players.Where(x => x.PPG == idPPG);
                                break;
                        }
                    }
                    return players;



                default:
                    return players;
            }
        }

        private static IQueryable<PlayerStatisticsBySeason> OrderPlayersStats(IQueryable<PlayerStatisticsBySeason> players, string sortColumn, string sortOrder)
        {
            switch (sortColumn)
            {
                case "IdCondition":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.idCondition) : players.OrderBy(c => c.idCondition);
                case "BirdRights":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.BirdRights) : players.OrderBy(c => c.BirdRights);

                case "Logo":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.Team) : players.OrderBy(c => c.Team);
                case "CompleteName":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.CompleteName) : players.OrderBy(c => c.CompleteName);
                case "Position":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.Position ) : players.OrderBy(c => c.Position);
                case "State":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.State) : players.OrderBy(c => c.State);
                case "Rol":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.Rol) : players.OrderBy(c => c.Rol);
                case "YearsPro":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.YearsPro) : players.OrderBy(c => c.YearsPro);
                case "Level2k":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.Level2k) : players.OrderBy(c => c.Level2k);
                case "Salary":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.Salary) : players.OrderBy(c => c.Salary);
                case "G":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.G) : players.OrderBy(c => c.G);
                case "GS":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.GS) : players.OrderBy(c => c.GS);
                case "MPG":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.MPG) : players.OrderBy(c => c.MPG);
                case "FG":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.FG) : players.OrderBy(c => c.FG);
                case "F3G":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.F3G) : players.OrderBy(c => c.F3G);
                case "FT":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.FT) : players.OrderBy(c => c.FT);
                case "OFR":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.OFR) : players.OrderBy(c => c.OFR);
                case "DEF":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.DEF) : players.OrderBy(c => c.DEF);
                case "RPG":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.RPG) : players.OrderBy(c => c.RPG);
                case "APG":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.APG) : players.OrderBy(c => c.APG);
                case "SPG":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.SPG) : players.OrderBy(c => c.SPG);
                case "BPG":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.BPG) : players.OrderBy(c => c.BPG);
                case "TOO":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.TOO) : players.OrderBy(c => c.TOO);
                case "PF":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.PF) : players.OrderBy(c => c.PF);
                case "PPG":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.PPG) : players.OrderBy(c => c.PPG);
                default:
                    return players;
            }
        }
        public JsonResult GetPlayersStatsByTeam(int idTeam, GridSettings settings)
        {
            int countPlayers = 0;
            IQueryable<PlayerStatisticsBySeason> listaPlayers;
            if (idTeam != 0)
            {
                listaPlayers = db.PlayerStatisticsBySeasons.Where(x => x.IdTeam == idTeam).OrderByDescending(x => x.G).AsQueryable();
            }
            else
            {
                listaPlayers = db.PlayerStatisticsBySeasons.OrderByDescending(x => x.G).AsQueryable();
            }


            IEnumerable<PlayerStatisticsBySeason> listPlayers = GetPlayersStats(settings, listaPlayers, out countPlayers);
            
            int totalRecords = countPlayers;
            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)settings.PageSize);
            int page = settings.PageIndex;

            int startRow = (settings.PageIndex - 1) * settings.PageSize;


            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from players in listPlayers
                    select new
                    {
                        id = players.Player,
                        cell = new string[]{
                            players.idCondition.ToString(),
                            players.BirdRights.ToString(),
                            players.Logo,
                            players.CompleteName ,
                            players.Position,
                            players.State,
                            players.Rol,
                            players.Level2k.ToString(),
                            players.YearsPro.ToString(),
                            players.Salary.ToString().Replace(",","."),                       
                            players.G.ToString(),
                            players.GS.ToString(),
                            players.MPG.ToString().Replace(",","."),
                            players.FG.ToString().Replace(",","."),
                            players.F3G.ToString().Replace(",","."),
                            players.FT.ToString().Replace(",","."),
                            players.OFR.ToString().Replace(",","."),
                            players.DEF.ToString().Replace(",","."),
                            players.RPG.ToString().Replace(",","."),
                            players.APG.ToString().Replace(",","."),
                            players.SPG.ToString().Replace(",","."),
                            players.BPG.ToString().Replace(",","."),
                            players.TOO.ToString().Replace(",","."),
                            players.PF.ToString().Replace(",","."),
                            players.PPG.ToString().Replace(",",".")
                        }

                    }
            ).ToArray()
            };
            //     return Json(jsonData);
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }
        public JsonResult GetPlayersStatsTotal(string idSeason, GridSettings settings)
        {
            int countPlayers = 0;
            IQueryable<PlayerStatisticsBySeason> listaPlayers;

           

            string query = "PlayerStatisticsTotals @RSPO ";
            SqlParameter RSPO = new SqlParameter("@RSPO", Convert.ToInt32(idSeason));

            object[] SqlArr = new object[1];
            SqlArr[0] = RSPO;

            listaPlayers = db.Database.SqlQuery<PlayerStatisticsBySeason>(query, SqlArr).ToList().AsQueryable();

            // listaPlayers = db.PlayerStatisticsBySeasons.OrderByDescending(x => x.G).AsQueryable();


            IEnumerable<PlayerStatisticsBySeason> listPlayers = GetPlayersStats(settings, listaPlayers, out countPlayers);

            int totalRecords = countPlayers;
            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)settings.PageSize);
            int page = settings.PageIndex;

            int startRow = (settings.PageIndex - 1) * settings.PageSize;

            //listPlayers = listPlayers.Skip(startRow).Take(settings.PageSize);


            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from players in listPlayers
                    select new
                    {
                        id = players.Player,
                        cell = new string[]{
                            players.idCondition.ToString(),
                            players.BirdRights.ToString(),
                            players.Logo,
                            players.CompleteName ,
                            players.Position,
                            players.State,
                            players.Rol,
                            players.Level2k.ToString(),
                            players.YearsPro.ToString(),
                            players.Salary.ToString().Replace(",","."),                       
                            players.G.ToString(),
                            players.GS.ToString(),
                            players.MPG.ToString().Replace(",","."),
                            players.FG.ToString().Replace(",","."),
                            players.F3G.ToString().Replace(",","."),
                            players.FT.ToString().Replace(",","."),
                            players.OFR.ToString().Replace(",","."),
                            players.DEF.ToString().Replace(",","."),
                            players.RPG.ToString().Replace(",","."),
                            players.APG.ToString().Replace(",","."),
                            players.SPG.ToString().Replace(",","."),
                            players.BPG.ToString().Replace(",","."),
                            players.TOO.ToString().Replace(",","."),
                            players.PF.ToString().Replace(",","."),
                            players.PPG.ToString().Replace(",",".")
                        }

                    }
            ).ToArray()
            };
            //     return Json(jsonData);
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }

        public JsonResult GetPlayersStatsGeneral(string idSeason,string idOrder, GridSettings settings)
        {
            int countPlayers = 0;
            IQueryable<PlayerStatisticsBySeason> listaPlayers;

            int iTemporada;
            int iRSoPO;
            if (idSeason.Length == 5)
            {
                iTemporada = Convert.ToInt32(idSeason.Substring(0, 4));
                iRSoPO = 0;
            }
            else
            {
                iTemporada = Convert.ToInt32(idSeason);
                iRSoPO = 1;
            }

            string query = "PlayerStatistics @Season, @RSPO ";
            SqlParameter SeasonId = new SqlParameter("@Season", iTemporada);
            SqlParameter RSPO = new SqlParameter("@RSPO", iRSoPO);

            object[] SqlArr = new object[2];
            SqlArr[0] = SeasonId;
            SqlArr[1] = RSPO;

            //listaPlayers = db.Database.SqlQuery<PlayerStatisticsBySeason>(query, SqlArr).ToList().AsQueryable();

            listaPlayers = db.PlayerStatisticsBySeasons.Where(x => x.Season == idSeason).OrderByDescending(x => x.G).AsQueryable();

            // listaPlayers = db.PlayerStatisticsBySeasons.OrderByDescending(x => x.G).AsQueryable();


            if (idOrder != "")
            {
                switch (idOrder)
                {
                    case "PPG":
                        settings.SortColumn = "PPG";
                        settings.SortOrder = "desc";
                        //listPlayers = listPlayers.OrderByDescending(x => x.PPG);
                        break;
                    case "ASS":
                        settings.SortColumn = "APG";
                        settings.SortOrder = "desc";
                        //listPlayers = listPlayers.OrderByDescending(x => x.APG);
                        break;
                    case "FG":
                        settings.SortColumn = "FG";
                        settings.SortOrder = "desc";
                        //listPlayers = listPlayers.OrderByDescending(x => x.FG);
                        break;
                    case "RPG":
                        settings.SortColumn = "RPG";
                        settings.SortOrder = "desc";
                        //listPlayers = listPlayers.OrderByDescending(x => x.RPG);
                        break;
                    case "SPG":
                        settings.SortColumn = "SPG";
                        settings.SortOrder = "desc";
                        //listPlayers = listPlayers.OrderByDescending(x => x.SPG);
                        break;
                    case "BPG":
                        settings.SortColumn = "BPG";
                        settings.SortOrder = "desc";
                        //listPlayers = listPlayers.OrderByDescending(x => x.BPG);
                        break;
                    default:
                        break;
                }
            }

            IEnumerable<PlayerStatisticsBySeason> listPlayers = GetPlayersStats(settings, listaPlayers, out countPlayers);

            int totalRecords = countPlayers;
            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)settings.PageSize);
            int page = settings.PageIndex;

            int startRow = (settings.PageIndex - 1) * settings.PageSize;

            //listPlayers = listPlayers.Skip(startRow).Take(settings.PageSize);


            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from players in listPlayers
                    select new
                    {
                        id = players.Player,
                        cell = new string[]{
                            players.idCondition.ToString(),
                            players.BirdRights.ToString(),
                            players.Logo,
                            players.CompleteName ,
                            players.Position,
                            players.State,
                            players.Rol,
                            players.Level2k.ToString(),
                            players.YearsPro.ToString(),
                            players.Salary.ToString().Replace(",","."),                       
                            players.G.ToString(),
                            players.GS.ToString(),
                            players.MPG.ToString().Replace(",","."),
                            players.FG.ToString().Replace(",","."),
                            players.F3G.ToString().Replace(",","."),
                            players.FT.ToString().Replace(",","."),
                            players.OFR.ToString().Replace(",","."),
                            players.DEF.ToString().Replace(",","."),
                            players.RPG.ToString().Replace(",","."),
                            players.APG.ToString().Replace(",","."),
                            players.SPG.ToString().Replace(",","."),
                            players.BPG.ToString().Replace(",","."),
                            players.TOO.ToString().Replace(",","."),
                            players.PF.ToString().Replace(",","."),
                            players.PPG.ToString().Replace(",",".")
                        }

                    }
            ).ToArray()
            };
            //     return Json(jsonData);
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }
        public JsonResult GetPlayersStatsRookies(GridSettings settings)
        {
            int countPlayers = 0;
            IQueryable<PlayerStatisticsBySeason> listaPlayers;
        
            listaPlayers = db.PlayerStatisticsBySeasons.Where(x=>x.YearsPro == 0).OrderByDescending(x => x.G).AsQueryable();
        

            IEnumerable<PlayerStatisticsBySeason> listPlayers = GetPlayersStats(settings, listaPlayers, out countPlayers);

            int totalRecords = countPlayers;
            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)settings.PageSize);
            int page = settings.PageIndex;

            int startRow = (settings.PageIndex - 1) * settings.PageSize;

            //listPlayers = listPlayers.Skip(startRow).Take(settings.PageSize);


            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from players in listPlayers
                    select new
                    {
                        id = players.Player,
                        cell = new string[]{
                            players.idCondition.ToString(),
                            players.BirdRights.ToString(),
                            players.Logo,
                            players.CompleteName ,
                            players.Position,
                            players.State,
                            players.Rol,
                            players.Level2k.ToString(),
                            players.YearsPro.ToString(),
                            players.Salary.ToString().Replace(",","."),                       
                            players.G.ToString(),
                            players.GS.ToString(),
                            players.MPG.ToString().Replace(",","."),
                            players.FG.ToString().Replace(",","."),
                            players.F3G.ToString().Replace(",","."),
                            players.FT.ToString().Replace(",","."),
                            players.OFR.ToString().Replace(",","."),
                            players.DEF.ToString().Replace(",","."),
                            players.RPG.ToString().Replace(",","."),
                            players.APG.ToString().Replace(",","."),
                            players.SPG.ToString().Replace(",","."),
                            players.BPG.ToString().Replace(",","."),
                            players.TOO.ToString().Replace(",","."),
                            players.PF.ToString().Replace(",","."),
                            players.PPG.ToString().Replace(",",".")
                        }

                    }
            ).ToArray()
            };
            //     return Json(jsonData);
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }
        public JsonResult GetPlayersStatsExpirings(GridSettings settings)
        {
            int countPlayers = 0;
            IQueryable<PlayerStatisticsBySeason> listaPlayers;

            listaPlayers = db.PlayerStatisticsBySeasons.Where(x => x.Salary2 == 0).OrderByDescending(x => x.G).AsQueryable();


            IEnumerable<PlayerStatisticsBySeason> listPlayers = GetPlayersStats(settings, listaPlayers, out countPlayers);

            int totalRecords = countPlayers;
            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)settings.PageSize);
            int page = settings.PageIndex;

            int startRow = (settings.PageIndex - 1) * settings.PageSize;



            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from players in listPlayers
                    select new
                    {
                        id = players.Player,
                        cell = new string[]{
                            players.idCondition.ToString(),
                            players.BirdRights.ToString(),
                            players.Logo,
                            players.CompleteName ,
                            players.Position,
                            players.State,
                            players.Rol,
                            players.Level2k.ToString(),
                            players.YearsPro.ToString(),
                            players.Salary.ToString().Replace(",","."),                       
                            players.G.ToString(),
                            players.GS.ToString(),
                            players.MPG.ToString().Replace(",","."),
                            players.FG.ToString().Replace(",","."),
                            players.F3G.ToString().Replace(",","."),
                            players.FT.ToString().Replace(",","."),
                            players.OFR.ToString().Replace(",","."),
                            players.DEF.ToString().Replace(",","."),
                            players.RPG.ToString().Replace(",","."),
                            players.APG.ToString().Replace(",","."),
                            players.SPG.ToString().Replace(",","."),
                            players.BPG.ToString().Replace(",","."),
                            players.TOO.ToString().Replace(",","."),
                            players.PF.ToString().Replace(",","."),
                            players.PPG.ToString().Replace(",",".")
                        }

                    }
            ).ToArray()
            };
            //     return Json(jsonData);
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }
        public JsonResult GetPlayersStatsTransferibles(GridSettings settings)
        {
            int countPlayers = 0;
            IQueryable<PlayerStatisticsBySeason> listaPlayers;

            listaPlayers = db.PlayerStatisticsBySeasons.Where(x => x.IdState == 1 || x.IdState == 3 || x.IdState == 8).OrderByDescending(x => x.G).AsQueryable();


            IEnumerable<PlayerStatisticsBySeason> listPlayers = GetPlayersStats(settings, listaPlayers, out countPlayers);

            int totalRecords = countPlayers;
            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)settings.PageSize);
            int page = settings.PageIndex;

            int startRow = (settings.PageIndex - 1) * settings.PageSize;



            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from players in listPlayers
                    select new
                    {
                        id = players.Player,
                        cell = new string[]{
                            players.idCondition.ToString(),
                            players.BirdRights.ToString(),
                            players.Logo,
                            players.CompleteName ,
                            players.Position,
                            players.State,
                            players.Rol,
                            players.Level2k.ToString(),
                            players.YearsPro.ToString(),
                            players.Salary.ToString().Replace(",","."),                       
                            players.G.ToString(),
                            players.GS.ToString(),
                            players.MPG.ToString().Replace(",","."),
                            players.FG.ToString().Replace(",","."),
                            players.F3G.ToString().Replace(",","."),
                            players.FT.ToString().Replace(",","."),
                            players.OFR.ToString().Replace(",","."),
                            players.DEF.ToString().Replace(",","."),
                            players.RPG.ToString().Replace(",","."),
                            players.APG.ToString().Replace(",","."),
                            players.SPG.ToString().Replace(",","."),
                            players.BPG.ToString().Replace(",","."),
                            players.TOO.ToString().Replace(",","."),
                            players.PF.ToString().Replace(",","."),
                            players.PPG.ToString().Replace(",",".")
                        }

                    }
            ).ToArray()
            };
            //     return Json(jsonData);
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }
        public JsonResult GetPlayersStatsForWeek(int idWeek, GridSettings settings)
        {
            int countPlayers = 0;
            IQueryable<PlayerStatisticsBySeason> listaPlayers;
            IEnumerable<PlayerStatisticsBySeason> listaPlayersAux;
            int Season = new Liga_DBContext().Parameters.First().Temporada;
            PlazosSemanales plazo = db.PlazosSemanales.Where(x => x.Plazo == idWeek).First();
            int IPlazo = plazo.Inicio;
            int FPlazo = plazo.Fin;

            
            string query = "TopWeekMonth @Season, @IniPlazo, @FinPlazo ";
            SqlParameter SeasonId = new SqlParameter("@Season", Season);
            SqlParameter IniPlazo = new SqlParameter("@IniPlazo", IPlazo);
            SqlParameter FinPlazo = new SqlParameter("@FinPlazo", FPlazo);

            //SqlParameterCollection SqlArray;
            //SqlArray.Add(SeasonId);
            //SqlArray.Add(IniPlazo);
            //SqlArray.Add(FinPlazo);

            object []SqlArr = new object[3];
            SqlArr[0] = SeasonId;
            SqlArr[1] = IniPlazo;
            SqlArr[2] = FinPlazo;


            listaPlayersAux = db.Database.SqlQuery<PlayerStatisticsBySeason>(query, SqlArr).ToList();

            listaPlayers = listaPlayersAux.AsQueryable();

            //  listaPlayers = db.PlayerStatisticsBySeasons.OrderByDescending(x => x.G).AsQueryable();


            
            IEnumerable<PlayerStatisticsBySeason> listPlayers = GetPlayersStats(settings, listaPlayers, out countPlayers);

            int totalRecords = countPlayers;
            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)settings.PageSize);
            int page = settings.PageIndex;

            int startRow = (settings.PageIndex - 1) * settings.PageSize;

            //listPlayers = listPlayers.Skip(startRow).Take(settings.PageSize);
           

            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from players in listPlayers
                    select new
                    {
                        id = players.Player,
                        cell = new string[]{
                        players.idCondition.ToString(),
                        players.BirdRights.ToString(),
                        players.Logo,
                        players.CompleteName ,
                        players.Position,
                        players.State,
                        players.Rol,
                        players.Level2k.ToString(),
                        players.YearsPro.ToString(),
                        players.Salary.ToString().Replace(",","."),                       
                        players.G.ToString(),
                        players.GS.ToString(),
                        players.MPG.ToString().Replace(",","."),
                        players.FG.ToString().Replace(",","."),
                        players.F3G.ToString().Replace(",","."),
                        players.FT.ToString().Replace(",","."),
                        players.OFR.ToString().Replace(",","."),
                        players.DEF.ToString().Replace(",","."),
                        players.RPG.ToString().Replace(",","."),
                        players.APG.ToString().Replace(",","."),
                        players.SPG.ToString().Replace(",","."),
                        players.BPG.ToString().Replace(",","."),
                        players.TOO.ToString().Replace(",","."),
                        players.PF.ToString().Replace(",","."),
                        players.PPG.ToString().Replace(",",".")
                    }

                    }
            ).ToArray()
            };
            //     return Json(jsonData);
            return Json(jsonData, JsonRequestBehavior.AllowGet);
            
        }
        public JsonResult GetPlayersStatsForMonth(int idMonth, GridSettings settings)
        {
            int countPlayers = 0;
            IQueryable<PlayerStatisticsBySeason> listaPlayers;
            IEnumerable<PlayerStatisticsBySeason> listaPlayersAux;
            int Season = new Liga_DBContext().Parameters.First().Temporada;
            PlazosMensuales plazo = db.PlazosMensuales.Where(x => x.Plazo == idMonth).First();
            int IPlazo = plazo.Inicio;
            int FPlazo = plazo.Fin;


            string query = "TopWeekMonth @Season, @IniPlazo, @FinPlazo ";
            SqlParameter SeasonId = new SqlParameter("@Season", Season);
            SqlParameter IniPlazo = new SqlParameter("@IniPlazo", IPlazo);
            SqlParameter FinPlazo = new SqlParameter("@FinPlazo", FPlazo);

            //SqlParameterCollection SqlArray;
            //SqlArray.Add(SeasonId);
            //SqlArray.Add(IniPlazo);
            //SqlArray.Add(FinPlazo);

            object[] SqlArr = new object[3];
            SqlArr[0] = SeasonId;
            SqlArr[1] = IniPlazo;
            SqlArr[2] = FinPlazo;


            listaPlayersAux = db.Database.SqlQuery<PlayerStatisticsBySeason>(query, SqlArr).ToList();

            listaPlayers = listaPlayersAux.AsQueryable();

   
            IEnumerable<PlayerStatisticsBySeason> listPlayers = GetPlayersStats(settings, listaPlayers, out countPlayers);

            int totalRecords = countPlayers;
            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)settings.PageSize);
            int page = settings.PageIndex;

            int startRow = (settings.PageIndex - 1) * settings.PageSize;

            //listPlayers = listPlayers.Skip(startRow).Take(settings.PageSize);


            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from players in listPlayers
                    select new
                    {
                        id = players.Player,
                        cell = new string[]{
                        players.idCondition.ToString(),
                        players.BirdRights.ToString(),
                        players.Logo,
                        players.CompleteName ,
                        players.Position,
                        players.State,
                        players.Rol,
                        players.Level2k.ToString(),
                        players.YearsPro.ToString(),
                        players.Salary.ToString().Replace(",","."),                       
                        players.G.ToString(),
                        players.GS.ToString(),
                        players.MPG.ToString().Replace(",","."),
                        players.FG.ToString().Replace(",","."),
                        players.F3G.ToString().Replace(",","."),
                        players.FT.ToString().Replace(",","."),
                        players.OFR.ToString().Replace(",","."),
                        players.DEF.ToString().Replace(",","."),
                        players.RPG.ToString().Replace(",","."),
                        players.APG.ToString().Replace(",","."),
                        players.SPG.ToString().Replace(",","."),
                        players.BPG.ToString().Replace(",","."),
                        players.TOO.ToString().Replace(",","."),
                        players.PF.ToString().Replace(",","."),
                        players.PPG.ToString().Replace(",",".")
                    }

                    }
            ).ToArray()
            };
            //     return Json(jsonData);
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }     
        #endregion

        #region Grid TeamStats
        public IEnumerable<TeamStats> GetTeamsStats(GridSettings gridSettings, IQueryable<TeamStats> teams, out int totalTeams)
        {
            var orderedTeams = OrderTeamsStats(teams, gridSettings.SortColumn, gridSettings.SortOrder);

            if (gridSettings.IsSearch)
            {
                orderedTeams = gridSettings.Where.rules.Aggregate(orderedTeams, FilterTeamsStats);
            }
            totalTeams = orderedTeams.Count();

            return orderedTeams.Skip((gridSettings.PageIndex - 1) * gridSettings.PageSize).Take(gridSettings.PageSize).AsEnumerable();
        }

        private static IQueryable<TeamStats> FilterTeamsStats(IQueryable<TeamStats> teams, Rule rule)
        {


            switch (rule.field)
            {

                case "Logo":
                    return teams.Where(c => c.Logo.ToLower().Contains(rule.data.ToLower()));
                case "Name":
                    return teams.Where(c => c.Name.ToLower().Contains(rule.data.ToLower()));
                case "G":
                    int idG = 0;
                    if (Int32.TryParse(rule.data.Replace(".", ",").ToString(), out idG))
                    {

                        switch (rule.op)
                        {
                            case "eq":
                                teams = teams.Where(x => x.G == idG);
                                break;
                            case "lt":
                                teams = teams.Where(x => x.G < idG);
                                break;
                            case "le":
                                teams = teams.Where(x => x.G <= idG);
                                break;
                            case "gt":
                                teams = teams.Where(x => x.G > idG);
                                break;
                            case "ge":
                                teams = teams.Where(x => x.G >= idG);
                                break;
                            case "cn":
                                teams = teams.Where(x => x.G == idG);
                                break;
                        }
                    }
                    return teams;
                case "FGM":
                    int idFGM = 0;
                    if (Int32.TryParse(rule.data.Replace(".", ",").ToString(), out idFGM))
                    {

                        switch (rule.op)
                        {
                            case "eq":
                                teams = teams.Where(x => x.FGM == idFGM);
                                break;
                            case "lt":
                                teams = teams.Where(x => x.FGM < idFGM);
                                break;
                            case "le":
                                teams = teams.Where(x => x.FGM <= idFGM);
                                break;
                            case "gt":
                                teams = teams.Where(x => x.FGM > idFGM);
                                break;
                            case "ge":
                                teams = teams.Where(x => x.FGM >= idFGM);
                                break;
                            case "cn":
                                teams = teams.Where(x => x.FGM == idFGM);
                                break;
                        }
                    }
                    return teams;
                case "FGA":
                    decimal idFGA = 0;
                    if (Decimal.TryParse(rule.data.Replace(".", ",").ToString(), out idFGA))
                    {
                        switch (rule.op)
                        {
                            case "eq":
                                teams = teams.Where(x => x.FGA == idFGA);
                                break;
                            case "lt":
                                teams = teams.Where(x => x.FGA < idFGA);
                                break;
                            case "le":
                                teams = teams.Where(x => x.FGA <= idFGA);
                                break;
                            case "gt":
                                teams = teams.Where(x => x.FGA > idFGA);
                                break;
                            case "ge":
                                teams = teams.Where(x => x.FGA >= idFGA);
                                break;
                            case "cn":
                                teams = teams.Where(x => x.FGA == idFGA);
                                break;
                        }
                    }
                    return teams;
                case "FG":
                    decimal idFG = 0;
                    if (Decimal.TryParse(rule.data.Replace(".", ",").ToString(), out idFG))
                    {
                        switch (rule.op)
                        {
                            case "eq":
                                teams = teams.Where(x => x.FG == idFG);
                                break;
                            case "lt":
                                teams = teams.Where(x => x.FG < idFG);
                                break;
                            case "le":
                                teams = teams.Where(x => x.FG <= idFG);
                                break;
                            case "gt":
                                teams = teams.Where(x => x.FG > idFG);
                                break;
                            case "ge":
                                teams = teams.Where(x => x.FG >= idFG);
                                break;
                            case "cn":
                                teams = teams.Where(x => x.FG == idFG);
                                break;
                        }
                    }
                    return teams;
                case "F3M":
                    decimal idF3M = 0;
                    if (Decimal.TryParse(rule.data.Replace(".", ",").ToString(), out idF3M))
                    {
                        switch (rule.op)
                        {
                            case "eq":
                                teams = teams.Where(x => x.F3M == idF3M);
                                break;
                            case "lt":
                                teams = teams.Where(x => x.F3M < idF3M);
                                break;
                            case "le":
                                teams = teams.Where(x => x.F3M <= idF3M);
                                break;
                            case "gt":
                                teams = teams.Where(x => x.F3M > idF3M);
                                break;
                            case "ge":
                                teams = teams.Where(x => x.F3M >= idF3M);
                                break;
                            case "cn":
                                teams = teams.Where(x => x.F3M == idF3M);
                                break;
                        }
                    }
                    return teams;
                case "F3A":
                    decimal idF3A = 0;
                    if (Decimal.TryParse(rule.data.Replace(".", ",").ToString(), out idF3A))
                    {
                        switch (rule.op)
                        {
                            case "eq":
                                teams = teams.Where(x => x.F3A == idF3A);
                                break;
                            case "lt":
                                teams = teams.Where(x => x.F3A < idF3A);
                                break;
                            case "le":
                                teams = teams.Where(x => x.F3A <= idF3A);
                                break;
                            case "gt":
                                teams = teams.Where(x => x.F3A > idF3A);
                                break;
                            case "ge":
                                teams = teams.Where(x => x.F3A >= idF3A);
                                break;
                            case "cn":
                                teams = teams.Where(x => x.F3A == idF3A);
                                break;
                        }
                    }
                    return teams;
                case "F3":
                    decimal idF3 = 0;
                    if (Decimal.TryParse(rule.data.Replace(".", ",").ToString(), out idF3))
                    {
                        switch (rule.op)
                        {
                            case "eq":
                                teams = teams.Where(x => x.F3 == idF3);
                                break;
                            case "lt":
                                teams = teams.Where(x => x.F3 < idF3);
                                break;
                            case "le":
                                teams = teams.Where(x => x.F3 <= idF3);
                                break;
                            case "gt":
                                teams = teams.Where(x => x.F3 > idF3);
                                break;
                            case "ge":
                                teams = teams.Where(x => x.F3 >= idF3);
                                break;
                            case "cn":
                                teams = teams.Where(x => x.F3 == idF3);
                                break;
                        }
                    }
                    return teams;
                case "FTM":
                    decimal idFTM = 0;
                    if (Decimal.TryParse(rule.data.Replace(".", ",").ToString(), out idFTM))
                    {
                        switch (rule.op)
                        {
                            case "eq":
                                teams = teams.Where(x => x.FTM == idFTM);
                                break;
                            case "lt":
                                teams = teams.Where(x => x.FTM < idFTM);
                                break;
                            case "le":
                                teams = teams.Where(x => x.FTM <= idFTM);
                                break;
                            case "gt":
                                teams = teams.Where(x => x.FTM > idFTM);
                                break;
                            case "ge":
                                teams = teams.Where(x => x.FTM >= idFTM);
                                break;
                            case "cn":
                                teams = teams.Where(x => x.FTM == idFTM);
                                break;
                        }
                    }
                    return teams;
                case "FTA":
                    decimal idFTA = 0;
                    if (Decimal.TryParse(rule.data.Replace(".", ",").ToString(), out idFTA))
                    {
                        switch (rule.op)
                        {
                            case "eq":
                                teams = teams.Where(x => x.FTA == idFTA);
                                break;
                            case "lt":
                                teams = teams.Where(x => x.FTA < idFTA);
                                break;
                            case "le":
                                teams = teams.Where(x => x.FTA <= idFTA);
                                break;
                            case "gt":
                                teams = teams.Where(x => x.FTA > idFTA);
                                break;
                            case "ge":
                                teams = teams.Where(x => x.FTA >= idFTA);
                                break;
                            case "cn":
                                teams = teams.Where(x => x.FTA == idFTA);
                                break;
                        }
                    }
                    return teams;
                case "FT":
                    decimal idFT = 0;
                    if (Decimal.TryParse(rule.data.Replace(".", ",").ToString(), out idFT))
                    {
                        switch (rule.op)
                        {
                            case "eq":
                                teams = teams.Where(x => x.FT == idFT);
                                break;
                            case "lt":
                                teams = teams.Where(x => x.FT < idFT);
                                break;
                            case "le":
                                teams = teams.Where(x => x.FT <= idFT);
                                break;
                            case "gt":
                                teams = teams.Where(x => x.FT > idFT);
                                break;
                            case "ge":
                                teams = teams.Where(x => x.FT >= idFT);
                                break;
                            case "cn":
                                teams = teams.Where(x => x.FT == idFT);
                                break;
                        }
                    }
                    return teams;
                case "RPG":
                    decimal idRPG = 0;
                    if (Decimal.TryParse(rule.data.Replace(".", ",").ToString(), out idRPG))
                    {
                        switch (rule.op)
                        {
                            case "eq":
                                teams = teams.Where(x => x.RPG == idRPG);
                                break;
                            case "lt":
                                teams = teams.Where(x => x.RPG < idRPG);
                                break;
                            case "le":
                                teams = teams.Where(x => x.RPG <= idRPG);
                                break;
                            case "gt":
                                teams = teams.Where(x => x.RPG > idRPG);
                                break;
                            case "ge":
                                teams = teams.Where(x => x.RPG >= idRPG);
                                break;
                            case "cn":
                                teams = teams.Where(x => x.RPG == idRPG);
                                break;
                        }
                    }
                    return teams;
                case "ROPG":
                    decimal idROPG = 0;
                    if (Decimal.TryParse(rule.data.Replace(".", ",").ToString(), out idROPG))
                    {
                        switch (rule.op)
                        {
                            case "eq":
                                teams = teams.Where(x => x.ROPG == idROPG);
                                break;
                            case "lt":
                                teams = teams.Where(x => x.ROPG < idROPG);
                                break;
                            case "le":
                                teams = teams.Where(x => x.ROPG <= idROPG);
                                break;
                            case "gt":
                                teams = teams.Where(x => x.ROPG > idROPG);
                                break;
                            case "ge":
                                teams = teams.Where(x => x.ROPG >= idROPG);
                                break;
                            case "cn":
                                teams = teams.Where(x => x.ROPG == idROPG);
                                break;
                        }
                    }
                    return teams;       
                case "RDPG":
                    decimal idRDPG = 0;
                    if (Decimal.TryParse(rule.data.Replace(".", ",").ToString(), out idRDPG))
                    {
                        switch (rule.op)
                        {
                            case "eq":
                                teams = teams.Where(x => x.RDPG == idRDPG);
                                break;
                            case "lt":
                                teams = teams.Where(x => x.RDPG < idRDPG);
                                break;
                            case "le":
                                teams = teams.Where(x => x.RDPG <= idRDPG);
                                break;
                            case "gt":
                                teams = teams.Where(x => x.RDPG > idRDPG);
                                break;
                            case "ge":
                                teams = teams.Where(x => x.RDPG >= idRDPG);
                                break;
                            case "cn":
                                teams = teams.Where(x => x.RDPG == idRDPG);
                                break;
                        }
                    }
                    return teams;             
                case "ASPG":
                    decimal idAPG = 0;
                    if (Decimal.TryParse(rule.data.Replace(".", ",").ToString(), out idAPG))
                    {
                        switch (rule.op)
                        {
                            case "eq":
                                teams = teams.Where(x => x.ASPG == idAPG);
                                break;
                            case "lt":
                                teams = teams.Where(x => x.ASPG < idAPG);
                                break;
                            case "le":
                                teams = teams.Where(x => x.ASPG <= idAPG);
                                break;
                            case "gt":
                                teams = teams.Where(x => x.ASPG > idAPG);
                                break;
                            case "ge":
                                teams = teams.Where(x => x.ASPG >= idAPG);
                                break;
                            case "cn":
                                teams = teams.Where(x => x.ASPG == idAPG);
                                break;
                        }
                    }
                    return teams;
                case "STPG":
                    decimal idSPG = 0;
                    if (Decimal.TryParse(rule.data.Replace(".", ",").ToString(), out idSPG))
                    {
                        switch (rule.op)
                        {
                            case "eq":
                                teams = teams.Where(x => x.STPG == idSPG);
                                break;
                            case "lt":
                                teams = teams.Where(x => x.STPG < idSPG);
                                break;
                            case "le":
                                teams = teams.Where(x => x.STPG <= idSPG);
                                break;
                            case "gt":
                                teams = teams.Where(x => x.STPG > idSPG);
                                break;
                            case "ge":
                                teams = teams.Where(x => x.STPG >= idSPG);
                                break;
                            case "cn":
                                teams = teams.Where(x => x.STPG == idSPG);
                                break;
                        }
                    }
                    return teams;
                case "BLPG":
                    decimal idBPG = 0;
                    if (Decimal.TryParse(rule.data.Replace(".", ",").ToString(), out idBPG))
                    {
                        switch (rule.op)
                        {
                            case "eq":
                                teams = teams.Where(x => x.BLPG == idBPG);
                                break;
                            case "lt":
                                teams = teams.Where(x => x.BLPG < idBPG);
                                break;
                            case "le":
                                teams = teams.Where(x => x.BLPG <= idBPG);
                                break;
                            case "gt":
                                teams = teams.Where(x => x.BLPG > idBPG);
                                break;
                            case "ge":
                                teams = teams.Where(x => x.BLPG >= idBPG);
                                break;
                            case "cn":
                                teams = teams.Where(x => x.BLPG == idBPG);
                                break;
                        }
                    }
                    return teams;
                case "TOPG":
                    decimal idTOO = 0;
                    if (Decimal.TryParse(rule.data.Replace(".", ",").ToString(), out idTOO))
                    {
                        switch (rule.op)
                        {
                            case "eq":
                                teams = teams.Where(x => x.TOPG == idTOO);
                                break;
                            case "lt":
                                teams = teams.Where(x => x.TOPG < idTOO);
                                break;
                            case "le":
                                teams = teams.Where(x => x.TOPG <= idTOO);
                                break;
                            case "gt":
                                teams = teams.Where(x => x.TOPG > idTOO);
                                break;
                            case "ge":
                                teams = teams.Where(x => x.TOPG >= idTOO);
                                break;
                            case "cn":
                                teams = teams.Where(x => x.TOPG == idTOO);
                                break;
                        }
                    }
                    return teams;
                case "FOPG":
                    decimal idPF = 0;
                    if (Decimal.TryParse(rule.data.Replace(".", ",").ToString(), out idPF))
                    {
                        switch (rule.op)
                        {
                            case "eq":
                                teams = teams.Where(x => x.FOPG == idPF);
                                break;
                            case "lt":
                                teams = teams.Where(x => x.FOPG < idPF);
                                break;
                            case "le":
                                teams = teams.Where(x => x.FOPG <= idPF);
                                break;
                            case "gt":
                                teams = teams.Where(x => x.FOPG > idPF);
                                break;
                            case "ge":
                                teams = teams.Where(x => x.FOPG >= idPF);
                                break;
                            case "cn":
                                teams = teams.Where(x => x.FOPG == idPF);
                                break;
                        }
                    }
                    return teams;
                case "PNTS":
                    decimal idPPG = 0;
                    if (Decimal.TryParse(rule.data.Replace(".", ",").ToString(), out idPPG))
                    {
                        switch (rule.op)
                        {
                            case "eq":
                                teams = teams.Where(x => x.PNTS == idPPG);
                                break;
                            case "lt":
                                teams = teams.Where(x => x.PNTS < idPPG);
                                break;
                            case "le":
                                teams = teams.Where(x => x.PNTS <= idPPG);
                                break;
                            case "gt":
                                teams = teams.Where(x => x.PNTS > idPPG);
                                break;
                            case "ge":
                                teams = teams.Where(x => x.PNTS >= idPPG);
                                break;
                            case "cn":
                                teams = teams.Where(x => x.PNTS == idPPG);
                                break;
                        }
                    }
                    return teams;



                default:
                    return teams;
            }
        }

        private static IQueryable<TeamStats> OrderTeamsStats(IQueryable<TeamStats> teams, string sortColumn, string sortOrder)
        {
            switch (sortColumn)
            {
                case "Logo":
                    return (sortOrder == "desc") ? teams.OrderByDescending(c => c.Logo) : teams.OrderBy(c => c.Logo);
                case "Name":
                    return (sortOrder == "desc") ? teams.OrderByDescending(c => c.Name) : teams.OrderBy(c => c.Name);
                case "G":
                    return (sortOrder == "desc") ? teams.OrderByDescending(c => c.G) : teams.OrderBy(c => c.G);
                case "FGM":
                    return (sortOrder == "desc") ? teams.OrderByDescending(c => c.FGM) : teams.OrderBy(c => c.FGM);
                case "FGA":
                    return (sortOrder == "desc") ? teams.OrderByDescending(c => c.FGA) : teams.OrderBy(c => c.FGA);
                case "FG":
                    return (sortOrder == "desc") ? teams.OrderByDescending(c => c.FG) : teams.OrderBy(c => c.FG);
                case "FTA":
                    return (sortOrder == "desc") ? teams.OrderByDescending(c => c.FTA) : teams.OrderBy(c => c.FTA);
                case "FTM":
                    return (sortOrder == "desc") ? teams.OrderByDescending(c => c.FTM) : teams.OrderBy(c => c.FTM);
                case "FT":
                    return (sortOrder == "desc") ? teams.OrderByDescending(c => c.FT) : teams.OrderBy(c => c.FT);
                case "F3A":
                    return (sortOrder == "desc") ? teams.OrderByDescending(c => c.F3A) : teams.OrderBy(c => c.F3A);
                case "F3M":
                    return (sortOrder == "desc") ? teams.OrderByDescending(c => c.F3M) : teams.OrderBy(c => c.F3M);
                case "F3":
                    return (sortOrder == "desc") ? teams.OrderByDescending(c => c.F3) : teams.OrderBy(c => c.F3);
                case "RPG":
                    return (sortOrder == "desc") ? teams.OrderByDescending(c => c.RPG) : teams.OrderBy(c => c.RPG);
                case "RDPG":
                    return (sortOrder == "desc") ? teams.OrderByDescending(c => c.RDPG) : teams.OrderBy(c => c.RDPG);
                case "ROPG":
                    return (sortOrder == "desc") ? teams.OrderByDescending(c => c.ROPG) : teams.OrderBy(c => c.ROPG);
                case "ASPG":
                    return (sortOrder == "desc") ? teams.OrderByDescending(c => c.ASPG) : teams.OrderBy(c => c.ASPG);
                case "TOPG":
                    return (sortOrder == "desc") ? teams.OrderByDescending(c => c.TOPG) : teams.OrderBy(c => c.TOPG);
                case "STPG":
                    return (sortOrder == "desc") ? teams.OrderByDescending(c => c.STPG) : teams.OrderBy(c => c.STPG);
                case "BLPG":
                    return (sortOrder == "desc") ? teams.OrderByDescending(c => c.BLPG) : teams.OrderBy(c => c.BLPG);
                case "FOPG":
                    return (sortOrder == "desc") ? teams.OrderByDescending(c => c.FOPG) : teams.OrderBy(c => c.FOPG);
                case "PNTS":
                    return (sortOrder == "desc") ? teams.OrderByDescending(c => c.PNTS) : teams.OrderBy(c => c.PNTS);   
                default:
                    return teams;
            }
        }

        public JsonResult GetTeamStats(int idSeason, GridSettings settings)
        {
            int countTeams = 0;
            IQueryable<TeamStats> listaTeams;


            string query = "TeamStatsProc @Season ";
            SqlParameter SeasonId = new SqlParameter("@Season", idSeason);


            object []SqlArr = new object[1];
            SqlArr[0] = SeasonId;

            listaTeams = db.Database.SqlQuery<TeamStats>(query, SqlArr).ToList().AsQueryable();


            IEnumerable<TeamStats> listTeams = GetTeamsStats(settings, listaTeams, out countTeams);

            int totalRecords = countTeams;
            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)settings.PageSize);
            int page = settings.PageIndex;

            int startRow = (settings.PageIndex - 1) * settings.PageSize;
          

            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from team in listTeams
                    select new
                    {
                        id = team.id,
                        cell = new string[]{
                        team.Logo,
                        team.Name,
                        team.G.ToString().Replace(",","."),
                        team.FGM.ToString().Replace(",","."),
                        team.FGA.ToString().Replace(",","."),
                        team.FG.ToString().Replace(",","."),
                        team.F3M.ToString().Replace(",","."),
                        team.F3A.ToString().Replace(",","."),
                        team.F3.ToString().Replace(",","."),
                        team.FTM.ToString().Replace(",","."),
                        team.FTA.ToString().Replace(",","."),
                        team.FT.ToString().Replace(",","."),
                        team.RPG.ToString().Replace(",","."),
                        team.RDPG.ToString().Replace(",","."),
                        team.ROPG.ToString().Replace(",","."),
                        team.ASPG.ToString().Replace(",","."),
                        team.TOPG.ToString().Replace(",","."),
                        team.STPG.ToString().Replace(",","."),
                        team.BLPG.ToString().Replace(",","."),
                        team.FOPG.ToString().Replace(",","."),
                        team.PNTS.ToString().Replace(",",".")
                    }

                    }
            ).ToArray()
            };
            //     return Json(jsonData);
            return Json(jsonData, JsonRequestBehavior.AllowGet);
            
        }


        public JsonResult GetOpponentStats(int idSeason, GridSettings settings)
        {
            int countTeams = 0;
            IQueryable<TeamStats> listaTeams;


            string query = "OpponentStatsProc @Season ";
            SqlParameter SeasonId = new SqlParameter("@Season", idSeason);


            object[] SqlArr = new object[1];
            SqlArr[0] = SeasonId;

            listaTeams = db.Database.SqlQuery<TeamStats>(query, SqlArr).ToList().AsQueryable();


            IEnumerable<TeamStats> listTeams = GetTeamsStats(settings, listaTeams, out countTeams);

            int totalRecords = countTeams;
            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)settings.PageSize);
            int page = settings.PageIndex;

            int startRow = (settings.PageIndex - 1) * settings.PageSize;


            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from team in listTeams
                    select new
                    {
                        id = team.id,
                        cell = new string[]{
                        team.Logo,
                        team.Name,
                        team.G.ToString().Replace(",","."),
                        team.FGM.ToString().Replace(",","."),
                        team.FGA.ToString().Replace(",","."),
                        team.FG.ToString().Replace(",","."),
                        team.F3M.ToString().Replace(",","."),
                        team.F3A.ToString().Replace(",","."),
                        team.F3.ToString().Replace(",","."),
                        team.FTM.ToString().Replace(",","."),
                        team.FTA.ToString().Replace(",","."),
                        team.FT.ToString().Replace(",","."),
                        team.RPG.ToString().Replace(",","."),
                        team.RDPG.ToString().Replace(",","."),
                        team.ROPG.ToString().Replace(",","."),
                        team.ASPG.ToString().Replace(",","."),
                        team.TOPG.ToString().Replace(",","."),
                        team.STPG.ToString().Replace(",","."),
                        team.BLPG.ToString().Replace(",","."),
                        team.FOPG.ToString().Replace(",","."),
                        team.PNTS.ToString().Replace(",",".")
                    }

                    }
            ).ToArray()
            };
            //     return Json(jsonData);
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }

        #endregion


        #region Report
        public JsonResult GetReportStatsEdit(int idGame,  GridSettings settings)
        {
            int countPlayers = 0;
            IQueryable<PlayersStatsGames> listaPlayers;

            listaPlayers = db.PlayersStatsGames.Where(x => x.idGame == idGame).OrderBy(x => x.Player.CompleteName).AsQueryable();


            IEnumerable<PlayersStatsGames> listPlayers = GetPlayersStatsForReport(settings, listaPlayers, out countPlayers);

            int totalRecords = countPlayers;
            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)settings.PageSize);
            int page = settings.PageIndex;

            int startRow = (settings.PageIndex - 1) * settings.PageSize;


            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = (
                    from players in listPlayers
                    select new
                    {
                        id = players.Player,
                        cell = new string[]{
                            players.LVNBAFranchis.Logo,
                            players.Player.CompleteName
                        }

                    }
            ).ToArray()
            };
            //     return Json(jsonData);
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }

        public IEnumerable<PlayersStatsGames> GetPlayersStatsForReport(GridSettings gridSettings, IQueryable<PlayersStatsGames> players, out int totalPlayers)
        {
            var orderedPlayers = OrderPlayersStatsForReport(players, gridSettings.SortColumn, gridSettings.SortOrder);

            if (gridSettings.IsSearch)
            {
            }
            totalPlayers = orderedPlayers.Count();

            return orderedPlayers.Skip((gridSettings.PageIndex - 1) * gridSettings.PageSize).Take(gridSettings.PageSize).AsEnumerable();
        }

        private static IQueryable<PlayersStatsGames> OrderPlayersStatsForReport(IQueryable<PlayersStatsGames> players, string sortColumn, string sortOrder)
        {
            switch (sortColumn)
            {
                case "Logo":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.idTeam) : players.OrderBy(c => c.idTeam);
                case "CompleteName":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.Player.CompleteName) : players.OrderBy(c => c.Player.CompleteName);

                default:
                    return players;
            }
        }

        public IEnumerable<StatsForReport> GetStatsForReport(GridSettings gridSettings, IQueryable<StatsForReport> players, out int totalPlayers)
        {
            var orderedPlayers = OrderPlayersStatsForReport(players, gridSettings.SortColumn, gridSettings.SortOrder);

            if (gridSettings.IsSearch)
            {
            }
            totalPlayers = orderedPlayers.Count();

            return orderedPlayers.Skip((gridSettings.PageIndex - 1) * gridSettings.PageSize).Take(gridSettings.PageSize).AsEnumerable();
        }



        private static IQueryable<StatsForReport> OrderPlayersStatsForReport(IQueryable<StatsForReport> players, string sortColumn, string sortOrder)
        {
            switch (sortColumn)
            {
                case "Logo":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.LOGO) : players.OrderBy(c => c.LOGO);
                case "Nombre":
                    return (sortOrder == "desc") ? players.OrderByDescending(c => c.Nombre) : players.OrderBy(c => c.Nombre);

                default:
                    return players;
            }
        }


        [HttpPost]
        public JsonResult GetPlayersStats(int idGame,int idTeam, GridSettings settings)
        {
            int countPlayers = 0;

            Games game = db.Games.Where(x => x.Id == idGame).First();

            IQueryable<StatsForReport> listaPlayers;

            listaPlayers = db.StatsForReports.Where(x => x.idGame == idGame && x.idTeam == idTeam && x.MIN > 0).OrderBy(x => x.Nombre).AsQueryable();


            IEnumerable<StatsForReport> listPlayers = GetStatsForReport(settings, listaPlayers, out countPlayers);


            int totalRecords = countPlayers;
            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)settings.PageSize);
            int page = settings.PageIndex;

            int startRow = (settings.PageIndex - 1) * settings.PageSize;

            var robos = listPlayers.Sum(x => x.ROB);

            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                userdata = new
                {
                    Name = "Rbos:",
                    ROB = robos.ToString()
                },
                rows = (
                    from players in listPlayers
                    select new
                    {
                        id = players.idPlayer,
                        cell = new string[]{
                            players.LOGO.ToString(),
                            players.Nombre ,
                            players.TI.ToString() ,
                            players.MIN.ToString(),
                            players.PNTS.ToString(),
                            players.REBS.ToString(),
                            players.AST.ToString(),
                            players.ROB.ToString(),
                            players.TAP.ToString(),
                            players.PER.ToString(),
                            players.TCM.ToString(),
                            players.TCA.ToString(),
                            players.C3TCM.ToString(),
                            players.C3TCA.ToString(),
                            players.TLM.ToString(),
                            players.TLA.ToString(),
                            players.RO.ToString(),
                            players.FP.ToString()
                        }

                    }
            ).ToArray()
            };
            //     return Json(jsonData);
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult GetAdminPlayersStats(int idGame, int idTeam, GridSettings settings)
        {
            int countPlayers = 0;

            Games game = db.Games.Where(x => x.Id == idGame).First();

            IQueryable<StatsForReport> listaPlayers;

            listaPlayers = db.StatsForReports.Where(x => x.idGame == idGame && x.idTeam == idTeam ).OrderBy(x => x.Nombre).AsQueryable();


            IEnumerable<StatsForReport> listPlayers = GetStatsForReport(settings, listaPlayers, out countPlayers);


            int totalRecords = countPlayers;
            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)settings.PageSize);
            int page = settings.PageIndex;

            int startRow = (settings.PageIndex - 1) * settings.PageSize;

            var robos = listPlayers.Sum(x => x.ROB);

            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                userdata = new
                {
                    Name = "Rbos:",
                    ROB = robos.ToString()
                },
                rows = (
                    from players in listPlayers
                    select new
                    {
                        id = players.idPlayer,
                        cell = new string[]{
                            players.LOGO.ToString(),
                            players.Nombre ,
                            players.TI.ToString() ,
                            players.MIN.ToString(),
                            players.PNTS.ToString(),
                            players.REBS.ToString(),
                            players.AST.ToString(),
                            players.ROB.ToString(),
                            players.TAP.ToString(),
                            players.PER.ToString(),
                            players.TCM.ToString(),
                            players.TCA.ToString(),
                            players.C3TCM.ToString(),
                            players.C3TCA.ToString(),
                            players.TLM.ToString(),
                            players.TLA.ToString(),
                            players.RO.ToString(),
                            players.FP.ToString()
                        }

                    }
            ).ToArray()
            };
            //     return Json(jsonData);
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }


        #endregion
    }
}
