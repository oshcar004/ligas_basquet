﻿
using Liga.Domain.Entities;
using Liga.Infrastructure.Data;
using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace Liga.Web.UI.Controllers
{
    [Authorize(Roles = "Administrador")]
    public class FranchisesController : BaseController
    {
        private Liga_DBContext db = new Liga_DBContext();

        //
        // GET: /Franchises/

        public ActionResult Index()
        {
            var lvnbafranchises = db.LVNBAFranchises.Include(l => l.Division).Include(l => l.Manager);
            return View(lvnbafranchises.OrderBy(x=>x.Name).ToList());
        }

        //
        // GET: /Franchises/Edit/5

        public ActionResult Edit(int id = 0)
        {
            LVNBAFranchises lvnbafranchises = db.LVNBAFranchises.Find(id);
            if (lvnbafranchises == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdDivision = new SelectList(db.Divisions, "Id", "Division1", lvnbafranchises.IdDivision);
            ViewBag.IdManager = new SelectList(db.Managers, "Id", "GamerTag", lvnbafranchises.IdManager);
          //  ViewBag.Id = new SelectList(db.TeamStandings, "Id", "Streak", lvnbafranchises.Id);
            return View(lvnbafranchises);
        }

        //
        // POST: /Franchises/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(LVNBAFranchises lvnbafranchises)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(lvnbafranchises).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.IdDivision = new SelectList(db.Divisions, "Id", "Division1", lvnbafranchises.IdDivision);
                ViewBag.IdManager = new SelectList(db.Managers, "Id", "GamerTag", lvnbafranchises.IdManager);
         //       ViewBag.Id = new SelectList(db.TeamStandings, "Id", "Streak", lvnbafranchises.Id);
                return View(lvnbafranchises);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}