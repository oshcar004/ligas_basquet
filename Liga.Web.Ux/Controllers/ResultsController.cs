﻿
using Liga.Domain.Entities;
using Liga.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Liga.Web.UI.Controllers
{
    public class ResultsController : BaseController
    {
        private Liga_DBContext db = new Liga_DBContext();
        private const int GamesPerPage = 10;
        private const int GamesPerFeed = 25;


        public ActionResult Index(int? id, DateTime? date)
        {
            int season = db.Parameters.First().Temporada;
            IEnumerable<Games> games;
            int pageNumber = id ?? 0;
            games = db.Games.Where(x => x.gameDate != null && x.season == season);
            if(date != null)
            {
                games = games.Where(x => x.gameDate.Value.Date == date.Value.Date);
            }
            games = games.OrderByDescending(x => x.gameDate).AsEnumerable().Skip(pageNumber * GamesPerPage).Take(GamesPerPage + 1);
            ViewBag.IsPreviousLinkVisible = pageNumber > 0;
            ViewBag.IsNextLinkVisible = games.Count() > GamesPerPage;
            ViewBag.PageNumber = pageNumber;
            ViewBag.Date = date;
            return View(games.Take(GamesPerPage));
        }

        public ActionResult GamesPerMonth(int? id, int year, int month)
        {
            int pageNumber = id ?? 0;
            int season = db.Parameters.First().Temporada;
            DateTime dateIni = new DateTime(year, month, 1);
            DateTime dateFin = new DateTime(year, month, DateTime.DaysInMonth(year, month));
            IEnumerable<Games> games = db.Games.Where(x => x.gameDate != null && x.gameDate >= dateIni && x.gameDate <= dateFin).OrderByDescending(x => x.gameDate).AsEnumerable().Skip(pageNumber * GamesPerPage).Take(GamesPerPage + 1);
            ViewBag.IsPreviousLinkVisible = pageNumber > 0;
            ViewBag.IsNextLinkVisible = games.Count() > GamesPerPage;
            ViewBag.PageNumber = pageNumber;
            ViewBag.Year = year;
            ViewBag.Month = month;
            return View(games.Take(GamesPerPage));

        }
        public ActionResult GamesPerDay(int? id, int year, int month, int day)
        {
            int pageNumber = id ?? 0;
            int season = db.Parameters.First().Temporada;
            DateTime date = new DateTime(year, month, day);
            DateTime finDate = date.AddHours(23).AddMinutes(59);
            IEnumerable<Games> games = db.Games.Where(x => x.gameDate != null && x.gameDate >= date && x.gameDate <= finDate).OrderByDescending(x => x.gameDate).AsEnumerable().Skip(pageNumber * GamesPerPage).Take(GamesPerPage + 1);
            ViewBag.IsPreviousLinkVisible = pageNumber > 0;
            ViewBag.IsNextLinkVisible = games.Count() > GamesPerPage;
            ViewBag.PageNumber = pageNumber;
            ViewBag.Year = year;
            ViewBag.Month = month;
            ViewBag.Day = day;
            return View(games.Take(GamesPerPage));

        }


        [HttpPost]
        public ActionResult GetResultsByDate(DateTime idDate)
        {
            DateTime finDate = idDate.AddHours(23).AddMinutes(59);
            int pageNumber = 0;
            int season = db.Parameters.First().Temporada;
            IEnumerable<Games> games = db.Games.Where(x => x.gameDate != null && x.gameDate >= idDate && x.gameDate <= finDate).OrderByDescending(x => x.gameDate).AsEnumerable().Skip(pageNumber * GamesPerPage).Take(GamesPerPage + 1);
            ViewBag.IsPreviousLinkVisible = pageNumber > 0;
            ViewBag.IsNextLinkVisible = games.Count() > GamesPerPage;
            ViewBag.PageNumber = pageNumber;
            ViewBag.Year = idDate.Year;
            ViewBag.Month = idDate.Month;
            ViewBag.Day = idDate.Day;

            return View("GamesPerDay", games.Take(GamesPerPage));
        }

        [HttpPost]
        public ActionResult GetGamesPerMonth()
        {

            List<GamesPerMonth> gameMY = db.GamesPerMonths.OrderByDescending(x => x.season).ThenByDescending(x => x.gameMonth).ToList();
            return Json(gameMY);
            // return Json(db.Tags.Distinct().ToList());
        }
        public ActionResult gameMonthYear(int month, int year)
        {

            int pageNumber = 0;
            int season = db.Parameters.First().Temporada;
            DateTime dateIni = new DateTime(year, month, 1);
            DateTime dateFin = new DateTime(year, month, DateTime.DaysInMonth(year, month));

            // IEnumerable<Games> games = db.Games.Where(x => x.gameDate != null  && x.season == season && x.gameDate >= dateIni && x.gameDate <= dateFin).OrderByDescending(x => x.gameDate).AsEnumerable().Skip(pageNumber * GamesPerPage).Take(GamesPerPage + 1);
            IEnumerable<Games> games = db.Games.Where(x => x.gameDate != null  && x.gameDate >= dateIni && x.gameDate <= dateFin).OrderByDescending(x => x.gameDate).AsEnumerable().Skip(pageNumber * GamesPerPage).Take(GamesPerPage + 1);

            ViewBag.IsPreviousLinkVisible = pageNumber > 0;
            ViewBag.IsNextLinkVisible = games.Count() > GamesPerPage;
            ViewBag.PageNumber = pageNumber;
            ViewBag.Year = year;
            ViewBag.Month = month;
            return View("GamesPerMonth", games.Take(GamesPerPage));

        }


        //SUPER OBJETO *********************************************************************************
        public ActionResult IndexAlternative(int? id)
        {
            List<SuperBoxScore> listaScores = new List<SuperBoxScore>();
            int season = db.Parameters.First().Temporada;
            IEnumerable<Games> games;
            int pageNumber = id ?? 0;
            games = db.Games.Where(x => x.gameDate != null && x.season == season).OrderByDescending(x => x.gameDate).AsEnumerable().Skip(pageNumber * GamesPerPage).Take(GamesPerPage + 1);
            foreach (Games game in games)
            {
                SuperBoxScore superBoxScore = new SuperBoxScore();
                superBoxScore.Partido = game;
                IEnumerable<GameStatsResume> resumenLocal = db.GameStatsResumes.Where(x => x.idGame == game.Id && x.idTeam == game.IdTeamLocal).AsEnumerable().Take(3);
                IEnumerable<GameStatsResume> resumenVisitante = db.GameStatsResumes.Where(x => x.idGame == game.Id && x.idTeam == game.IdTeamAway).AsEnumerable().Take(3);

                superBoxScore.listStatsTeamAway = resumenVisitante;
                superBoxScore.listStatsTeamLocal = resumenLocal;

                listaScores.Add(superBoxScore);
            }


            ViewBag.IsPreviousLinkVisible = pageNumber > 0;
            ViewBag.IsNextLinkVisible = games.Count() > GamesPerPage;
            ViewBag.PageNumber = pageNumber;
            return View(listaScores.Take(GamesPerPage));
        }

        public ActionResult GamesPerMonthAlternative(int? id, int year, int month)
        {
            List<SuperBoxScore> listaScores = new List<SuperBoxScore>();
            int pageNumber = id ?? 0;
            int season = db.Parameters.First().Temporada;
            DateTime dateIni = new DateTime(year, month, 1);
            DateTime dateFin = new DateTime(year, month, DateTime.DaysInMonth(year, month));
            IEnumerable<Games> games = db.Games.Where(x => x.gameDate != null && x.gameDate >= dateIni && x.gameDate <= dateFin).OrderByDescending(x => x.gameDate).AsEnumerable().Skip(pageNumber * GamesPerPage).Take(GamesPerPage + 1);
            foreach (Games game in games)
            {
                SuperBoxScore superBoxScore = new SuperBoxScore();
                superBoxScore.Partido = game;
                IEnumerable<GameStatsResume> resumenLocal = db.GameStatsResumes.Where(x => x.idGame == game.Id && x.idTeam == game.IdTeamLocal).AsEnumerable().Take(3);
                IEnumerable<GameStatsResume> resumenVisitante = db.GameStatsResumes.Where(x => x.idGame == game.Id && x.idTeam == game.IdTeamAway).AsEnumerable().Take(3);

                superBoxScore.listStatsTeamAway = resumenVisitante;
                superBoxScore.listStatsTeamLocal = resumenLocal;

                listaScores.Add(superBoxScore);
            }

            ViewBag.IsPreviousLinkVisible = pageNumber > 0;
            ViewBag.IsNextLinkVisible = games.Count() > GamesPerPage;
            ViewBag.PageNumber = pageNumber;
            ViewBag.Year = year;
            ViewBag.Month = month;
            return View(listaScores.Take(GamesPerPage));

        }
        public ActionResult GamesPerDayAlternative(int? id, int year, int month, int day)
        {
            List<SuperBoxScore> listaScores = new List<SuperBoxScore>();
            int pageNumber = id ?? 0;
            int season = db.Parameters.First().Temporada;
            DateTime date = new DateTime(year, month, day);
            DateTime finDate = date.AddHours(23).AddMinutes(59);
            IEnumerable<Games> games = db.Games.Where(x => x.gameDate != null && x.gameDate >= date && x.gameDate <= finDate).OrderByDescending(x => x.gameDate).AsEnumerable().Skip(pageNumber * GamesPerPage).Take(GamesPerPage + 1);
            foreach (Games game in games)
            {
                SuperBoxScore superBoxScore = new SuperBoxScore();
                superBoxScore.Partido = game;
                IEnumerable<GameStatsResume> resumenLocal = db.GameStatsResumes.Where(x => x.idGame == game.Id && x.idTeam == game.IdTeamLocal).AsEnumerable().Take(3);
                IEnumerable<GameStatsResume> resumenVisitante = db.GameStatsResumes.Where(x => x.idGame == game.Id && x.idTeam == game.IdTeamAway).AsEnumerable().Take(3);

                superBoxScore.listStatsTeamAway = resumenVisitante;
                superBoxScore.listStatsTeamLocal = resumenLocal;

                listaScores.Add(superBoxScore);
            }

            ViewBag.IsPreviousLinkVisible = pageNumber > 0;
            ViewBag.IsNextLinkVisible = games.Count() > GamesPerPage;
            ViewBag.PageNumber = pageNumber;
            ViewBag.Year = year;
            ViewBag.Month = month;
            ViewBag.Day = day;
            return View(listaScores.Take(GamesPerPage));

        }
        [HttpPost]
        public ActionResult GetResultsByDateAlternative(DateTime idDate)
        {
            List<SuperBoxScore> listaScores = new List<SuperBoxScore>();
            DateTime finDate = idDate.AddHours(23).AddMinutes(59);
            int pageNumber = 0;
            int season = db.Parameters.First().Temporada;
            IEnumerable<Games> games = db.Games.Where(x => x.gameDate != null && x.gameDate >= idDate && x.gameDate <= finDate).OrderByDescending(x => x.gameDate).AsEnumerable().Skip(pageNumber * GamesPerPage).Take(GamesPerPage + 1);
            foreach (Games game in games)
            {
                SuperBoxScore superBoxScore = new SuperBoxScore();
                superBoxScore.Partido = game;
                IEnumerable<GameStatsResume> resumenLocal = db.GameStatsResumes.Where(x => x.idGame == game.Id && x.idTeam == game.IdTeamLocal).AsEnumerable().Take(3);
                IEnumerable<GameStatsResume> resumenVisitante = db.GameStatsResumes.Where(x => x.idGame == game.Id && x.idTeam == game.IdTeamAway).AsEnumerable().Take(3);

                superBoxScore.listStatsTeamAway = resumenVisitante;
                superBoxScore.listStatsTeamLocal = resumenLocal;

                listaScores.Add(superBoxScore);
            }
            ViewBag.IsPreviousLinkVisible = pageNumber > 0;
            ViewBag.IsNextLinkVisible = games.Count() > GamesPerPage;
            ViewBag.PageNumber = pageNumber;
            ViewBag.Year = idDate.Year;
            ViewBag.Month = idDate.Month;
            ViewBag.Day = idDate.Day;

            return View("GamesPerDayAlternative", listaScores.Take(GamesPerPage));
        }

        public ActionResult gameMonthYearAlternative(int month, int year)
        {
            List<SuperBoxScore> listaScores = new List<SuperBoxScore>();
            int pageNumber = 0;
            int season = db.Parameters.First().Temporada;
            DateTime dateIni = new DateTime(year, month, 1);
            DateTime dateFin = new DateTime(year, month, DateTime.DaysInMonth(year, month));

            // IEnumerable<Games> games = db.Games.Where(x => x.gameDate != null  && x.season == season && x.gameDate >= dateIni && x.gameDate <= dateFin).OrderByDescending(x => x.gameDate).AsEnumerable().Skip(pageNumber * GamesPerPage).Take(GamesPerPage + 1);
            IEnumerable<Games> games = db.Games.Where(x => x.gameDate != null && x.gameDate >= dateIni && x.gameDate <= dateFin).OrderByDescending(x => x.gameDate).AsEnumerable().Skip(pageNumber * GamesPerPage).Take(GamesPerPage + 1);
            foreach (Games game in games)
            {
                SuperBoxScore superBoxScore = new SuperBoxScore();
                superBoxScore.Partido = game;
                IEnumerable<GameStatsResume> resumenLocal = db.GameStatsResumes.Where(x => x.idGame == game.Id && x.idTeam == game.IdTeamLocal).AsEnumerable().Take(3);
                IEnumerable<GameStatsResume> resumenVisitante = db.GameStatsResumes.Where(x => x.idGame == game.Id && x.idTeam == game.IdTeamAway).AsEnumerable().Take(3);

                superBoxScore.listStatsTeamAway = resumenVisitante;
                superBoxScore.listStatsTeamLocal = resumenLocal;

                listaScores.Add(superBoxScore);
            }
            ViewBag.IsPreviousLinkVisible = pageNumber > 0;
            ViewBag.IsNextLinkVisible = games.Count() > GamesPerPage;
            ViewBag.PageNumber = pageNumber;
            ViewBag.Year = year;
            ViewBag.Month = month;
            return View("GamesPerMonthAlternative", listaScores.Take(GamesPerPage));

        }

        //SUPER OBJETO *********************************************************************************



        public PartialViewResult FlashResults(int id, int idTeamLocal, int idTeamAway)
        {
            IEnumerable<GameStatsResume> resumenLocal = db.GameStatsResumes.Where(x => x.idGame == id && x.idTeam == idTeamLocal).AsEnumerable().Take(3);
            IEnumerable<GameStatsResume> resumenVisitante = db.GameStatsResumes.Where(x => x.idGame == id && x.idTeam == idTeamAway).AsEnumerable().Take(3);

            if (resumenLocal.Count() > 0 && resumenVisitante.Count() > 0)
            {
                ResumeStatsModel modelo = new ResumeStatsModel();
                modelo.listStatsTeamLocal = resumenLocal;
                modelo.listStatsTeamAway = resumenVisitante;

                return PartialView("FlashStats", modelo);
            }
            else
            {
                return PartialView("Pendiente");
            }
        }

        public ActionResult GetScoreInfo(int idGame)
        {
            BoxScore boxScore = new BoxScore();
            boxScore.ListaStatsPlayers = db.BoxScoresPlayers.Where(x => x.id == idGame).AsEnumerable();
            boxScore.ListaStatsTeams = db.BoxScoresTeams.Where(x => x.id == idGame).AsEnumerable();
            if (boxScore.ListaStatsPlayers.First().ActualTeam  != 0)
            {
                return View("BoxScore", boxScore);
            }
            else
            {
                return View("SinStats");
            }
        }

        [HttpPost]
        public ActionResult GetLast10Results()
        {
            int season = db.Parameters.First().Temporada;
            List<ShortResult> listResults = new List<ShortResult>();
            List<Games> gameResults = db.Games.Where(x => x.gameDate != null  &&  x.season == season).OrderByDescending(x => x.gameDate).Take(10).ToList();
            foreach (Games g in gameResults)
            {
                ShortResult sR = new ShortResult();
                sR.idGame = g.Id;
                sR.idTeamLocal = g.IdTeamLocal;
                sR.idTeamAway = g.IdTeamAway;
                sR.iScoreAway = g.idScoreAway;
                sR.iScoreLocal = g.idScoreLocal;
                sR.sAbrevLocal = g.LVNBAFranchisLocal.Abbreviation;
                sR.sAbrevAway = g.LVNBAFranchisAway.Abbreviation;
                sR.sLogoLocal = g.LVNBAFranchisLocal.Icono;
                sR.sLogoAway = g.LVNBAFranchisAway.Icono;
                listResults.Add(sR);
            }
            //return Json(gameResults);
            // return Json(db.Tags.Distinct().ToList());
            return Json(listResults.ToList());
        
        }

        [HttpPost]
        public ActionResult GetLast20ResultsByTeam(int idTeam)
        {
            List<ShortResult> listResults = new List<ShortResult>();
            int season = db.Parameters.First().Temporada;
            List<Games> gameResults = db.Games.Where(x => x.gameDate != null  &&  x.season == season && ( x.IdTeamAway == idTeam || x.IdTeamLocal == idTeam) ).OrderByDescending(x => x.gameDate).Take(20).ToList();
            foreach (Games g in gameResults)
            {
                ShortResult sR = new ShortResult();
                sR.idGame = g.Id;
                sR.idTeamLocal = g.IdTeamLocal;
                sR.idTeamAway = g.IdTeamAway;
                sR.iScoreAway = g.idScoreAway;
                sR.iScoreLocal = g.idScoreLocal;
                sR.sAbrevLocal = g.LVNBAFranchisLocal.Abbreviation;
                sR.sAbrevAway = g.LVNBAFranchisAway.Abbreviation;
                sR.sLogoLocal = g.LVNBAFranchisLocal.Icono;
                sR.sLogoAway = g.LVNBAFranchisAway.Icono;
                if (g.IdTeamAway == idTeam)
                {
                    if (g.idScoreAway > g.idScoreLocal)
                        sR.sVictoriaDerrota = "V";
                    else
                        sR.sVictoriaDerrota = "D";
                }
                else
                {
                    if (g.idScoreLocal > g.idScoreAway)
                        sR.sVictoriaDerrota = "V";
                    else
                        sR.sVictoriaDerrota = "D";
                }

                listResults.Add(sR);
            }
            //return Json(gameResults);
            // return Json(db.Tags.Distinct().ToList());
            return Json(listResults.ToList());
        }

        [HttpPost]
        public ActionResult GetResultsByTeam(int idTeam)
        {
            List<ShortResult> listResults = new List<ShortResult>();
            int season = db.Parameters.First().Temporada;
            List<Games> gameResults = db.Games.Where(x => x.gameDate != null && x.season == season && (x.IdTeamAway == idTeam || x.IdTeamLocal == idTeam)).OrderByDescending(x => x.gameDate).ToList();
            foreach (Games g in gameResults)
            {
                ShortResult sR = new ShortResult();
                sR.idGame = g.Id;
                sR.idTeamLocal = g.IdTeamLocal;
                sR.idTeamAway = g.IdTeamAway;
                sR.iScoreAway = g.idScoreAway;
                sR.iScoreLocal = g.idScoreLocal;
                sR.sAbrevLocal = g.LVNBAFranchisLocal.Abbreviation;
                sR.sAbrevAway = g.LVNBAFranchisAway.Abbreviation;
                sR.sLogoLocal = g.LVNBAFranchisLocal.Icono;
                sR.sLogoAway = g.LVNBAFranchisAway.Icono;
                if (g.IdTeamAway == idTeam)
                {
                    if (g.idScoreAway > g.idScoreLocal)
                        sR.sVictoriaDerrota = "V";
                    else
                        sR.sVictoriaDerrota = "D";
                }
                else
                {
                    if (g.idScoreLocal > g.idScoreAway)
                        sR.sVictoriaDerrota = "V";
                    else
                        sR.sVictoriaDerrota = "D";
                }
                listResults.Add(sR);
            }
            //return Json(gameResults);
            // return Json(db.Tags.Distinct().ToList());
            return Json(listResults.ToList());
        }

        [HttpPost]
        public ActionResult GetPendingByTeam(int idTeam)
        {
            List<ShortResult> listResults = new List<ShortResult>();
            int season = db.Parameters.First().Temporada;
            List<Games> gameResults = db.Games.Where(x => x.gameDate == null && x.season == season && (x.IdTeamAway == idTeam || x.IdTeamLocal == idTeam)).OrderByDescending(x => x.gameDate).ToList();
            foreach (Games g in gameResults)
            {
                ShortResult sR = new ShortResult();
                sR.idGame = g.Id;
                sR.idTeamLocal = g.IdTeamLocal;
                sR.idTeamAway = g.IdTeamAway;
                sR.iScoreAway = g.idScoreAway;
                sR.iScoreLocal = g.idScoreLocal;
                sR.sAbrevLocal = g.LVNBAFranchisLocal.Abbreviation;
                sR.sAbrevAway = g.LVNBAFranchisAway.Abbreviation;
                sR.sLogoLocal = g.LVNBAFranchisLocal.Icono;
                sR.sLogoAway = g.LVNBAFranchisAway.Icono;
                listResults.Add(sR);
            }
            //return Json(gameResults);
            // return Json(db.Tags.Distinct().ToList());
            return Json(listResults.ToList());
        }
    }
    public class ShortResult
    {

        public int idGame { get; set; }
        public int idTeamLocal { get; set; }
        public int idTeamAway { get; set; }
        public int iScoreLocal { get; set; }
        public int iScoreAway { get; set; }
        public string sAbrevLocal { get; set; }
        public string sAbrevAway { get; set; }
        public string sLogoLocal { get; set; }
        public string sLogoAway { get; set; }
        public string sVictoriaDerrota { get; set; }

    }
    public class ResumeStatsModel
    {
        public ResumeStatsModel()
        {

        }

        public IEnumerable<GameStatsResume> listStatsTeamLocal { get; set; }
        public IEnumerable<GameStatsResume> listStatsTeamAway { get; set; }
    }


}
