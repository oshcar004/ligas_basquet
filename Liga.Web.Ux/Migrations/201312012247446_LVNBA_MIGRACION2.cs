namespace Liga.Web.UI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LVNBA_MIGRACION2 : DbMigration
    {
        public override void Up()
        {
            AddForeignKey("dbo.ManagerRequests", "UserId", "dbo.UserProfile", "UserId", cascadeDelete: false);
            CreateIndex("dbo.ManagerRequests", "UserId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.ManagerRequests", new[] { "UserId" });
            DropForeignKey("dbo.ManagerRequests", "UserId", "dbo.UserProfile");
        }
    }
}
