namespace Liga.Web.UI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LVNBA_MIGRACION : DbMigration
    {
        public override void Up()
        {

        }
        
        public override void Down()
        {
            AddColumn("dbo.Posts", "UserProfiles_UserId1", c => c.Int());
            AddColumn("dbo.Posts", "UserProfiles_UserId", c => c.Int(nullable: false));
            DropIndex("dbo.Posts", new[] { "User_UserId" });
            DropForeignKey("dbo.Posts", "User_UserId", "dbo.UserProfile");
            DropColumn("dbo.Posts", "User_UserId");
            DropColumn("dbo.Posts", "IdAuthor");
            CreateIndex("dbo.Posts", "UserProfiles_UserId1");
            AddForeignKey("dbo.Posts", "UserProfiles_UserId1", "dbo.UserProfile", "UserId");
        }
    }
}
