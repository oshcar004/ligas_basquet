﻿
function viewPlayerInfo(idPlayer) {
   // $("#dialog:ui-dialog").dialog("destroy");
    $("#FillTable").html("");
    
   $('#loading-image').show();
    
    $.post('../PlayerInfo/GetPlayerInfo', { idPlayer: idPlayer }, function (data) {

       // $("#dialog:ui-dialog").dialog("destroy");

        $("#FillTable").html(data);
        $("#dialog-modal").dialog({
            title: "Player Info",
            height: 755,
            width: 840,
            modal: true,
            resizable: false
            //buttons: {
            //    Cancel: function () {
            //        $(this).dialog("close");
            //    }
            //},
        });
        $("#tabsPlayerInfo").tabs({
            selected: 0,
            ajaxOptions: {
                error: function (xhr, status, index, anchor) {
                    $(anchor.hash).html(
                        "Couldn't load this tab. We'll try to fix this as soon as possible. " +
                        "If this wouldn't be a demo.");
                }
            }
        });

    }).done(function (data){
        $('#loading-image').hide();
    });


    //$("#dialog-modal").dialog({
    //    title: "Player Info",
    //    height: 755,
    //    width: 840,
    //    modal: true,
    //    resizable: false
    //});
  

}

function getSideBarCategories() {
    var options = '';
    options += '<li><h2>LVPedia</h2><ul>';
    options += '<li><a href="/LVPedia/About"><span class="tab">Qué es LVNBA</span></a></li>';
    options += '<li><a href="/LVPedia/Juego"><span class="tab">Juego</span></a></li>';
    options += '<li><a href="/LVPedia/Quejas"><span class="tab">Quejas</span></a></li>';
    options += '<li><a href="/LVPedia/Salarios"><span class="tab">Salarios</span></a></li>';
    options += '<li><a href="/LVPedia/Traspasos"><span class="tab">Traspasos</span></a></li>';
    options += '<li><a href="/LVPedia/AgenciaLibre"><span class="tab">Agencia Libre</span></a></li>';
    options += '<li><a href="/LVPedia/JugFranquicia"><span class="tab">Jugadores Franquicia</span></a></li>';

    options += '</ul></li>';
    jQuery('#firstListUL').html(options);

}

function getSideBarMedia() {
    var options = '';
    options += '<li><h2>LVMedia</h2><ul>';
    options += '<li><a href="/LVPedia/VideoTutoriales"><span class="tab">Videotutoriales</span></a></li>';
    options += '<li><a href="/LVPedia/Podcasts"><span class="tab">Podcasts LVNBA</span></a></li>';
    options += '<li><a href="/LVPedia/Revista"><span class="tab">Revista LVNBA</span></a></li>';
    options += '</ul></li>';
    jQuery('#secondListUL').html(options);

}