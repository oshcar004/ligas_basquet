﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Liga.Domain.Entities
{
    public class GeneralFranchiseInfo
    {

        public IEnumerable<Players> ListPlayersRoster { get; set; }
        public IEnumerable<Players> ListPlayersRightsoCutted { get; set; }
        public IEnumerable<Players> ListPlayersFinantialInfo { get; set; }
        public ManagerCardMO ManagerCard { get; set; }
        public TeamCardMO TeamCard { get; set; }
        public PlotInfoSalary PlotInfo { get; set; }
        public GoalsTeam TeamGoalsInfo { get; set; }
        public RoundsTeam RoundsTeamInfo { get; set; }
        public DateTime Today { get; set; }
        public LVNBAFranchises Franchise { get; set; }
        public IEnumerable<SalaryConditions> Conditions { get; set; }
        public IQueryable<PlayerStatisticsBySeason> listaPlayers { get; set; }
        public List<ChartData> ChartData { get; set; }
    }
    public class ManagerCardMO
    {
        public Managers Manager { get; set; }
        public IEnumerable<PFMAwards> ManagerAwards { get; set; }
        public IEnumerable<Rounds> ManagerRounds { get; set; }
        public IEnumerable<ManagersFranchises> ManagerTrayectoria { get; set; }

    }
    public class TeamCardMO
    {
        public LVNBAFranchises Franchise { get; set; }
        public IEnumerable<PFMAwards> FranchiseAwards { get; set; }
    }
    public class GoalsTeam
    {
        public FranchiseSportGoal SportGoal { get; set; }
        public FranchiseManagementGoal FinancialGoal { get; set; }
    }

    public class RoundsTeam
    {
        public IEnumerable<Rounds> Rounds { get; set; }
    }
    public class PlotInfoSalary
    {
        public string SalaryTeam1 { get; set; }
        public string SalaryTeam2 { get; set; }
        public string SalaryTeam3 { get; set; }
        public string SalaryTeam4 { get; set; }
        public string SalaryTeam5 { get; set; }
        public string SalaryTeam6 { get; set; }

        public string LuxuryTax { get; set; }
        public string SalaryTax { get; set; }
        public int SalaryTaxForPlot { get; set; }
        public int LuxuryTaxForPlot { get; set; }

    }
}