using System;
using System.Collections.Generic;

namespace Liga.Domain.Entities
{
    public partial class Games
    {
        public Games()
        {
            PlayersStatsGames = new List<PlayersStatsGames>();
            Quejas = new List<ResumenQueja>();
            GamesInjuries = new List<InjuryGames>();
            InjuriesInGame = new List<Injuries>();

        }

        public int Id { get; set; }
        public int IdTeamLocal { get; set; }
        public int IdTeamAway { get; set; }
        public int idScoreLocal { get; set; }
        public int idScoreAway { get; set; }
        public DateTime? date2k { get; set; }
        public DateTime? gameDate { get; set; }
        public int? idleague { get; set; }
        public int? season { get; set; }
        public int? regularseason { get; set; }
        public int? state { get; set; }
        public int? aVal { get; set; }
        public int? lVal { get; set; }
        public bool aQueja { get; set; }
        public bool lQueja { get; set; }
        public string aMotivo { get; set; }
        public string lMotivo { get; set; }
        public int? errores { get; set; }
        public int? lidManager { get; set; }
        public int? aidManager { get; set; }
        public int? lCountGame { get; set; }
        public int? aCountGame { get; set; }
        public int? cMVP { get; set; }
        public int? lMVP { get; set; }
        public int? aMVP { get; set; }
        public Nullable<bool> withoutStatsLocal { get; set; }
        public Nullable<bool> withoutStatsAway { get; set; }



        public virtual LVNBAFranchises LVNBAFranchisLocal { get; set; }
        public virtual LVNBAFranchises LVNBAFranchisAway { get; set; }
        public virtual Players cMVPPlayerInfo { get; set; }
        public virtual Players aMVPPlayerInfo { get; set; }
        public virtual Players lMVPPlayerInfo { get; set; }
        public virtual ICollection<PlayersStatsGames> PlayersStatsGames { get; set; }
        public virtual ICollection<ResumenQueja> Quejas { get; set; }
        public virtual Managers ManagerLocal { get; set; }
        public virtual Managers ManagerAway { get; set; }
        public virtual ICollection<Valoraciones> ValoracionesGames { get; set; }

        public virtual ICollection<InjuryGames> GamesInjuries { get; set; }
        public virtual ICollection<Injuries> InjuriesInGame { get; set; }

    }
}
