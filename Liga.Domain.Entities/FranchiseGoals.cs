﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Liga.Domain.Entities
{
    public class FranchiseGoals
    {
        public FranchiseSportGoal SportGoal { get; set; }
        public FranchiseManagementGoal ManagerGoal { get; set; }
        public IEnumerable<Goals> GoalsList { get; set; }
    }
}