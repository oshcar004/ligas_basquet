using System;
using System.Collections.Generic;

namespace Liga.Domain.Entities
{
    public partial class Awards
    {
        public Awards()
        {
            PFMAwards = new List<PFMAwards>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Class { get; set; }
        public int? Priority { get; set; }
        public int? Bonus { get; set; }
        public string Image { get; set; }
        public string BigImage { get; set; }
        public virtual AwardsClass AwardsClass { get; set; }
        public virtual ICollection<PFMAwards> PFMAwards { get; set; }
    }
}
