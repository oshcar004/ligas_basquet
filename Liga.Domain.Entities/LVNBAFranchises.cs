using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Liga.Domain.Entities
{
    public partial class LVNBAFranchises
    {
        public LVNBAFranchises()
        {
            Games = new List<Games>();
            Games1 = new List<Games>();
            ManagerRequests = new List<ManagerRequests>();
            ManagersFranchises = new List<ManagersFranchises>();
            PFMAwards = new List<PFMAwards>();
            Players = new List<Players>();
            PlayersHistories = new List<PlayersHistories>();
            PlayersSalaries = new List<PlayersSalariesFull>();
            PlayersStatsGames = new List<PlayersStatsGames>();
            Rounds = new List<Rounds>();
            Rounds1 = new List<Rounds>();
            Transfers = new List<Transfers>();
            Transfers1 = new List<Transfers>();
            Transfers2 = new List<Transfers>();
            Transfers3 = new List<Transfers>();
            TransfersPlayersLists = new List<TransfersPlayersLists>();
            TransfersPlayersLists1 = new List<TransfersPlayersLists>();
            TransfersPlayersLists2 = new List<TransfersPlayersLists>();
            TransfersRoundsLists = new List<TransfersRoundsLists>();
            TransfersRoundsLists1 = new List<TransfersRoundsLists>();
            FranchiseManagementGoals = new List<FranchiseManagementGoal>();
            FranchiseSportGoals = new List<FranchiseSportGoal>();
            TeamStats = new List<TeamStats>();
            PlayoffsLocal = new List<Playoffs>();
            PlayoffsAway = new List<Playoffs>();
            FreeAgentOffers = new List<FreeAgentOffers>();
        }

        public int Id { get; set; }
        public int IdDivision { get; set; }
        public string Name { get; set; }
        [Required]
        public string Abbreviation { get; set; }
        public string NickName { get; set; }
        public int? IdManager { get; set; }
        public string Icono { get; set; }
        public string Logo { get; set; }
        public string BigLogo { get; set; }
        public string StadiumBoard { get; set; }
        public string SideBar { get; set; }
        public string Color { get; set; }
        public string History { get; set; }
        public int? Category { get; set; }
        public decimal? TotalBonus { get; set; }
        public string Needs { get; set; }
        public string ManagerOpinion { get; set; }
        public virtual Divisions Division { get; set; }
        public virtual ICollection<Games> Games { get; set; }
        public virtual ICollection<Games> Games1 { get; set; }
        public virtual Managers Manager { get; set; }
        public virtual ICollection<ManagerRequests> ManagerRequests { get; set; }
        public virtual ICollection<ManagersFranchises> ManagersFranchises { get; set; }
        public virtual ICollection<PFMAwards> PFMAwards { get; set; }
        public virtual ICollection<Players> Players { get; set; }
        public virtual ICollection<PlayersHistories> PlayersHistories { get; set; }
        public virtual ICollection<PlayersSalariesFull> PlayersSalaries { get; set; }
        public virtual ICollection<PlayersStatsGames> PlayersStatsGames { get; set; }
        public virtual ICollection<Rounds> Rounds { get; set; }
        public virtual ICollection<Rounds> Rounds1 { get; set; }
        public virtual TeamStandings TeamStanding { get; set; }
        public virtual ICollection<TeamStats> TeamStats { get; set; }
        public virtual ICollection<Transfers> Transfers { get; set; }
        public virtual ICollection<Transfers> Transfers1 { get; set; }
        public virtual ICollection<Transfers> Transfers2 { get; set; }
        public virtual ICollection<Transfers> Transfers3 { get; set; }
        public virtual ICollection<TransfersPlayersLists> TransfersPlayersLists { get; set; }
        public virtual ICollection<TransfersPlayersLists> TransfersPlayersLists1 { get; set; }
        public virtual ICollection<TransfersPlayersLists> TransfersPlayersLists2 { get; set; }
        public virtual ICollection<TransfersRoundsLists> TransfersRoundsLists { get; set; }
        public virtual ICollection<TransfersRoundsLists> TransfersRoundsLists1 { get; set; }
        public virtual ICollection<FranchiseManagementGoal> FranchiseManagementGoals { get; set; }
        public virtual ICollection<FranchiseSportGoal> FranchiseSportGoals { get; set; }
        public virtual Clasificacion Posicion { get; set; }
        public virtual ICollection<Playoffs> PlayoffsLocal { get; set; }
        public virtual ICollection<Playoffs> PlayoffsAway { get; set; }
        public virtual ICollection<FreeAgentOffers> FreeAgentOffers { get; set; }
        public virtual LVNBAFranchisesParams LVNBAFranchisesParam { get; set; }
        public virtual int IdExterno { get; set; }
    }
}
