﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Liga.Domain.Entities
{
    public partial class FranchiseSportGoal
    {
        public int Id { get; set; }
        public int IdGoal { get; set; }
        public int IdFranchise { get; set; }
        public virtual Goals Goal { get; set; }
        public virtual LVNBAFranchises LVNBAFranchis { get; set; }
    }
}