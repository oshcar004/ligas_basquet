using System;
using System.Collections.Generic;

namespace Liga.Domain.Entities
{
    public partial class TransfersPlayersLists
    {
        public int Id { get; set; }
        public int IdPlayer { get; set; }
        public int IdTransfer { get; set; }
        public int Destination { get; set; }
        public int? IdFranchise { get; set; }
        public virtual LVNBAFranchises LVNBAFranchis { get; set; }
        public virtual LVNBAFranchises LVNBAFranchis1 { get; set; }
        public virtual LVNBAFranchises LVNBAFranchis2 { get; set; }
        public virtual Players Player { get; set; }
        public virtual Transfers Transfer { get; set; }
    }
}
