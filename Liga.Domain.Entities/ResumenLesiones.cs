﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Liga.Domain.Entities
{
    public class ResumenLesiones
    {
        public int IdPlayer { get; set; }
        public string NamePlayer { get; set; }
        public string AbrevTeam { get; set; }
        public int PartidosLesionado { get; set; }
        public int Lesiones { get; set; }
    }
}