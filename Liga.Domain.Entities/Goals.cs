using System;
using System.Collections.Generic;

namespace Liga.Domain.Entities
{
    public partial class Goals
    {
        public Goals()
        {
            HistoryGoals = new List<HistoryGoals>();
            FranchiseManagementGoals = new List<FranchiseManagementGoal>();
            FranchiseSportGoals = new List<FranchiseSportGoal>();

        }

        public int Id { get; set; }
        public string Goal1 { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public int? Bonus { get; set; }
        public virtual ICollection<HistoryGoals> HistoryGoals { get; set; }
        public virtual ICollection<FranchiseManagementGoal> FranchiseManagementGoals { get; set; }
        public virtual ICollection<FranchiseSportGoal> FranchiseSportGoals { get; set; }

    }
}
