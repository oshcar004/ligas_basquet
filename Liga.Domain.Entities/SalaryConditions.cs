using System;
using System.Collections.Generic;

namespace Liga.Domain.Entities
{
    public partial class SalaryConditions
    {
        public SalaryConditions()
        {
            PlayersSalariesFull = new List<PlayersSalariesFull>();
        }

        public int Id { get; set; }
        public string Condition { get; set; }
        public string Abrev { get; set; }
        public string Colour { get; set; }
        public virtual ICollection<PlayersSalariesFull> PlayersSalariesFull { get; set; }
    }
}
