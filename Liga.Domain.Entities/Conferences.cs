using System;
using System.Collections.Generic;

namespace Liga.Domain.Entities
{
    public partial class Conferences
    {
        public Conferences()
        {
            Divisions = new List<Divisions>();
        }

        public int Id { get; set; }
        public string Conferencia { get; set; }
        public virtual ICollection<Divisions> Divisions { get; set; }
    }
}
