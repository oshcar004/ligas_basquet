﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Liga.Domain.Entities
{
    public partial class PlayerStatisticsBySeason
    {

        public int Player { get; set; }
        public string FaceImage { get; set; }
        public string CompleteName { get; set; }
        public string Season { get; set; }
        public int G { get; set; }
        public int GS { get; set; }
        public int IdTeam { get; set; }
        public int IdRol { get; set; }
        public int IdPosition { get; set; }
        public int IdState { get; set; }
        public string Team { get; set; }
        public string Logo { get; set; }
        public int Age { get; set; }
        public string Position { get; set; }
        public int YearsPro { get; set; }
        public decimal Salary { get; set; }
        public decimal Salary2 { get; set; }
        public string State { get; set; }
        public string Rol { get; set; }
        public decimal MPG { get; set; }
        public decimal FG { get; set; }
        public decimal F3G { get; set; }
        public decimal FT { get; set; }
        public decimal OFR { get; set; }
        public decimal DEF { get; set; }
        public decimal RPG { get; set; }
        public decimal APG { get; set; }
        public decimal SPG { get; set; }
        public decimal BPG { get; set; }
        public decimal TOO { get; set; }
        public decimal PF { get; set; }
        public decimal PPG { get; set; }
        public string RolLong { get; set; }
        public string StateLong { get; set; }
        public int BirdRights { get; set; }
        public string Condition { get; set; }
        public int idCondition { get; set; }
        public string ConditionLong { get; set; }
        public int Level2k { get; set; }
    }

}