﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Liga.Domain.Entities
{
    [Table("UserProfile")]
    public class UserProfile
    {
        public UserProfile()
        {
            Managers = new HashSet<Managers>();
            Comments = new HashSet<Comments>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public virtual ICollection<Managers> Managers { get; set; }
        public virtual ICollection<Comments> Comments { get; set; }
        public virtual ICollection<Webpages_UsersInRoles> Webpages_UsersInRoles { get; set; }
    }
}
