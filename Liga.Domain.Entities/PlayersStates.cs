using System;
using System.Collections.Generic;

namespace Liga.Domain.Entities
{
    public partial class PlayersStates
    {
        public PlayersStates()
        {
            Players = new List<Players>();
        }

        public int Id { get; set; }
        public string State { get; set; }
        public string Abrev { get; set; }
        public virtual ICollection<Players> Players { get; set; }
    }
}
