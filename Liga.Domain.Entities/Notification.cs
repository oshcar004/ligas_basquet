﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Liga.Domain.Entities
{
    public partial class Notification
    {
        public int id { get; set; }
        public int idtype { get; set; }
        public string title { get; set; }
        public string notification1 { get; set; }
        public int idReceiver { get; set; }
        public int idSender { get; set; }
        public DateTime sendDate { get; set; }
        public int season { get; set; }
        public virtual NotificationType NotificationType { get; set; }
    }
}