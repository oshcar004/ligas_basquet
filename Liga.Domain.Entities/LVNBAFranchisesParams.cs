﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Liga.Domain.Entities
{
    public class LVNBAFranchisesParams
    {
        public int ID { get; set; }
        public decimal? MidLevelWasted { get; set; }
        public int? OffSeasonTradedIn { get; set; }
        public int? OfSeasonTradedOut { get; set; }
        public int? SeasonTradedIn { get; set; }
        public int? SeasonTradedOut { get; set; }
        public int? SeasonTrades { get; set; }
        public int? OffSeasonTrades { get; set; }
        public virtual LVNBAFranchises LVNBAFranchis { get; set; }
    }
}