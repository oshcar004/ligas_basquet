using System;
using System.Collections.Generic;

namespace Liga.Domain.Entities
{
    public partial class Divisions
    {
        public Divisions()
        {
            LVNBAFranchises = new List<LVNBAFranchises>();
        }

        public int Id { get; set; }
        public string Division1 { get; set; }
        public int? IdConference { get; set; }
        public virtual Conferences Conference { get; set; }
        public virtual ICollection<LVNBAFranchises> LVNBAFranchises { get; set; }
    }
}
