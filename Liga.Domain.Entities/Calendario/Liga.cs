﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Liga.Domain.Entities.Calendario
{
    public class Liga
    {
        public IEnumerable<Partido> Standard { get; set; }
        public IEnumerable<Partido> Africa { get; set; }
        public IEnumerable<Partido> Sacramento { get; set; }
        public IEnumerable<Partido> Vegas { get; set; }
        public IEnumerable<Partido> Utah { get; set; }
    }
}
