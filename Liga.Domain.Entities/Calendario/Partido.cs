﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Liga.Domain.Entities.Calendario
{
    public class Partido
    {
        public int SeasonStageId { get; set; }
        public DateTime StartTimeUTC { get; set; }
        public Equipo HTeam { get; set; }
        public Equipo VTeam { get; set; }
    }
}
