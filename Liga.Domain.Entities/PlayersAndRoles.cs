﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Liga.Domain.Entities
{
    public class PlayersAndRoles
    {
        public IEnumerable<Players> ListPlayers { get; set; }
        public IEnumerable<PlayersRoles> PlayersRoles { get; set; }

    }
}