using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Liga.Domain.Entities
{
    public partial class PlayersSalariesFull
    {
        public int Id { get; set; }
        public int IdPlayer { get; set; }
        public decimal Salarie { get; set; }
        public int IdCondition { get; set; }
        public int IdLVNBAFranchises { get; set; }
        public DateTime DateSalarie { get; set; }
        public virtual LVNBAFranchises LVNBAFranchis { get; set; }
        [ForeignKey("PlayerId")]
        public virtual Players Player { get; set; }
        public virtual SalaryConditions SalaryCondition { get; set; }

    }
}
