using System;
using System.Collections.Generic;

namespace Liga.Domain.Entities
{
    public partial class Drafts
    {
        public int Id { get; set; }
        public int IdPlayer { get; set; }
        public int? Year { get; set; }
        public int? IdRound { get; set; }
        public DateTime? DraftDate { get; set; }
        public int? Position { get; set; }
        public virtual Players Player { get; set; }
        public virtual Rounds Round { get; set; }
    }
}
