﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Liga.Domain.Entities
{
    public partial class Clasificacion
    {
        public string Name { get; set; }
        public int id { get; set; }
        public int? G { get; set; }
        public int? W { get; set; }
        public int? L { get; set; }
        public decimal? Pct { get; set; }
        public decimal? PPG { get; set; }
        public decimal? APG { get; set; }
        public string Casa { get; set; }
        public string Fuera { get; set; }
        public string Division { get; set; }
        public string Conf { get; set; }
        public string L10 { get; set; }
        public string Racha { get; set; }

        public virtual LVNBAFranchises TeamInfo { get; set; }

    }
}