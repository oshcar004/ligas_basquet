﻿namespace Liga.Domain.Entities
{
    public class ManagersWithUsername
    {
        public int Id { get; set; }
        public string Gamertag { get; set; }
        public int? UserId { get; set; }
        public string Username { get; set; }
    }
}
