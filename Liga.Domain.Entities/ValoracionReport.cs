﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Liga.Domain.Entities
{
    public class ValoracionReport
    {

            public IEnumerable<ValMediaLocalVisitanteOtorgada> ListaValoracionesOtorgadas { get; set; }
            public IEnumerable<ValMediaLocalVisitanteRecibida> ListaValoracionesRecibidas { get; set; }
            public IEnumerable<ValMediaRecibida> MediasRecibidas { get; set; }
            public IEnumerable<ValMediaOtorgada> MediasOtorgadas { get; set; }
            public IEnumerable<Valoraciones> Valoraciones { get; set; }
    }
    public partial class ValMediaLocalVisitanteOtorgada
    {
        public decimal? Media { get; set; }
        public int? Partidos { get; set; }
        public Int64 id { get; set; }
        public int idManager { get; set; }

        public string LocalOVisitante { get; set; }
        public virtual Managers  ManagerInfo { get; set; }
    }
    public partial class ValMediaLocalVisitanteRecibida
    {
        public decimal? Media { get; set; }
        public int? Partidos { get; set; }
        public Int64 id { get; set; }
        public int idManager { get; set; }
        public string LocalOVisitante { get; set; }
        public virtual Managers  ManagerInfo { get; set; }
    }

    public partial class ValMediaRecibida
    {
        public decimal? Media { get; set; }
        public Int64 id { get; set; }
        public int? idManager { get; set; }
        public virtual Managers ManagerInfo { get; set; }

    }
    public partial class ValMediaOtorgada
    {
        public decimal? Media { get; set; }
        public Int64 id { get; set; }
        public int? idManager { get; set; }
        public virtual Managers ManagerInfo { get; set; }

    }

    public partial class Valoraciones
    {
        public Decimal nota { get; set; }
        public Int64 id { get; set; }
        public int? idManagerRecibe { get; set; }
        public int? idManagerOtorga { get; set; }
        public int? season { get; set; }
        public DateTime? gameDate { get; set; }
        public int? idGame { get; set; }

        public virtual Games GameInfo { get; set; }
        public virtual Managers ManagerInfoRecibido { get; set; }
        public virtual Managers ManagerInfoOtorgado { get; set; }

    }

   
}