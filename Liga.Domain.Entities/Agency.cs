﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Liga.Domain.Entities
{
    public class Agency
    {
        public int Id { get; set; }
        public int IdPlayer { get; set; }
        public string Tipo { get; set; }
        public bool Quit { get; set; }
        public bool Ejercida { get; set; }
    }
}
