﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Liga.Domain.Entities
{
    public partial class RankPPG
    {
        public long? POS { get; set; }
        public int Player { get; set; }
        public string Season { get; set; }
        public decimal? PPG { get; set; }
        public string CompleteName { get; set; }
        public string FaceImage { get; set; }
        public string Logo { get; set; }
    }
}