using System;
using System.Collections.Generic;

namespace Liga.Domain.Entities
{
    public partial class TransfersStates
    {
        public TransfersStates()
        {
            Transfers = new List<Transfers>();
        }

        public int Id { get; set; }
        public string TransferState { get; set; }
        public virtual ICollection<Transfers> Transfers { get; set; }
    }
}
