using System;
using System.Collections.Generic;

namespace Liga.Domain.Entities
{
    public partial class PlayersPositions
    {
        public PlayersPositions()
        {
            Players = new List<Players>();
        }

        public int Id { get; set; }
        public string Position { get; set; }
        public string Description { get; set; }
        public virtual ICollection<Players> Players { get; set; }
    }
}
