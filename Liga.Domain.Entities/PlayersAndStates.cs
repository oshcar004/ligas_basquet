﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Liga.Domain.Entities
{
    public class PlayersAndStates
    {
        public IEnumerable<Players> ListPlayers { get; set; }
        public IEnumerable<PlayersStates> PlayersStates { get; set; }
        public LVNBAFranchises Team { get; set; }
    }
}