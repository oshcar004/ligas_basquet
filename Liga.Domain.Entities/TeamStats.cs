using System;
using System.Collections.Generic;

namespace Liga.Domain.Entities
{
    public partial class TeamStats
    {
        
        public int id { get; set; }
        public string Logo { get; set; }
        public string Name { get; set; }
        public int? G { get; set; }
        public decimal? FGM { get; set; }
        public decimal? FGA { get; set; }
        public decimal? FG { get; set; }
        public decimal? FTM { get; set; }
        public decimal? FTA { get; set; }
        public decimal? FT { get; set; }
        public decimal? F3M { get; set; }
        public decimal? F3A { get; set; }
        public decimal? F3 { get; set; }
        public decimal? RPG { get; set; }
        public decimal? RDPG { get; set; }
        public decimal? ROPG { get; set; }
        public decimal? ASPG { get; set; }
        public decimal? TOPG { get; set; }
        public decimal? STPG { get; set; }
        public decimal? BLPG { get; set; }
        public decimal? FOPG { get; set; }
        public decimal? PNTS { get; set; }

        public virtual LVNBAFranchises LVNBAFranchis { get; set; }
    }
}
