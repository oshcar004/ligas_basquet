﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Liga.Domain.Entities
{
    public partial class PlayerUsage
    {
        public int Id { get; set; }
        public int? ActualTeam { get; set; }
        public int? season { get; set; }
        public decimal? PNTS { get; set; }
        public decimal? FGA { get; set; }
        public decimal? FTA { get; set; }
        public decimal? F3A { get; set; }
        public decimal? REBS { get; set; }
        public decimal? ASS { get; set; }
        public decimal? STLS { get; set; }
        public decimal? TRNS { get; set; }
        public decimal? BLKS { get; set; }
    }
}