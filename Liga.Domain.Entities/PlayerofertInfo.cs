﻿using System.Collections.Generic;

namespace Liga.Domain.Entities
{
    public class PlayerOfertInfo
    {
        public Players Player { get; set; }
        public string Salary { get; set; }
        public string LuxuryTax { get; set; }
        public string LimSalary { get; set; }
    }

}
