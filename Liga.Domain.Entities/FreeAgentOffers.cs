﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Liga.Domain.Entities
{
    public partial class FreeAgentOffers
    {
        public int Id { get; set; }
        public int IdPlayer { get; set; }
        public decimal? Salary1 { get; set; }
        public decimal? Salary2 { get; set; }
        public decimal? Salary3 { get; set; }
        public decimal? Salary4 { get; set; }
        public decimal? Salary5 { get; set; }
        public bool? Accepted { get; set; }
        public bool? Rejected { get; set; }
        public int IdFranchise { get; set; }
        public DateTime OfferDate { get; set; }
        public virtual LVNBAFranchises LVNBAFranchis { get; set; }
        public virtual Players Player { get; set; }
        public int? IdCondition1 { get; set; }
        public int? IdCondition2 { get; set; }
        public int? IdCondition3 { get; set; }
        public int? IdCondition4 { get; set; }
        public int? IdCondition5 { get; set; }
    }
}