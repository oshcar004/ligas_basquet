using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Liga.Domain.Entities
{
    public partial class Players
    {
        public Players()
        {
            
            Drafts = new List<Drafts>();
            PlayersMVPCPU = new List<Games>();
            PlayersMVPLocal = new List<Games>();
            PlayersMVPAway = new List<Games>();
            PlayersInjuries = new List<InjuryGames>();
            FreeAgentOffers = new List<FreeAgentOffers>(); 
            Injuries = new List<Injuries>();
            PFMAwards = new List<PFMAwards>();
            PlayersHistories = new List<PlayersHistories>();
            PlayersStatsGames = new List<PlayersStatsGames>();
            TransfersPlayersLists = new List<TransfersPlayersLists>();
            Managers = new List<Managers>();
            PlayersSalariesFull = new List<PlayersSalariesFull>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string NickName { get; set; }
        public string FirstName { get; set; }
        public int? Height { get; set; }
        public int? Weight { get; set; }
        public int? ActualTeam { get; set; }
        public string ManagerOpinion { get; set; }
        public int? IdRol { get; set; }
        public int? IdPosition { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? BornDate { get; set; }
        public int? IdState { get; set; }
        public int? Cutted { get; set; }
        public int? Injuried { get; set; }
        public int BirdRights { get; set; }
        public bool Retired { get; set; }
        public string FaceImage { get; set; }
        public string MainImage { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string CompleteName { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int? Age { get; set; }
        public int? Injury { get; set; }
        public int? YearsPro { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string AbrevName { get; set; }
        public int? Level2k { get; set; }
        public int? Followers { get; set; }
        public string ManagerAdvert { get; set; }
        public int? IdCollege { get; set; }
        public int? FromAgency { get; set; }
        public virtual ICollection<Drafts> Drafts { get; set; }
        public virtual ICollection<Games> PlayersMVPCPU { get; set; }
        public virtual ICollection<Games> PlayersMVPLocal { get; set; }
        public virtual ICollection<Games> PlayersMVPAway { get; set; }
        public virtual ICollection<Injuries> Injuries { get; set; }
        public virtual LVNBAFranchises LVNBAFranchis { get; set; }
        public virtual ICollection<PFMAwards> PFMAwards { get; set; }
        public virtual PlayersStates PlayersState { get; set; }
        public virtual ICollection<PlayersHistories> PlayersHistories { get; set; }
        public virtual PlayersPositions PlayersPosition { get; set; }
        public virtual PlayersRoles PlayersRole { get; set; }
        public virtual ICollection<PlayersStatsGames> PlayersStatsGames { get; set; }
        public virtual ICollection<TransfersPlayersLists> TransfersPlayersLists { get; set; }
        public virtual ICollection<Managers> Managers { get; set; }
        public virtual Colleges PlayersCollege { get; set; }
        public virtual ICollection<InjuryGames> PlayersInjuries { get; set; }
        public virtual ICollection<FreeAgentOffers> FreeAgentOffers { get; set; }
        public virtual ICollection<PlayersSalariesFull> PlayersSalariesFull { get; set; }
        public virtual PlayersFranchisesStars PlayersFranchisesStar { get; set; }
     
    }
}
