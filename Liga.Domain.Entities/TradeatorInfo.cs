﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Liga.Domain.Entities
{
    public class TradeatorInfo
    {
        public TradeatorInfo()
        {

            listaRondasTeam1 = new List<Rounds>(); // These are important, the model binder
            listaRondasTeam2 = new List<Rounds>(); // will not instantiate nested classes
            listaRondasTeam3 = new List<Rounds>(); 
            listaRondasTeam4 = new List<Rounds>();
            listaRondasDa1 = new List<Rounds>();
            listaRondasDa2 = new List<Rounds>();
            listaRondasDa3 = new List<Rounds>();
            listaRondasDa4 = new List<Rounds>();
            listaPlayers1 = new List<Players>();
            listaPlayers2 = new List<Players>();
            listaPlayers3 = new List<Players>();
            listaPlayers4 = new List<Players>();
            listaPlayersDa1 = new List<Players>();
            listaPlayersDa2 = new List<Players>();
            listaPlayersDa3 = new List<Players>();
            listaPlayersDa4 = new List<Players>();

        }



        public int idTrade { get; set; }
        public List<Rounds> listaRondasTeam1 { get; set; }
        public List<Rounds> listaRondasTeam2 { get; set; }
        public List<Rounds> listaRondasTeam3 { get; set; }
        public List<Rounds> listaRondasTeam4 { get; set; }
        public List<Players> listaPlayers1 { get; set; }
        public List<Players> listaPlayers2 { get; set; }
        public List<Players> listaPlayers3 { get; set; }
        public List<Players> listaPlayers4 { get; set; }
        public List<Players> listaPlayersDa1 { get; set; }
        public List<Players> listaPlayersDa2 { get; set; }
        public List<Players> listaPlayersDa3 { get; set; }
        public List<Players> listaPlayersDa4 { get; set; }
        public List<Rounds> listaRondasDa1 { get; set; }
        public List<Rounds> listaRondasDa2 { get; set; }
        public List<Rounds> listaRondasDa3 { get; set; }
        public List<Rounds> listaRondasDa4 { get; set; }
        public int esValido { get; set; }
        public string sMotivoNoValido { get; set; }

        public virtual LVNBAFranchises Equipo1 { get; set; }
        public virtual LVNBAFranchises Equipo2 { get; set; }
        public virtual LVNBAFranchises Equipo3 { get; set; }
        public virtual LVNBAFranchises Equipo4 { get; set; }

        public decimal PresupuestoAntes1 { get; set; }
        public decimal PresupuestoAntes2 { get; set; }
        public decimal PresupuestoAntes3 { get; set; }
        public decimal PresupuestoAntes4 { get; set; }
        public decimal PresupuestoDespues1 { get; set; }
        public decimal PresupuestoDespues2 { get; set; }
        public decimal PresupuestoDespues3 { get; set; }
        public decimal PresupuestoDespues4 { get; set; }

        public string sComentario { get; set; }

    }
}