using System;
using System.Collections.Generic;

namespace Liga.Domain.Entities
{
    public partial class ManagerRequests
    {
        public int Id { get; set; }
        public string Gamertag { get; set; }
        public string Email { get; set; }
        public DateTime? BornDate { get; set; }
        public int? Question1 { get; set; }
        public int? FavouriteTeam { get; set; }
        public string Availability { get; set; }
        public int? KnowUs { get; set; }
        public int? State { get; set; }
        public DateTime RequestDate { get; set; }
        public int UserId { get; set; }
        public virtual LVNBAFranchises LVNBAFranchis { get; set; }
        public virtual UserProfile User { get; set; }
    }
}
