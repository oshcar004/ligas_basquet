﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Liga.Domain.Entities
{
    public class InjuryResume
    {

        public IEnumerable<Injuries> ListaLesionados { get; set; }
        public IEnumerable<InjuryGames> ListaPartidosPerdidosPorLesion { get; set; }
        public IEnumerable<ResumenLesiones> ListaResumenLesiones { get; set; }

    }
}