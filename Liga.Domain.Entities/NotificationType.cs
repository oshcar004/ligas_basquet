﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Liga.Domain.Entities
{
    public partial class NotificationType
    {
        public NotificationType()
        {
            Notifications = new List<Notification>();
        }

        public int id { get; set; }
        public string type { get; set; }
        public virtual ICollection<Notification> Notifications { get; set; }
    }
}