﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Liga.Domain.Entities
{
    public class GeneralPlayersInfo
    {
        public IEnumerable<Players> ListPlayers { get; set; }
        public IEnumerable<PFMAwards> PlayersAwards { get; set; }
        public int idTeam { get; set; }
    }
}