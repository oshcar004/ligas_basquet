﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Liga.Domain.Entities
{
    public partial class PlazosMensuales
    {
        public PlazosMensuales()
        {

        }
        public int Plazo { get; set; }
        public int Inicio { get; set; }
        public int Fin { get; set; }

    }
}