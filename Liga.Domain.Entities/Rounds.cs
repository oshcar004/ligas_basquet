using System;
using System.Collections.Generic;

namespace Liga.Domain.Entities
{
    public partial class Rounds
    {
        public Rounds()
        {
            Drafts = new List<Drafts>();
            TransfersRoundsLists = new List<TransfersRoundsLists>();
        }

        public int Id { get; set; }
        public int Type { get; set; }
        public int Year { get; set; }
        public int IdFranchise { get; set; }
        public int Owner { get; set; }
        public int? IdManager { get; set; }
        public virtual ICollection<Drafts> Drafts { get; set; }
        public virtual LVNBAFranchises LVNBAFranchis { get; set; }
        public virtual LVNBAFranchises LVNBAFranchisProp { get; set; }
        public virtual Managers Manager { get; set; }


        public virtual ICollection<TransfersRoundsLists> TransfersRoundsLists { get; set; }

    }
}
