using System;
using System.Collections.Generic;

namespace Liga.Domain.Entities
{
    public partial class Transfers
    {
        public Transfers()
        {
            TransfersPlayersLists = new List<TransfersPlayersLists>();
            TransfersRoundsLists = new List<TransfersRoundsLists>(); 
            PlayersHistories = new List<PlayersHistories>();
        }

        public int Id { get; set; }
        public int? IdFranchise1 { get; set; } // Puede ser nulo solo en el caso de ser una transferencia de jugador libre
        public int IdFranchise2 { get; set; }
        public DateTime? ResolutionDate { get; set; }
        public int? IdFranchise3 { get; set; }
        public int? IdFranchise4 { get; set; }
        public DateTime? TransferDate { get; set; }
        public int IdTransferState { get; set; }
        public string ReasonDeny { get; set; }
        public string Comentario { get; set; }
        public bool Confirmed1 { get; set; }
        public bool Confirmed2 { get; set; }
        public bool Confirmed3 { get; set; }
        public bool Confirmed4 { get; set; }
        public int? ManagerCreator { get; set; }

        public virtual Managers Manager { get; set; }
        public virtual LVNBAFranchises LVNBAFranchis { get; set; }
        public virtual LVNBAFranchises LVNBAFranchis1 { get; set; }
        public virtual LVNBAFranchises LVNBAFranchis2 { get; set; }
        public virtual LVNBAFranchises LVNBAFranchis3 { get; set; }
        public virtual TransfersStates TransfersState { get; set; }
        public virtual ICollection<TransfersPlayersLists> TransfersPlayersLists { get; set; }
        public virtual ICollection<TransfersRoundsLists> TransfersRoundsLists { get; set; }
        public virtual ICollection<PlayersHistories> PlayersHistories { get; set; }
    }
}
