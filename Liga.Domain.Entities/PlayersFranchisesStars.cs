﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Liga.Domain.Entities
{
    public partial class PlayersFranchisesStars
    {
        public int Id { get; set; }
        public int Type { get; set; }
        public virtual Players Player { get; set; }
    }
}