﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Liga.Domain.Entities
{
    public partial class Playoffs
    {
        public int Id { get; set; }
        public int IdTeam1 { get; set; }
        public int IdTeam2 { get; set; }
        public int PosTeam1 { get; set; }
        public int PosTeam2 { get; set; }
        public int? WinsTeam1 { get; set; }
        public int? WinsTeam2 { get; set; }
        public int? Position { get; set; }
        public int? Season { get; set; }
        public virtual LVNBAFranchises LVNBAFranchisLocal { get; set; }
        public virtual LVNBAFranchises LVNBAFranchisAway { get; set; }
    }
}