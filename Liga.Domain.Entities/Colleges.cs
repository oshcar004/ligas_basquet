﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Liga.Domain.Entities
{
    public partial class Colleges
    {
        public Colleges()
        {
            Players = new List<Players>();
        }

        public int Id { get; set; }
        public string College { get; set; }
        public virtual ICollection<Players> Players { get; set; }
    }
}