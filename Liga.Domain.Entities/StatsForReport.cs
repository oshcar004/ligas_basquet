﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Liga.Domain.Entities
{
    public partial class StatsForReport
    {
        public int idPlayer { get; set; }
        public int idTeam { get; set; }
        public int idGame { get; set; }
        public string ABRE { get; set; }
        public string LOGO { get; set; }
        public string Nombre { get; set; }
        public int TI { get; set; }
        public int? MIN { get; set; }
        public int PNTS { get; set; }
        public int REBS { get; set; }
        public int AST { get; set; }
        public int ROB { get; set; }
        public int TAP { get; set; }
        public int PER { get; set; }
        public int TCA { get; set; }
        public int TCM { get; set; }
        public int C3TCA { get; set; }
        public int C3TCM { get; set; }
        public int TLA { get; set; }
        public int TLM { get; set; }
        public int RO { get; set; }
        public int FP { get; set; }
        public int MASMIN { get; set; }
    }
}