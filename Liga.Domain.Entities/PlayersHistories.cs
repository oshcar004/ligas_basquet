using System;
using System.Collections.Generic;

namespace Liga.Domain.Entities
{
    public partial class PlayersHistories
    {
        public int Id { get; set; }
        public int IdPlayer { get; set; }
        public int IdFranchise { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? IdTransfer { get; set; }
        public virtual LVNBAFranchises LVNBAFranchis { get; set; }
        public virtual Transfers Transfer { get; set; }
        public virtual Players Player { get; set; }
    }
}
