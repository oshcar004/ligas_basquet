using System;
using System.Collections.Generic;

namespace Liga.Domain.Entities
{
    public partial class BoxScoreTeams
    {
        public int id { get; set; }
        public int idScoreLocal { get; set; }
        public int idScoreAway { get; set; }
        public DateTime? gameDate { get; set; }
        public string LocalName { get; set; }
        public string AwayName { get; set; }
        public string AbreLocal { get; set; }
        public string AbreAway { get; set; }
        public int? IdLocal { get; set; }
        public int? IdAway { get; set; }
        public string LocalLogo { get; set; }
        public string AwayLogo { get; set; }
        public string LocalStadium { get; set; }
        public string AwayStadium { get; set; }
        public string LocalSidebar { get; set; }
        public string AwaySidebar { get; set; }
        public string LocalIcono { get; set; }
        public string AwayIcono { get; set; }
        public int TMIN { get; set; }
        public int TPNTS { get; set; }
        public int TROFS { get; set; }
        public int TRDEFS { get; set; }
        public int TREBS { get; set; }
        public int TASSS { get; set; }
        public int TBLKS { get; set; }
        public int TSTLS { get; set; }
        public int TTRNS { get; set; }
        public int TFOLS { get; set; }
        public int TFGM { get; set; }
        public int TFGA { get; set; }
        public int TF3M { get; set; }
        public int TF3A { get; set; }
        public int TFTM { get; set; }
        public int TFTA { get; set; }
        public int? idTeam { get; set; }
        
    }
}
