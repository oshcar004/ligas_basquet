using System;
using System.Collections.Generic;

namespace Liga.Domain.Entities
{
    public partial class PlayersRoles
    {
        public PlayersRoles()
        {
            Players = new List<Players>();
        }

        public int Id { get; set; }
        public string Rol { get; set; }
        public string Description { get; set; }
        public string Abrev { get; set; }
        public virtual ICollection<Players> Players { get; set; }
    }
}
