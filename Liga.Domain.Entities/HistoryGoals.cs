using System;
using System.Collections.Generic;

namespace Liga.Domain.Entities
{
    public partial class HistoryGoals
    {
        public int Id { get; set; }
        public int IdGoal { get; set; }
        public string Type { get; set; }
        public string Who { get; set; }
        public int Season { get; set; }
        public virtual Goals Goal { get; set; }
    }
}
