﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Liga.Domain.Entities
{
    public class SalaryTeamInfo
    {
        public IEnumerable<PlayersSalariesFull> ListPlayersRoster { get; set; }
        public LVNBAFranchises Franchise { get; set; }
        public LVNBAFranchisesParams FranchiseParams { get; set; }
        public IEnumerable<Rounds> RoundsTeamInfo { get; set; }
    }
}