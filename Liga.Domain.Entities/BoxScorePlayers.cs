using System;
using System.Collections.Generic;

namespace Liga.Domain.Entities
{
    public partial class BoxScorePlayers
    {
        public int id { get; set; }
        public int idScoreLocal { get; set; }
        public int idScoreAway { get; set; }
        public DateTime? gameDate { get; set; }
        public string LocalName { get; set; }
        public string AwayName { get; set; }
        public string AbreLocal { get; set; }
        public string AbreAway { get; set; }
        public int? IdLocal { get; set; }
        public int? IdAway { get; set; }
        public string LocalLogo { get; set; }
        public string AwayLogo { get; set; }
        public string LocalStadium { get; set; }
        public string AwayStadium { get; set; }
        public string LocalSidebar { get; set; }
        public string AwaySidebar { get; set; }
        public string LocalIcono { get; set; }
        public string AwayIcono { get; set; }
        public int IdPlayer { get; set; }
        public string CompleteName { get; set; }
        public string AbrevName { get; set; }
        public int ActualTeam { get; set; }
        public int TIT { get; set; }
        public int MINU { get; set; }
        public int PTS { get; set; }
        public int REBO { get; set; }
        public int REB { get; set; }
        public int REBT { get; set; }
        public int ASSISTS { get; set; }
        public int BLOCKS { get; set; }
        public int STEALS { get; set; }
        public int TURNOVERS { get; set; }
        public int FOULTS { get; set; }
        public int FGA { get; set; }
        public int FGM { get; set; }
        public int P3GA { get; set; }
        public int P3GM { get; set; }
        public int FTA { get; set; }
        public int FTM { get; set; }
        public int PLUSMINUS { get; set; }
    }
}
