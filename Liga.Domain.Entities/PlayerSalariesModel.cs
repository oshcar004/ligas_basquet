﻿namespace Liga.Domain.Entities
{
    public class PlayerSalariesModel
    {
        public decimal Salarie1 { get; set; }
        public decimal Salarie2 { get; set; }
        public decimal Salarie3 { get; set; }
        public decimal Salarie4 { get; set; }
        public decimal Salarie5 { get; set; }
        public int IdCondition1 { get; set; }
        public int IdCondition2 { get; set; }
        public int IdCondition3 { get; set; }
        public int IdCondition4 { get; set; }
        public int IdCondition5 { get; set; }
        public int ActualTeam { get; set; }
        public int Id { get; set; }
        public string CompleteName { get; set; }
        public int Today { get; set; }
        /*
        public PlayerSalariesModel()
        {
            Salarie1 = 0;
            Salarie2 = 0;
            Salarie3 = 0;
            Salarie4 = 0;
            Salarie5 = 0;
            Condition1 = 0;
            Condition2 = 0;
            Condition3 = 0;
            Condition4 = 0;
            Condition5 = 0;
            Teamid = 0;
            Id = 0;
            CompleteName = "";
        }*/

    }
}