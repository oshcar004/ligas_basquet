﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Liga.Domain.Entities
{
    public class InjuryGames
    {
        public int Id { get; set; }
        public int IdPlayer { get; set; }
        public int IdGame { get; set; }
    
        public virtual Games Game { get; set; }
        public virtual Players Player { get; set; }
    }
}