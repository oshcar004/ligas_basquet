﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Liga.Domain.Entities
{
    public class PlayersForChangeTeam
    {
        public int Id { get; set; }

        public int ActualTeam  { get; set; }
        public string CompleteName { get; set; }
        public string FranchiseName { get; set; }
   
    }
}