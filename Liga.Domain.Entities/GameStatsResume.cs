using System;
using System.Collections.Generic;

namespace Liga.Domain.Entities
{
    public partial class GameStatsResume
    {
        public int idPlayer { get; set; }
        public string CompleteName { get; set; }
        public int? stat { get; set; }
        public string Tipo { get; set; }
        public int idGame { get; set; }
        public int idTeam { get; set; }
        public string Abbreviation { get; set; }
        public string Color { get; set; }
    }
}
