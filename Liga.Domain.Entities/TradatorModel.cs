﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Liga.Domain.Entities
{
    public class ListPlayersTradeTeam
    {
        public IEnumerable<PlayersSalariesFull> ListPlayersTeam { get; set; }
        public IEnumerable<Rounds> ListRoundsTeam { get; set; }
        public int idPosTeam { get; set; }
        public decimal PresupuestoActual { get; set; }
    }

    public class TradatorModel
    {
        public ListPlayersTradeTeam ListPlayersTeam1 { get; set; }
        public ListPlayersTradeTeam ListPlayersTeam2 { get; set; }
        public ListPlayersTradeTeam ListPlayersTeam3 { get; set; }
        public ListPlayersTradeTeam ListPlayersTeam4 { get; set; }
        public LVNBAFranchises Team1 { get; set; }
        public LVNBAFranchises Team2 { get; set; }
        public LVNBAFranchises Team3 { get; set; }
        public LVNBAFranchises Team4 { get; set; }

    }
}