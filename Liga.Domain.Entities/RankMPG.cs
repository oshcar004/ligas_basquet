﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Liga.Domain.Entities
{
    public partial class RankMPG
    {
        public Nullable<long> POS { get; set; }
        public int Player { get; set; }
        public string season { get; set; }
        public int? MPG { get; set; }
    }
}