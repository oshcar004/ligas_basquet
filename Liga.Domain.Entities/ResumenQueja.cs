﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Liga.Domain.Entities
{

    public partial class ResumenQueja
    {

        public int id { get; set; }
        public int? GameId { get; set; }
        public int? SenderIdTeam { get; set; }
        public string SenderTeam { get; set; }
        public string SenderIcono { get; set; }
        public int? SenderIdManager { get; set; }
        public string SenderGamertag { get; set; }
        public string SenderMotivo { get; set; }
        public int? ReceiverIdTeam { get; set; }
        public string ReceiverTeam { get; set; }
        public string ReceiverIcono { get; set; }
        public int? ReceiverIdManager { get; set; }
        public string ReceiverGamertag { get; set; }
        public DateTime? gameDate { get; set; }
        public DateTime? date2k { get; set; }

        public virtual Games GameInfo { get; set; }
    }
}