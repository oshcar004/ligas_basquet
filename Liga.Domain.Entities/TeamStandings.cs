using System;
using System.Collections.Generic;

namespace Liga.Domain.Entities
{
    public partial class TeamStandings
    {
        public int Id { get; set; }
        public int? Wins { get; set; }
        public int? Losses { get; set; }
        public int? Games { get; set; }
        public int? Blocks { get; set; }
        public int? Rebounds { get; set; }
        public int? Steals { get; set; }
        public int? PointsF { get; set; }
        public int? PointsA { get; set; }
        public int? GamesHome { get; set; }
        public int? GamesAway { get; set; }
        public int? WinsHome { get; set; }
        public int? LossHome { get; set; }
        public int? WinsAway { get; set; }
        public int? LossAway { get; set; }
        public string Streak { get; set; }
        public int? Assists { get; set; }
        public int? ThreePointAttemptes { get; set; }
        public int? ThreePointScored { get; set; }
        public int? FieldGoalAttempted { get; set; }
        public int? FieldGoalScored { get; set; }
        public int? FreeThrowAttempted { get; set; }
        public int? FreeThrowScored { get; set; }
        public virtual LVNBAFranchises LVNBAFranchis { get; set; }
    }
}
