using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Liga.Domain.Entities
{
    public partial class ManagersFranchises
    {
        public int IdFranchise { get; set; }
        public int IdManager { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}",ApplyFormatInEditMode = true)]
        public DateTime StartDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? EndDate { get; set; }
        public virtual LVNBAFranchises LVNBAFranchis { get; set; }
        public virtual Managers Manager { get; set; }
    }
}
