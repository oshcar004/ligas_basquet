using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Liga.Domain.Entities
{
    [Table("webpages_Roles")]
    public partial class Webpages_Roles
    {
        public Webpages_Roles()
        {

        }

        public int RoleId { get; set; }
        public string RoleName { get; set; }

    }
}
