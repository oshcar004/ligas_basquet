﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Liga.Domain.Entities
{
    public class SuperBoxScore
    {
        public Games Partido { get; set; }
        public IEnumerable<GameStatsResume> listStatsTeamLocal { get; set; }
        public IEnumerable<GameStatsResume> listStatsTeamAway { get; set; }
    }
}