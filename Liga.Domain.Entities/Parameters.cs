using System;
using System.Collections.Generic;

namespace Liga.Domain.Entities
{
    public partial class Parameters
    {
        public int Id { get; set; }
        public decimal? LimiteSalarial { get; set; }
        public decimal? ImpuestoLujo { get; set; }
        public decimal? MidLevel { get; set; }
        public decimal? MiniMidLevel { get; set; }
        public int? NivelTraspaso { get; set; }
        public int? Agencia { get; set; }
        public int? RegularSeason { get; set; }
        public int? Playoffs { get; set; }
        public int? SuperTasaLujo { get; set; }
        public int Temporada { get; set; }
        public bool Pretemporada { get; set; }
        public bool Postemporada { get; set; }
        public int MercadoAbierto { get; set; }
        public int OffSeasonMaxTradesIn { get; set; }
        public int OffSeasonMaxTradesOut { get; set; }
        public int OffSeasonMaxTrades { get; set; }
        public int SeasonMaxTrades { get; set; }
        public int SeasonMaxTradesIn { get; set; }
        public int SeasonMaxTradesOut { get; set; }
        public int WithoutSummerMaxTrades { get; set; }
        public int WithoutSummerMaxTradesIn { get; set; }
        public int WithoutSummerMaxTradesOut { get; set; }
        public int? PlazoMin { get; set; }
        public int? PlazoMax { get; set; }
        public int? CifraNoSuperable { get; set; }

    }
}
