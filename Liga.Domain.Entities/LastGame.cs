﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Liga.Domain.Entities
{
    public partial class LastGame
    {
        public int idPlayer { get; set; }
        public int? season { get; set; }
        public DateTime? gameDate { get; set; }
        public string Opponent { get; set; }
        public string Result { get; set; }
        public int? MIN { get; set; }
        public string FG { get; set; }
        public string C3P { get; set; }
        public string FT { get; set; }
        public int? rofs { get; set; }
        public int? rdefs { get; set; }
        public int? rebs { get; set; }
        public int? asss { get; set; }
        public int? stls { get; set; }
        public int? blks { get; set; }
        public int? trns { get; set; }
        public int? fols { get; set; }
        public int? pnts { get; set; }
    }
}