using System;
using System.Collections.Generic;

namespace Liga.Domain.Entities
{
    public partial class Comments
    {
        public int Id { get; set; }
        public int PostId { get; set; }
        public DateTime DateTime { get; set; }
        public string Comment1 { get; set; }
        public int UserId { get; set; }
        public virtual UserProfile User { get; set; }
    }
}
