using System;
using System.Collections.Generic;

namespace Liga.Domain.Entities
{
    public partial class TransfersRoundsLists
    {
        public int Id { get; set; }
        public int IdRound { get; set; }
        public int IdTransfer { get; set; }
        public int IdFranchise { get; set; }
        public int Destination { get; set; }
        public virtual LVNBAFranchises LVNBAFranchis { get; set; }
        public virtual LVNBAFranchises LVNBAFranchis1 { get; set; }
        public virtual Rounds Round { get; set; }
        public virtual Transfers Transfer { get; set; }
    }
}
