using System;
using System.Collections.Generic;

namespace Liga.Domain.Entities
{
    public partial class Injuries
    {
        public int Id { get; set; }
        public int IdPlayer { get; set; }
        public int IdType { get; set; }
        public int GamesOut { get; set; }
        public int GamesRest { get; set; }
        public int? IdGame { get; set; }

        public virtual Games Game { get; set; }
        public virtual InjuryTypes InjuryType { get; set; }
        public virtual Players Player { get; set; }
    }
}
