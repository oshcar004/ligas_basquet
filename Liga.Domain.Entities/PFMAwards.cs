using System;
using System.Collections.Generic;

namespace Liga.Domain.Entities
{
    public partial class PFMAwards
    {
        public int Id { get; set; }
        public int IdAward { get; set; }
        public int? IdFranchise { get; set; }
        public int? IdManager { get; set; }
        public int? IdPlayer { get; set; }
        public int? Info { get; set; }
        public int? Year { get; set; }
        public int? MWY { get; set; }
        public DateTime AwardDate { get; set; }
        public virtual Awards Award { get; set; }
        public virtual LVNBAFranchises LVNBAFranchis { get; set; }
        public virtual Managers Manager { get; set; }
        public virtual Players Player { get; set; }
    }
}
