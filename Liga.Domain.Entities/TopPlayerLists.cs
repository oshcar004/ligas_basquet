using System;
using System.Collections.Generic;

namespace Liga.Domain.Entities
{
    public partial class TopPlayerLists
    {
        public int id { get; set; }
        public string CompleteName { get; set; }
        public string FaceImage { get; set; }
        public string Logo { get; set; }
        public decimal? STAT { get; set; }
        public string Tipo { get; set; }
    }
}
