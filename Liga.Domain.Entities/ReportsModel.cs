﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Liga.Domain.Entities
{
    public class ReportsModel
    {
    }

    public class JugadorMinuto
    {
        public int Minuto { get; set; }
        public string Player { get; set; }
    }

    public class StatEquipos
    {
        public int IdGame { get; set; }
        public int IdTeamLocal { get; set; }
        public int IdTeamAway { get; set; }
    }

    public class ErrorReporte
    {
        public string Mensaje { get; set; }
        public string Color { get; set; }
    }
}
