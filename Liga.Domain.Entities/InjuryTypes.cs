using System;
using System.Collections.Generic;

namespace Liga.Domain.Entities
{
    public partial class InjuryTypes
    {
        public InjuryTypes()
        {
            Injuries = new List<Injuries>();
        }

        public int Id { get; set; }
        public string Description { get; set; }
        public int? MinDuration { get; set; }
        public int? MaxDuration { get; set; }
        public virtual ICollection<Injuries> Injuries { get; set; }
    }
}
