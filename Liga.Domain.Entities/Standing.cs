﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Liga.Domain.Entities
{
    public partial class Standing
    {
        public int Id { get; set; }
        public string Icono { get; set; }
        public int? IdDivision { get; set; }
        public string Division { get; set; }
        public string Name { get; set; }
        public int? G { get; set; }
        public int? W { get; set; }
        public int? L { get; set; }
        public decimal? PCT { get; set; }
        public string CONF { get; set; }
        public string DIV { get; set; }
        public string HOME { get; set; }
        public string ROAD { get; set; }
        public string L10 { get; set; }
        public string STREAK { get; set; }
    }
}