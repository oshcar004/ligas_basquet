using System;
using System.Collections.Generic;

namespace Liga.Domain.Entities
{
    public partial class PlayersStatsGames
    {
        public int idGame { get; set; }
        public int idPlayer { get; set; }
        public int idTeam { get; set; }
        public int? starter { get; set; }
        public int? segs { get; set; }
        public int? pnts { get; set; }
        public int? rofs { get; set; }
        public int? rdefs { get; set; }
        public int? rebs { get; set; }
        public int? asss { get; set; }
        public int? blks { get; set; }
        public int? stls { get; set; }
        public int? trns { get; set; }
        public int? fols { get; set; }
        public int? fga { get; set; }
        public int? fgm { get; set; }
        public int? f3a { get; set; }
        public int? f3m { get; set; }
        public int? fta { get; set; }
        public int? ftm { get; set; }
        public int? plusminus { get; set; }
        public virtual Games Game { get; set; }
        public virtual LVNBAFranchises LVNBAFranchis { get; set; }
        public virtual Players Player { get; set; }
    }
}
