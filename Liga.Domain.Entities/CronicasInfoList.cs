﻿using System.Collections.Generic;

namespace Liga.Domain.Entities
{
    public class CronicasInfoList
    {
        public IEnumerable<Cronicas> ListCronicas { get; set; }
        public int IdTeam { get; set; }

    }
    public class Cronicas
    {
        public Games gameCronica {get;set;}
    }
}