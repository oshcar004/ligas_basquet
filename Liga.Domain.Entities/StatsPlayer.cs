﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Liga.Domain.Entities
{
    public partial class StatsPlayer
    {
        public int PlayerId { get; set; }
        public Players Players { get; set; }
        public int Season { get; set; }
        public decimal? APG { get; set; }
        public decimal? BPG { get; set; }
        public decimal? FG { get; set; }
        public int? MPG { get; set; }
        public decimal? PPG { get; set; }
        public decimal? RPG { get; set; }
        public decimal? SPG { get; set; }
    }
}