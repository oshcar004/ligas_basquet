using System;
using System.Collections.Generic;

namespace Liga.Domain.Entities
{
    public partial class AwardsClass
    {
        public AwardsClass()
        {
            Awards = new List<Awards>();
        }
        public string Id { get; set; }
        public string Class { get; set; }
        public virtual ICollection<Awards> Awards { get; set; }
    }
}
