using System;
using System.Collections.Generic;

namespace Liga.Domain.Entities
{
    public partial class GamesPerMonth
    {
        public int gameMonth { get; set; }
        public int season { get; set; }
        public int? gameTotal { get; set; }
    }
}
