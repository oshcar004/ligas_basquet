using System;
using System.Collections.Generic;

namespace Liga.Domain.Entities
{
    public partial class Managers
    {
        public Managers()
        {
            LVNBAFranchises = new List<LVNBAFranchises>();
            ManagersFranchises = new List<ManagersFranchises>();
            PFMAwards = new List<PFMAwards>();
            Players = new List<Players>();
            GamesLocal = new List<Games>();
            GamesAway = new List<Games>();
            Rounds = new List<Rounds>();
            Trades = new List<Transfers>();

        }

        public int Id { get; set; }
        public string GamerTag { get; set; }
        public string ForumName { get; set; }
        public string RealName { get; set; }
        public int? Age { get; set; }
        public string AboutMe { get; set; }
        public string Experience { get; set; }
        public int? LVNBAAge { get; set; }
        public int? UserId { get; set; }
        public DateTime? BornDate { get; set; }
        public string Email { get; set; }
        public string Photo { get; set; }
        public string Category { get; set; }
        public int? TotalBonus { get; set; }
        public bool Active { get; set; }
        public bool Expulsado { get; set; }
        public int? Warnings { get; set; }
        public virtual ICollection<Games> GamesLocal { get; set; }
        public virtual ICollection<Games> GamesAway { get; set; }
        public virtual ICollection<LVNBAFranchises> LVNBAFranchises { get; set; }
        public virtual ICollection<ManagersFranchises> ManagersFranchises { get; set; }
        public virtual ICollection<PFMAwards> PFMAwards { get; set; }
        public virtual ICollection<Players> Players { get; set; }
        public virtual ICollection<ValMediaRecibida> ValRecibidas { get; set; }
        public virtual ICollection<ValMediaOtorgada> ValOtorgadas { get; set; }
        public virtual ICollection<ValMediaLocalVisitanteOtorgada> ValLVOtorgada { get; set; }
        public virtual ICollection<ValMediaLocalVisitanteRecibida> ValLVRecibida { get; set; }
        public virtual ICollection<Valoraciones> ValoracionesOtorga { get; set; }
        public virtual ICollection<Valoraciones> ValoracionesRecibe { get; set; }
        public virtual ICollection<Rounds> Rounds { get; set; }
        public virtual UserProfile Users { get; set; }
        public virtual ICollection<Transfers> Trades { get; set; }

    }
}
