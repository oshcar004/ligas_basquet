﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Liga.Domain.Entities
{
 
    public partial class TopPlayersList_Result
    {
        public int id { get; set; }
        public string CompleteName { get; set; }
        public string FaceImage { get; set; }
        public string Logo { get; set; }
        public decimal? STAT { get; set; }
        public string Tipo { get; set; }
    }
    
}