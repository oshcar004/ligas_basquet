﻿using System.Collections.Generic;

namespace Liga.Domain.Entities
{
    public class BoxScore
    {
        public IEnumerable<BoxScorePlayers> ListaStatsPlayers{ get; set; }
        public IEnumerable<BoxScoreTeams> ListaStatsTeams { get; set; }  
    }
}