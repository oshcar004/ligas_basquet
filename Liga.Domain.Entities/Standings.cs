﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Liga.Domain.Entities
{
    public class PreviewStanding
    {
        public int Id { get; set; }
        public string TeamName { get; set; }
        public int Wins { get; set; }
        public int Losses { get; set; }
        public string Logo { get; set; }

    }
    public class TeamStandingFormat
    {
        public string TeamName { get; set; }
        public string Conferencia { get; set; }
        public int PosicionConferencia { get; set; }
        public int Wins { get; set; }
        public int Losses { get; set; }
        public double Pct { get; set; }
        public string Logo { get; set; }
        public int Id { get; set; }
        public string Home { get; set; }
        public string Away { get; set; }
        public string Racha { get; set; }
        public string L10 { get; set; }
        public double PPG { get; set; }
        public double PAPG { get; set; }
        public double RPG { get; set; }
        public double BPG { get; set; }
        public double APG { get; set; }
        public double SPG { get; set; }
        public double FG3 { get; set; }
        public double FT { get; set; }
        public double FG { get; set; }

    }




    public class TopPlayer
    {
        public string CompleteName { get; set; }
        public string FaceImage { get; set; }
        public decimal Stat { get; set; }
    }
}