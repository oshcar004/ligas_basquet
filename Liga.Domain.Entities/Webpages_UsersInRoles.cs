using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Liga.Domain.Entities
{
    [Table("webpages_UsersInRoles")]
    public class Webpages_UsersInRoles
    {
        public Webpages_UsersInRoles()
        {

        }

        public Webpages_UsersInRoles(int UserId, int RoleId)
        {
            this.UserId = UserId;
            this.RoleId = RoleId;
        }
        [Key]
        [Column(Order = 1)]
        public int UserId { get; set; }
        [Key]
        [Column(Order = 2)]
        public int RoleId { get; set; }
        [ForeignKey("UserId")]
        public virtual UserProfile UserProfile { get; set; }
        [ForeignKey("RoleId")]
        public virtual Webpages_Roles Webpages_Roles { get; set; }
    }
}
