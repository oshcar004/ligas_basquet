﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Liga.Domain.Entities
{
    public class PlayerCompleteInfo
    {
        public PosRanksPlayer PositionRank { get; set; }
        public Players Player { get; set; }
        public PlayerStatisticsBySeason PlayerStatThisRS { get; set; }
        public PlayerStatisticsByPof PlayerStatThisPlayoffs { get; set; }
        public IEnumerable<PlayersHistories> PlayerHistories { get; set; }
        public PlayersSalariesFull PlayerSalary { get; set; }
        public IEnumerable<LastGame> PlayersGames { get; set; }
        public IEnumerable<PFMAwards> PlayerAwards { get; set; }


    }       
    public class PosRanksPlayer
    {
            public PosRanksPlayer()
            {

            }
            public RankMPG PosMPG { get; set; }
            public RankPPG PosPPG { get; set; }
            public RankRPG PosRPG { get; set; }
            public RankAPG PosAPG { get; set; }
            public RankSPG PosSPG { get; set; }
            public RankBPG PosBPG { get; set; }
            public PlayerStatisticsBySeason PlayerStat { get; set; }

    }
}