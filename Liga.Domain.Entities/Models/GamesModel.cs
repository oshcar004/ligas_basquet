﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Liga.Domain.Entities.Models
{
    public class GamesModel
    {
        public int SeasonStageId { get; set; }
        public DateTime StartTimeUTC { get; set; }
        public TeamGameModel HTeam { get; set; }
        public TeamGameModel VTeam { get; set; }
    }
}
