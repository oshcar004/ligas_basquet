﻿using System.Collections.Generic;

namespace Liga.Domain.Entities.Models
{
    public class League
    {
        public IList<GamesModel> Standard { get; set; }
        public IList<GamesModel> Africa { get; set; }
        public IList<GamesModel> Sacramento { get; set; }
        public IList<GamesModel> Vegas { get; set; }
        public IList<GamesModel> Utah { get; set; }
    }
}