﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Liga.Domain.Entities
{
    public class PlayersForOutNBA
    {
        public int Id { get; set; }

        public string EuropeTeam  { get; set; }
        public string CompleteName { get; set; }
        public string SalaryTeam { get; set; }
   
    }
}