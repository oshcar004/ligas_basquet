﻿using Liga.Domain.Entities;
using Liga.Domain.Entities.Models;
using Liga.Infrastructure.Data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Liga.Service.Impl
{
    public class SeasonService
    {
        static HttpClient client = new HttpClient();
        static readonly Liga_DBContext dBContext = new Liga_DBContext();

        public async Task CreateNewSeasonAsync(int year)
        {
            try
            {
                dBContext.Games.RemoveRange(dBContext.Games);
                dBContext.Database.ExecuteSqlCommand("DBCC CHECKIDENT('Games', RESEED, 0)");
                dBContext.SaveChanges();

                WebRequest request = WebRequest.Create("http://data.nba.net/data/10s/prod/v1/2018/schedule.json");
                request.Timeout = 30 * 60 * 1000;
                request.UseDefaultCredentials = true;
                request.Proxy.Credentials = request.Credentials;
                WebResponse response = request.GetResponse();
                var responseStream = response.GetResponseStream();
                if (responseStream != null)
                {
                    var myStreamReader = new StreamReader(responseStream, Encoding.Default);
                    var json = myStreamReader.ReadToEnd();
                    var calendar = JsonConvert.DeserializeObject<Calendar>(json);
                    //liga standar
                    var standar = calendar.League.Standard.Where(x => x.SeasonStageId == 2);
                    foreach (var partido in standar)
                    {
                        var h = dBContext.LVNBAFranchises.Where(x => x.IdExterno == partido.HTeam.TeamId).FirstOrDefault();
                        var v = dBContext.LVNBAFranchises.Where(x => x.IdExterno == partido.VTeam.TeamId).FirstOrDefault();
                        var game = new Games();
                        game.season = year;
                        game.IdTeamAway = v.Id;
                        game.IdTeamLocal = h.Id;
                        dBContext.Games.Add(game);
                        dBContext.SaveChanges();
                    }

                    var africa = calendar.League.Africa.Where(x => x.SeasonStageId == 2);
                    foreach (var partido in africa)
                    {
                        var h = dBContext.LVNBAFranchises.Where(x => x.IdExterno == partido.HTeam.TeamId).FirstOrDefault();
                        var v = dBContext.LVNBAFranchises.Where(x => x.IdExterno == partido.VTeam.TeamId).FirstOrDefault();
                        var game = new Games();
                        game.IdTeamAway = v.Id;
                        game.IdTeamLocal = h.Id;
                        game.gameDate = partido.StartTimeUTC;
                        dBContext.Games.Add(game);
                        dBContext.SaveChanges();
                    }

                    var sacramento = calendar.League.Sacramento.Where(x => x.SeasonStageId == 2);
                    foreach (var partido in sacramento)
                    {
                        var h = dBContext.LVNBAFranchises.Where(x => x.IdExterno == partido.HTeam.TeamId).FirstOrDefault();
                        var v = dBContext.LVNBAFranchises.Where(x => x.IdExterno == partido.VTeam.TeamId).FirstOrDefault();
                        var game = new Games();
                        game.IdTeamAway = v.Id;
                        game.IdTeamLocal = h.Id;
                        game.gameDate = partido.StartTimeUTC;
                        dBContext.Games.Add(game);
                        dBContext.SaveChanges();
                    }

                    var utah = calendar.League.Utah.Where(x => x.SeasonStageId == 2);
                    foreach (var partido in utah)
                    {
                        var h = dBContext.LVNBAFranchises.Where(x => x.IdExterno == partido.HTeam.TeamId).FirstOrDefault();
                        var v = dBContext.LVNBAFranchises.Where(x => x.IdExterno == partido.VTeam.TeamId).FirstOrDefault();
                        var game = new Games();
                        game.IdTeamAway = v.Id;
                        game.IdTeamLocal = h.Id;
                        game.gameDate = partido.StartTimeUTC;
                        dBContext.Games.Add(game);
                        dBContext.SaveChanges();
                    }

                    var vegas = calendar.League.Vegas.Where(x => x.SeasonStageId == 2);
                    foreach (var partido in vegas)
                    {
                        var h = dBContext.LVNBAFranchises.Where(x => x.IdExterno == partido.HTeam.TeamId).FirstOrDefault();
                        var v = dBContext.LVNBAFranchises.Where(x => x.IdExterno == partido.VTeam.TeamId).FirstOrDefault();
                        var game = new Games();
                        game.IdTeamAway = v.Id;
                        game.IdTeamLocal = h.Id;
                        game.gameDate = partido.StartTimeUTC;
                        dBContext.Games.Add(game);
                        dBContext.SaveChanges();
                    }

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }

        }

        public async Task GenerateRounds(int actualYear)
        {
            try
            {
                List<Rounds> roundToAdd = new List<Rounds>();
                List<LVNBAFranchises> allTeams = dBContext.LVNBAFranchises.Where(x => x.Id > 0).ToList();
                if(allTeams != null && allTeams.Count() > 0)
                {
                    for (int i = actualYear; i <= (actualYear+1); i++)
                    {
                        foreach (var team in allTeams)
                        {
                            Rounds round1 = dBContext.Rounds.Where(x => x.IdFranchise == team.Id && x.Year == i && x.Type == 1).FirstOrDefault();
                            Rounds round2 = dBContext.Rounds.Where(x => x.IdFranchise == team.Id && x.Year == i && x.Type == 2).FirstOrDefault();
                            if (round1 == null)
                            {
                                round1 = new Rounds()
                                {
                                    Type = 1,
                                    IdFranchise = team.Id,
                                    Owner = team.Id,
                                    Year = i
                                };
                                roundToAdd.Add(round1);
                            }
                            if (round2 == null)
                            {
                                round2 = new Rounds()
                                {
                                    Type = 2,
                                    IdFranchise = team.Id,
                                    Owner = team.Id,
                                    Year = i
                                };
                                roundToAdd.Add(round2);
                            }
                        }
                    }
                    dBContext.Rounds.AddRange(roundToAdd);
                    dBContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }
    }
}
